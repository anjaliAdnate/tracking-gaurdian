import { Component, OnInit } from '@angular/core';
import { ActionSheetController, LoadingController, ToastController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
// import { File } from '@ionic-native/file/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { AppService } from '../app.service';
import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer/ngx';
import * as moment from 'moment';

@Component({
  selector: 'app-add-device',
  templateUrl: './add-device.page.html',
  styleUrls: ['./add-device.page.scss'],
})
export class AddDevicePage implements OnInit {
  respData: any;
  userdetails: any;
  fileUrl: string;
  simno: number;
  imei: number;
  device_name: any;

  constructor(
    private actionSheetController: ActionSheetController,
    private camera: Camera,
    private loadingController: LoadingController,
    private crop: Crop,
    private transfer: FileTransfer,
    private apiCall: AppService,
    private taostCtrl: ToastController
  ) {
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
  }

  ngOnInit() {
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          // this.openGallery();
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
          //SAVEDPHOTOALBUM

        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  pickImage(sourceType) {
    // var url = this.apiCall.mainUrl + "devices/uploadDeviceImage";
    var url = "http://13.126.36.205/devices/uploadDeviceImage";

    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      this.crop.crop(imageData, { quality: 100 })
        .then(
          newImage => {
            var dlink123 = newImage.split('?');
            var wear = dlink123[0];
            const fileTransfer: FileTransferObject = this.transfer.create();
            const uploadOpts: FileUploadOptions = {
              fileKey: 'photo',
              // fileName: imageData.substr(imageData.lastIndexOf('/') + 1)
              fileName: wear.substr(wear.lastIndexOf('/') + 1)
            };
            this.loadingController.create({
              message: 'Please wait....',
            }).then((loadEl) => {
              loadEl.present();
              fileTransfer.upload(wear, url, uploadOpts)
                .then((data) => {
                  loadEl.dismiss();
                  this.respData = JSON.parse(JSON.stringify(data)).response;
                  console.log("image data response: ", this.respData);
                }, (err) => {
                  loadEl.dismiss();
                  console.log(err);
                  this.taostCtrl.create({
                    message: 'Something went wrong while uploading file... Please try after some time..',
                    duration: 2000,
                    position: 'bottom'
                  }).then((toastEl) => {
                    toastEl.present();
                  });
                });
            })

          })
    }, (err) => {
      console.log("imageData err: ", err)
      // Handle error
    });
  }

  dlUpdate(dllink) {
    // debugger
    // let that = this;
    var dlink123 = dllink.split('?');
    var wear = dlink123[0];
    console.log("new download link: ", wear);
    var _burl = this.apiCall.mainUrl + "users/updateImagePath";
    // var _burl = this.apiCall.mainUrl + "devices/uploadDeviceImage";
    var payload = {
      deviceImage: [dllink],
      // _id: this.userdetails._id
    }
    this.loadingController.create({
      message: 'Uploading image...'
    }).then((loadEl) => {
      loadEl.present();
      this.apiCall.urlWithdata(_burl, payload)
        .subscribe(respData => {
          loadEl.dismiss();
          console.log("check profile upload: ", respData)
          // this.getImgUrl();
        },
          err => {
            loadEl.dismiss();
          });
    });
  }

  getImgUrl() {
    var url = this.apiCall.mainUrl + "users/shareProfileImage?uid=" + this.userdetails._id;
    this.loadingController.create({
      message: 'Loading image...'
    }).then((loadEl => {
      loadEl.present();
      this.apiCall.getdevicesForAllVehiclesApi(url)
        .subscribe(resp => {
          loadEl.dismiss();
          console.log("server image url=> ", resp);
          if (JSON.parse(JSON.stringify(resp)).imageDoc.length > 0) {
            var imgUrl = JSON.parse(JSON.stringify(resp)).imageDoc[0];
            var str1 = imgUrl.split('public/');
            this.fileUrl = this.apiCall.mainUrl + str1[1];
          }
        },
          err => {
            loadEl.dismiss();
          })
    }));
  }
  devicedetails: any = {};
  saveDevice() {
    // var devicedetails: any = {};
    var tempdate = new Date();
    tempdate.setDate(tempdate.getDate() + 365);
    var currentYear = moment(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
    debugger
    // if (this.userdetails.isSuperAdmin == true) {
    this.devicedetails = {
      "devicename": this.device_name,
      "deviceid": this.imei,
      // "driver_name": this.addvehicleForm.value.driver,
      // "contact_number": this.addvehicleForm.value.contact_num,
      "typdev": "Tracker",
      "sim_number": this.simno,
      "user": this.userdetails._id,
      "emailid": this.userdetails.email,
      "iconType": "",
      // "vehicleGroup": this.groupstaus_id,
      "device_model": "5e733cf65289634bd44dbf67",
      "expdate": new Date(currentYear).toISOString(),
      "supAdmin": this.userdetails._id
    }
    // } else {
    //   if (this.userdetails.isDealer == true) {
    //     this.devicedetails = {
    //       // "devicename": this.addvehicleForm.value.device_name,
    //       "deviceid": this.imei,
    //       // "driver_name": this.addvehicleForm.value.driver,
    //       // "contact_number": this.addvehicleForm.value.contact_num,
    //       "typdev": "Tracker",
    //       "sim_number": this.simno,
    //       "user": this.userdetails._id,
    //       "emailid": this.userdetails.email,
    //       "iconType": "",
    //       // "vehicleGroup": this.groupstaus_id,
    //       "device_model": "5e733cf65289634bd44dbf67",
    //       "expdate": new Date(currentYear).toISOString(),
    //       "supAdmin": this.userdetails.supAdmin,
    //       "Dealer": this.userdetails._id
    //     }
    //   }
    // }

    if (this.respData != undefined) {
      this.devicedetails.deviceImage = [this.respData];
    }
    debugger
    this.loadingController.create({
      message: 'please wait we are adding device...'
    }).then((loadEL) => {
      loadEL.present();
      var url1 = this.apiCall.mainUrl + "devices/addDevice";
      this.apiCall.urlWithdata(url1, this.devicedetails)
        .subscribe(respData => {
          loadEL.dismiss();
          var res = JSON.parse(JSON.stringify(respData));
          console.log("check if device added or not: ", res);
          if (res) {
            this.taostCtrl.create({
              message: 'Device added successfully.',
              position: 'bottom',
              duration: 2000
            }).then((toastEl) => {
              toastEl.present();
            });
            this.simno = undefined;
            this.imei = undefined;
            this.respData = undefined;
          }
        },
          err => {
            console.log(err)
            loadEL.dismiss();
          })
    })
  }

}
