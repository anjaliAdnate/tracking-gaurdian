import { Component, ElementRef, ViewChild, AfterViewInit, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import * as io from 'socket.io-client';
import * as _ from "lodash";
import { AppService } from '../app.service';
import { GoogleMapsMapTypeId, GoogleMaps, LatLngBounds, Marker, LatLng, Spherical, ILatLng, Polygon, GoogleMap, GoogleMapsEvent, Circle } from '@ionic-native/google-maps';
import { Platform, LoadingController, ToastController } from '@ionic/angular';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { NativeGeocoderOptions, NativeGeocoderResult, NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import * as moment from 'moment';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, AfterViewInit, OnDestroy {
  map: GoogleMap;
  // @ViewChild('map', { static: true }) mapElementRef: ElementRef;

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};
  userdetails: any;
  socketSwitch: any = {};
  socketChnl: any = [];
  _io: any;
  allData: any = {};
  mapKey: any;
  isSuperAdmin: boolean;
  isDealer: boolean;
  mapData: any = [];
  impkey: any;
  deviceDeatils: any;
  token: string;
  geoShape: any = [];
  geodata: any = [];
  generalPolygon: any;
  fileUrl: any = null;
  _io1: any;
  imgBase64: any;
  cityCircle: Circle;
  someKey: any = [];

  constructor(
    private router: Router,
    private apiCall: AppService,
    private plt: Platform,
    private loadingController: LoadingController,
    private toastCtrl: ToastController,
    private push: Push,
    private androidPermissions: AndroidPermissions,
    private nativeGeocoder: NativeGeocoder,
    // private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy
  ) {
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> " + JSON.stringify(this.userdetails));
    this.addPushNotify();
  }

  backButtonSubscription;
  // initialMapLoad: boolean = true;
  ionViewWillEnter() {
    console.log("Home:  ionViewWillEnter()");
    // debugger


    this._io = io.connect('https://www.oneqlik.in/gps', { transports: ["websocket"] });
    this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
    this.backButtonSubscription = this.plt.backButton.subscribe(async () => {
      navigator['app'].exitApp();
      // edit your code here
      // either you exit or navigate to the desired path
    });
  }
  ionViewWillLeave() {
    // debugger
    // console.log("socket channel: ", this.socketChnl);
    // console.log("some key: ", this.someKey);
    for (var e = 0; e < this.someKey.length; e++) {
      if (this.allData[this.someKey[e]].mark) {
        this.allData[this.someKey[e]].mark.remove();
      }
    }
  }

  onFunc() {
    // debugger
    this.router.navigateByUrl('/device-list');
  }

  ionViewDidLeave() {
    console.log("Home:  ionViewDidLeave()");

    this.backButtonSubscription.unsubscribe();
    for (var i = 0; i < this.socketChnl.length; i++)
      this._io.removeAllListeners(this.socketChnl[i]);
    this._io.on('disconnect', (data) => {
      console.log('Ignition IO disconnected', data);
    })
  }

  ionViewDidEnter() {
    console.log("Home:  ionViewDidEnter()");

    this.openChatSocket();
    this.checkGPSPermission();
    this.getImgUrl();

    if (this.socketChnl.length > 0) {
      this.ngOnDestroy();
    }
    this.allData = {};
    this.deviceDeatils = undefined;

    // this._io = io.connect('https://www.oneqlik.in/gps', { transports: ["websocket"] });
    // this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
    this.userDevices();
    // debugger;

  }
  ngAfterViewInit() {
    console.log("Home:  ngAfterViewInit()");
    // debugger
    // if (this.map) {
    //   this.map.remove();
    //   this.map = this.newMap();
    // } else {
    //   this.map = this.newMap();
    // }
    localStorage.removeItem("Home_FLAG");
    this.map = this.newMap();
  }

  ngOnDestroy() {
    for (var i = 0; i < this.socketChnl.length; i++)
      this._io.removeAllListeners(this.socketChnl[i]);
    this._io.on('disconnect', () => {
      this._io.open();
    })
  }

  ngOnInit() {
    console.log("Home:  ngOnInit()");

  }

  openChatSocket() {
    this._io1 = io('https://www.oneqlik.in/userChat', {
      transports: ['websocket']
    });
    this._io1.on('connect', (data) => {
      console.log("userChat connect data: ", data);
    })
  }

  getImgUrl() {
    var url = "https://www.oneqlik.in/users/shareProfileImage?uid=" + this.userdetails._id;
    this.apiCall.getdevicesForAllVehiclesApi(url)
      .subscribe(resp => {
        if (JSON.parse(JSON.stringify(resp)).imageDoc.length > 0) {
          var imgUrl = JSON.parse(JSON.stringify(resp)).imageDoc[0];
          var str1 = imgUrl.split('public/');
          this.fileUrl = "http://13.126.36.205/" + str1[1];
          // console.log("home image: ", this.fileUrl);
        }
      })
  }

  newMap() {
    let mapOptions = {
      controls: {
        zoom: false,
        myLocation: true,
        myLocationButton: false,
      },
      mapTypeControlOptions: {
        mapTypeIds: [GoogleMapsMapTypeId.ROADMAP, 'map_style']
      },
      gestures: {
        rotate: false,
        tilt: false
      },
      mapType: this.mapKey
    }
    let map = GoogleMaps.create('map_canvas', mapOptions);
    if (this.plt.is('android')) {
      map.setPadding(300, 50, 100, 50);
    }
    return map;
  }

  userDevices() {
    var baseURLp;
    let that = this;
    baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.userdetails._id + '&email=' + this.userdetails.email;

    if (this.userdetails.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.userdetails._id;
      this.isSuperAdmin = true;
    } else {
      if (this.userdetails.isDealer == true) {
        baseURLp += '&dealer=' + this.userdetails._id;
        this.isDealer = true;
      }
    }
    if (localStorage.getItem("Home_FLAG") == null) {
      that.loadingController.create({
        message: 'Please wait...'
      }).then((loadEl => {
        loadEl.present();
        that.accessFunc(loadEl, baseURLp);
      }))
    } else {
      that.accessFunc(null, baseURLp);
    }
  }

  accessFunc(loadEl, baseURLp) {
    let that = this;
    that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(respData => {
        if (loadEl) {
          loadEl.dismiss();
        }

        if (localStorage.getItem("Home_FLAG") === null) {
          localStorage.setItem("Home_FLAG", "Home_FLAG");
        }
        var resp = JSON.parse(JSON.stringify(respData));
        that.mapData = [];
        that.mapData = resp.devices.map(function (d) {
          if (d.last_location !== undefined) {
            return { lat: d.last_location['lat'], lng: d.last_location['long'] };
          } else {
            if (d.last_loc !== undefined) {
              if (d.last_loc.coordinates[1] !== 0 && d.last_loc.coordinates[0] !== 0) {
                return { lat: d.last_loc.coordinates[1], lng: d.last_loc.coordinates[0] }
              }
            }
          }
        });

        let bounds = new LatLngBounds(that.mapData);
        that.map.moveCamera({
          target: bounds,
          // zoom: 10
        })
        for (var i = 0; i < resp.devices.length; i++) {
          if (resp.devices[i].status != "Expired") {
            that.socketInit(resp.devices[i]);
          }
        }

        that.callGeofence(); // drawing geofence

      },
        err => {
          if (loadEl) {
            loadEl.dismiss();
          }
          console.log(err);
        });
  }

  socketInit(pdata, center = false) {
    let that = this;
    that._io.emit('acc', pdata.Device_ID);
    that.socketChnl.push(pdata.Device_ID + 'acc');
    that.someKey.push(pdata._id);
    that._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {

      if (d4 != undefined)
        (function (data) {

          if (data == undefined) {
            return;
          }
          console.log("on ping: ", data)

          if (data._id != undefined && (data.last_location != undefined || data.last_loc != undefined)) {
            var key = data._id;
            that.impkey = data._id;
            that.deviceDeatils = data;
          
            var strStr;
            if (that.allData[key]) {
              strStr = ' Speed: ' + data.last_speed + ' kmph' + '\n Battery: ' + (data.battery ? data.battery : '0') + '%' + '\n Pedometer: ' + (data.steps ? data.steps : 'N/A') + '\n Heartbeat: ' + (data.tumbling_num ? data.tumbling_num : 'N/A') + '\n Last Updated Time: ' + moment(new Date(data.last_ping_on)).format('LLLL');
              // strStr = ' Speed: ' + data.last_speed + ' kmph' + '\n Battery: ' + data.batteryStatus + '%' + '\n Pedometer: ' + data.last_speed + '\n Heartbeat: ' + data.last_speed + '\n Last Updated Time: ' + moment(new Date(data.last_ping_on)).format('LLLL');
              that.socketSwitch[key] = data;
              if (that.allData[key].mark != undefined) {
                that.allData[key].mark.setSnippet(strStr);
                that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                var temp = _.cloneDeep(that.allData[key].poly[1]);
                that.allData[key].poly[0] = _.cloneDeep(temp);
                that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };

                var speed = data.status == "RUNNING" ? data.last_speed : 0;

                that.liveTrack(that.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
              } else {
                return;
              }
            }
            else {
              that.allData[key] = {};
              that.socketSwitch[key] = data;
              that.allData[key].poly = [];
              if (data.last_location) {
                that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });

              } else {
                if (data.last_loc) {
                  that.allData[key].poly.push({ lat: data.last_loc.coordinates[1], lng: data.last_loc.coordinates[0] });

                }
              }
              strStr = ' Speed: ' + data.last_speed + ' kmph' + '\n Battery: ' + (data.battery ? data.battery : '0') + '%' + '\n Pedometer: ' + (data.steps ? data.steps : 'N/A') + '\n Heartbeat: ' + (data.tumbling_num ? data.tumbling_num : 'N/A') + '\n Last Updated Time: ' + moment(new Date(data.last_ping_on)).format('LLLL');
              var fileUrl;
              var imgUrl, str1;
              if (data.deviceImage.length > 0) {
                imgUrl = data.deviceImage[0];
                str1 = imgUrl.split('public/');
                fileUrl = "http://13.126.36.205/" + str1[1];
              }
              // console.log("file url on home page: ", fileUrl);

              if (data.last_location != undefined) {
                /////////////// merge images using canvas
                let canvas: any = document.createElement('canvas');
                canvas.width = 150;
                canvas.height = 150;
                var ctx = canvas.getContext('2d');

                // ctx.fillStyle = "black";
                // ctx.font = "50px Arial";
                // ctx.fillText('ASD', 0, 50);
                // ctx.globalCompositeOperation = "destination-over";
                // ctx.fillStyle = "#00FFFF";
                // // ctx.fillRect(0, 0, canvas.width, canvas.height);//for white background
                // ctx.globalCompositeOperation = "source-over";
                // ctx.lineWidth = 2;
                // ctx.strokeStyle = "#FF0000";
                // ctx.strokeRect(0, 0, canvas.width, canvas.height);//for white background

                var imageObj1 = new Image();
                imageObj1.crossOrigin = "anonymous";
                var imageObj2 = new Image();
                if (!fileUrl) {
                  imageObj1.src = "./assets/Images/dummy-profile-pic.png";
                } else {
                  imageObj1.src = fileUrl;
                }
                imageObj1.onload = function () {
                  ctx.drawImage(imageObj1, 36, 15, 78, 78);
                  imageObj2.src = "./assets/Images/32.png";
                  imageObj2.onload = function () {
                    ctx.drawImage(imageObj2, 0, 0, 150, 150);
                    var img = canvas.toDataURL("image/png");
                    that.map.addMarker({
                      title: data.Device_Name,
                      position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                      icon: {
                        url: img,
                        size: {
                          height: 90,
                          width: 90
                        }
                      },
                    }).then((marker: Marker) => {
                      that.allData[key].mark = marker;
                      marker.setSnippet(strStr);
                      marker.addEventListener(GoogleMapsEvent.MARKER_CLICK).subscribe(e => {
                        marker.showInfoWindow();
                        that.getAddressTitle(marker, strStr);
                      });
                    });
                    ///////////
                    // that.map.addMarker({
                    //   title: data.Device_Name,
                    //   position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                    //   // icon: img
                    //   icon: {
                    //     url: img,
                    //     size: {
                    //       height: 150,
                    //       width: 150
                    //     }
                    //   },
                    // }).then((marker: Marker) => {
                    //   that.allData[key].mark = marker;
                    //   marker.addEventListener(GoogleMapsEvent.MARKER_CLICK).subscribe(e => {
                    //     that.getAddressTitle(marker);
                    //     marker.showInfoWindow();
                    //   });
                    // });
                  }
                }
              }
            }
          }
        })(d4)
    })
  }

  getAddressTitle(marker, strStr) {
    console.log("issue coming from getAddressTitle")
    var payload = {
      "lat": marker.getPosition().lat,
      "long": marker.getPosition().lng,
    }
    var addr;
    this.apiCall.getAddress(payload)
      .subscribe((respData) => {
        if (respData) {
          var data = JSON.parse(JSON.stringify(respData));
          console.log("got address: " + data.results)
          if (data.message !== "Address not found in databse") {
            addr = data.address;
            marker.setSnippet(strStr + "\n Address:- " + addr);
          } else {
            let options: NativeGeocoderOptions = {
              useLocale: true,
              maxResults: 5
            };
            this.nativeGeocoder.reverseGeocode(payload.lat, payload.long, options)
              .then((result: NativeGeocoderResult[]) => {
                // debugger
                console.log("reverse geocode result: " + JSON.stringify(result[0]));
                var add = this.convertResult(result[0]);
                marker.setSnippet(strStr + "\n Address:- " + addr);
                // marker.setSnippet("Address:- " + add);
              })
              .catch((error: any) => console.log(error));
          }
        } else return;
      })
  }
  convertResult(result) {
    var a = result.thoroughfare ? result.thoroughfare : null;
    var b = result.subThoroughfare ? result.subThoroughfare : null;
    var c = result.subLocality ? result.subLocality : null;
    var d = result.subAdministrativeArea ? result.subAdministrativeArea : null;
    var e = result.postalCode ? result.postalCode : null;
    var f = result.locality ? result.locality : null;
    var g = result.countryName ? result.countryName : null;
    var h = result.administrativeArea ? result.administrativeArea : null;
    let str = '';
    if (a != null && a != 'Unnamed Road')
      str = a + ', ';
    if (b != null && b != 'Unnamed Road')
      str = str + b + ', ';
    if (c != null && c != 'Unnamed Road')
      str = str + c + ', ';
    if (d != null && d != 'Unnamed Road')
      str = str + d + ', ';
    if (e != null && e != 'Unnamed Road')
      str = str + e + ', ';
    if (f != null && f != 'Unnamed Road')
      str = str + f + ', ';
    if (g != null && g != 'Unnamed Road')
      str = str + g + ', ';
    if (h != null && h != 'Unnamed Road')
      str = str + h + ', ';
    console.log("address string: ", str);
    return str;
    // this.autocomplete.query = str;
  }

  liveTrack(map, mark, coords, speed, delay, center, id, that) {

    var target = 0;

    clearTimeout(that.ongoingGoToPoint[id]);
    clearTimeout(that.ongoingMoveMarker[id]);

    if (center) {
      map.setCameraTarget(coords[0]);
    }

    function _goToPoint() {
      if (target > coords.length)
        return;

      var lat = 0;
      var lng = 0;
      if (mark.getPosition().lat !== undefined || mark.getPosition().lng !== undefined) {
        lat = mark.getPosition().lat;
        lng = mark.getPosition().lng;
      }

      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target] == undefined)
        return;

      var dest = new LatLng(coords[target].lat, coords[target].lng);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      // function changeMarker(mark, deg) {
      //   // mark.setRotation(deg);
      //   // mark.setIcon(icons);
      // }
      function _moveMarker() {

        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            // changeMarker(mark, head);
          }
          // if (that.selectedVehicle != undefined) {
          //   map.setCameraTarget(new LatLng(lat, lng));
          // }
          that.latlngCenter = new LatLng(lat, lng);
          mark.setPosition(new LatLng(lat, lng));
          that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            // changeMarker(mark, head);
          }
          // if (that.selectedVehicle != undefined) {
          //   map.setCameraTarget(dest);
          // }
          that.latlngCenter = dest;
          mark.setPosition(dest);
          target++;
          that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }

  onHistory() {
    this.router.navigateByUrl('/history');
  }

  onSettings() {
    this.router.navigateByUrl('/settings');
  }

  onLocation() {
    this.router.navigateByUrl('/location-setup');
  }

  onChat() {
    // this.router.navigateByUrl('/chat');
    this.router.navigateByUrl('/user-list');
  }

  onProfile() {
    this.router.navigateByUrl('/profile');
  }

  addPushNotify() {
    let that = this;
    that.token = "";
    var pushdata;
    that.token = localStorage.getItem("DEVICE_TOKEN");
    if (that.token == null) {
      this.pushSetup();
    } else {
      if (this.plt.is('android')) {
        pushdata = {
          "uid": that.userdetails._id,
          "token": that.token,
          "os": "android"
        }
      } else {
        pushdata = {
          "uid": that.userdetails._id,
          "token": that.token,
          "os": "ios"
        }
      }

      that.apiCall.pushnotifyCall(pushdata)
        .subscribe(data => {
          console.log("push notifications updated " + JSON.parse(JSON.stringify(data)).message)
        },
          error => {
            console.log(error);
          });
    }
  }

  pushSetup() {
    // to check if we have permission
    this.push.hasPermission()
      .then((res: any) => {
        if (res.isEnabled) {
          console.log('We have permission to send push notifications');
        } else {
          console.log('We do not have permission to send push notifications');
        }
      });

    // Create a channel (Android O and above). You'll need to provide the id, description and importance properties.
    this.push.createChannel({
      id: "ignitionon",
      description: "ignition on description",
      sound: 'ignitionon',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('ignitionon Channel created'));

    this.push.createChannel({
      id: "ignitionoff",
      description: "ignition off description",
      sound: 'ignitionoff',
      importance: 4
    }).then(() => console.log('ignitionoff Channel created'));

    this.push.createChannel({
      id: "devicepowerdisconnected",
      description: "devicepowerdisconnected description",
      sound: 'devicepowerdisconnected',
      importance: 4
    }).then(() => console.log('devicepowerdisconnected Channel created'));

    this.push.createChannel({
      id: "default",
      description: "default description",
      sound: 'default',
      importance: 4
    }).then(() => console.log('default Channel created'));

    this.push.createChannel({
      id: "sos",
      description: "default description",
      sound: 'notif',
      importance: 4
    }).then(() => console.log('sos Channel created'));

    // Return a list of currently configured channels
    this.push.listChannels().then((channels) => console.log('List of channels', channels))

    // to initialize push notifications
    let that = this;
    const options: PushOptions = {
      android: {},
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };

    const pushObject: PushObject = that.push.init(options);


    pushObject.on('notification').subscribe((notification: any) => {

      if (notification.additionalData.foreground) {
        this.toastCtrl.create({
          message: notification.message,
          duration: 2000
        }).then(toastEl => {
          toastEl.present();
        });
      }
    });

    pushObject.on('registration')
      .subscribe((registration: any) => {
        console.log("device token => " + registration.registrationId);
        // console.log("reg type=> " + registration.registrationType);
        localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
        // that.storage.set("DEVICE_TOKEN", registration.registrationId);
        that.addPushNotify();
      });

    pushObject.on('error').subscribe(error => {
      console.error('Error with Push plugin: ', error)
    });

  }

  callGeofence() {
    // debugger
    var url = this.apiCall.mainUrl + "poi/getPois?user=" + this.userdetails._id;
    this.geoShape = [];
    // this.apiCall.getGeofenceCall(this.userdetails._id)
    this.apiCall.getdevicesForAllVehiclesApi(url)
      .subscribe(respData => {
        var data = JSON.parse(JSON.stringify(respData));
        console.log("geofence data=> " + data.length)
        if (data.length > 0) {
          this.innerFunc(data);
        } else {
          this.toastCtrl.create({
            message: 'Geofence not found..!!',
            position: 'bottom',
            duration: 1500
          }).then(toastEl => {
            toastEl.present();
          })
        }
      },
        err => {
          console.log(err)
        });

  }

  innerFunc(data) {
    var i = 0, howManyTimes = data.length;
    let that = this;
    function f() {
      that.geoShape.push({
        "poiname": data[i].poi.poiname,
        "radius": data[i].radius ? data[i].radius : 'N/A',
        "_id": data[i]._id,
        "start_location": {
          'lat': data[i].poi.location.coordinates[1],
          'long': data[i].poi.location.coordinates[0]
        },
        "address": data[i].poi.address ? data[i].poi.address : 'N/A'
      });
      let ltln: ILatLng = { "lat": that.geoShape[i].start_location.lat, "lng": that.geoShape[i].start_location.long };
      let circle: Circle = that.map.addCircleSync({
        'center': ltln,
        'radius': that.geoShape[i].radius,
        'strokeColor': '#4dc4ec',
        'strokeWidth': 2,
        'fillColor': 'rgba(77, 196, 236, 0.2)'
      });

      that.cityCircle = circle;
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 200);
      }
    } f();
  }


  // drawPolygon(polyData) {
  //   let that = this;
  //   that.geodata = [];
  //   that.geodata = polyData.map(function (d) {
  //     return { lat: d[0], lng: d[1] }
  //   });
  //   console.log("geodata=> ", that.geodata)

  //   let GORYOKAKU_POINTS: ILatLng[] = that.geodata;
  //   console.log("GORYOKAKU_POINTS=> ", GORYOKAKU_POINTS)
  //   // that.map.addPolygonSync({
  //   //   'points': GORYOKAKU_POINTS,
  //   //   'strokeColor': '#AA00FF',
  //   //   'fillColor': '#00FFAA',
  //   //   'strokeWidth': 2
  //   // }).then((polygon: Polygon) => {
  //   //   this.generalPolygon = polygon;
  //   // });
  //   var generalPolygon: Polygon = that.map.addPolygonSync({
  //     'points': GORYOKAKU_POINTS,
  //     'strokeColor': '#AA00FF',
  //     'fillColor': '#00FFAA',
  //     'strokeWidth': 2
  //   });
  //   this.generalPolygon = generalPolygon;
  // }

  //Check if application having GPS access permission  
  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {

          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        } else {

          //If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              //Show alert if user click on 'No Thanks'
              // console.log(error);
              console.log('requestPermission Error requesting location permissions ' + JSON.stringify(error))
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        // this.getLocationCoordinates()
      },
      error => {
        console.log('Error requesting location permissions ' + JSON.stringify(error))
      }
    );
  }

  ////// custom marker code ///

}
