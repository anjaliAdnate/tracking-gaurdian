import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WalkthroughPage } from './walkthrough.page';
// import { ShellModule } from '../shell/shell.module';
// import { ComponentsModule } from '../components/components.module';

const routes: Routes = [
  {
    path: '',
    component: WalkthroughPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    // ShellModule
    // ComponentsModule
  ],
  declarations: [WalkthroughPage]
})
export class WalkthroughPageModule {}
