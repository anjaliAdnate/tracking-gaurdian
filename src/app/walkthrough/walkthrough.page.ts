import { Component, OnInit, ViewChild, HostBinding, AfterViewInit } from '@angular/core';
import { IonSlides, MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router'

@Component({
  selector: 'app-walkthrough',
  templateUrl: './walkthrough.page.html',
  styleUrls: [
    './styles/walkthrough.page.scss',
    './styles/walkthrough.shell.scss',
    './styles/walkthrough.responsive.scss'
  ]
  // styleUrls: ['./walkthrough.page.scss'],
})
export class WalkthroughPage implements OnInit {
  slidesOptions: any = {
    zoom: {
      toggle: false // Disable zooming to prevent weird double tap zomming on slide images
    }
  };
  @ViewChild(IonSlides, { static: true }) slides: IonSlides;
  @HostBinding('class.first-slide-active') isFirstSlide = true;

  @HostBinding('class.last-slide-active') isLastSlide = false;

  // @HostBinding('style.padding') ratioPadding = '0px';
  constructor(public menu: MenuController, private storage: Storage, private router: Router) { }

  ngOnInit() {
    this.menu.enable(false);
  }

  async finish() {
    await this.storage.set('walkthroughComplete', true);
    this.router.navigateByUrl('/');
  }

  next() {
    this.slides.slideNext();
  }

  // ngAfterViewInit(): void {
  //   // ViewChild is set
  //   this.slides.isBeginning().then(isBeginning => {
  //     this.isFirstSlide = isBeginning;
  //   });
  //   this.slides.isEnd().then(isEnd => {
  //     this.isLastSlide = isEnd;
  //   });

  //   // Subscribe to changes
  //   this.slides.ionSlideWillChange.subscribe(changes => {
  //     this.slides.isBeginning().then(isBeginning => {
  //       this.isFirstSlide = isBeginning;
  //     });
  //     this.slides.isEnd().then(isEnd => {
  //       this.isLastSlide = isEnd;
  //     });
  //   });
  // }

  skipWalkthrough(): void {
    // Skip to the last slide
    this.slides.length().then(length => {
      this.slides.slideTo(length);
    });
  }

}
