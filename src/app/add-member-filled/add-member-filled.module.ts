import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddMemberFilledPage } from './add-member-filled.page';

const routes: Routes = [
  {
    path: '',
    component: AddMemberFilledPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddMemberFilledPage]
})
export class AddMemberFilledPageModule {}
