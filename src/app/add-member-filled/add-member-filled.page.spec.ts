import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMemberFilledPage } from './add-member-filled.page';

describe('AddMemberFilledPage', () => {
  let component: AddMemberFilledPage;
  let fixture: ComponentFixture<AddMemberFilledPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMemberFilledPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMemberFilledPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
