import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { ToastController, LoadingController } from '@ionic/angular';
import * as moment from 'moment';

@Component({
  selector: 'app-add-member-filled',
  templateUrl: './add-member-filled.page.html',
  styleUrls: ['./add-member-filled.page.scss'],
})
export class AddMemberFilledPage implements OnInit {
  // segment: string;
  fn: string;
  ln: string;
  email: any;
  exp_date: any;
  pass: any;
  userdetails: any;
  phone: any;
  userId: any;

  constructor(
    // private actionSheetController: ActionSheetController,
    // private camera: Camera,
    private loadingController: LoadingController,
    // private crop: Crop,
    // private transfer: FileTransfer,
    private apiCall: AppService,
    private taostCtrl: ToastController
  ) {
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    // var currentDate = moment(new Date());
    // var tesmp = moment(new Date(currentDate), 'DD-MM-YYYY').format("YYYY-MM-DD")
    // var futureMonth = moment(tesmp).add(3, 'Y');
    // var futureMonthEnd = moment().add(3, 'years');
    // console.log("future year: ", (futureMonthEnd).format('dddd Do MMMM, YYYY'));
    // console.log(futureMonthEnd);
    // var temp = 
    // this.exp_date = new Date((futureMonthEnd).format('dddd Do MMMM, YYYY')).toISOString();

  }

  ngOnInit() {
  }

  submit() {
    var url = this.apiCall.mainUrl + "users/createUserPeronalTracker";
    let payload = {
      first_name: this.fn ? this.fn : null,
      last_name: this.ln ? this.ln : null,
      email: this.email ? this.email : null,
      pass: this.pass ? this.pass : null,
      phone: this.phone ? this.phone : null,
      isDealer: false,
      expdate: new Date(this.exp_date).toISOString(),
      Dealer: this.userdetails._id,
      user_id: this.userId ? this.userId : null,
    }
    this.loadingController.create({
      message: 'Please wait, we are adding new member...'
    }).then((loadEl) => {
      loadEl.present();
      this.apiCall.urlWithdata(url, payload)
        .subscribe((respData) => {
          loadEl.dismiss();
          if (respData) {
            var res = JSON.parse(JSON.stringify(respData));
            console.log("add member response: ", res);
            if (res) {
              this.taostCtrl.create({
                message: "member is added successfully.",
                position: 'middle',
                duration: 3000
              }).then((toastEl) => {
                toastEl.present();
                this.fn = undefined; this.ln = undefined; this.phone = undefined; this.email = undefined; this.userId = undefined; this.exp_date = undefined; this.pass = undefined;
              });
            }
          }
        },
          err => {
            loadEl.dismiss();
            console.log(err);
            if (err.error.message) {
              this.taostCtrl.create({
                message: err.error.message,
                position: 'middle',
                duration: 2000
              }).then((toastEl) => {
                toastEl.present();
              });
            }

          });
    })

  }

}
