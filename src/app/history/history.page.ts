import { Component, OnInit, ViewChild, ElementRef, Renderer2, AfterViewInit } from '@angular/core';
import { DrawerState } from 'ion-bottom-drawer';
import { AppService } from '../app.service';
import { LoadingController, AlertController, ToastController, Platform } from '@ionic/angular';
import { LatLngBounds, GoogleMaps, GoogleMapsEvent, Marker, Polygon, ILatLng, GoogleMapsMapTypeId, GoogleMap, LatLng, Spherical, LocationService, GoogleMapOptions, MyLocation } from '@ionic-native/google-maps';
import * as moment from 'moment';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit, AfterViewInit {
  map: GoogleMap;
  shouldBounce = true;
  // dockedHeight = 250;
  dockedHeight = 100;
  distanceTop = 378;
  drawerState = DrawerState.Docked;
  states = DrawerState;
  minimumHeight = 100;
  dataArrayCoords: any = [];
  mapData: any = [];
  playPause: boolean;
  mark: any;
  vehObj: any;

  @ViewChild('map', { static: true }) mapElementRef: ElementRef;
  islogin: any;
  portstemp: any = [];
  trackerId: any;
  trackerType: any;
  DeviceId: any;
  trackerName: any;
  allData: any = {};
  datetimeStart: any;
  datetimeEnd: any;
  selectedVehicle: any;
  fromtime: string;
  totime: string;
  data2: any;
  latlongObjArr: any;
  hideplayback: boolean;
  // dataArrayCoords: any = [];
  // mapData: any = [];
  // mapKey: any;
  drawerHidden1: boolean;
  geofenceData: any = [];
  geoShape: any = [];
  geodata: any = [];
  generalPolygon: any;
  show_string: string = "";

  constructor(
    // private renderer: Renderer2,
    private apiCall: AppService,
    private loadingController: LoadingController,
    private alertController: AlertController,
    private toastCtrl: ToastController,
    private plt: Platform
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);
  }

  ngOnInit() {
    localStorage.removeItem("markerTarget");
    this.getdevices();
    this.plt.ready().then(() => {
      this.loadMap();
    });
  }

  setDocHeight() {
    let that = this;
    that.distanceTop = 150;
    that.drawerState = DrawerState.Top;
  }

  ngAfterViewInit() {
    // this.getGoogleMap().then(googleMaps => {
    //   const mapEl = this.mapElementRef.nativeElement;
    //   const map = new googleMaps.Map(mapEl, {
    //     center: { lat: 18.5204, lng: 73.8567 },
    //     zoom: 10
    //   });
    //   googleMaps.event.addListenerOnce(map, 'idle', () => {
    //     this.renderer.addClass(mapEl, 'visible');
    //   });
    // }).catch(err => {
    //   console.log(err);
    // });
  }
  loadMap() {
    LocationService.getMyLocation().then((myLocation: MyLocation) => {
      let options: GoogleMapOptions = {
        camera: {
          target: myLocation.latLng
        },
        preferences: {
          zoom: {
            minZoom: 5,
            maxZoom: 50
          },

          padding: {
            left: 10,
            top: 10,
            bottom: 10,
            right: 10
          },

          building: true
        }
      };
      this.map = GoogleMaps.create('map_canvas', options);
    });
  }

  // private getGoogleMap(): Promise<any> {
  //   const win = window as any;
  //   const googleModule = win.google;
  //   if (googleModule && googleModule.maps) {
  //     return Promise.resolve(googleModule.maps);
  //   }
  //   return new Promise((resolve, reject) => {
  //     const script = document.createElement('script');
  //     script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8';
  //     script.async = true;
  //     script.defer = true;
  //     document.body.appendChild(script);
  //     script.onload = () => {
  //       const loadedGoogleMpdule = win.google;
  //       if (loadedGoogleMpdule && loadedGoogleMpdule.maps) {
  //         resolve(loadedGoogleMpdule.maps);
  //       } else {
  //         reject('Google maps SDK not available.');
  //       }
  //     };
  //   });
  // }

  getdevices() {
    var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.loadingController.create({
      message: 'Loading vehicle list...'
    }).then((loadEl => {
      loadEl.present();
      this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
        .subscribe(data => {
          loadEl.dismiss();
          this.portstemp = JSON.parse(JSON.stringify(data)).devices;
        },
          error => {
            loadEl.dismiss();
            console.log(error);
          });
    }));
  }

  checkIfMapExist() {
    if (this.map != undefined) {
      this.map.remove();
      this.loadMap();
    }
  }

  onChangedSelect(item) {
    debugger
    let that = this;
    console.log("item: ", item);
    this.checkIfMapExist();
    this.vehObj = item;
    console.log("selected vehicl: ", that.selectedVehicle);
    that.getHistoryOfSelectedVehicle();

  }

  onChangeDatetime() {
    debugger
    this.checkIfMapExist();
    if (this.vehObj.Device_ID !== undefined) {
      this.getHistoryOfSelectedVehicle();

    }
  }

  // btnClicked(timeStart, timeEnd) {

  //   this.geoFence();
  //   if (localStorage.getItem("MainHistory") != null) {
  //     if (this.selectedVehicle == undefined) {
  //       this.alertController.create({
  //         message: "Please select the vehicle first!!",
  //         buttons: ['OK']
  //       }).then((alertEl => {
  //         alertEl.present();
  //       }))
  //     } else {
  //       this.maphistory(timeStart, timeEnd);
  //     }
  //   } else {
  //     this.maphistory(timeStart, timeEnd);
  //   }
  // }

  getHistoryOfSelectedVehicle() {

    if (new Date(this.datetimeEnd).toISOString() <= new Date(this.datetimeStart).toISOString()) {
      this.toastCtrl.create({
        message: 'Please select valid time and try again..(to time always greater than from Time).',
        duration: 3000,
        position: 'bottom'
      }).then((toastEl) => {
        toastEl.present();
      });
      return;
    }
    // this.callGeofence();
    this.geoFence();
    this.loadingController.create({
      message: 'Loading history data...',
    }).then((loadEl) => {
      loadEl.present();
      this.getHistoryData(loadEl);
    });
  }

  getDistanceAndSpeed(loadEl) {
    var url = this.apiCall.mainUrl + "gps/getDistanceSpeed?imei=" + this.vehObj.Device_ID + "&from=" + new Date(this.datetimeStart).toISOString() + "&to=" + new Date(this.datetimeEnd).toISOString();
    this.apiCall.getdevicesForAllVehiclesApi(url)
      .subscribe(respData => {
        loadEl.dismiss();
        if (respData != {}) {
          var res = JSON.parse(JSON.stringify(respData));
          console.log("check history: " + res.Distance);
        }

      },
        err => {
          console.log(err);
          loadEl.dismiss();
        });
  }

  getHistoryData(loadEl) {
    var url1 = this.apiCall.mainUrl + "gps/v2?id=" + this.vehObj.Device_ID + "&from=" + new Date(this.datetimeStart).toISOString() + "&to=" + new Date(this.datetimeEnd).toISOString();
    this.apiCall.getdevicesForAllVehiclesApi(url1)
      .subscribe(respData => {
        loadEl.dismiss();
        if (respData != undefined) {
          var res = JSON.parse(JSON.stringify(respData));
          console.log("res gps data: ", res);
          if (res.length > 1) {
            this.plotHstory(res.reverse());
          } else {
            this.alertController.create({
              message: 'Oops.. history data not found for selected dates.',
              buttons: ['Okay']
            }).then((alertEl) => {
              alertEl.present();
            });
          }
        }
      },
        err => {
          loadEl.dismiss();
          console.log(err);
        });
  }

  plotHstory(data) {
    let that = this;
    that.dataArrayCoords = [];
    for (var i = 0; i < data.length; i++) {
      if (data[i].lat && data[i].lng) {
        var arr = [];
        var cumulativeDistance = 0;
        var startdatetime = new Date(data[i].insertionTime);
        arr.push(data[i].lat);
        arr.push(data[i].lng);
        arr.push({ "time": startdatetime.toLocaleString() });
        arr.push({ "speed": data[i].speed });
        arr.push({ "imei": data[i].imei });

        if (data[i].isPastData != true) {
          if (i === 0) {
            cumulativeDistance += 0;
          } else {
            cumulativeDistance += data[i].distanceFromPrevious ? parseFloat(data[i].distanceFromPrevious) : 0;
          }
          data[i]['cummulative_distance'] = (cumulativeDistance).toFixed(2);
          arr.push({ "cumu_dist": data[i]['cummulative_distance'] });
        } else {
          data[i]['cummulative_distance'] = (cumulativeDistance).toFixed(2);
          arr.push({ "cumu_dist": data[i]['cummulative_distance'] });
        }
        that.dataArrayCoords.push(arr);
      }
    }
    that.mapData = [];
    that.mapData = data.map(function (d) {
      return { lat: d.lat, lng: d.lng };
    })
    that.mapData.reverse();

    this.drawOnMap();
  }

  drawOnMap() {
    let that = this;
    let bounds = new LatLngBounds(that.mapData);

    that.map.moveCamera({
      target: bounds
    });

    that.map.addMarker({
      title: 'D',
      position: that.mapData[0],
      icon: 'red',
      styles: {
        'text-align': 'center',
        'font-style': 'italic',
        'font-weight': 'bold',
        'color': 'red'
      },
    }).then((marker: Marker) => {
      marker.showInfoWindow();

      that.map.addMarker({
        title: 'S',
        position: that.mapData[that.mapData.length - 1],
        icon: 'green',
        styles: {
          'text-align': 'center',
          'font-style': 'italic',
          'font-weight': 'bold',
          'color': 'green'
        },
      }).then((marker: Marker) => {
        marker.showInfoWindow();
      });
    });

    that.map.addPolyline({
      points: that.mapData,
      color: '#635400',
      width: 3,
      geodesic: true
    })
  }

  geoFence() {
    var url = this.apiCall.mainUrl + "notifs/getNotifByFilters?from_date=" + new Date(this.datetimeStart).toISOString() + "&to_date=" + new Date(this.datetimeEnd).toISOString() + "&user=" + this.islogin._id + "&type=route-poi&sortOrder=-1"
    // var url = this.apiCall.mainUrl + "poi/getPois?user=" + this.islogin._id;
    this.geoShape = [];
    this.geofenceData = [];
    this.show_string = "Loading geofences...";
    this.apiCall.getdevicesForAllVehiclesApi(url)
      .subscribe(respData => {
        this.show_string = "";
        var data = JSON.parse(JSON.stringify(respData));
        console.log("geofence data=> " + data.length)
        if (data.length > 0) {
          this.innerFunc(data);
        }
      },
        err => {
          this.show_string = "Geofence not found...!!";
          console.log(err)
        });
  }
  innerFunc(data) {
    var i = 0, howManyTimes = data.length;
    let that = this;
    function f() {
      // that.geofenceData.push({
      //   "poiname": data[i].poi.poiname,
      //   "radius": data[i].radius ? data[i].radius : 'N/A',
      //   "_id": data[i]._id,
      //   "start_location": {
      //     'lat': data[i].poi.location.coordinates[1],
      //     'long': data[i].poi.location.coordinates[0]
      //   },
      //   "address": data[i].poi.address ? data[i].poi.address : 'N/A'
      // });
      var str = data[i].item.sentence;
      var str1 = str.split('POI');
      var str2 = str1[1];
      var direction;
      if(data[i].poiAction === 'reached') {
        direction = "IN";
      } else if(data[i].poiAction === 'left'){
        direction = "OUT";
      }
      that.geofenceData.push({
        "geo_name": str2,
        "direction": direction,
        "timestamp": data[i].timestamp
      });
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 200);
      }
    } f();
  }

  target: number = 0;
  speed: number = 0;
  animateHistory() {
    let that = this;
    // that.showZoom = true;
    if (localStorage.getItem("markerTarget") != null) {
      that.target = JSON.parse(localStorage.getItem("markerTarget"));
    }
    that.playPause = !that.playPause; // This would alternate the state each time

    var coord = that.dataArrayCoords[that.target];
    // that.coordreplaydata = coord;
    var lat = coord[0];
    var lng = coord[1];

    var startPos = [lat, lng];
    that.speed = 200; // km/h

    if (that.playPause) {
      that.map.setCameraTarget({ lat: lat, lng: lng });
      if (that.mark == undefined) {
        // debugger
        // var icicon;
        var fileUrl;
        // var imgUrl, str1;
        let canvas: any = document.createElement('canvas');
        canvas.width = 150;
        canvas.height = 150;
        var ctx = canvas.getContext('2d');
        var imageObj1 = new Image();
        imageObj1.crossOrigin = "anonymous";
        var imageObj2 = new Image();
        if (!fileUrl) {
          imageObj1.src = "./assets/Images/dummy-profile-pic.png";
        } else {
          imageObj1.src = fileUrl;
        }
        imageObj1.onload = function () {
          ctx.drawImage(imageObj1, 36, 15, 78, 78);
          imageObj2.src = "./assets/Images/32.png";
          imageObj2.onload = function () {
            ctx.drawImage(imageObj2, 0, 0, 150, 150);
            var img = canvas.toDataURL("image/png");
            that.map.addMarker({
              icon: {
                url: img,
                size: {
                  height: 90,
                  width: 90
                }
              },
              position: new LatLng(startPos[0], startPos[1]),
            }).then((marker: Marker) => {
              that.mark = marker;
              that.liveTrack(that.map, that.mark, that.dataArrayCoords, that.target, startPos, that.speed, 100);
            });
          }
        }
      } else {
        that.liveTrack(that.map, that.mark, that.dataArrayCoords, that.target, startPos, that.speed, 100);
      }
    } else {
      that.mark.setPosition(new LatLng(startPos[0], startPos[1]));
    }
  }

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};
  liveTrack(map, mark, coords, target, startPos, speed, delay) {
    let that = this;
    // that.events.subscribe("SpeedValue:Updated", (sdata) => {
    //   speed = sdata;
    // })
    var target = target;
    clearTimeout(that.ongoingGoToPoint[coords[target][4].imei]);
    clearTimeout(that.ongoingMoveMarker[coords[target][4].imei]);
    console.log("check coord imei: ", coords[target][4].imei);
    if (!startPos.length)
      coords.push([startPos[0], startPos[1]]);

    function _gotoPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000;
      if (coords[target] == undefined)
        return;
      var dest = new LatLng(coords[target][0], coords[target][1]);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); //in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target][0] - lat) / numStep;
      var deltaLng = (coords[target][1] - lng) / numStep;

      // function changeMarker(mark, deg) {
      //   mark.setRotation(deg);
      // }

      function _moveMarker() {
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          // if ((head != 0) || (head == NaN)) {
          //   changeMarker(mark, head);
          // }

          mark.setPosition(new LatLng(lat, lng));
          map.setCameraTarget(new LatLng(lat, lng))
          that.ongoingMoveMarker[coords[target][4].imei] = setTimeout(_moveMarker, delay);
        } else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          // if ((head != 0) || (head == NaN)) {
          //   changeMarker(mark, head);
          // }

          mark.setPosition(dest);
          map.setCameraTarget(dest);
          target++;
          that.ongoingGoToPoint[coords[target][4].imei] = setTimeout(_gotoPoint, delay);
        }
      }
      a++
      if (a > coords.length) {

      } else {
        // that.speedMarker = coords[target][3].speed;
        // that.updatetimedate = coords[target][2].time;
        // that.cumu_distance = coords[target][5].cumu_dist;

        if (that.playPause) {
          _moveMarker();
          target = target;
          localStorage.setItem("markerTarget", target);

        } else { }
        // km_h = km_h;
      }
    }
    var a = 0;
    _gotoPoint();
  }

}
