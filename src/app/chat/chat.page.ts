import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../app.service';
import * as moment from 'moment';
import * as io from 'socket.io-client';
// import { Keyboard } from '@ionic-native/keyboard/ngx';
// declare var $: any;

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  start_typing: any;
  @ViewChild('IonContent', { static: true }) content: IonContent;
  paramData: any = {};
  userName: any;
  msgList: any = [];
  user_input: string = "";
  // User: string = "Me";
  // toUser: string = "HealthBot";
  User: string;
  toUser: string;
  loader: boolean;
  fromDate: string;
  toDate: string;
  islogin: any;
  _io: any;
  public innerWidth: any;
  kbHeight: number;
  constructor(
    public activRoute: ActivatedRoute,
    private apiCall: AppService,
    private loaderController: LoadingController,
    // private keyboard: Keyboard
  ) {
    this.innerWidth = window.innerWidth;
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.fromDate = moment({ hours: 0 }).format();

    this.toDate = moment().format();
    if (this.activRoute.snapshot.data['special']) {
      this.paramData = this.activRoute.snapshot.data['special'];
      this.userName = this.paramData.fisrt_name;
      console.log("param data: ", this.paramData);
      this.User = this.islogin._id;
      this.toUser = this.paramData._id;
    }
  }
  ngAfterViewInit() {
    this.content.scrollToBottom();
    // This element never changes.
    // let ionapp = document.getElementsByTagName("ion-app")[0];

    window.addEventListener('keyboardDidShow', async (event) => {
      // Move ion-app up, to give room for keyboard
      let kbHeight: number = event["keyboardHeight"];
      this.kbHeight = kbHeight;
    });
    window.addEventListener('keyboardWillHide', () => {
      // this.keyboard.show();
      // Describe your logic which will be run each time when keyboard is about to be closed.
    });
  }

  ngOnInit() {
    this.openChatSocket();
    this.getChatHistory();
    // this.innerWidth = window.innerWidth;
    // console.log("window test: ", this.innerWidth)
  }

  openChatSocket() {
    this._io = io('https://www.oneqlik.in/userChat', {
      transports: ['websocket']
    });
    this._io.on('connect', (data) => {
      console.log("userChat connect data: ", data);
    });
    let that = this;
    that._io.on(that.User + "-" + that.toUser, (d4) => {
      if (d4 != undefined)
        (function (data) {
          if (data == undefined) {
            return;
          }
          that.loader = true;
          setTimeout(() => {
            that.msgList.push({
              userId: that.User,
              userName: that.User,
              // userAvatar: "../../assets/chat/chat5.jpg",
              time: new Date().toISOString(),
              message: data
            });
            that.loader = false;
            that.scrollDown();
          }, 2000)
          that.scrollDown();
        })(d4)
    })
  }

  getChatHistory() {
    let that = this;
    this.loaderController.create({
      message: 'please wait we are fetching history...'
    }).then((loadEl) => {
      loadEl.present();
      var url = this.apiCall.mainUrl + "broadcastNotification/getchatmsg?from=" + this.islogin._id + "&to=" + that.paramData._id;
      this.apiCall.getdevicesForAllVehiclesApi(url)
        .subscribe(respData => {
          loadEl.dismiss();
          if (respData) {
            var res = JSON.parse(JSON.stringify(respData));
            for (var i = 0; i < res.length; i++) {
              if (res[i].sender === this.toUser) {
                this.msgList.push({
                  userId: this.User,
                  userName: this.User,
                  time: res[i].timestamp,
                  message: res[i].message,
                  id: this.msgList.length + 1
                })
              } else {
                if (res[i].sender === this.User) {
                  this.msgList.push({
                    userId: this.toUser,
                    userName: this.toUser,
                    time: res[i].timestamp,
                    message: res[i].message,
                    id: this.msgList.length + 1
                  })
                }
              }
            }
            setTimeout(() => {
              this.content.scrollToBottom(100);
            }, 50);

            // this.scrollDown();
          }
        },
          err => {
            loadEl.dismiss();
            console.log("chat err: ", err)
          });
    })

  }
  sendMsg() {
    if (this.user_input !== '') {
      this.msgList.push({
        userId: this.toUser,
        userName: this.toUser,
        time: new Date().toISOString(),
        message: this.user_input,
        id: this.msgList.length + 1
      })
      let that = this;
      that._io.emit('send', this.User, this.toUser, this.user_input);   // three parameters, from(who is sending msg), to(to whom ur sending msg), msg(message string)

      this.user_input = "";
      // setTimeout(() => {
      //   this.content.scrollToBottom(100);
      // }, 50);
      // this.scrollDown();
      // setTimeout(() => {
      //   this.senderSends()
      // }, 500);
    }
  }

  // senderSends() {
  //   this.loader = true;
  //   setTimeout(() => {

  //     this.msgList.push({
  //       userId: this.User,
  //       userName: this.User,
  //       // userAvatar: "../../assets/chat/chat5.jpg",
  //       time: new Date().toISOString(),
  //       message: "Sorry, didn't get what you said. Can you repeat that please"
  //     });
  //     this.loader = false;
  //     this.scrollDown()
  //   }, 2000)
  //   this.scrollDown()
  // }

  scrollDown() {
    setTimeout(() => {
      console.log("window height: ", this.innerWidth);
      console.log("keyboard height: ", this.kbHeight);
      // this.content.scrollToBottom(this.innerWidth)
      this.content.scrollToPoint(1500, 1500, 500).then((res) => {
        console.log("scrollToPoint: ", res);
      })
    }, 50);
  }
  userTyping(event: any) {
    debugger
    console.log(event);
    this.start_typing = event.target.value;
    this.scrollDown()
  }

}
