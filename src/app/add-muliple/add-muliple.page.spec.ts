import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMuliplePage } from './add-muliple.page';

describe('AddMuliplePage', () => {
  let component: AddMuliplePage;
  let fixture: ComponentFixture<AddMuliplePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMuliplePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMuliplePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
