import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { CameraPosition, ILatLng, LatLng, GoogleMap, GoogleMaps, Circle, LocationService, MyLocation, Marker } from '@ionic-native/google-maps';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { AppService } from '../app.service';
// import { Geolocation } from '@ionic-native/geolocation';
declare var google;

@Component({
  selector: 'app-add-muliple',
  templateUrl: './add-muliple.page.html',
  styleUrls: ['./add-muliple.page.scss'],
})
export class AddMuliplePage implements OnInit, AfterViewInit {
  // map: any;
  key: string;
  acService: any;
  autocompleteItems: any = [];
  autocomplete: any = {};
  newLat: any;
  newLng: any;
  map: GoogleMap;
  // mapElement: HTMLElement;
  fence: number = 200;

  // @ViewChild('map', { static: true }) mapElementRef: ElementRef;
  cityCircle: Circle;
  islogin: any;
  ppname: string;
  markObj: Marker;
  constructor(
    // private renderer: Renderer2,
    private loadingController: LoadingController,
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private nativeGeocoder: NativeGeocoder,
    private androidPermissions: AndroidPermissions,
    // private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy,
    private apiCall: AppService,
    private toastCtrl: ToastController
    // public geoLocation: Geolocation,
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.acService = new google.maps.places.AutocompleteService();
  }

  ngOnInit() {

    this.activatedRoute.paramMap.subscribe((paramMap) => {
      if (!paramMap.has('key')) {
        this.navCtrl.navigateBack('/location-setup');
        return;
      }
      this.key = paramMap.get('key');
      this.ppname = this.key;
      console.log("key: ", paramMap.get('key'));
      this.autocompleteItems = [];
      this.autocomplete = {
        query: '',
        creation_type: 'circular'
      };
    })
  }

  ionViewDidEnter() {
    this.checkGPSPermission();
  }

  //Check if application having GPS access permission  
  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {

          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        } else {

          //If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              //Show alert if user click on 'No Thanks'
              console.log('requestPermission Error requesting location permissions ' + JSON.stringify(error))
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        // this.getLocationCoordinates()
        this.drawGeofence();
      },
      error => console.log('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

  ngAfterViewInit() {
    this.drawGeofence();
    // this.getGoogleMap().then(googleMaps => {
    //   const mapEl = this.mapElementRef.nativeElement;
    //   const map = new googleMaps.Map(mapEl, {
    //     center: { lat: 18.5204, lng: 73.8567 },
    //     zoom: 10
    //   });
    //   googleMaps.event.addListenerOnce(map, 'idle', () => {
    //     this.renderer.addClass(mapEl, 'visible');
    //   });
    // }).catch(err => {
    //   console.log(err);
    // });
  }

  // private getGoogleMap(): Promise<any> {
  //   const win = window as any;
  //   const googleModule = win.google;
  //   if (googleModule && googleModule.maps) {
  //     return Promise.resolve(googleModule.maps);
  //   }
  //   return new Promise((resolve, reject) => {
  //     const script = document.createElement('script');
  //     script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8';
  //     script.async = true;
  //     script.defer = true;
  //     document.body.appendChild(script);
  //     script.onload = () => {
  //       const loadedGoogleMpdule = win.google;
  //       if (loadedGoogleMpdule && loadedGoogleMpdule.maps) {
  //         resolve(loadedGoogleMpdule.maps);
  //       } else {
  //         reject('Google maps SDK not available.');
  //       }
  //     };
  //   });
  // }

  updateSearch() {
    // debugger
    console.log('modal > updateSearch');
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let that = this;
    let config = {
      //types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
      input: that.autocomplete.query,
      componentRestrictions: {}
    }
    this.acService.getPlacePredictions(config, function (predictions, status) {
      console.log('modal > getPlacePredictions > status > ', status);
      console.log("lat long not find ", predictions);

      that.autocompleteItems = [];
      predictions.forEach(function (prediction) {
        that.autocompleteItems.push(prediction);
      });
      console.log("autocompleteItems=> " + that.autocompleteItems)
    });
  }

  chooseItem(item) {
    let that = this;
    that.autocomplete.query = item.description;
    // console.log("console items=> " + JSON.stringify(item))
    that.autocompleteItems = [];

    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };

    this.nativeGeocoder.forwardGeocode(item.description, options)
      .then((coordinates: NativeGeocoderResult[]) => {
        // console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude)
        that.newLat = Number(coordinates[0].latitude);
        that.newLng = Number(coordinates[0].longitude);
        console.log('latitude= ' + that.newLat)
        console.log('longitude= ' + that.newLng)
        debugger
        if (that.cityCircle) {
          that.cityCircle.remove();
        }
        if (that.markObj) {
          that.markObj.remove();
        }
        let pos: CameraPosition<ILatLng> = {
          target: new LatLng(that.newLat, that.newLng),
          zoom: 15,
          tilt: 30
        };
        this.map.moveCamera(pos);
        let markTemp: Marker = that.map.addMarkerSync({
          title: '',
          position: new LatLng(that.newLat, that.newLng),
          icon: {
            url: '../assets/Images/321.png',
            size: {
              width: 100,
              height: 100
            }
          },
        });
        that.markObj = markTemp;

        let ltln: ILatLng = { "lat": that.newLat, "lng": that.newLng };
        let circle: Circle = that.map.addCircleSync({
          'center': ltln,
          'radius': that.fence,
          'strokeColor': '#4dc4ec',
          'strokeWidth': 3,
          'fillColor': 'rgb(77, 196, 236, 0.5)'
        });

        that.cityCircle = circle;
        console.log("circle data: => " + that.cityCircle)
      })
      .catch((error: any) => console.log(error));

  }

  reverseGeocode(lat, lng) {
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    this.nativeGeocoder.reverseGeocode(lat, lng, options)
      .then((result: NativeGeocoderResult[]) => {
        // debugger
        console.log("reverse geocode result: " + JSON.stringify(result[0]));
        this.convertResult(result[0]);
      })
      .catch((error: any) => console.log(error));
  }

  convertResult(result) {
    var a = result.thoroughfare ? result.thoroughfare : null;
    var b = result.subThoroughfare ? result.subThoroughfare : null;
    var c = result.subLocality ? result.subLocality : null;
    var d = result.subAdministrativeArea ? result.subAdministrativeArea : null;
    var e = result.postalCode ? result.postalCode : null;
    var f = result.locality ? result.locality : null;
    var g = result.countryName ? result.countryName : null;
    var h = result.administrativeArea ? result.administrativeArea : null;
    let str = '';
    if (a != null && a != 'Unnamed Road')
      str = a + ', ';
    if (b != null && b != 'Unnamed Road')
      str = str + b + ', ';
    if (c != null && c != 'Unnamed Road')
      str = str + c + ', ';
    if (d != null && d != 'Unnamed Road')
      str = str + d + ', ';
    if (e != null && e != 'Unnamed Road')
      str = str + e + ', ';
    if (f != null && f != 'Unnamed Road')
      str = str + f + ', ';
    if (g != null && g != 'Unnamed Road')
      str = str + g + ', ';
    if (h != null && h != 'Unnamed Road')
      str = str + h + ', ';
    console.log("address string: ", str);
    this.autocomplete.query = str;
  }

  drawGeofence() {
    // this.cityCircle = undefined;

    if (this.map != undefined) {
      this.map.remove();
    }
    this.map = GoogleMaps.create('mapGeofence');
    LocationService.getMyLocation().then((result: MyLocation) => {
      console.log("my location: ", result)
      this.reverseGeocode(Number(result.latLng.lat), Number(result.latLng.lng));
      this.map.animateCamera({
        target: { lat: result.latLng.lat, lng: result.latLng.lng },
        zoom: 15,
        // tilt: 60,
        // bearing: 140,
        duration: 1000,
        padding: 0  // default = 20px
      }).then(() => {
        // alert("Camera target has been changed");
      });
      let markTemp: Marker = this.map.addMarkerSync({
        title: ["Current your location:\n",
          "latitude:" + result.latLng.lat.toFixed(3),
          "longitude:" + result.latLng.lng.toFixed(3),
          "speed:" + result.speed,
          "time:" + result.time].join("\n"),
        position: result.latLng,
        icon: {
          url: '../assets/Images/321.png',
          size: {
            width: 100,
            height: 100
          }
        },
      });
      this.markObj = markTemp;
      // let that = this;
      let ltln: ILatLng = { "lat": result.latLng.lat, "lng": result.latLng.lng };
      let circle: Circle = this.map.addCircleSync({
        'center': ltln,
        'radius': this.fence,
        'strokeColor': '#4dc4ec',
        'strokeWidth': 2,
        'fillColor': 'rgb(77, 196, 236, 0.5)'
      });

      this.cityCircle = circle;
    });
  }
  callThis(fence) {
    let that = this;
    debugger
    if (that.cityCircle) {
      that.cityCircle.setRadius(fence);
    }
    // if (that.cityCircle != undefined) {
    //   that.cityCircle.setRadius(fence);
    // }
  }

  saveGeofence() {
    let that = this;
    if (this.ppname && that.cityCircle != undefined) {
      debugger
      var key;
      if (that.key === 'grocery store') {
        key = "grocery_store";
      }
      var payload = {
        "poi": [
          {
            "location": {
              "type": "Point",
              "coordinates": [
                that.cityCircle.getCenter().lng,
                that.cityCircle.getCenter().lat
              ]
            },
            "poiname": that.ppname,
            "poi_type": key ? key : that.key,
            "status": "Active",
            "user": that.islogin._id,
            "address": that.autocomplete.query ? that.autocomplete.query : 'N/A',
            "radius": that.cityCircle.getRadius()
          }
        ]
      };
      this.loadingController.create({
        message: 'please wait...'
      }).then((loadEl) => {
        loadEl.present();
        var url = this.apiCall.mainUrl + "poi/addpoi";
        this.apiCall.urlWithdata(url, payload)
          .subscribe(data => {
            console.log("response adding poi: ", data)
            loadEl.dismiss();
            this.navCtrl.pop();
            this.toastCtrl.create({
              message: "Geofence added successfully!",
              duration: 1500,
              position: 'top'
            }).then((toastEl) => {
              toastEl.present();
            });
          },
            err => {
              loadEl.dismiss();
              console.log("error adding poi: ", err);

            })
      })

    }
  }

}
