import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { LoadingController, ToastController, AlertController } from '@ionic/angular';
// import { AppService } from '../app.service';
import { NgForm } from '@angular/forms';
// import { AuthenticationService } from '../auth.service';
// import { Storage } from '@ionic/storage';
import { AuthenticationService } from '../auth.service';
// const TOKEN_KEY = 'auth-token';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public email: any;
  public password: any;
  public data: any = {};

  constructor(
    private router: Router,
    // private loadingController: LoadingController,
    // private appService: AppService,
    // private toastController: ToastController,
    // private alertContoller: AlertController,
    // private storage: Storage,
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
  }

  goToSignup() {
    this.router.navigateByUrl('/signup');
  }

  onSubmit(form: NgForm) {
    // debugger
    if (!form.valid) {
      return;
    }
    // userlogin() {
    // this.submitAttempt = true;
    this.email = form.value.email;
    this.password = form.value.password;
    if (this.email == "") {
      /*alert("invalid");*/
      return false;
    }
    else if (this.password == "") {
      /*alert("invalid");*/
      return false;
    }

    function validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }

    var isEmail = validateEmail(this.email);
    var isName = isNaN(this.email);
    var strNum;


    if (isName == false) {
      strNum = this.email.trim();
    }

    if (isEmail == false && isName == false && strNum.length == 10) {

      this.data = {

        "psd": this.password,
        "ph_num": this.email
      };
    }
    else if (isEmail) {

      this.data = {

        "psd": this.password,
        "emailid": this.email
      };
    }
    else {

      this.data = {
        "psd": this.password,
        "user_id": this.email
      };
    }

    this.authService.login(this.data);

    // this.loadingController.create({
    //   message: 'Please wait..logging you in...',
    // }).then((loadEl => {
    //   loadEl.present();
    //   this.appService.authLogin(this.data)
    //     .subscribe(response => {
    //       // this.authService.login();
    //       this.storage.set(TOKEN_KEY, 'Bearer 1234567').then(() => {
    //         this.authenticationState.next(true);
    //       });
    //       var logindetails = JSON.parse(JSON.stringify(response));
    //       // this.logindata = JSON.stringify(response);
    //       // var logindetails = JSON.parse(this.logindata);
    //       var userDetails = window.atob(logindetails.token.split('.')[1]);
    //       var details = JSON.parse(userDetails);
    //       console.log(details.email);
    //       localStorage.setItem("loginflag", "loginflag");
    //       localStorage.setItem('details', JSON.stringify(details));
    //       // localStorage.setItem('condition_chk', details.isDealer);
    //       // localStorage.setItem('VeryFirstLoginUser', details._id)
    //       // localStorage.setItem("INTRO", "INTRO")

    //       loadEl.dismiss();

    //       this.toastController.create({
    //         message: "Welcome! You're logged In successfully.",
    //         duration: 3000,
    //         position: 'bottom'
    //       }).then((toastEl => {
    //         toastEl.present()
    //       }));

    //       // this.router.navigateByUrl('/home');
    //       this.router.navigate(['home']);
    //     },
    //       error => {
    //         // console.log("login error => "+error)
    //         var body = error._body;
    //         var msg = JSON.parse(body);
    //         if (msg.message == "Mobile Phone Not Verified") {
    //           this.alertContoller.create({
    //             header: 'Login failed!',
    //             message: msg.message = !msg.message ? '  Do you Want to verify it?' : msg.message + '  Do you Want to verify it?',
    //             buttons: [
    //               { text: 'Cancel' },
    //               {
    //                 text: 'Okay',
    //                 handler: () => {
    //                   // this.navCtrl.push(MobileVerify);
    //                 }
    //               }
    //             ]
    //           }).then((alertEl => {
    //             alertEl.present();
    //           }));
    //         } else {
    //           this.alertContoller.create({
    //             header: 'Login failed!',
    //             message: msg.message,
    //             buttons: ['Okay']
    //           }).then((alertEl => {
    //             alertEl.present();
    //           }))
    //         }
    //         loadEl.dismiss();
    //       });
    // }));

    // }
  }

}
