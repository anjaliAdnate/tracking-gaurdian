import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  // stopLoading() {
  //   throw new Error("Method not implemented.");
  // }
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  public mainUrl: string = 'https://www.oneqlik.in/';

  constructor(
    private http: HttpClient
  ) { }

  authLogin(data) {
    return this.http.post(this.mainUrl + 'users/LoginWithOtp', data, { headers: this.headers })
      .pipe();
  }
  getdevicesForAllVehiclesApi(link) {
    return this.http.get(link, { headers: this.headers })
      .pipe();
  }

  imageupload(img) {
    return this.http.post(this.mainUrl + "/users/uploadProfilePicture", img)
      .pipe();
  }

  getAddress(cord) {
    return this.http.post(this.mainUrl + "googleAddress/getGoogleAddress", cord, { headers: this.headers })
      .pipe();
  }

  urlWithdata(url, data) {
    return this.http.post(url, data, { headers: this.headers })
      .pipe();
  }
  getDistanceSpeedCall(device_id, from, to) {
    return this.http.get('https://www.oneqlik.in/gps/getDistanceSpeed?imei=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
      .pipe();
  }

  gpsCall(device_id, from, to) {
    return this.http.get('https://www.oneqlik.in/gps/v2?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
      .pipe();
  }
  pushnotifyCall(pushdata) {
    return this.http.post("https://www.oneqlik.in/users/PushNotification", pushdata, { headers: this.headers })
      .pipe();
  }

  getGeofenceCall(_id) {
    return this.http.get('https://www.oneqlik.in/geofencing/getgeofence?uid=' + _id, { headers: this.headers })
      .pipe();
  }
}
