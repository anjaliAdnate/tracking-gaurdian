import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-add-sos',
  templateUrl: './add-sos.page.html',
  styleUrls: ['./add-sos.page.scss'],
})
export class AddSosPage implements OnInit {
  userdetails: any;
  someKey: boolean;
  anArray: any = [];
  user_details: any;

  constructor(
    private apiCall: AppService,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private toastController: ToastController
  ) {
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getUserDetails();
  }

  getUserDetails() {
    var url = this.apiCall.mainUrl + "users/getuserDetail?uid=" + this.userdetails._id;
    this.loadingController.create({
      message: 'Please wait...'
    }).then((loadEl) => {
      loadEl.present();
      this.apiCall.getdevicesForAllVehiclesApi(url)
      .subscribe(respData => {
        loadEl.dismiss();
        if (respData) {
          var res = JSON.parse(JSON.stringify(respData));
          console.log("check user details: ", res);
          this.user_details = res;
        }
      },
        err => {
          loadEl.dismiss();
          console.log("error: ", err);
        });
    })
   
  }

  addMoreNumbers() {

  }

  Add() {
    this.anArray.push({ 'value': '' });
  }

  submit() {
    console.log('this.anArray', this.anArray);
    for (var i = 0; i < this.anArray.length; i++) {
      var val = this.anArray[i].value;
      if (/^\d{10}$/.test(val)) {
        this.user_details.alert.sos.phones.push(this.anArray[i].value);
        console.log(this.user_details.alert);
      } else {
        if (this.anArray.length === (i + 1)) {
          if (this.anArray[i].value !== "") {
            this.alertController.create({
              message: 'Invalid numbers, must be ten digits',
              buttons: ['Okay']
            }).then(alertEl => {
              alertEl.present();
            });
          }
        }
      }
    }
    var url = this.apiCall.mainUrl + "users/editUserDetails";
    var payload = {
      contactid: this.userdetails._id,
      alert: this.user_details.alert
    }
    this.loadingController.create({
      message: 'Please wait...'
    }).then((loadEl) => {
      loadEl.present();
      this.apiCall.urlWithdata(url, payload)
        .subscribe((respData) => {
          loadEl.dismiss();
          if (respData) {
            var res = JSON.parse(JSON.stringify(res));
            console.log("check res edited user details: ", res);
            if (res.message) {
              if(res.message === 'Saved')
              this.toastController.create({
                message: "Congratulations..!! Emergency contacts saved successfully.",
                duration: 1500,
                position: 'middle'
              }).then((toastEl) => {
                toastEl.present();
              });
              this.anArray = [];
            }
          }
        },
          err => {
            loadEl.dismiss();
            console.log(err);
          });
    });
  }
}
