import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSosPage } from './add-sos.page';

describe('AddSosPage', () => {
  let component: AddSosPage;
  let fixture: ComponentFixture<AddSosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
