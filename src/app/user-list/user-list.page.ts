import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.page.html',
  styleUrls: ['./user-list.page.scss'],
})
export class UserListPage implements OnInit {
  userdetails: any;
  userItems: any = [];
  // role: string;

  constructor(
    private apiCall: AppService,
    private loadinController: LoadingController,
    private toastController: ToastController,
    private router: Router,
    private dataService: DataService
  ) {
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
  }

  ngOnInit() {
    // this.getChatHistory();
  }

  ionViewDidEnter() {
    this.getUserList();
  }

  gotoChatWindow(item) {
    this.dataService.setData(42, item);
    this.router.navigateByUrl('/chat/42');
    // this.router.navigate(['/chat', navigationExtras]);
  }

  getUserList() {
    var url;
    if (this.userdetails.isDealer !== false || this.userdetails.isSuperAdmin !== false) {
      // this.role = "member";
      url = this.apiCall.mainUrl + "users/getAllUsers?dealer=" + this.userdetails._id;
    } else {
      // if(this.userdetails.isDealer) {
      // this.role = "admin";
      url = this.apiCall.mainUrl + "users/getAllUsers?user=" + this.userdetails._id;
      // }
    }
    this.loadinController.create({
      message: 'loading users..please wait...'
    }).then((loadEl) => {
      loadEl.present();
      this.apiCall.getdevicesForAllVehiclesApi(url)
        .subscribe(respData => {
          loadEl.dismiss();
          if (respData) {
            var res = JSON.parse(JSON.stringify(respData));
            console.log("respData: ", res);
            this.userItems = res;
          }
        },
          err => {
            console.log(err);
            loadEl.dismiss();
            if (err.error.message) {
              this.toastController.create({
                message: err.error.message,
                duration: 3000,
                position: 'bottom'
              }).then((toastEl) => {
                toastEl.present();
              })
            }
          });
    })

  }

  inviteMem() {
    this.router.navigateByUrl('/add-member-filled');
  }
  // getChatHistory() {
  //   let that = this;
  //   // this.loaderController.create({
  //   //   message: 'please wait we are fetching history...'
  //   // }).then((loadEl) => {
  //   // loadEl.present();
  //   var url = this.apiCall.mainUrl + "broadcastNotification/getchatmsg?from=" + this.userdetails._id + "&to=" + that.paramData._id;
  //   this.apiCall.getdevicesForAllVehiclesApi(url)
  //     .subscribe(respData => {
  //       // loadEl.dismiss();
  //       if (respData) {
  //         var res = JSON.parse(JSON.stringify(respData));
  //         for (var i = 0; i < res.length; i++) {
           
  //         }
  //         // this.scrollDown();
  //       }
  //     },
  //       err => {
  //         // loadEl.dismiss();
  //         console.log("chat err: ", err)
  //       });
  //   // })

  // }


}
