import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../app.service';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-location-setup',
  templateUrl: './location-setup.page.html',
  styleUrls: ['./location-setup.page.scss'],
})
export class LocationSetupPage implements OnInit {
  userdetails: any;
  homeCount: number = 0;
  workCount: number = 0;
  groceryCount: number = 0;
  schoolCount: number = 0;
  gymCount: number = 0;
  poiData: any = [];

  allDataArray: any = [
    {
      key: "home",
      title: "Add your home",
      subTitle: "add poi",
      count: this.homeCount,
      imageSrc: "assets/Images/home.png",

    },
    {
      key: "work",
      title: "Add your work",
      subTitle: "add poi",
      count: this.workCount,
      imageSrc: "assets/Images/suitcase.png",

    },
    {
      key: "school",
      title: "Add your school",
      subTitle: "add poi",
      count: this.schoolCount,
      imageSrc: "assets/Images/school.png",

    },
    {
      key: "gym",
      title: "Add your gym",
      subTitle: "add poi",
      count: this.gymCount,
      imageSrc: "assets/Images/weightlifting.png",

    },
    {
      key: "grocery_store",
      title: "Add your grocery store",
      subTitle: "add poi",
      count: this.groceryCount,
      imageSrc: "assets/Images/shop.png",

    }
  ];

  constructor(
    private router: Router,
    private apiCall: AppService,
    private alertCtrl: AlertController,
    private loadingController: LoadingController,
    private toastCtrl: ToastController
  ) {
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
  }

  ngOnInit() {
  }

  setLoaction(key) {
    this.router.navigateByUrl('/add-muliple/' + key);
  }

  ionViewDidEnter() {
    this.callGeofence();
  }

  callGeofence() {
    debugger
    this.homeCount = 0; this.workCount = 0; this.schoolCount = 0; this.gymCount = 0; this.groceryCount = 0;
    var url = this.apiCall.mainUrl + "poi/getPois?user=" + this.userdetails._id;
    // this.geoShape = [];
    // this.apiCall.getGeofenceCall(this.userdetails._id)
    this.apiCall.getdevicesForAllVehiclesApi(url)
      .subscribe(respData => {
        var data = JSON.parse(JSON.stringify(respData));
        console.log("geofence data=> " + data)
        if (data.length > 0) {

          this.poiData = data;
          // this.innerFunc(data);
          for (var i = 0; i < data.length; i++) {
            if (data[i].poi.poi_type === 'home') {
              this.homeCount += 1;
            } else if (data[i].poi.poi_type === 'work') {
              this.workCount += 1;
            } else if (data[i].poi.poi_type === 'gym') {
              this.gymCount += 1;
            } else if (data[i].poi.poi_type === 'school') {
              this.schoolCount += 1;
            } else if (data[i].poi.poi_type === 'grocery_store') {
              this.groceryCount += 1;
            }
          }

          for (var v = 0; v < this.allDataArray.length; v++) {
            if (this.allDataArray[v].key === 'home') {
              this.allDataArray[v].count = this.homeCount;
            } else if (this.allDataArray[v].key === 'work') {
              this.allDataArray[v].count = this.workCount;
            } else if (this.allDataArray[v].key === 'gym') {
              this.allDataArray[v].count = this.gymCount;
            } else if (this.allDataArray[v].key === 'school') {
              this.allDataArray[v].count = this.schoolCount;
            } else if (this.allDataArray[v].key === 'grocery_store') {
              this.allDataArray[v].count = this.groceryCount;
            }
          }
        } else {
          // this.toastCtrl.create({
          //   message: 'Geofence not found..!!',
          //   position: 'bottom',
          //   duration: 1500
          // }).then(toastEl => {
          //   toastEl.present();
          // })
        }
      },
        err => {
          console.log(err)
        });

  }

  deletePOI(key) {
    debugger
    // deletePOI(data) {
    var data;
    console.log("delete data: ", this.poiData[key])
    for (var r = 0; r < this.poiData.length; r++) {
      if (this.poiData[r].poi.poi_type === key) {
        data = this.poiData[r];
      }
    }
    this.alertCtrl.create({
      message: 'Do you want to delete this POI?',
      buttons: [{
        text: 'Cancel'
      }, {
        text: 'YES PROCEED',
        handler: () => {
          this.loadingController.create({
            message: 'please wait.. we are deleting POI..'
          }).then((loadEl) => {
            loadEl.present();
            this.deleteCall(loadEl, data);
          })

        }
      }]
    }).then((alertEl) => {
      alertEl.present();
    })
    // alert.present();

  }

  deleteCall(loadEl, data) {
    debugger
    var url = this.apiCall.mainUrl + "poi/deletePoi?_id=" + data._id
    this.apiCall.getdevicesForAllVehiclesApi(url)
      .subscribe(data => {
        loadEl.dismiss();
        if (data) {
          var res = JSON.parse(JSON.stringify(data));
          console.log("deleted: " + res);
        }
        this.toastCtrl.create({
          message: 'deleted successfully.',
          position: "middle",
          duration: 1500
        }).then((toastEl) => {
          toastEl.present();
        })
        this.callGeofence();
      },
        err => {
          loadEl.dismiss();
          console.log("error occured while deleting POI: ", err)
          this.toastCtrl.create({
            message: 'deleted successfully.',
            position: "middle",
            duration: 1500
          }).then((toastEl) => {
            toastEl.present();
          })
          this.callGeofence();
        })
  }

  // innerFunc(data) {
  //   var i = 0, howManyTimes = data.length;
  //   let that = this;
  //   function f() {
  //     that.geoShape.push({
  //       "poiname": data[i].poi.poiname,
  //       "radius": data[i].radius ? data[i].radius : 'N/A',
  //       "_id": data[i]._id,
  //       "start_location": {
  //         'lat': data[i].poi.location.coordinates[1],
  //         'long': data[i].poi.location.coordinates[0]
  //       },
  //       "address": data[i].poi.address ? data[i].poi.address : 'N/A'
  //     });
  //     let ltln: ILatLng = { "lat": that.geoShape[i].start_location.lat, "lng": that.geoShape[i].start_location.long };
  //     let circle: Circle = that.map.addCircleSync({
  //       'center': ltln,
  //       'radius': that.geoShape[i].radius,
  //       'strokeColor': '#4dc4ec',
  //       'strokeWidth': 2,
  //       'fillColor': 'rgb(77, 196, 236, 0.5)'
  //     });

  //     that.cityCircle = circle;
  //     i++;
  //     if (i < howManyTimes) {
  //       setTimeout(f, 200);
  //     }
  //   } f();
  // }

}
