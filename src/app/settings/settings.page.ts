import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  index: number = 0;
  listItems: any = [
    {
      id: this.index + 1,
      title: "Push Notification",
      icon: "assets/svg/notif.svg",
      btnType: "toggle"
    },
    {
      id: this.index + 1,
      title: "Vibration Alarm",
      icon: "assets/svg/vibrate.svg",
      btnType: "toggle"
    },
    {
      id: this.index + 1,
      title: "Led",
      icon: "assets/svg/led.svg",
      btnType: "toggle"
    },
    {
      id: this.index + 1,
      title: "Speaker Setting",
      icon: "assets/svg/speaker.svg",
      btnType: "toggle"
    },
    {
      id: this.index + 1,
      title: "Voice Record to App",
      icon: "assets/svg/recorder.svg",
      btnType: "toggle"
    },
    {
      id: this.index + 1,
      title: "Voice Record to TF",
      icon: "assets/svg/recorder.svg",
      btnType: "toggle"
    },
    {
      id: this.index + 1,
      title: "Sound Callback",
      icon: "assets/svg/soundcall.svg",
      btnType: "toggle"
    },
    {
      id: this.index + 1,
      title: "Shutdown",
      icon: "assets/svg/shutdown.svg",
      btnType: "button"
    },
    {
      id: this.index + 1,
      title: "Reboot",
      icon: "assets/svg/reboot.svg",
      btnType: "buttons"
    }
  ];
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  onTroubleshoot() {
    this.router.navigateByUrl('/troubleshooting');
  }

}
