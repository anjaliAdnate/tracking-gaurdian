import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.page.html',
  styleUrls: ['./device-list.page.scss'],
})
export class DeviceListPage implements OnInit {
  userdetails: any;
  deviceList: any = [];

  constructor(
    private apiCall: AppService,
    private loadingController: LoadingController,
    private router: Router
  ) {
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.userDevices();
  }

  userDevices() {
    var baseURLp;
    let that = this;
    baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.userdetails._id + '&email=' + this.userdetails.email;

    if (this.userdetails.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.userdetails._id;
      // this.isSuperAdmin = true;
    } else {
      if (this.userdetails.isDealer == true) {
        baseURLp += '&dealer=' + this.userdetails._id;
        // this.isDealer = true;
      }
    }

    that.loadingController.create({
      message: 'Please wait...'
    }).then((loadEl => {
      loadEl.present();
      that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
        .subscribe(respData => {
          loadEl.dismiss();
          var resp = JSON.parse(JSON.stringify(respData));
          this.deviceList = resp.devices;
        },
          err => {
            loadEl.dismiss();
            // that.apiCall.stopLoading();
            console.log(err);
          });
    }))

  }

  addDevice() {
    // this.router.navigateByUrl('/device-list');
    this.router.navigateByUrl('/add-device')
  }

}
