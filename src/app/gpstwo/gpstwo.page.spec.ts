import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpstwoPage } from './gpstwo.page';

describe('GpstwoPage', () => {
  let component: GpstwoPage;
  let fixture: ComponentFixture<GpstwoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpstwoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpstwoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
