import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { DataResolverService } from './resolver/data-resolver.service';
import { WalkthroughGuard } from './gaurds/walkthrough.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  // { path: '', redirectTo: 'walkthrough', pathMatch: 'full' },
  // { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomePageModule) },
  {
    path: 'login',
    loadChildren: './login/login.module#LoginPageModule',
    canActivate: [WalkthroughGuard]

  },
  {
    path: 'home',
    canActivate: [AuthGuard],
    loadChildren: './home/home.module#HomePageModule'
  },
  { path: 'add-member-filled', loadChildren: './add-member-filled/add-member-filled.module#AddMemberFilledPageModule' },
  { path: 'settings', loadChildren: './settings/settings.module#SettingsPageModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'location-setup', loadChildren: './location-setup/location-setup.module#LocationSetupPageModule' },
  { path: 'troubleshooting', loadChildren: './troubleshooting/troubleshooting.module#TroubleshootingPageModule' },
  { path: 'gpsone', loadChildren: './gpsone/gpsone.module#GpsonePageModule' },
  { path: 'gpstwo', loadChildren: './gpstwo/gpstwo.module#GpstwoPageModule' },
  { path: 'add-muliple/:key', loadChildren: './add-muliple/add-muliple.module#AddMuliplePageModule' },
  { path: 'chat', loadChildren: './chat/chat.module#ChatPageModule' },
  // { path: 'chat/:param', loadChildren: './chat/chat.module#ChatPageModule' },
  {
    path: 'chat/:id',
    resolve: {
      special: DataResolverService
    },
    loadChildren: './chat/chat.module#ChatPageModule'
  },
  { path: 'history', loadChildren: './history/history.module#HistoryPageModule' },
  { path: 'prof', loadChildren: './profile/prof/prof.module#ProfPageModule' },
  { path: 'notif', loadChildren: './profile/notif/notif.module#NotifPageModule' },
  { path: 'add-device', loadChildren: './add-device/add-device.module#AddDevicePageModule' },
  { path: 'user-list', loadChildren: './user-list/user-list.module#UserListPageModule' },
  { path: 'add-sos', loadChildren: './add-sos/add-sos.module#AddSosPageModule' },
  { path: 'device-list', loadChildren: './device-list/device-list.module#DeviceListPageModule' },
  { path: 'walkthrough', loadChildren: './walkthrough/walkthrough.module#WalkthroughPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  // { path: 'showcase', loadChildren: () => import('./showcase/showcase.module').then(m => m.ShowcasePageModule) },



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
