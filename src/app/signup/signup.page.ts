import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../app.service';
// import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  submitAttempt: boolean;
  usersignupdetails: any;
  email_add: any;
  cnfrm_passwrd: any;
  pass: any;
  mob_num: any;
  lName: any;
  Name: any;
  userId: any;

  constructor(
    private router: Router,
    private apiService: AppService,
    private toastCtrl: ToastController,
    private loadingController: LoadingController
  ) {
  }

  ngOnInit() {
  }

  type = "password";
  // type1 = "password";
  show = false;
  show1 = false;
  toggleShow(ev) {
    if (ev == 0) {
      this.show = !this.show;
      if (this.show) {
        this.type = "text";
      }
      else {
        this.type = "password";
      }
    } 
    // else {
    //   this.show1 = !this.show1;
    //   if (this.show1) {
    //     this.type1 = "text";
    //   }
    //   else {
    //     this.type1 = "password";
    //   }
    // }
  }

  gotoLogin() {
    this.router.navigateByUrl("/login");
  }
  doSignup() {
    function validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }
    function phonenumber(inputtxt) {
      if (/^\d{10}$/.test(inputtxt)) {
        return true;
      } else {
        return false
      }
    }
    // this.isdealer = false;
    this.submitAttempt = true;
    // if (this.signupForm.valid) {
    // this.usersignupdetails = this.signupForm.value;
    // localStorage.setItem('usersignupdetails', this.usersignupdetails);
    // this.signupDetails = localStorage.getItem("usersignupdetails");
    // if (this.signupForm.value.cnfrm_passwrd && this.signupForm.value.email_add && this.signupForm.value.mob_num && this.signupForm.value.Name && this.signupForm.value.pass) {
    if (this.userId && this.Name && this.pass) {
      if (this.email_add !== null) {

        var isEmail = validateEmail(this.email_add);
        if (!isEmail) {
          this.toastCtrl.create({
            message: 'Please enter valid email address and try again',
            duration: 1800,
            position: 'top'
          }).then((toastEl) => {
            toastEl.present();
          });
          return;
        }
      }
      if (this.mob_num !== null) {
        var isMobNo = phonenumber(this.mob_num);
        if (!isMobNo) {
          this.toastCtrl.create({
            message: 'Please enter valid mobile number, it should be 10 digits.',
            duration: 1800,
            position: 'top'
          }).then((toastEl) => {
            toastEl.present();
          });
          return;
        }
      }

      var usersignupdata = {
        "first_name": this.Name,
        "last_name": this.lName,
        "password": (this.pass ? this.pass : '123'),
        "org_name": null,
        "email": (this.email_add ? this.email_add : null),
        "phone": (this.mob_num ? this.mob_num : null),
        // "expdate": "2021-04-02T06:56:41.356Z",
        "supAdmin": "59cbbdbe508f164aa2fef3d8",
        "isDealer": false,
        "custumer": true,
        "user_id": this.userId,
        "imageDoc": []
      }
      this.loadingController.create({
        message: 'Please wait.. we are signing you up...'
      }).then((loadEl) => {
        loadEl.present();
        this.innerFunc(loadEl, usersignupdata);
      })
    } else {
      this.toastCtrl.create({
        message: 'Required field should not be empty..!',
        duration: 2000,
        position: 'middle'
      }).then((toastEl) => {
        toastEl.present();
      });
      return;
    }
  }

  innerFunc(loadEl, usersignupdata) {
    var url = this.apiService.mainUrl + "users/signUp";
    this.apiService.urlWithdata(url, usersignupdata)
      .subscribe(response => {
        loadEl.dismiss();
        if (response) {
          var res = JSON.parse(JSON.stringify(response));
          console.log("signup res: ", res);
          if (usersignupdata.phone !== null) {
            // var phone = usersignupdata.phone;
            // localStorage.setItem("mobnum", phone)

            // // this.signupUseradd = response;

          }

          this.toastCtrl.create({
            message: res.message,
            duration: 3000,
            position: 'top'
          }).then((toastEl) => {
            toastEl.present();
          });

          this.mob_num = undefined;
          this.Name = undefined;
          this.lName = undefined;
          this.pass = undefined;
          this.cnfrm_passwrd = undefined;
          this.userId = undefined;
          this.email_add = undefined;

          // toast.onDidDismiss(() => {
          //   if (response.message === 'Email ID or Mobile Number already exists') {
          //     this.navCtrl.push("LoginPage");
          //   } else if (response.message === "OTP sent successfully") {
          //     this.navCtrl.push('SignupOtpPage');
          //   } else if (response.message === "Registered Sucessfully") {
          //     this.navCtrl.push("LoginPage");
          //   }
          // });
          // toast.present();
        }
      },
        err => {
          loadEl.dismiss();
          console.log(err)
          this.toastCtrl.create({
            message: "Something went wrong. Please check your net connection..",
            duration: 2000,
            position: "top"
          }).then((toastEl) => {
            toastEl.present();
          })
        })
  }

}
