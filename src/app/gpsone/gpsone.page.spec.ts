import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpsonePage } from './gpsone.page';

describe('GpsonePage', () => {
  let component: GpsonePage;
  let fixture: ComponentFixture<GpsonePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpsonePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpsonePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
