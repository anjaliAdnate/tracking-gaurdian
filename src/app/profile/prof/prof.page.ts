import { Component, OnInit } from '@angular/core';
import { Crop } from '@ionic-native/crop/ngx';
// import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { AppService } from 'src/app/app.service';
import { LoadingController, ActionSheetController, ToastController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-prof',
  templateUrl: './prof.page.html',
  styleUrls: ['./prof.page.scss'],
})
export class ProfPage implements OnInit {
  fileUrl: any = null;
  respData: any;
  userdetails: any;
  selectedFile: File;
  constructor(
    // private imagePicker: ImagePicker,
    private crop: Crop,
    private transfer: FileTransfer,
    private apiCall: AppService,
    private loadingController: LoadingController,
    private camera: Camera,
    public actionSheetController: ActionSheetController,
    // private file: File,
    private taostCtrl: ToastController
  ) {
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    // console.log("user details=> " + JSON.stringify(this.userdetails));
    this.getImgUrl();
  }

  ngOnInit() {
  }

  pickImage(sourceType) {
    var url = this.apiCall.mainUrl + "users/uploadProfilePicture";

    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      // let base64Image = 'data:image/jpeg;base64,' + imageData;
      console.log("imageData: ", imageData)
      // debugger
      this.crop.crop(imageData, { quality: 100 })
        .then(
          newImage => {
            var dlink123 = newImage.split('?');
            var wear = dlink123[0];
            const fileTransfer: FileTransferObject = this.transfer.create();
            const uploadOpts: FileUploadOptions = {
              fileKey: 'photo',
              // fileName: imageData.substr(imageData.lastIndexOf('/') + 1)
              fileName: wear.substr(wear.lastIndexOf('/') + 1)
            };
            this.loadingController.create({
              message: 'Please wait....',
            }).then((loadEl) => {
              loadEl.present();
              fileTransfer.upload(wear, url, uploadOpts)
                .then((data) => {
                  loadEl.dismiss();
                  console.log(data);
                  // this.selectedFile = <File>event.target.files[0];
                  this.respData = JSON.parse(JSON.stringify(data)).response;
                  console.log("image data response: ", this.respData);
                  this.dlUpdate(this.respData);
                  // this.fileUrl = this.respData.fileUrl;
                  // this.fileUrl = this.respData;
                }, (err) => {
                  loadEl.dismiss();
                  console.log(err);
                  this.taostCtrl.create({
                    message: 'Something went wrong while uploading file... Please try after some time..',
                    duration: 2000,
                    position: 'bottom'
                  }).then((toastEl) => {
                    toastEl.present();
                  });
                });
            })

          })
    }, (err) => {
      console.log("imageData err: ", err)
      // Handle error
    });
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          // this.openGallery();
          this.pickImage(this.camera.PictureSourceType.SAVEDPHOTOALBUM);

        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  dlUpdate(dllink) {
    // debugger
    // let that = this;
    var dlink123 = dllink.split('?');
    var wear = dlink123[0];
    console.log("new download link: ", wear);
    var _burl = this.apiCall.mainUrl + "users/updateImagePath";
    var payload = {
      imageDoc: [dllink],
      _id: this.userdetails._id
    }
    this.loadingController.create({
      message: 'Uploading image...'
    }).then((loadEl) => {
      loadEl.present();
      this.apiCall.urlWithdata(_burl, payload)
        .subscribe(respData => {
          loadEl.dismiss();
          console.log("check profile upload: ", respData)
          this.getImgUrl();
        },
          err => {
            loadEl.dismiss();
          });
    });
  }

  getImgUrl() {
    var url = this.apiCall.mainUrl + "users/shareProfileImage?uid=" + this.userdetails._id;
    this.loadingController.create({
      message: 'Loading image...'
    }).then((loadEl => {
      loadEl.present();
      this.apiCall.getdevicesForAllVehiclesApi(url)
        .subscribe(resp => {
          loadEl.dismiss();
          console.log("server image url=> ", resp);
          if (JSON.parse(JSON.stringify(resp)).imageDoc.length > 0) {
            var imgUrl = JSON.parse(JSON.stringify(resp)).imageDoc[0];
            var str1 = imgUrl.split('public/');
            this.fileUrl = "http://13.126.36.205/" + str1[1];
          }
        },
          err => {
            loadEl.dismiss();
          })
    }));
  }
}
