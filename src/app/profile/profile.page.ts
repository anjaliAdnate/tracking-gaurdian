import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, Platform, LoadingController } from '@ionic/angular';
import { AuthenticationService } from '../auth.service';
import { AppService } from '../app.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  userdetails: any;
  count: number = 0;

  constructor(
    private router: Router,
    private alertController: AlertController,
    private authenticationService: AuthenticationService,
    private platform: Platform,
    private apiCall: AppService,
    private storage: Storage
    // private loadingController: LoadingController
  ) {
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};

  }

  ngOnInit() { }

  ionViewDidEnter() {
    this.getUserList();
  }

  addMember() {
    this.router.navigateByUrl('/add-member-filled');
  }

  updateProfile() {
    this.router.navigate(['prof']);
  }

  showNotifs() {
    this.router.navigate(['notif']);
  }

  onLogout() {
    this.alertController.create({
      message: 'Do you want to logout from app?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.logout();
          }
        },
        {
          text: 'No'
        }
      ]
    }).then((alertEl => {
      alertEl.present();
    }))
  }

  logout() {
    var token = localStorage.getItem("DEVICE_TOKEN");
    var pushdata = {};
    if (token !== null) {
      if (this.platform.is('android')) {
        pushdata = {
          "uid": this.userdetails._id,
          "token": token,
          "os": "android"
        }
      } else {
        pushdata = {
          "uid": this.userdetails._id,
          "token": token,
          "os": "ios"
        }
      }

      this.removePushToken(pushdata);
    }
    localStorage.clear();
    this.storage.clear();
    this.authenticationService.logout();
    this.authenticationService.authenticationState.subscribe(state => {
      if (state) {
        this.router.navigate(['home']);
      } else {
        this.router.navigate(['login']);
      }
    });
  }

  removePushToken(pushdata) {
    var url = this.apiCall.mainUrl + "/users/PullNotification";
    // this.loadingController.create({
    //   message: 'Logging out...'
    // }).then((loadEl) => {
    // loadEl.present();
    this.apiCall.urlWithdata(url, pushdata)
      .subscribe(respData => {
        // loadEl.dismiss();
        if (respData) {
          var data = JSON.parse(JSON.stringify(respData));
          console.log("push notifications updated " + data.message)
          // localStorage.clear();
        }
      },
        err => {
          // loadEl.dismiss();
          console.log(err)
        });
    // });

  }

  addSOS() {
    this.router.navigateByUrl('/add-sos');
  }

  getUserList() {
    this.count = 0;
    var url;
    if (this.userdetails.isDealer !== false || this.userdetails.isSuperAdmin !== false) {
      url = this.apiCall.mainUrl + "users/getAllUsers?dealer=" + this.userdetails._id;
    } else {
      url = this.apiCall.mainUrl + "users/getAllUsers?user=" + this.userdetails._id;
      // }
    }
    // this.loadingController.create({
    //   message: 'loading users..please wait...'
    // }).then((loadEl) => {
    //   loadEl.present();

    this.apiCall.getdevicesForAllVehiclesApi(url)
      .subscribe(respData => {
        // loadEl.dismiss();
        if (respData) {
          var res = JSON.parse(JSON.stringify(respData));
          // console.log("respData: ", res);
          // this.userItems = res;
          for (var r = 0; r < res.length; r++) {
            this.count += 1;
          }
        }
      },
        err => {
          console.log(err);
          // loadEl.dismiss();
          if (err.error.message) {
            console.log(err.error.message);
          }
        });
    // })

  }

}
