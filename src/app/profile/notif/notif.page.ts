import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { LoadingController } from '@ionic/angular';
// import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-notif',
  templateUrl: './notif.page.html',
  styleUrls: ['./notif.page.scss'],
})
export class NotifPage implements OnInit {
  userdetails: any;
  items: any = [];
  // @ViewChild(IonInfiniteScroll, {
  //   static: true
  // }) infiniteScroll: IonInfiniteScroll;
  constructor(
    private apiCall: AppService,
    private loadingController: LoadingController
  ) {
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
  }

  ngOnInit() {
    this.getNotifications();
    // this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

  getNotifications() {
    this.page = 1;
    this.limit = 15;
    var url = this.apiCall.mainUrl + "notifs/getNotifiLimit?user=" + this.userdetails._id + "&pageNo=" + this.page + "&size=" + this.limit;
    this.loadingController.create({
      message: 'Loading notifications...'
    }).then(loadEl => {
      loadEl.present();
      this.apiCall.getdevicesForAllVehiclesApi(url)
        .subscribe(respData => {
          loadEl.dismiss();
          let variable = JSON.parse(JSON.stringify(respData));
          this.items = variable;
          console.log("notifs: ", this.items)
        },
          err => {
            loadEl.dismiss();
          });
    });
  }
  page: number = 1;
  limit: number = 15;
  loadData(event) {
    debugger
    // this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    let that = this;
    that.page = that.page + 1;
    var url = this.apiCall.mainUrl + "notifs/getNotifiLimit?user=" + this.userdetails._id + "&pageNo=" + that.page + "&size=" + that.limit;

    this.apiCall.getdevicesForAllVehiclesApi(url)
      .subscribe(respData => {
        let variable = JSON.parse(JSON.stringify(respData));
        for (var i = 0; i < variable.length; i++) {
          that.items.push(variable[i]);
        }
        // this.items = variable;
        console.log("notifs length: ", this.items.length)
        event.target.disabled = true;
        // this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
      },
        err => {
        });
    // setTimeout(() => {
    //   console.log('Done');
    //   event.target.complete();

    //   // App logic to determine if all data is loaded
    //   // and disable the infinite scroll
    //   if (data.length == 1000) {
    //     event.target.disabled = true;
    //   }
    // }, 500);
  }

  // toggleInfiniteScroll() {
  //   this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  // }

}
