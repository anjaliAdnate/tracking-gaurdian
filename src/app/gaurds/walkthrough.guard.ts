import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,

  CanActivate,
  Router
} from '@angular/router';
import { Storage } from '@ionic/storage';
// import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WalkthroughGuard implements CanActivate {
  constructor(private storage: Storage, private router: Router) {

  }
  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    debugger
    console.log("walkthorugh key: ", this.storage.get('walkthroughComplete'))
    const isComplete = await this.storage.get('walkthroughComplete')


    if (!isComplete) {
      this.router.navigateByUrl('/walkthrough');
    }
    return isComplete;
  }
}
