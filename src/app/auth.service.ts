import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Platform, AlertController, ToastController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AppService } from './app.service';
const TOKEN_KEY = 'auth-token';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {
  authenticationState = new BehaviorSubject(false);

  constructor(
    private storage: Storage,
    private plt: Platform,
    // private router: Router,
    private loadingController: LoadingController,
    private appService: AppService,
    private toastController: ToastController,
    private alertContoller: AlertController,
  ) {
    this.plt.ready().then(() => {
      this.checkToken();
    });
  }

  checkToken() {
    // this.storage.get(TOKEN_KEY).then(res => {
    //   if (res) {
    //     this.authenticationState.next(true);
    //   }
    // })
    if(localStorage.getItem("loginflag")) {
      this.authenticationState.next(true);
    }
  }

  login(data) {
    return this.storage.set(TOKEN_KEY, 'Bearer 1234567').then(() => {
      this.loadingController.create({
        message: 'Please wait..logging you in...',
      }).then((loadEl => {
        loadEl.present();
        this.appService.authLogin(data)
          .subscribe(response => {

            var logindetails = JSON.parse(JSON.stringify(response));
            var userDetails = window.atob(logindetails.token.split('.')[1]);
            var details = JSON.parse(userDetails);
            console.log(details.email);
            localStorage.setItem("loginflag", "loginflag");
            localStorage.setItem('details', JSON.stringify(details));

            loadEl.dismiss();

            this.toastController.create({
              message: "Welcome! You're logged In successfully.",
              duration: 3000,
              position: 'bottom'
            }).then((toastEl => {
              toastEl.present()
            }));
            this.authenticationState.next(true);
          },
            error => {
              loadEl.dismiss();
              if (error.error.message) {
                this.alertContoller.create({
                  header: 'Login failed!',
                  message: error.error.message,
                  buttons: ['Okay']
                }).then((alertEl) => {
                  alertEl.present();
                });
              } else {
                this.alertContoller.create({
                  header: 'Login failed!',
                  message: 'Something went wrong.. please try again later...',
                  buttons: ['Okay']
                }).then((alertEl => {
                  alertEl.present();
                }))
              }
            });
      }));

    });
  }

  logout() {
    return this.storage.remove(TOKEN_KEY).then(() => {
      this.authenticationState.next(false);
    });
  }

  isAuthenticated() {
    // return this.authenticationState.value;
    return this.authenticationState.value;

  }

}
//   authenticationState = new BehaviorSubject(false);

//   constructor(
//     private platform: Platform) {
//     this.platform.ready().then(async () => {
//       this.checkToken();
//     });
//   }

//   checkToken() {
//     if (localStorage.getItem('loginflag') || localStorage.getItem('loginflag') !== null) {
//       this.authenticationState.next(true);
//     }
//   }

//   login() {
//     this.authenticationState.next(true);
//   }

//   logout() {
//     localStorage.clear();
//     this.authenticationState.next(false);
//   }

//   isAuthenticated() {
//     return this.authenticationState.value;
//   }

// }