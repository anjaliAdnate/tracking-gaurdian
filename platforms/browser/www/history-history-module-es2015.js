(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["history-history-module"],{

/***/ "./node_modules/hammerjs/hammer.js":
/*!*****************************************!*\
  !*** ./node_modules/hammerjs/hammer.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;/*! Hammer.JS - v2.0.7 - 2016-04-22
 * http://hammerjs.github.io/
 *
 * Copyright (c) 2016 Jorik Tangelder;
 * Licensed under the MIT license */
(function(window, document, exportName, undefined) {
  'use strict';

var VENDOR_PREFIXES = ['', 'webkit', 'Moz', 'MS', 'ms', 'o'];
var TEST_ELEMENT = document.createElement('div');

var TYPE_FUNCTION = 'function';

var round = Math.round;
var abs = Math.abs;
var now = Date.now;

/**
 * set a timeout with a given scope
 * @param {Function} fn
 * @param {Number} timeout
 * @param {Object} context
 * @returns {number}
 */
function setTimeoutContext(fn, timeout, context) {
    return setTimeout(bindFn(fn, context), timeout);
}

/**
 * if the argument is an array, we want to execute the fn on each entry
 * if it aint an array we don't want to do a thing.
 * this is used by all the methods that accept a single and array argument.
 * @param {*|Array} arg
 * @param {String} fn
 * @param {Object} [context]
 * @returns {Boolean}
 */
function invokeArrayArg(arg, fn, context) {
    if (Array.isArray(arg)) {
        each(arg, context[fn], context);
        return true;
    }
    return false;
}

/**
 * walk objects and arrays
 * @param {Object} obj
 * @param {Function} iterator
 * @param {Object} context
 */
function each(obj, iterator, context) {
    var i;

    if (!obj) {
        return;
    }

    if (obj.forEach) {
        obj.forEach(iterator, context);
    } else if (obj.length !== undefined) {
        i = 0;
        while (i < obj.length) {
            iterator.call(context, obj[i], i, obj);
            i++;
        }
    } else {
        for (i in obj) {
            obj.hasOwnProperty(i) && iterator.call(context, obj[i], i, obj);
        }
    }
}

/**
 * wrap a method with a deprecation warning and stack trace
 * @param {Function} method
 * @param {String} name
 * @param {String} message
 * @returns {Function} A new function wrapping the supplied method.
 */
function deprecate(method, name, message) {
    var deprecationMessage = 'DEPRECATED METHOD: ' + name + '\n' + message + ' AT \n';
    return function() {
        var e = new Error('get-stack-trace');
        var stack = e && e.stack ? e.stack.replace(/^[^\(]+?[\n$]/gm, '')
            .replace(/^\s+at\s+/gm, '')
            .replace(/^Object.<anonymous>\s*\(/gm, '{anonymous}()@') : 'Unknown Stack Trace';

        var log = window.console && (window.console.warn || window.console.log);
        if (log) {
            log.call(window.console, deprecationMessage, stack);
        }
        return method.apply(this, arguments);
    };
}

/**
 * extend object.
 * means that properties in dest will be overwritten by the ones in src.
 * @param {Object} target
 * @param {...Object} objects_to_assign
 * @returns {Object} target
 */
var assign;
if (typeof Object.assign !== 'function') {
    assign = function assign(target) {
        if (target === undefined || target === null) {
            throw new TypeError('Cannot convert undefined or null to object');
        }

        var output = Object(target);
        for (var index = 1; index < arguments.length; index++) {
            var source = arguments[index];
            if (source !== undefined && source !== null) {
                for (var nextKey in source) {
                    if (source.hasOwnProperty(nextKey)) {
                        output[nextKey] = source[nextKey];
                    }
                }
            }
        }
        return output;
    };
} else {
    assign = Object.assign;
}

/**
 * extend object.
 * means that properties in dest will be overwritten by the ones in src.
 * @param {Object} dest
 * @param {Object} src
 * @param {Boolean} [merge=false]
 * @returns {Object} dest
 */
var extend = deprecate(function extend(dest, src, merge) {
    var keys = Object.keys(src);
    var i = 0;
    while (i < keys.length) {
        if (!merge || (merge && dest[keys[i]] === undefined)) {
            dest[keys[i]] = src[keys[i]];
        }
        i++;
    }
    return dest;
}, 'extend', 'Use `assign`.');

/**
 * merge the values from src in the dest.
 * means that properties that exist in dest will not be overwritten by src
 * @param {Object} dest
 * @param {Object} src
 * @returns {Object} dest
 */
var merge = deprecate(function merge(dest, src) {
    return extend(dest, src, true);
}, 'merge', 'Use `assign`.');

/**
 * simple class inheritance
 * @param {Function} child
 * @param {Function} base
 * @param {Object} [properties]
 */
function inherit(child, base, properties) {
    var baseP = base.prototype,
        childP;

    childP = child.prototype = Object.create(baseP);
    childP.constructor = child;
    childP._super = baseP;

    if (properties) {
        assign(childP, properties);
    }
}

/**
 * simple function bind
 * @param {Function} fn
 * @param {Object} context
 * @returns {Function}
 */
function bindFn(fn, context) {
    return function boundFn() {
        return fn.apply(context, arguments);
    };
}

/**
 * let a boolean value also be a function that must return a boolean
 * this first item in args will be used as the context
 * @param {Boolean|Function} val
 * @param {Array} [args]
 * @returns {Boolean}
 */
function boolOrFn(val, args) {
    if (typeof val == TYPE_FUNCTION) {
        return val.apply(args ? args[0] || undefined : undefined, args);
    }
    return val;
}

/**
 * use the val2 when val1 is undefined
 * @param {*} val1
 * @param {*} val2
 * @returns {*}
 */
function ifUndefined(val1, val2) {
    return (val1 === undefined) ? val2 : val1;
}

/**
 * addEventListener with multiple events at once
 * @param {EventTarget} target
 * @param {String} types
 * @param {Function} handler
 */
function addEventListeners(target, types, handler) {
    each(splitStr(types), function(type) {
        target.addEventListener(type, handler, false);
    });
}

/**
 * removeEventListener with multiple events at once
 * @param {EventTarget} target
 * @param {String} types
 * @param {Function} handler
 */
function removeEventListeners(target, types, handler) {
    each(splitStr(types), function(type) {
        target.removeEventListener(type, handler, false);
    });
}

/**
 * find if a node is in the given parent
 * @method hasParent
 * @param {HTMLElement} node
 * @param {HTMLElement} parent
 * @return {Boolean} found
 */
function hasParent(node, parent) {
    while (node) {
        if (node == parent) {
            return true;
        }
        node = node.parentNode;
    }
    return false;
}

/**
 * small indexOf wrapper
 * @param {String} str
 * @param {String} find
 * @returns {Boolean} found
 */
function inStr(str, find) {
    return str.indexOf(find) > -1;
}

/**
 * split string on whitespace
 * @param {String} str
 * @returns {Array} words
 */
function splitStr(str) {
    return str.trim().split(/\s+/g);
}

/**
 * find if a array contains the object using indexOf or a simple polyFill
 * @param {Array} src
 * @param {String} find
 * @param {String} [findByKey]
 * @return {Boolean|Number} false when not found, or the index
 */
function inArray(src, find, findByKey) {
    if (src.indexOf && !findByKey) {
        return src.indexOf(find);
    } else {
        var i = 0;
        while (i < src.length) {
            if ((findByKey && src[i][findByKey] == find) || (!findByKey && src[i] === find)) {
                return i;
            }
            i++;
        }
        return -1;
    }
}

/**
 * convert array-like objects to real arrays
 * @param {Object} obj
 * @returns {Array}
 */
function toArray(obj) {
    return Array.prototype.slice.call(obj, 0);
}

/**
 * unique array with objects based on a key (like 'id') or just by the array's value
 * @param {Array} src [{id:1},{id:2},{id:1}]
 * @param {String} [key]
 * @param {Boolean} [sort=False]
 * @returns {Array} [{id:1},{id:2}]
 */
function uniqueArray(src, key, sort) {
    var results = [];
    var values = [];
    var i = 0;

    while (i < src.length) {
        var val = key ? src[i][key] : src[i];
        if (inArray(values, val) < 0) {
            results.push(src[i]);
        }
        values[i] = val;
        i++;
    }

    if (sort) {
        if (!key) {
            results = results.sort();
        } else {
            results = results.sort(function sortUniqueArray(a, b) {
                return a[key] > b[key];
            });
        }
    }

    return results;
}

/**
 * get the prefixed property
 * @param {Object} obj
 * @param {String} property
 * @returns {String|Undefined} prefixed
 */
function prefixed(obj, property) {
    var prefix, prop;
    var camelProp = property[0].toUpperCase() + property.slice(1);

    var i = 0;
    while (i < VENDOR_PREFIXES.length) {
        prefix = VENDOR_PREFIXES[i];
        prop = (prefix) ? prefix + camelProp : property;

        if (prop in obj) {
            return prop;
        }
        i++;
    }
    return undefined;
}

/**
 * get a unique id
 * @returns {number} uniqueId
 */
var _uniqueId = 1;
function uniqueId() {
    return _uniqueId++;
}

/**
 * get the window object of an element
 * @param {HTMLElement} element
 * @returns {DocumentView|Window}
 */
function getWindowForElement(element) {
    var doc = element.ownerDocument || element;
    return (doc.defaultView || doc.parentWindow || window);
}

var MOBILE_REGEX = /mobile|tablet|ip(ad|hone|od)|android/i;

var SUPPORT_TOUCH = ('ontouchstart' in window);
var SUPPORT_POINTER_EVENTS = prefixed(window, 'PointerEvent') !== undefined;
var SUPPORT_ONLY_TOUCH = SUPPORT_TOUCH && MOBILE_REGEX.test(navigator.userAgent);

var INPUT_TYPE_TOUCH = 'touch';
var INPUT_TYPE_PEN = 'pen';
var INPUT_TYPE_MOUSE = 'mouse';
var INPUT_TYPE_KINECT = 'kinect';

var COMPUTE_INTERVAL = 25;

var INPUT_START = 1;
var INPUT_MOVE = 2;
var INPUT_END = 4;
var INPUT_CANCEL = 8;

var DIRECTION_NONE = 1;
var DIRECTION_LEFT = 2;
var DIRECTION_RIGHT = 4;
var DIRECTION_UP = 8;
var DIRECTION_DOWN = 16;

var DIRECTION_HORIZONTAL = DIRECTION_LEFT | DIRECTION_RIGHT;
var DIRECTION_VERTICAL = DIRECTION_UP | DIRECTION_DOWN;
var DIRECTION_ALL = DIRECTION_HORIZONTAL | DIRECTION_VERTICAL;

var PROPS_XY = ['x', 'y'];
var PROPS_CLIENT_XY = ['clientX', 'clientY'];

/**
 * create new input type manager
 * @param {Manager} manager
 * @param {Function} callback
 * @returns {Input}
 * @constructor
 */
function Input(manager, callback) {
    var self = this;
    this.manager = manager;
    this.callback = callback;
    this.element = manager.element;
    this.target = manager.options.inputTarget;

    // smaller wrapper around the handler, for the scope and the enabled state of the manager,
    // so when disabled the input events are completely bypassed.
    this.domHandler = function(ev) {
        if (boolOrFn(manager.options.enable, [manager])) {
            self.handler(ev);
        }
    };

    this.init();

}

Input.prototype = {
    /**
     * should handle the inputEvent data and trigger the callback
     * @virtual
     */
    handler: function() { },

    /**
     * bind the events
     */
    init: function() {
        this.evEl && addEventListeners(this.element, this.evEl, this.domHandler);
        this.evTarget && addEventListeners(this.target, this.evTarget, this.domHandler);
        this.evWin && addEventListeners(getWindowForElement(this.element), this.evWin, this.domHandler);
    },

    /**
     * unbind the events
     */
    destroy: function() {
        this.evEl && removeEventListeners(this.element, this.evEl, this.domHandler);
        this.evTarget && removeEventListeners(this.target, this.evTarget, this.domHandler);
        this.evWin && removeEventListeners(getWindowForElement(this.element), this.evWin, this.domHandler);
    }
};

/**
 * create new input type manager
 * called by the Manager constructor
 * @param {Hammer} manager
 * @returns {Input}
 */
function createInputInstance(manager) {
    var Type;
    var inputClass = manager.options.inputClass;

    if (inputClass) {
        Type = inputClass;
    } else if (SUPPORT_POINTER_EVENTS) {
        Type = PointerEventInput;
    } else if (SUPPORT_ONLY_TOUCH) {
        Type = TouchInput;
    } else if (!SUPPORT_TOUCH) {
        Type = MouseInput;
    } else {
        Type = TouchMouseInput;
    }
    return new (Type)(manager, inputHandler);
}

/**
 * handle input events
 * @param {Manager} manager
 * @param {String} eventType
 * @param {Object} input
 */
function inputHandler(manager, eventType, input) {
    var pointersLen = input.pointers.length;
    var changedPointersLen = input.changedPointers.length;
    var isFirst = (eventType & INPUT_START && (pointersLen - changedPointersLen === 0));
    var isFinal = (eventType & (INPUT_END | INPUT_CANCEL) && (pointersLen - changedPointersLen === 0));

    input.isFirst = !!isFirst;
    input.isFinal = !!isFinal;

    if (isFirst) {
        manager.session = {};
    }

    // source event is the normalized value of the domEvents
    // like 'touchstart, mouseup, pointerdown'
    input.eventType = eventType;

    // compute scale, rotation etc
    computeInputData(manager, input);

    // emit secret event
    manager.emit('hammer.input', input);

    manager.recognize(input);
    manager.session.prevInput = input;
}

/**
 * extend the data with some usable properties like scale, rotate, velocity etc
 * @param {Object} manager
 * @param {Object} input
 */
function computeInputData(manager, input) {
    var session = manager.session;
    var pointers = input.pointers;
    var pointersLength = pointers.length;

    // store the first input to calculate the distance and direction
    if (!session.firstInput) {
        session.firstInput = simpleCloneInputData(input);
    }

    // to compute scale and rotation we need to store the multiple touches
    if (pointersLength > 1 && !session.firstMultiple) {
        session.firstMultiple = simpleCloneInputData(input);
    } else if (pointersLength === 1) {
        session.firstMultiple = false;
    }

    var firstInput = session.firstInput;
    var firstMultiple = session.firstMultiple;
    var offsetCenter = firstMultiple ? firstMultiple.center : firstInput.center;

    var center = input.center = getCenter(pointers);
    input.timeStamp = now();
    input.deltaTime = input.timeStamp - firstInput.timeStamp;

    input.angle = getAngle(offsetCenter, center);
    input.distance = getDistance(offsetCenter, center);

    computeDeltaXY(session, input);
    input.offsetDirection = getDirection(input.deltaX, input.deltaY);

    var overallVelocity = getVelocity(input.deltaTime, input.deltaX, input.deltaY);
    input.overallVelocityX = overallVelocity.x;
    input.overallVelocityY = overallVelocity.y;
    input.overallVelocity = (abs(overallVelocity.x) > abs(overallVelocity.y)) ? overallVelocity.x : overallVelocity.y;

    input.scale = firstMultiple ? getScale(firstMultiple.pointers, pointers) : 1;
    input.rotation = firstMultiple ? getRotation(firstMultiple.pointers, pointers) : 0;

    input.maxPointers = !session.prevInput ? input.pointers.length : ((input.pointers.length >
        session.prevInput.maxPointers) ? input.pointers.length : session.prevInput.maxPointers);

    computeIntervalInputData(session, input);

    // find the correct target
    var target = manager.element;
    if (hasParent(input.srcEvent.target, target)) {
        target = input.srcEvent.target;
    }
    input.target = target;
}

function computeDeltaXY(session, input) {
    var center = input.center;
    var offset = session.offsetDelta || {};
    var prevDelta = session.prevDelta || {};
    var prevInput = session.prevInput || {};

    if (input.eventType === INPUT_START || prevInput.eventType === INPUT_END) {
        prevDelta = session.prevDelta = {
            x: prevInput.deltaX || 0,
            y: prevInput.deltaY || 0
        };

        offset = session.offsetDelta = {
            x: center.x,
            y: center.y
        };
    }

    input.deltaX = prevDelta.x + (center.x - offset.x);
    input.deltaY = prevDelta.y + (center.y - offset.y);
}

/**
 * velocity is calculated every x ms
 * @param {Object} session
 * @param {Object} input
 */
function computeIntervalInputData(session, input) {
    var last = session.lastInterval || input,
        deltaTime = input.timeStamp - last.timeStamp,
        velocity, velocityX, velocityY, direction;

    if (input.eventType != INPUT_CANCEL && (deltaTime > COMPUTE_INTERVAL || last.velocity === undefined)) {
        var deltaX = input.deltaX - last.deltaX;
        var deltaY = input.deltaY - last.deltaY;

        var v = getVelocity(deltaTime, deltaX, deltaY);
        velocityX = v.x;
        velocityY = v.y;
        velocity = (abs(v.x) > abs(v.y)) ? v.x : v.y;
        direction = getDirection(deltaX, deltaY);

        session.lastInterval = input;
    } else {
        // use latest velocity info if it doesn't overtake a minimum period
        velocity = last.velocity;
        velocityX = last.velocityX;
        velocityY = last.velocityY;
        direction = last.direction;
    }

    input.velocity = velocity;
    input.velocityX = velocityX;
    input.velocityY = velocityY;
    input.direction = direction;
}

/**
 * create a simple clone from the input used for storage of firstInput and firstMultiple
 * @param {Object} input
 * @returns {Object} clonedInputData
 */
function simpleCloneInputData(input) {
    // make a simple copy of the pointers because we will get a reference if we don't
    // we only need clientXY for the calculations
    var pointers = [];
    var i = 0;
    while (i < input.pointers.length) {
        pointers[i] = {
            clientX: round(input.pointers[i].clientX),
            clientY: round(input.pointers[i].clientY)
        };
        i++;
    }

    return {
        timeStamp: now(),
        pointers: pointers,
        center: getCenter(pointers),
        deltaX: input.deltaX,
        deltaY: input.deltaY
    };
}

/**
 * get the center of all the pointers
 * @param {Array} pointers
 * @return {Object} center contains `x` and `y` properties
 */
function getCenter(pointers) {
    var pointersLength = pointers.length;

    // no need to loop when only one touch
    if (pointersLength === 1) {
        return {
            x: round(pointers[0].clientX),
            y: round(pointers[0].clientY)
        };
    }

    var x = 0, y = 0, i = 0;
    while (i < pointersLength) {
        x += pointers[i].clientX;
        y += pointers[i].clientY;
        i++;
    }

    return {
        x: round(x / pointersLength),
        y: round(y / pointersLength)
    };
}

/**
 * calculate the velocity between two points. unit is in px per ms.
 * @param {Number} deltaTime
 * @param {Number} x
 * @param {Number} y
 * @return {Object} velocity `x` and `y`
 */
function getVelocity(deltaTime, x, y) {
    return {
        x: x / deltaTime || 0,
        y: y / deltaTime || 0
    };
}

/**
 * get the direction between two points
 * @param {Number} x
 * @param {Number} y
 * @return {Number} direction
 */
function getDirection(x, y) {
    if (x === y) {
        return DIRECTION_NONE;
    }

    if (abs(x) >= abs(y)) {
        return x < 0 ? DIRECTION_LEFT : DIRECTION_RIGHT;
    }
    return y < 0 ? DIRECTION_UP : DIRECTION_DOWN;
}

/**
 * calculate the absolute distance between two points
 * @param {Object} p1 {x, y}
 * @param {Object} p2 {x, y}
 * @param {Array} [props] containing x and y keys
 * @return {Number} distance
 */
function getDistance(p1, p2, props) {
    if (!props) {
        props = PROPS_XY;
    }
    var x = p2[props[0]] - p1[props[0]],
        y = p2[props[1]] - p1[props[1]];

    return Math.sqrt((x * x) + (y * y));
}

/**
 * calculate the angle between two coordinates
 * @param {Object} p1
 * @param {Object} p2
 * @param {Array} [props] containing x and y keys
 * @return {Number} angle
 */
function getAngle(p1, p2, props) {
    if (!props) {
        props = PROPS_XY;
    }
    var x = p2[props[0]] - p1[props[0]],
        y = p2[props[1]] - p1[props[1]];
    return Math.atan2(y, x) * 180 / Math.PI;
}

/**
 * calculate the rotation degrees between two pointersets
 * @param {Array} start array of pointers
 * @param {Array} end array of pointers
 * @return {Number} rotation
 */
function getRotation(start, end) {
    return getAngle(end[1], end[0], PROPS_CLIENT_XY) + getAngle(start[1], start[0], PROPS_CLIENT_XY);
}

/**
 * calculate the scale factor between two pointersets
 * no scale is 1, and goes down to 0 when pinched together, and bigger when pinched out
 * @param {Array} start array of pointers
 * @param {Array} end array of pointers
 * @return {Number} scale
 */
function getScale(start, end) {
    return getDistance(end[0], end[1], PROPS_CLIENT_XY) / getDistance(start[0], start[1], PROPS_CLIENT_XY);
}

var MOUSE_INPUT_MAP = {
    mousedown: INPUT_START,
    mousemove: INPUT_MOVE,
    mouseup: INPUT_END
};

var MOUSE_ELEMENT_EVENTS = 'mousedown';
var MOUSE_WINDOW_EVENTS = 'mousemove mouseup';

/**
 * Mouse events input
 * @constructor
 * @extends Input
 */
function MouseInput() {
    this.evEl = MOUSE_ELEMENT_EVENTS;
    this.evWin = MOUSE_WINDOW_EVENTS;

    this.pressed = false; // mousedown state

    Input.apply(this, arguments);
}

inherit(MouseInput, Input, {
    /**
     * handle mouse events
     * @param {Object} ev
     */
    handler: function MEhandler(ev) {
        var eventType = MOUSE_INPUT_MAP[ev.type];

        // on start we want to have the left mouse button down
        if (eventType & INPUT_START && ev.button === 0) {
            this.pressed = true;
        }

        if (eventType & INPUT_MOVE && ev.which !== 1) {
            eventType = INPUT_END;
        }

        // mouse must be down
        if (!this.pressed) {
            return;
        }

        if (eventType & INPUT_END) {
            this.pressed = false;
        }

        this.callback(this.manager, eventType, {
            pointers: [ev],
            changedPointers: [ev],
            pointerType: INPUT_TYPE_MOUSE,
            srcEvent: ev
        });
    }
});

var POINTER_INPUT_MAP = {
    pointerdown: INPUT_START,
    pointermove: INPUT_MOVE,
    pointerup: INPUT_END,
    pointercancel: INPUT_CANCEL,
    pointerout: INPUT_CANCEL
};

// in IE10 the pointer types is defined as an enum
var IE10_POINTER_TYPE_ENUM = {
    2: INPUT_TYPE_TOUCH,
    3: INPUT_TYPE_PEN,
    4: INPUT_TYPE_MOUSE,
    5: INPUT_TYPE_KINECT // see https://twitter.com/jacobrossi/status/480596438489890816
};

var POINTER_ELEMENT_EVENTS = 'pointerdown';
var POINTER_WINDOW_EVENTS = 'pointermove pointerup pointercancel';

// IE10 has prefixed support, and case-sensitive
if (window.MSPointerEvent && !window.PointerEvent) {
    POINTER_ELEMENT_EVENTS = 'MSPointerDown';
    POINTER_WINDOW_EVENTS = 'MSPointerMove MSPointerUp MSPointerCancel';
}

/**
 * Pointer events input
 * @constructor
 * @extends Input
 */
function PointerEventInput() {
    this.evEl = POINTER_ELEMENT_EVENTS;
    this.evWin = POINTER_WINDOW_EVENTS;

    Input.apply(this, arguments);

    this.store = (this.manager.session.pointerEvents = []);
}

inherit(PointerEventInput, Input, {
    /**
     * handle mouse events
     * @param {Object} ev
     */
    handler: function PEhandler(ev) {
        var store = this.store;
        var removePointer = false;

        var eventTypeNormalized = ev.type.toLowerCase().replace('ms', '');
        var eventType = POINTER_INPUT_MAP[eventTypeNormalized];
        var pointerType = IE10_POINTER_TYPE_ENUM[ev.pointerType] || ev.pointerType;

        var isTouch = (pointerType == INPUT_TYPE_TOUCH);

        // get index of the event in the store
        var storeIndex = inArray(store, ev.pointerId, 'pointerId');

        // start and mouse must be down
        if (eventType & INPUT_START && (ev.button === 0 || isTouch)) {
            if (storeIndex < 0) {
                store.push(ev);
                storeIndex = store.length - 1;
            }
        } else if (eventType & (INPUT_END | INPUT_CANCEL)) {
            removePointer = true;
        }

        // it not found, so the pointer hasn't been down (so it's probably a hover)
        if (storeIndex < 0) {
            return;
        }

        // update the event in the store
        store[storeIndex] = ev;

        this.callback(this.manager, eventType, {
            pointers: store,
            changedPointers: [ev],
            pointerType: pointerType,
            srcEvent: ev
        });

        if (removePointer) {
            // remove from the store
            store.splice(storeIndex, 1);
        }
    }
});

var SINGLE_TOUCH_INPUT_MAP = {
    touchstart: INPUT_START,
    touchmove: INPUT_MOVE,
    touchend: INPUT_END,
    touchcancel: INPUT_CANCEL
};

var SINGLE_TOUCH_TARGET_EVENTS = 'touchstart';
var SINGLE_TOUCH_WINDOW_EVENTS = 'touchstart touchmove touchend touchcancel';

/**
 * Touch events input
 * @constructor
 * @extends Input
 */
function SingleTouchInput() {
    this.evTarget = SINGLE_TOUCH_TARGET_EVENTS;
    this.evWin = SINGLE_TOUCH_WINDOW_EVENTS;
    this.started = false;

    Input.apply(this, arguments);
}

inherit(SingleTouchInput, Input, {
    handler: function TEhandler(ev) {
        var type = SINGLE_TOUCH_INPUT_MAP[ev.type];

        // should we handle the touch events?
        if (type === INPUT_START) {
            this.started = true;
        }

        if (!this.started) {
            return;
        }

        var touches = normalizeSingleTouches.call(this, ev, type);

        // when done, reset the started state
        if (type & (INPUT_END | INPUT_CANCEL) && touches[0].length - touches[1].length === 0) {
            this.started = false;
        }

        this.callback(this.manager, type, {
            pointers: touches[0],
            changedPointers: touches[1],
            pointerType: INPUT_TYPE_TOUCH,
            srcEvent: ev
        });
    }
});

/**
 * @this {TouchInput}
 * @param {Object} ev
 * @param {Number} type flag
 * @returns {undefined|Array} [all, changed]
 */
function normalizeSingleTouches(ev, type) {
    var all = toArray(ev.touches);
    var changed = toArray(ev.changedTouches);

    if (type & (INPUT_END | INPUT_CANCEL)) {
        all = uniqueArray(all.concat(changed), 'identifier', true);
    }

    return [all, changed];
}

var TOUCH_INPUT_MAP = {
    touchstart: INPUT_START,
    touchmove: INPUT_MOVE,
    touchend: INPUT_END,
    touchcancel: INPUT_CANCEL
};

var TOUCH_TARGET_EVENTS = 'touchstart touchmove touchend touchcancel';

/**
 * Multi-user touch events input
 * @constructor
 * @extends Input
 */
function TouchInput() {
    this.evTarget = TOUCH_TARGET_EVENTS;
    this.targetIds = {};

    Input.apply(this, arguments);
}

inherit(TouchInput, Input, {
    handler: function MTEhandler(ev) {
        var type = TOUCH_INPUT_MAP[ev.type];
        var touches = getTouches.call(this, ev, type);
        if (!touches) {
            return;
        }

        this.callback(this.manager, type, {
            pointers: touches[0],
            changedPointers: touches[1],
            pointerType: INPUT_TYPE_TOUCH,
            srcEvent: ev
        });
    }
});

/**
 * @this {TouchInput}
 * @param {Object} ev
 * @param {Number} type flag
 * @returns {undefined|Array} [all, changed]
 */
function getTouches(ev, type) {
    var allTouches = toArray(ev.touches);
    var targetIds = this.targetIds;

    // when there is only one touch, the process can be simplified
    if (type & (INPUT_START | INPUT_MOVE) && allTouches.length === 1) {
        targetIds[allTouches[0].identifier] = true;
        return [allTouches, allTouches];
    }

    var i,
        targetTouches,
        changedTouches = toArray(ev.changedTouches),
        changedTargetTouches = [],
        target = this.target;

    // get target touches from touches
    targetTouches = allTouches.filter(function(touch) {
        return hasParent(touch.target, target);
    });

    // collect touches
    if (type === INPUT_START) {
        i = 0;
        while (i < targetTouches.length) {
            targetIds[targetTouches[i].identifier] = true;
            i++;
        }
    }

    // filter changed touches to only contain touches that exist in the collected target ids
    i = 0;
    while (i < changedTouches.length) {
        if (targetIds[changedTouches[i].identifier]) {
            changedTargetTouches.push(changedTouches[i]);
        }

        // cleanup removed touches
        if (type & (INPUT_END | INPUT_CANCEL)) {
            delete targetIds[changedTouches[i].identifier];
        }
        i++;
    }

    if (!changedTargetTouches.length) {
        return;
    }

    return [
        // merge targetTouches with changedTargetTouches so it contains ALL touches, including 'end' and 'cancel'
        uniqueArray(targetTouches.concat(changedTargetTouches), 'identifier', true),
        changedTargetTouches
    ];
}

/**
 * Combined touch and mouse input
 *
 * Touch has a higher priority then mouse, and while touching no mouse events are allowed.
 * This because touch devices also emit mouse events while doing a touch.
 *
 * @constructor
 * @extends Input
 */

var DEDUP_TIMEOUT = 2500;
var DEDUP_DISTANCE = 25;

function TouchMouseInput() {
    Input.apply(this, arguments);

    var handler = bindFn(this.handler, this);
    this.touch = new TouchInput(this.manager, handler);
    this.mouse = new MouseInput(this.manager, handler);

    this.primaryTouch = null;
    this.lastTouches = [];
}

inherit(TouchMouseInput, Input, {
    /**
     * handle mouse and touch events
     * @param {Hammer} manager
     * @param {String} inputEvent
     * @param {Object} inputData
     */
    handler: function TMEhandler(manager, inputEvent, inputData) {
        var isTouch = (inputData.pointerType == INPUT_TYPE_TOUCH),
            isMouse = (inputData.pointerType == INPUT_TYPE_MOUSE);

        if (isMouse && inputData.sourceCapabilities && inputData.sourceCapabilities.firesTouchEvents) {
            return;
        }

        // when we're in a touch event, record touches to  de-dupe synthetic mouse event
        if (isTouch) {
            recordTouches.call(this, inputEvent, inputData);
        } else if (isMouse && isSyntheticEvent.call(this, inputData)) {
            return;
        }

        this.callback(manager, inputEvent, inputData);
    },

    /**
     * remove the event listeners
     */
    destroy: function destroy() {
        this.touch.destroy();
        this.mouse.destroy();
    }
});

function recordTouches(eventType, eventData) {
    if (eventType & INPUT_START) {
        this.primaryTouch = eventData.changedPointers[0].identifier;
        setLastTouch.call(this, eventData);
    } else if (eventType & (INPUT_END | INPUT_CANCEL)) {
        setLastTouch.call(this, eventData);
    }
}

function setLastTouch(eventData) {
    var touch = eventData.changedPointers[0];

    if (touch.identifier === this.primaryTouch) {
        var lastTouch = {x: touch.clientX, y: touch.clientY};
        this.lastTouches.push(lastTouch);
        var lts = this.lastTouches;
        var removeLastTouch = function() {
            var i = lts.indexOf(lastTouch);
            if (i > -1) {
                lts.splice(i, 1);
            }
        };
        setTimeout(removeLastTouch, DEDUP_TIMEOUT);
    }
}

function isSyntheticEvent(eventData) {
    var x = eventData.srcEvent.clientX, y = eventData.srcEvent.clientY;
    for (var i = 0; i < this.lastTouches.length; i++) {
        var t = this.lastTouches[i];
        var dx = Math.abs(x - t.x), dy = Math.abs(y - t.y);
        if (dx <= DEDUP_DISTANCE && dy <= DEDUP_DISTANCE) {
            return true;
        }
    }
    return false;
}

var PREFIXED_TOUCH_ACTION = prefixed(TEST_ELEMENT.style, 'touchAction');
var NATIVE_TOUCH_ACTION = PREFIXED_TOUCH_ACTION !== undefined;

// magical touchAction value
var TOUCH_ACTION_COMPUTE = 'compute';
var TOUCH_ACTION_AUTO = 'auto';
var TOUCH_ACTION_MANIPULATION = 'manipulation'; // not implemented
var TOUCH_ACTION_NONE = 'none';
var TOUCH_ACTION_PAN_X = 'pan-x';
var TOUCH_ACTION_PAN_Y = 'pan-y';
var TOUCH_ACTION_MAP = getTouchActionProps();

/**
 * Touch Action
 * sets the touchAction property or uses the js alternative
 * @param {Manager} manager
 * @param {String} value
 * @constructor
 */
function TouchAction(manager, value) {
    this.manager = manager;
    this.set(value);
}

TouchAction.prototype = {
    /**
     * set the touchAction value on the element or enable the polyfill
     * @param {String} value
     */
    set: function(value) {
        // find out the touch-action by the event handlers
        if (value == TOUCH_ACTION_COMPUTE) {
            value = this.compute();
        }

        if (NATIVE_TOUCH_ACTION && this.manager.element.style && TOUCH_ACTION_MAP[value]) {
            this.manager.element.style[PREFIXED_TOUCH_ACTION] = value;
        }
        this.actions = value.toLowerCase().trim();
    },

    /**
     * just re-set the touchAction value
     */
    update: function() {
        this.set(this.manager.options.touchAction);
    },

    /**
     * compute the value for the touchAction property based on the recognizer's settings
     * @returns {String} value
     */
    compute: function() {
        var actions = [];
        each(this.manager.recognizers, function(recognizer) {
            if (boolOrFn(recognizer.options.enable, [recognizer])) {
                actions = actions.concat(recognizer.getTouchAction());
            }
        });
        return cleanTouchActions(actions.join(' '));
    },

    /**
     * this method is called on each input cycle and provides the preventing of the browser behavior
     * @param {Object} input
     */
    preventDefaults: function(input) {
        var srcEvent = input.srcEvent;
        var direction = input.offsetDirection;

        // if the touch action did prevented once this session
        if (this.manager.session.prevented) {
            srcEvent.preventDefault();
            return;
        }

        var actions = this.actions;
        var hasNone = inStr(actions, TOUCH_ACTION_NONE) && !TOUCH_ACTION_MAP[TOUCH_ACTION_NONE];
        var hasPanY = inStr(actions, TOUCH_ACTION_PAN_Y) && !TOUCH_ACTION_MAP[TOUCH_ACTION_PAN_Y];
        var hasPanX = inStr(actions, TOUCH_ACTION_PAN_X) && !TOUCH_ACTION_MAP[TOUCH_ACTION_PAN_X];

        if (hasNone) {
            //do not prevent defaults if this is a tap gesture

            var isTapPointer = input.pointers.length === 1;
            var isTapMovement = input.distance < 2;
            var isTapTouchTime = input.deltaTime < 250;

            if (isTapPointer && isTapMovement && isTapTouchTime) {
                return;
            }
        }

        if (hasPanX && hasPanY) {
            // `pan-x pan-y` means browser handles all scrolling/panning, do not prevent
            return;
        }

        if (hasNone ||
            (hasPanY && direction & DIRECTION_HORIZONTAL) ||
            (hasPanX && direction & DIRECTION_VERTICAL)) {
            return this.preventSrc(srcEvent);
        }
    },

    /**
     * call preventDefault to prevent the browser's default behavior (scrolling in most cases)
     * @param {Object} srcEvent
     */
    preventSrc: function(srcEvent) {
        this.manager.session.prevented = true;
        srcEvent.preventDefault();
    }
};

/**
 * when the touchActions are collected they are not a valid value, so we need to clean things up. *
 * @param {String} actions
 * @returns {*}
 */
function cleanTouchActions(actions) {
    // none
    if (inStr(actions, TOUCH_ACTION_NONE)) {
        return TOUCH_ACTION_NONE;
    }

    var hasPanX = inStr(actions, TOUCH_ACTION_PAN_X);
    var hasPanY = inStr(actions, TOUCH_ACTION_PAN_Y);

    // if both pan-x and pan-y are set (different recognizers
    // for different directions, e.g. horizontal pan but vertical swipe?)
    // we need none (as otherwise with pan-x pan-y combined none of these
    // recognizers will work, since the browser would handle all panning
    if (hasPanX && hasPanY) {
        return TOUCH_ACTION_NONE;
    }

    // pan-x OR pan-y
    if (hasPanX || hasPanY) {
        return hasPanX ? TOUCH_ACTION_PAN_X : TOUCH_ACTION_PAN_Y;
    }

    // manipulation
    if (inStr(actions, TOUCH_ACTION_MANIPULATION)) {
        return TOUCH_ACTION_MANIPULATION;
    }

    return TOUCH_ACTION_AUTO;
}

function getTouchActionProps() {
    if (!NATIVE_TOUCH_ACTION) {
        return false;
    }
    var touchMap = {};
    var cssSupports = window.CSS && window.CSS.supports;
    ['auto', 'manipulation', 'pan-y', 'pan-x', 'pan-x pan-y', 'none'].forEach(function(val) {

        // If css.supports is not supported but there is native touch-action assume it supports
        // all values. This is the case for IE 10 and 11.
        touchMap[val] = cssSupports ? window.CSS.supports('touch-action', val) : true;
    });
    return touchMap;
}

/**
 * Recognizer flow explained; *
 * All recognizers have the initial state of POSSIBLE when a input session starts.
 * The definition of a input session is from the first input until the last input, with all it's movement in it. *
 * Example session for mouse-input: mousedown -> mousemove -> mouseup
 *
 * On each recognizing cycle (see Manager.recognize) the .recognize() method is executed
 * which determines with state it should be.
 *
 * If the recognizer has the state FAILED, CANCELLED or RECOGNIZED (equals ENDED), it is reset to
 * POSSIBLE to give it another change on the next cycle.
 *
 *               Possible
 *                  |
 *            +-----+---------------+
 *            |                     |
 *      +-----+-----+               |
 *      |           |               |
 *   Failed      Cancelled          |
 *                          +-------+------+
 *                          |              |
 *                      Recognized       Began
 *                                         |
 *                                      Changed
 *                                         |
 *                                  Ended/Recognized
 */
var STATE_POSSIBLE = 1;
var STATE_BEGAN = 2;
var STATE_CHANGED = 4;
var STATE_ENDED = 8;
var STATE_RECOGNIZED = STATE_ENDED;
var STATE_CANCELLED = 16;
var STATE_FAILED = 32;

/**
 * Recognizer
 * Every recognizer needs to extend from this class.
 * @constructor
 * @param {Object} options
 */
function Recognizer(options) {
    this.options = assign({}, this.defaults, options || {});

    this.id = uniqueId();

    this.manager = null;

    // default is enable true
    this.options.enable = ifUndefined(this.options.enable, true);

    this.state = STATE_POSSIBLE;

    this.simultaneous = {};
    this.requireFail = [];
}

Recognizer.prototype = {
    /**
     * @virtual
     * @type {Object}
     */
    defaults: {},

    /**
     * set options
     * @param {Object} options
     * @return {Recognizer}
     */
    set: function(options) {
        assign(this.options, options);

        // also update the touchAction, in case something changed about the directions/enabled state
        this.manager && this.manager.touchAction.update();
        return this;
    },

    /**
     * recognize simultaneous with an other recognizer.
     * @param {Recognizer} otherRecognizer
     * @returns {Recognizer} this
     */
    recognizeWith: function(otherRecognizer) {
        if (invokeArrayArg(otherRecognizer, 'recognizeWith', this)) {
            return this;
        }

        var simultaneous = this.simultaneous;
        otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
        if (!simultaneous[otherRecognizer.id]) {
            simultaneous[otherRecognizer.id] = otherRecognizer;
            otherRecognizer.recognizeWith(this);
        }
        return this;
    },

    /**
     * drop the simultaneous link. it doesnt remove the link on the other recognizer.
     * @param {Recognizer} otherRecognizer
     * @returns {Recognizer} this
     */
    dropRecognizeWith: function(otherRecognizer) {
        if (invokeArrayArg(otherRecognizer, 'dropRecognizeWith', this)) {
            return this;
        }

        otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
        delete this.simultaneous[otherRecognizer.id];
        return this;
    },

    /**
     * recognizer can only run when an other is failing
     * @param {Recognizer} otherRecognizer
     * @returns {Recognizer} this
     */
    requireFailure: function(otherRecognizer) {
        if (invokeArrayArg(otherRecognizer, 'requireFailure', this)) {
            return this;
        }

        var requireFail = this.requireFail;
        otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
        if (inArray(requireFail, otherRecognizer) === -1) {
            requireFail.push(otherRecognizer);
            otherRecognizer.requireFailure(this);
        }
        return this;
    },

    /**
     * drop the requireFailure link. it does not remove the link on the other recognizer.
     * @param {Recognizer} otherRecognizer
     * @returns {Recognizer} this
     */
    dropRequireFailure: function(otherRecognizer) {
        if (invokeArrayArg(otherRecognizer, 'dropRequireFailure', this)) {
            return this;
        }

        otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
        var index = inArray(this.requireFail, otherRecognizer);
        if (index > -1) {
            this.requireFail.splice(index, 1);
        }
        return this;
    },

    /**
     * has require failures boolean
     * @returns {boolean}
     */
    hasRequireFailures: function() {
        return this.requireFail.length > 0;
    },

    /**
     * if the recognizer can recognize simultaneous with an other recognizer
     * @param {Recognizer} otherRecognizer
     * @returns {Boolean}
     */
    canRecognizeWith: function(otherRecognizer) {
        return !!this.simultaneous[otherRecognizer.id];
    },

    /**
     * You should use `tryEmit` instead of `emit` directly to check
     * that all the needed recognizers has failed before emitting.
     * @param {Object} input
     */
    emit: function(input) {
        var self = this;
        var state = this.state;

        function emit(event) {
            self.manager.emit(event, input);
        }

        // 'panstart' and 'panmove'
        if (state < STATE_ENDED) {
            emit(self.options.event + stateStr(state));
        }

        emit(self.options.event); // simple 'eventName' events

        if (input.additionalEvent) { // additional event(panleft, panright, pinchin, pinchout...)
            emit(input.additionalEvent);
        }

        // panend and pancancel
        if (state >= STATE_ENDED) {
            emit(self.options.event + stateStr(state));
        }
    },

    /**
     * Check that all the require failure recognizers has failed,
     * if true, it emits a gesture event,
     * otherwise, setup the state to FAILED.
     * @param {Object} input
     */
    tryEmit: function(input) {
        if (this.canEmit()) {
            return this.emit(input);
        }
        // it's failing anyway
        this.state = STATE_FAILED;
    },

    /**
     * can we emit?
     * @returns {boolean}
     */
    canEmit: function() {
        var i = 0;
        while (i < this.requireFail.length) {
            if (!(this.requireFail[i].state & (STATE_FAILED | STATE_POSSIBLE))) {
                return false;
            }
            i++;
        }
        return true;
    },

    /**
     * update the recognizer
     * @param {Object} inputData
     */
    recognize: function(inputData) {
        // make a new copy of the inputData
        // so we can change the inputData without messing up the other recognizers
        var inputDataClone = assign({}, inputData);

        // is is enabled and allow recognizing?
        if (!boolOrFn(this.options.enable, [this, inputDataClone])) {
            this.reset();
            this.state = STATE_FAILED;
            return;
        }

        // reset when we've reached the end
        if (this.state & (STATE_RECOGNIZED | STATE_CANCELLED | STATE_FAILED)) {
            this.state = STATE_POSSIBLE;
        }

        this.state = this.process(inputDataClone);

        // the recognizer has recognized a gesture
        // so trigger an event
        if (this.state & (STATE_BEGAN | STATE_CHANGED | STATE_ENDED | STATE_CANCELLED)) {
            this.tryEmit(inputDataClone);
        }
    },

    /**
     * return the state of the recognizer
     * the actual recognizing happens in this method
     * @virtual
     * @param {Object} inputData
     * @returns {Const} STATE
     */
    process: function(inputData) { }, // jshint ignore:line

    /**
     * return the preferred touch-action
     * @virtual
     * @returns {Array}
     */
    getTouchAction: function() { },

    /**
     * called when the gesture isn't allowed to recognize
     * like when another is being recognized or it is disabled
     * @virtual
     */
    reset: function() { }
};

/**
 * get a usable string, used as event postfix
 * @param {Const} state
 * @returns {String} state
 */
function stateStr(state) {
    if (state & STATE_CANCELLED) {
        return 'cancel';
    } else if (state & STATE_ENDED) {
        return 'end';
    } else if (state & STATE_CHANGED) {
        return 'move';
    } else if (state & STATE_BEGAN) {
        return 'start';
    }
    return '';
}

/**
 * direction cons to string
 * @param {Const} direction
 * @returns {String}
 */
function directionStr(direction) {
    if (direction == DIRECTION_DOWN) {
        return 'down';
    } else if (direction == DIRECTION_UP) {
        return 'up';
    } else if (direction == DIRECTION_LEFT) {
        return 'left';
    } else if (direction == DIRECTION_RIGHT) {
        return 'right';
    }
    return '';
}

/**
 * get a recognizer by name if it is bound to a manager
 * @param {Recognizer|String} otherRecognizer
 * @param {Recognizer} recognizer
 * @returns {Recognizer}
 */
function getRecognizerByNameIfManager(otherRecognizer, recognizer) {
    var manager = recognizer.manager;
    if (manager) {
        return manager.get(otherRecognizer);
    }
    return otherRecognizer;
}

/**
 * This recognizer is just used as a base for the simple attribute recognizers.
 * @constructor
 * @extends Recognizer
 */
function AttrRecognizer() {
    Recognizer.apply(this, arguments);
}

inherit(AttrRecognizer, Recognizer, {
    /**
     * @namespace
     * @memberof AttrRecognizer
     */
    defaults: {
        /**
         * @type {Number}
         * @default 1
         */
        pointers: 1
    },

    /**
     * Used to check if it the recognizer receives valid input, like input.distance > 10.
     * @memberof AttrRecognizer
     * @param {Object} input
     * @returns {Boolean} recognized
     */
    attrTest: function(input) {
        var optionPointers = this.options.pointers;
        return optionPointers === 0 || input.pointers.length === optionPointers;
    },

    /**
     * Process the input and return the state for the recognizer
     * @memberof AttrRecognizer
     * @param {Object} input
     * @returns {*} State
     */
    process: function(input) {
        var state = this.state;
        var eventType = input.eventType;

        var isRecognized = state & (STATE_BEGAN | STATE_CHANGED);
        var isValid = this.attrTest(input);

        // on cancel input and we've recognized before, return STATE_CANCELLED
        if (isRecognized && (eventType & INPUT_CANCEL || !isValid)) {
            return state | STATE_CANCELLED;
        } else if (isRecognized || isValid) {
            if (eventType & INPUT_END) {
                return state | STATE_ENDED;
            } else if (!(state & STATE_BEGAN)) {
                return STATE_BEGAN;
            }
            return state | STATE_CHANGED;
        }
        return STATE_FAILED;
    }
});

/**
 * Pan
 * Recognized when the pointer is down and moved in the allowed direction.
 * @constructor
 * @extends AttrRecognizer
 */
function PanRecognizer() {
    AttrRecognizer.apply(this, arguments);

    this.pX = null;
    this.pY = null;
}

inherit(PanRecognizer, AttrRecognizer, {
    /**
     * @namespace
     * @memberof PanRecognizer
     */
    defaults: {
        event: 'pan',
        threshold: 10,
        pointers: 1,
        direction: DIRECTION_ALL
    },

    getTouchAction: function() {
        var direction = this.options.direction;
        var actions = [];
        if (direction & DIRECTION_HORIZONTAL) {
            actions.push(TOUCH_ACTION_PAN_Y);
        }
        if (direction & DIRECTION_VERTICAL) {
            actions.push(TOUCH_ACTION_PAN_X);
        }
        return actions;
    },

    directionTest: function(input) {
        var options = this.options;
        var hasMoved = true;
        var distance = input.distance;
        var direction = input.direction;
        var x = input.deltaX;
        var y = input.deltaY;

        // lock to axis?
        if (!(direction & options.direction)) {
            if (options.direction & DIRECTION_HORIZONTAL) {
                direction = (x === 0) ? DIRECTION_NONE : (x < 0) ? DIRECTION_LEFT : DIRECTION_RIGHT;
                hasMoved = x != this.pX;
                distance = Math.abs(input.deltaX);
            } else {
                direction = (y === 0) ? DIRECTION_NONE : (y < 0) ? DIRECTION_UP : DIRECTION_DOWN;
                hasMoved = y != this.pY;
                distance = Math.abs(input.deltaY);
            }
        }
        input.direction = direction;
        return hasMoved && distance > options.threshold && direction & options.direction;
    },

    attrTest: function(input) {
        return AttrRecognizer.prototype.attrTest.call(this, input) &&
            (this.state & STATE_BEGAN || (!(this.state & STATE_BEGAN) && this.directionTest(input)));
    },

    emit: function(input) {

        this.pX = input.deltaX;
        this.pY = input.deltaY;

        var direction = directionStr(input.direction);

        if (direction) {
            input.additionalEvent = this.options.event + direction;
        }
        this._super.emit.call(this, input);
    }
});

/**
 * Pinch
 * Recognized when two or more pointers are moving toward (zoom-in) or away from each other (zoom-out).
 * @constructor
 * @extends AttrRecognizer
 */
function PinchRecognizer() {
    AttrRecognizer.apply(this, arguments);
}

inherit(PinchRecognizer, AttrRecognizer, {
    /**
     * @namespace
     * @memberof PinchRecognizer
     */
    defaults: {
        event: 'pinch',
        threshold: 0,
        pointers: 2
    },

    getTouchAction: function() {
        return [TOUCH_ACTION_NONE];
    },

    attrTest: function(input) {
        return this._super.attrTest.call(this, input) &&
            (Math.abs(input.scale - 1) > this.options.threshold || this.state & STATE_BEGAN);
    },

    emit: function(input) {
        if (input.scale !== 1) {
            var inOut = input.scale < 1 ? 'in' : 'out';
            input.additionalEvent = this.options.event + inOut;
        }
        this._super.emit.call(this, input);
    }
});

/**
 * Press
 * Recognized when the pointer is down for x ms without any movement.
 * @constructor
 * @extends Recognizer
 */
function PressRecognizer() {
    Recognizer.apply(this, arguments);

    this._timer = null;
    this._input = null;
}

inherit(PressRecognizer, Recognizer, {
    /**
     * @namespace
     * @memberof PressRecognizer
     */
    defaults: {
        event: 'press',
        pointers: 1,
        time: 251, // minimal time of the pointer to be pressed
        threshold: 9 // a minimal movement is ok, but keep it low
    },

    getTouchAction: function() {
        return [TOUCH_ACTION_AUTO];
    },

    process: function(input) {
        var options = this.options;
        var validPointers = input.pointers.length === options.pointers;
        var validMovement = input.distance < options.threshold;
        var validTime = input.deltaTime > options.time;

        this._input = input;

        // we only allow little movement
        // and we've reached an end event, so a tap is possible
        if (!validMovement || !validPointers || (input.eventType & (INPUT_END | INPUT_CANCEL) && !validTime)) {
            this.reset();
        } else if (input.eventType & INPUT_START) {
            this.reset();
            this._timer = setTimeoutContext(function() {
                this.state = STATE_RECOGNIZED;
                this.tryEmit();
            }, options.time, this);
        } else if (input.eventType & INPUT_END) {
            return STATE_RECOGNIZED;
        }
        return STATE_FAILED;
    },

    reset: function() {
        clearTimeout(this._timer);
    },

    emit: function(input) {
        if (this.state !== STATE_RECOGNIZED) {
            return;
        }

        if (input && (input.eventType & INPUT_END)) {
            this.manager.emit(this.options.event + 'up', input);
        } else {
            this._input.timeStamp = now();
            this.manager.emit(this.options.event, this._input);
        }
    }
});

/**
 * Rotate
 * Recognized when two or more pointer are moving in a circular motion.
 * @constructor
 * @extends AttrRecognizer
 */
function RotateRecognizer() {
    AttrRecognizer.apply(this, arguments);
}

inherit(RotateRecognizer, AttrRecognizer, {
    /**
     * @namespace
     * @memberof RotateRecognizer
     */
    defaults: {
        event: 'rotate',
        threshold: 0,
        pointers: 2
    },

    getTouchAction: function() {
        return [TOUCH_ACTION_NONE];
    },

    attrTest: function(input) {
        return this._super.attrTest.call(this, input) &&
            (Math.abs(input.rotation) > this.options.threshold || this.state & STATE_BEGAN);
    }
});

/**
 * Swipe
 * Recognized when the pointer is moving fast (velocity), with enough distance in the allowed direction.
 * @constructor
 * @extends AttrRecognizer
 */
function SwipeRecognizer() {
    AttrRecognizer.apply(this, arguments);
}

inherit(SwipeRecognizer, AttrRecognizer, {
    /**
     * @namespace
     * @memberof SwipeRecognizer
     */
    defaults: {
        event: 'swipe',
        threshold: 10,
        velocity: 0.3,
        direction: DIRECTION_HORIZONTAL | DIRECTION_VERTICAL,
        pointers: 1
    },

    getTouchAction: function() {
        return PanRecognizer.prototype.getTouchAction.call(this);
    },

    attrTest: function(input) {
        var direction = this.options.direction;
        var velocity;

        if (direction & (DIRECTION_HORIZONTAL | DIRECTION_VERTICAL)) {
            velocity = input.overallVelocity;
        } else if (direction & DIRECTION_HORIZONTAL) {
            velocity = input.overallVelocityX;
        } else if (direction & DIRECTION_VERTICAL) {
            velocity = input.overallVelocityY;
        }

        return this._super.attrTest.call(this, input) &&
            direction & input.offsetDirection &&
            input.distance > this.options.threshold &&
            input.maxPointers == this.options.pointers &&
            abs(velocity) > this.options.velocity && input.eventType & INPUT_END;
    },

    emit: function(input) {
        var direction = directionStr(input.offsetDirection);
        if (direction) {
            this.manager.emit(this.options.event + direction, input);
        }

        this.manager.emit(this.options.event, input);
    }
});

/**
 * A tap is ecognized when the pointer is doing a small tap/click. Multiple taps are recognized if they occur
 * between the given interval and position. The delay option can be used to recognize multi-taps without firing
 * a single tap.
 *
 * The eventData from the emitted event contains the property `tapCount`, which contains the amount of
 * multi-taps being recognized.
 * @constructor
 * @extends Recognizer
 */
function TapRecognizer() {
    Recognizer.apply(this, arguments);

    // previous time and center,
    // used for tap counting
    this.pTime = false;
    this.pCenter = false;

    this._timer = null;
    this._input = null;
    this.count = 0;
}

inherit(TapRecognizer, Recognizer, {
    /**
     * @namespace
     * @memberof PinchRecognizer
     */
    defaults: {
        event: 'tap',
        pointers: 1,
        taps: 1,
        interval: 300, // max time between the multi-tap taps
        time: 250, // max time of the pointer to be down (like finger on the screen)
        threshold: 9, // a minimal movement is ok, but keep it low
        posThreshold: 10 // a multi-tap can be a bit off the initial position
    },

    getTouchAction: function() {
        return [TOUCH_ACTION_MANIPULATION];
    },

    process: function(input) {
        var options = this.options;

        var validPointers = input.pointers.length === options.pointers;
        var validMovement = input.distance < options.threshold;
        var validTouchTime = input.deltaTime < options.time;

        this.reset();

        if ((input.eventType & INPUT_START) && (this.count === 0)) {
            return this.failTimeout();
        }

        // we only allow little movement
        // and we've reached an end event, so a tap is possible
        if (validMovement && validTouchTime && validPointers) {
            if (input.eventType != INPUT_END) {
                return this.failTimeout();
            }

            var validInterval = this.pTime ? (input.timeStamp - this.pTime < options.interval) : true;
            var validMultiTap = !this.pCenter || getDistance(this.pCenter, input.center) < options.posThreshold;

            this.pTime = input.timeStamp;
            this.pCenter = input.center;

            if (!validMultiTap || !validInterval) {
                this.count = 1;
            } else {
                this.count += 1;
            }

            this._input = input;

            // if tap count matches we have recognized it,
            // else it has began recognizing...
            var tapCount = this.count % options.taps;
            if (tapCount === 0) {
                // no failing requirements, immediately trigger the tap event
                // or wait as long as the multitap interval to trigger
                if (!this.hasRequireFailures()) {
                    return STATE_RECOGNIZED;
                } else {
                    this._timer = setTimeoutContext(function() {
                        this.state = STATE_RECOGNIZED;
                        this.tryEmit();
                    }, options.interval, this);
                    return STATE_BEGAN;
                }
            }
        }
        return STATE_FAILED;
    },

    failTimeout: function() {
        this._timer = setTimeoutContext(function() {
            this.state = STATE_FAILED;
        }, this.options.interval, this);
        return STATE_FAILED;
    },

    reset: function() {
        clearTimeout(this._timer);
    },

    emit: function() {
        if (this.state == STATE_RECOGNIZED) {
            this._input.tapCount = this.count;
            this.manager.emit(this.options.event, this._input);
        }
    }
});

/**
 * Simple way to create a manager with a default set of recognizers.
 * @param {HTMLElement} element
 * @param {Object} [options]
 * @constructor
 */
function Hammer(element, options) {
    options = options || {};
    options.recognizers = ifUndefined(options.recognizers, Hammer.defaults.preset);
    return new Manager(element, options);
}

/**
 * @const {string}
 */
Hammer.VERSION = '2.0.7';

/**
 * default settings
 * @namespace
 */
Hammer.defaults = {
    /**
     * set if DOM events are being triggered.
     * But this is slower and unused by simple implementations, so disabled by default.
     * @type {Boolean}
     * @default false
     */
    domEvents: false,

    /**
     * The value for the touchAction property/fallback.
     * When set to `compute` it will magically set the correct value based on the added recognizers.
     * @type {String}
     * @default compute
     */
    touchAction: TOUCH_ACTION_COMPUTE,

    /**
     * @type {Boolean}
     * @default true
     */
    enable: true,

    /**
     * EXPERIMENTAL FEATURE -- can be removed/changed
     * Change the parent input target element.
     * If Null, then it is being set the to main element.
     * @type {Null|EventTarget}
     * @default null
     */
    inputTarget: null,

    /**
     * force an input class
     * @type {Null|Function}
     * @default null
     */
    inputClass: null,

    /**
     * Default recognizer setup when calling `Hammer()`
     * When creating a new Manager these will be skipped.
     * @type {Array}
     */
    preset: [
        // RecognizerClass, options, [recognizeWith, ...], [requireFailure, ...]
        [RotateRecognizer, {enable: false}],
        [PinchRecognizer, {enable: false}, ['rotate']],
        [SwipeRecognizer, {direction: DIRECTION_HORIZONTAL}],
        [PanRecognizer, {direction: DIRECTION_HORIZONTAL}, ['swipe']],
        [TapRecognizer],
        [TapRecognizer, {event: 'doubletap', taps: 2}, ['tap']],
        [PressRecognizer]
    ],

    /**
     * Some CSS properties can be used to improve the working of Hammer.
     * Add them to this method and they will be set when creating a new Manager.
     * @namespace
     */
    cssProps: {
        /**
         * Disables text selection to improve the dragging gesture. Mainly for desktop browsers.
         * @type {String}
         * @default 'none'
         */
        userSelect: 'none',

        /**
         * Disable the Windows Phone grippers when pressing an element.
         * @type {String}
         * @default 'none'
         */
        touchSelect: 'none',

        /**
         * Disables the default callout shown when you touch and hold a touch target.
         * On iOS, when you touch and hold a touch target such as a link, Safari displays
         * a callout containing information about the link. This property allows you to disable that callout.
         * @type {String}
         * @default 'none'
         */
        touchCallout: 'none',

        /**
         * Specifies whether zooming is enabled. Used by IE10>
         * @type {String}
         * @default 'none'
         */
        contentZooming: 'none',

        /**
         * Specifies that an entire element should be draggable instead of its contents. Mainly for desktop browsers.
         * @type {String}
         * @default 'none'
         */
        userDrag: 'none',

        /**
         * Overrides the highlight color shown when the user taps a link or a JavaScript
         * clickable element in iOS. This property obeys the alpha value, if specified.
         * @type {String}
         * @default 'rgba(0,0,0,0)'
         */
        tapHighlightColor: 'rgba(0,0,0,0)'
    }
};

var STOP = 1;
var FORCED_STOP = 2;

/**
 * Manager
 * @param {HTMLElement} element
 * @param {Object} [options]
 * @constructor
 */
function Manager(element, options) {
    this.options = assign({}, Hammer.defaults, options || {});

    this.options.inputTarget = this.options.inputTarget || element;

    this.handlers = {};
    this.session = {};
    this.recognizers = [];
    this.oldCssProps = {};

    this.element = element;
    this.input = createInputInstance(this);
    this.touchAction = new TouchAction(this, this.options.touchAction);

    toggleCssProps(this, true);

    each(this.options.recognizers, function(item) {
        var recognizer = this.add(new (item[0])(item[1]));
        item[2] && recognizer.recognizeWith(item[2]);
        item[3] && recognizer.requireFailure(item[3]);
    }, this);
}

Manager.prototype = {
    /**
     * set options
     * @param {Object} options
     * @returns {Manager}
     */
    set: function(options) {
        assign(this.options, options);

        // Options that need a little more setup
        if (options.touchAction) {
            this.touchAction.update();
        }
        if (options.inputTarget) {
            // Clean up existing event listeners and reinitialize
            this.input.destroy();
            this.input.target = options.inputTarget;
            this.input.init();
        }
        return this;
    },

    /**
     * stop recognizing for this session.
     * This session will be discarded, when a new [input]start event is fired.
     * When forced, the recognizer cycle is stopped immediately.
     * @param {Boolean} [force]
     */
    stop: function(force) {
        this.session.stopped = force ? FORCED_STOP : STOP;
    },

    /**
     * run the recognizers!
     * called by the inputHandler function on every movement of the pointers (touches)
     * it walks through all the recognizers and tries to detect the gesture that is being made
     * @param {Object} inputData
     */
    recognize: function(inputData) {
        var session = this.session;
        if (session.stopped) {
            return;
        }

        // run the touch-action polyfill
        this.touchAction.preventDefaults(inputData);

        var recognizer;
        var recognizers = this.recognizers;

        // this holds the recognizer that is being recognized.
        // so the recognizer's state needs to be BEGAN, CHANGED, ENDED or RECOGNIZED
        // if no recognizer is detecting a thing, it is set to `null`
        var curRecognizer = session.curRecognizer;

        // reset when the last recognizer is recognized
        // or when we're in a new session
        if (!curRecognizer || (curRecognizer && curRecognizer.state & STATE_RECOGNIZED)) {
            curRecognizer = session.curRecognizer = null;
        }

        var i = 0;
        while (i < recognizers.length) {
            recognizer = recognizers[i];

            // find out if we are allowed try to recognize the input for this one.
            // 1.   allow if the session is NOT forced stopped (see the .stop() method)
            // 2.   allow if we still haven't recognized a gesture in this session, or the this recognizer is the one
            //      that is being recognized.
            // 3.   allow if the recognizer is allowed to run simultaneous with the current recognized recognizer.
            //      this can be setup with the `recognizeWith()` method on the recognizer.
            if (session.stopped !== FORCED_STOP && ( // 1
                    !curRecognizer || recognizer == curRecognizer || // 2
                    recognizer.canRecognizeWith(curRecognizer))) { // 3
                recognizer.recognize(inputData);
            } else {
                recognizer.reset();
            }

            // if the recognizer has been recognizing the input as a valid gesture, we want to store this one as the
            // current active recognizer. but only if we don't already have an active recognizer
            if (!curRecognizer && recognizer.state & (STATE_BEGAN | STATE_CHANGED | STATE_ENDED)) {
                curRecognizer = session.curRecognizer = recognizer;
            }
            i++;
        }
    },

    /**
     * get a recognizer by its event name.
     * @param {Recognizer|String} recognizer
     * @returns {Recognizer|Null}
     */
    get: function(recognizer) {
        if (recognizer instanceof Recognizer) {
            return recognizer;
        }

        var recognizers = this.recognizers;
        for (var i = 0; i < recognizers.length; i++) {
            if (recognizers[i].options.event == recognizer) {
                return recognizers[i];
            }
        }
        return null;
    },

    /**
     * add a recognizer to the manager
     * existing recognizers with the same event name will be removed
     * @param {Recognizer} recognizer
     * @returns {Recognizer|Manager}
     */
    add: function(recognizer) {
        if (invokeArrayArg(recognizer, 'add', this)) {
            return this;
        }

        // remove existing
        var existing = this.get(recognizer.options.event);
        if (existing) {
            this.remove(existing);
        }

        this.recognizers.push(recognizer);
        recognizer.manager = this;

        this.touchAction.update();
        return recognizer;
    },

    /**
     * remove a recognizer by name or instance
     * @param {Recognizer|String} recognizer
     * @returns {Manager}
     */
    remove: function(recognizer) {
        if (invokeArrayArg(recognizer, 'remove', this)) {
            return this;
        }

        recognizer = this.get(recognizer);

        // let's make sure this recognizer exists
        if (recognizer) {
            var recognizers = this.recognizers;
            var index = inArray(recognizers, recognizer);

            if (index !== -1) {
                recognizers.splice(index, 1);
                this.touchAction.update();
            }
        }

        return this;
    },

    /**
     * bind event
     * @param {String} events
     * @param {Function} handler
     * @returns {EventEmitter} this
     */
    on: function(events, handler) {
        if (events === undefined) {
            return;
        }
        if (handler === undefined) {
            return;
        }

        var handlers = this.handlers;
        each(splitStr(events), function(event) {
            handlers[event] = handlers[event] || [];
            handlers[event].push(handler);
        });
        return this;
    },

    /**
     * unbind event, leave emit blank to remove all handlers
     * @param {String} events
     * @param {Function} [handler]
     * @returns {EventEmitter} this
     */
    off: function(events, handler) {
        if (events === undefined) {
            return;
        }

        var handlers = this.handlers;
        each(splitStr(events), function(event) {
            if (!handler) {
                delete handlers[event];
            } else {
                handlers[event] && handlers[event].splice(inArray(handlers[event], handler), 1);
            }
        });
        return this;
    },

    /**
     * emit event to the listeners
     * @param {String} event
     * @param {Object} data
     */
    emit: function(event, data) {
        // we also want to trigger dom events
        if (this.options.domEvents) {
            triggerDomEvent(event, data);
        }

        // no handlers, so skip it all
        var handlers = this.handlers[event] && this.handlers[event].slice();
        if (!handlers || !handlers.length) {
            return;
        }

        data.type = event;
        data.preventDefault = function() {
            data.srcEvent.preventDefault();
        };

        var i = 0;
        while (i < handlers.length) {
            handlers[i](data);
            i++;
        }
    },

    /**
     * destroy the manager and unbinds all events
     * it doesn't unbind dom events, that is the user own responsibility
     */
    destroy: function() {
        this.element && toggleCssProps(this, false);

        this.handlers = {};
        this.session = {};
        this.input.destroy();
        this.element = null;
    }
};

/**
 * add/remove the css properties as defined in manager.options.cssProps
 * @param {Manager} manager
 * @param {Boolean} add
 */
function toggleCssProps(manager, add) {
    var element = manager.element;
    if (!element.style) {
        return;
    }
    var prop;
    each(manager.options.cssProps, function(value, name) {
        prop = prefixed(element.style, name);
        if (add) {
            manager.oldCssProps[prop] = element.style[prop];
            element.style[prop] = value;
        } else {
            element.style[prop] = manager.oldCssProps[prop] || '';
        }
    });
    if (!add) {
        manager.oldCssProps = {};
    }
}

/**
 * trigger dom event
 * @param {String} event
 * @param {Object} data
 */
function triggerDomEvent(event, data) {
    var gestureEvent = document.createEvent('Event');
    gestureEvent.initEvent(event, true, true);
    gestureEvent.gesture = data;
    data.target.dispatchEvent(gestureEvent);
}

assign(Hammer, {
    INPUT_START: INPUT_START,
    INPUT_MOVE: INPUT_MOVE,
    INPUT_END: INPUT_END,
    INPUT_CANCEL: INPUT_CANCEL,

    STATE_POSSIBLE: STATE_POSSIBLE,
    STATE_BEGAN: STATE_BEGAN,
    STATE_CHANGED: STATE_CHANGED,
    STATE_ENDED: STATE_ENDED,
    STATE_RECOGNIZED: STATE_RECOGNIZED,
    STATE_CANCELLED: STATE_CANCELLED,
    STATE_FAILED: STATE_FAILED,

    DIRECTION_NONE: DIRECTION_NONE,
    DIRECTION_LEFT: DIRECTION_LEFT,
    DIRECTION_RIGHT: DIRECTION_RIGHT,
    DIRECTION_UP: DIRECTION_UP,
    DIRECTION_DOWN: DIRECTION_DOWN,
    DIRECTION_HORIZONTAL: DIRECTION_HORIZONTAL,
    DIRECTION_VERTICAL: DIRECTION_VERTICAL,
    DIRECTION_ALL: DIRECTION_ALL,

    Manager: Manager,
    Input: Input,
    TouchAction: TouchAction,

    TouchInput: TouchInput,
    MouseInput: MouseInput,
    PointerEventInput: PointerEventInput,
    TouchMouseInput: TouchMouseInput,
    SingleTouchInput: SingleTouchInput,

    Recognizer: Recognizer,
    AttrRecognizer: AttrRecognizer,
    Tap: TapRecognizer,
    Pan: PanRecognizer,
    Swipe: SwipeRecognizer,
    Pinch: PinchRecognizer,
    Rotate: RotateRecognizer,
    Press: PressRecognizer,

    on: addEventListeners,
    off: removeEventListeners,
    each: each,
    merge: merge,
    extend: extend,
    assign: assign,
    inherit: inherit,
    bindFn: bindFn,
    prefixed: prefixed
});

// this prevents errors when Hammer is loaded in the presence of an AMD
//  style loader but by script tag, not by the loader.
var freeGlobal = (typeof window !== 'undefined' ? window : (typeof self !== 'undefined' ? self : {})); // jshint ignore:line
freeGlobal.Hammer = Hammer;

if (true) {
    !(__WEBPACK_AMD_DEFINE_RESULT__ = (function() {
        return Hammer;
    }).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
} else {}

})(window, document, 'Hammer');


/***/ }),

/***/ "./node_modules/ion-bottom-drawer/fesm2015/ion-bottom-drawer.js":
/*!**********************************************************************!*\
  !*** ./node_modules/ion-bottom-drawer/fesm2015/ion-bottom-drawer.js ***!
  \**********************************************************************/
/*! exports provided: DrawerState, IonBottomDrawerModule, ɵa */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DrawerState", function() { return DrawerState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonBottomDrawerModule", function() { return IonBottomDrawerModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵa", function() { return IonBottomDrawerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_3__);






var DrawerState;
(function (DrawerState) {
    DrawerState[DrawerState["Bottom"] = 0] = "Bottom";
    DrawerState[DrawerState["Docked"] = 1] = "Docked";
    DrawerState[DrawerState["Top"] = 2] = "Top";
})(DrawerState || (DrawerState = {}));

let IonBottomDrawerComponent = class IonBottomDrawerComponent {
    constructor(_element, _renderer, _domCtrl, _platform) {
        this._element = _element;
        this._renderer = _renderer;
        this._domCtrl = _domCtrl;
        this._platform = _platform;
        this.dockedHeight = 50;
        this.shouldBounce = true;
        this.disableDrag = false;
        this.distanceTop = 0;
        this.transition = '0.25s ease-in-out';
        this.state = DrawerState.Bottom;
        this.minimumHeight = 0;
        this.stateChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this._BOUNCE_DELTA = 30;
    }
    ngAfterViewInit() {
        this._renderer.setStyle(this._element.nativeElement.querySelector('.ion-bottom-drawer-scrollable-content :first-child'), 'touch-action', 'none');
        this._setDrawerState(this.state);
        const hammer = new hammerjs__WEBPACK_IMPORTED_MODULE_3__(this._element.nativeElement);
        hammer.get('pan').set({ enable: true, direction: hammerjs__WEBPACK_IMPORTED_MODULE_3__["DIRECTION_VERTICAL"] });
        hammer.on('pan panstart panend', (ev) => {
            if (this.disableDrag) {
                return;
            }
            switch (ev.type) {
                case 'panstart':
                    this._handlePanStart();
                    break;
                case 'panend':
                    this._handlePanEnd(ev);
                    break;
                default:
                    this._handlePan(ev);
            }
        });
    }
    ngOnChanges(changes) {
        if (!changes.state) {
            return;
        }
        this._setDrawerState(changes.state.currentValue);
    }
    _setDrawerState(state) {
        this._renderer.setStyle(this._element.nativeElement, 'transition', this.transition);
        switch (state) {
            case DrawerState.Bottom:
                this._setTranslateY('calc(100vh - ' + this.minimumHeight + 'px)');
                break;
            case DrawerState.Docked:
                this._setTranslateY((this._platform.height() - this.dockedHeight) + 'px');
                break;
            default:
                this._setTranslateY(this.distanceTop + 'px');
        }
    }
    _handlePanStart() {
        this._startPositionTop = this._element.nativeElement.getBoundingClientRect().top;
    }
    _handlePanEnd(ev) {
        if (this.shouldBounce && ev.isFinal) {
            this._renderer.setStyle(this._element.nativeElement, 'transition', this.transition);
            switch (this.state) {
                case DrawerState.Docked:
                    this._handleDockedPanEnd(ev);
                    break;
                case DrawerState.Top:
                    this._handleTopPanEnd(ev);
                    break;
                default:
                    this._handleBottomPanEnd(ev);
            }
        }
        this.stateChange.emit(this.state);
    }
    _handleTopPanEnd(ev) {
        if (ev.deltaY > this._BOUNCE_DELTA) {
            this.state = DrawerState.Docked;
        }
        else {
            this._setTranslateY(this.distanceTop + 'px');
        }
    }
    _handleDockedPanEnd(ev) {
        const absDeltaY = Math.abs(ev.deltaY);
        if (absDeltaY > this._BOUNCE_DELTA && ev.deltaY < 0) {
            this.state = DrawerState.Top;
        }
        else if (absDeltaY > this._BOUNCE_DELTA && ev.deltaY > 0) {
            this.state = DrawerState.Bottom;
        }
        else {
            this._setTranslateY((this._platform.height() - this.dockedHeight) + 'px');
        }
    }
    _handleBottomPanEnd(ev) {
        if (-ev.deltaY > this._BOUNCE_DELTA) {
            this.state = DrawerState.Docked;
        }
        else {
            this._setTranslateY('calc(100vh - ' + this.minimumHeight + 'px)');
        }
    }
    _handlePan(ev) {
        const pointerY = ev.center.y;
        this._renderer.setStyle(this._element.nativeElement, 'transition', 'none');
        if (pointerY > 0 && pointerY < this._platform.height()) {
            if (ev.additionalEvent === 'panup' || ev.additionalEvent === 'pandown') {
                const newTop = this._startPositionTop + ev.deltaY;
                if (newTop >= this.distanceTop) {
                    this._setTranslateY(newTop + 'px');
                }
                else if (newTop < this.distanceTop) {
                    this._setTranslateY(this.distanceTop + 'px');
                }
                if (newTop > this._platform.height() - this.minimumHeight) {
                    this._setTranslateY((this._platform.height() - this.minimumHeight) + 'px');
                }
            }
        }
    }
    _setTranslateY(value) {
        this._domCtrl.write(() => {
            this._renderer.setStyle(this._element.nativeElement, 'transform', 'translateY(' + value + ')');
        });
    }
};
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
], IonBottomDrawerComponent.prototype, "dockedHeight", void 0);
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
], IonBottomDrawerComponent.prototype, "shouldBounce", void 0);
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
], IonBottomDrawerComponent.prototype, "disableDrag", void 0);
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
], IonBottomDrawerComponent.prototype, "distanceTop", void 0);
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
], IonBottomDrawerComponent.prototype, "transition", void 0);
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Number)
], IonBottomDrawerComponent.prototype, "state", void 0);
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
], IonBottomDrawerComponent.prototype, "minimumHeight", void 0);
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
], IonBottomDrawerComponent.prototype, "stateChange", void 0);
IonBottomDrawerComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'ion-bottom-drawer',
        template: "<ion-content class=\"ion-bottom-drawer-scrollable-content\" no-bounce>\n  <ng-content></ng-content>\n</ion-content>\n",
        styles: [":host{width:100%;height:100%;position:absolute;left:0;z-index:11!important;background-color:#fff;-webkit-transform:translateY(100vh);transform:translateY(100vh)}"]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["DomController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]])
], IonBottomDrawerComponent);

let IonBottomDrawerModule = class IonBottomDrawerModule {
};
IonBottomDrawerModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [IonBottomDrawerComponent],
        imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"]],
        exports: [IonBottomDrawerComponent]
    })
], IonBottomDrawerModule);

/**
 * Generated bundle index. Do not edit.
 */


//# sourceMappingURL=ion-bottom-drawer.js.map


/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/index.js":
/*!********************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/index.js ***!
  \********************************************************/
/*! exports provided: IonicSelectableAddItemTemplateDirective, IonicSelectableCloseButtonTemplateDirective, IonicSelectableFooterTemplateDirective, IonicSelectableGroupEndTemplateDirective, IonicSelectableGroupTemplateDirective, IonicSelectableHeaderTemplateDirective, IonicSelectableItemEndTemplateDirective, IonicSelectableItemIconTemplateDirective, IonicSelectableItemTemplateDirective, IonicSelectableMessageTemplateDirective, IonicSelectableModalComponent, IonicSelectablePlaceholderTemplateDirective, IonicSelectableSearchFailTemplateDirective, IonicSelectableTitleTemplateDirective, IonicSelectableValueTemplateDirective, IonicSelectableIconTemplateDirective, IonicSelectableComponent, IonicSelectableModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./src/app/components/ionic-selectable/ionic-selectable.module */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableAddItemTemplateDirective", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableAddItemTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableCloseButtonTemplateDirective", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableCloseButtonTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableFooterTemplateDirective", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableFooterTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableGroupEndTemplateDirective", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableGroupEndTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableGroupTemplateDirective", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableGroupTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableHeaderTemplateDirective", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableHeaderTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableItemEndTemplateDirective", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableItemEndTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableItemIconTemplateDirective", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableItemIconTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableItemTemplateDirective", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableItemTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableMessageTemplateDirective", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableMessageTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableModalComponent", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableModalComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectablePlaceholderTemplateDirective", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectablePlaceholderTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableSearchFailTemplateDirective", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableSearchFailTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableTitleTemplateDirective", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableTitleTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableValueTemplateDirective", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableValueTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableIconTemplateDirective", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableIconTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableComponent", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableModule", function() { return _src_app_components_ionic_selectable_ionic_selectable_module__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableModule"]; });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9pb25pYy1zZWxlY3RhYmxlLyIsInNvdXJjZXMiOlsiaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLHFzQkFBYywrREFBK0QsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCAqIGZyb20gJy4vc3JjL2FwcC9jb21wb25lbnRzL2lvbmljLXNlbGVjdGFibGUvaW9uaWMtc2VsZWN0YWJsZS5tb2R1bGUnO1xuXG4iXX0=

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/ionic-selectable.min.js":
/*!***********************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/ionic-selectable.min.js ***!
  \***********************************************************************/
/*! exports provided: IonicSelectableAddItemTemplateDirective, IonicSelectableCloseButtonTemplateDirective, IonicSelectableFooterTemplateDirective, IonicSelectableGroupEndTemplateDirective, IonicSelectableGroupTemplateDirective, IonicSelectableHeaderTemplateDirective, IonicSelectableItemEndTemplateDirective, IonicSelectableItemIconTemplateDirective, IonicSelectableItemTemplateDirective, IonicSelectableMessageTemplateDirective, IonicSelectableModalComponent, IonicSelectablePlaceholderTemplateDirective, IonicSelectableSearchFailTemplateDirective, IonicSelectableTitleTemplateDirective, IonicSelectableValueTemplateDirective, IonicSelectableIconTemplateDirective, IonicSelectableComponent, IonicSelectableModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index */ "./node_modules/ionic-selectable/esm2015/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableAddItemTemplateDirective", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableAddItemTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableCloseButtonTemplateDirective", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableCloseButtonTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableFooterTemplateDirective", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableFooterTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableGroupEndTemplateDirective", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableGroupEndTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableGroupTemplateDirective", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableGroupTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableHeaderTemplateDirective", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableHeaderTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableItemEndTemplateDirective", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableItemEndTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableItemIconTemplateDirective", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableItemIconTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableItemTemplateDirective", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableItemTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableMessageTemplateDirective", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableMessageTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableModalComponent", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableModalComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectablePlaceholderTemplateDirective", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectablePlaceholderTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableSearchFailTemplateDirective", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableSearchFailTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableTitleTemplateDirective", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableTitleTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableValueTemplateDirective", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableValueTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableIconTemplateDirective", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableIconTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableComponent", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableModule", function() { return _index__WEBPACK_IMPORTED_MODULE_0__["IonicSelectableModule"]; });



/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-add-item-template.directive.js":
/*!***********************************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-add-item-template.directive.js ***!
  \***********************************************************************************************************************************/
/*! exports provided: IonicSelectableAddItemTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableAddItemTemplateDirective", function() { return IonicSelectableAddItemTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

class IonicSelectableAddItemTemplateDirective {
}
IonicSelectableAddItemTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ionicSelectableAddItemTemplate]',
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS1hZGQtaXRlbS10ZW1wbGF0ZS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9pb25pYy1zZWxlY3RhYmxlLyIsInNvdXJjZXMiOlsic3JjL2FwcC9jb21wb25lbnRzL2lvbmljLXNlbGVjdGFibGUvaW9uaWMtc2VsZWN0YWJsZS1hZGQtaXRlbS10ZW1wbGF0ZS5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFLMUMsTUFBTSxPQUFPLHVDQUF1Qzs7O1lBSG5ELFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsa0NBQWtDO2FBQzdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1tpb25pY1NlbGVjdGFibGVBZGRJdGVtVGVtcGxhdGVdJyxcbn0pXG5leHBvcnQgY2xhc3MgSW9uaWNTZWxlY3RhYmxlQWRkSXRlbVRlbXBsYXRlRGlyZWN0aXZlIHsgfVxuIl19

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-close-button-template.directive.js":
/*!***************************************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-close-button-template.directive.js ***!
  \***************************************************************************************************************************************/
/*! exports provided: IonicSelectableCloseButtonTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableCloseButtonTemplateDirective", function() { return IonicSelectableCloseButtonTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

class IonicSelectableCloseButtonTemplateDirective {
}
IonicSelectableCloseButtonTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ionicSelectableCloseButtonTemplate]',
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS1jbG9zZS1idXR0b24tdGVtcGxhdGUuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vaW9uaWMtc2VsZWN0YWJsZS8iLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9pb25pYy1zZWxlY3RhYmxlL2lvbmljLXNlbGVjdGFibGUtY2xvc2UtYnV0dG9uLXRlbXBsYXRlLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUsxQyxNQUFNLE9BQU8sMkNBQTJDOzs7WUFIdkQsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxzQ0FBc0M7YUFDakQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW2lvbmljU2VsZWN0YWJsZUNsb3NlQnV0dG9uVGVtcGxhdGVdJyxcbn0pXG5leHBvcnQgY2xhc3MgSW9uaWNTZWxlY3RhYmxlQ2xvc2VCdXR0b25UZW1wbGF0ZURpcmVjdGl2ZSB7IH1cbiJdfQ==

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-footer-template.directive.js":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-footer-template.directive.js ***!
  \*********************************************************************************************************************************/
/*! exports provided: IonicSelectableFooterTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableFooterTemplateDirective", function() { return IonicSelectableFooterTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

class IonicSelectableFooterTemplateDirective {
}
IonicSelectableFooterTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ionicSelectableFooterTemplate]',
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS1mb290ZXItdGVtcGxhdGUuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vaW9uaWMtc2VsZWN0YWJsZS8iLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9pb25pYy1zZWxlY3RhYmxlL2lvbmljLXNlbGVjdGFibGUtZm9vdGVyLXRlbXBsYXRlLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUsxQyxNQUFNLE9BQU8sc0NBQXNDOzs7WUFIbEQsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxpQ0FBaUM7YUFDNUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW2lvbmljU2VsZWN0YWJsZUZvb3RlclRlbXBsYXRlXScsXG59KVxuZXhwb3J0IGNsYXNzIElvbmljU2VsZWN0YWJsZUZvb3RlclRlbXBsYXRlRGlyZWN0aXZlIHsgfVxuIl19

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-group-end-template.directive.js":
/*!************************************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-group-end-template.directive.js ***!
  \************************************************************************************************************************************/
/*! exports provided: IonicSelectableGroupEndTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableGroupEndTemplateDirective", function() { return IonicSelectableGroupEndTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

class IonicSelectableGroupEndTemplateDirective {
}
IonicSelectableGroupEndTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ionicSelectableGroupEndTemplate]',
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS1ncm91cC1lbmQtdGVtcGxhdGUuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vaW9uaWMtc2VsZWN0YWJsZS8iLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9pb25pYy1zZWxlY3RhYmxlL2lvbmljLXNlbGVjdGFibGUtZ3JvdXAtZW5kLXRlbXBsYXRlLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUsxQyxNQUFNLE9BQU8sd0NBQXdDOzs7WUFIcEQsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxtQ0FBbUM7YUFDOUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW2lvbmljU2VsZWN0YWJsZUdyb3VwRW5kVGVtcGxhdGVdJyxcbn0pXG5leHBvcnQgY2xhc3MgSW9uaWNTZWxlY3RhYmxlR3JvdXBFbmRUZW1wbGF0ZURpcmVjdGl2ZSB7IH1cbiJdfQ==

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-group-template.directive.js":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-group-template.directive.js ***!
  \********************************************************************************************************************************/
/*! exports provided: IonicSelectableGroupTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableGroupTemplateDirective", function() { return IonicSelectableGroupTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

class IonicSelectableGroupTemplateDirective {
}
IonicSelectableGroupTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ionicSelectableGroupTemplate]',
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS1ncm91cC10ZW1wbGF0ZS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9pb25pYy1zZWxlY3RhYmxlLyIsInNvdXJjZXMiOlsic3JjL2FwcC9jb21wb25lbnRzL2lvbmljLXNlbGVjdGFibGUvaW9uaWMtc2VsZWN0YWJsZS1ncm91cC10ZW1wbGF0ZS5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFLMUMsTUFBTSxPQUFPLHFDQUFxQzs7O1lBSGpELFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZ0NBQWdDO2FBQzNDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1tpb25pY1NlbGVjdGFibGVHcm91cFRlbXBsYXRlXScsXG59KVxuZXhwb3J0IGNsYXNzIElvbmljU2VsZWN0YWJsZUdyb3VwVGVtcGxhdGVEaXJlY3RpdmUgeyB9XG4iXX0=

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-header-template.directive.js":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-header-template.directive.js ***!
  \*********************************************************************************************************************************/
/*! exports provided: IonicSelectableHeaderTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableHeaderTemplateDirective", function() { return IonicSelectableHeaderTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

class IonicSelectableHeaderTemplateDirective {
}
IonicSelectableHeaderTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ionicSelectableHeaderTemplate]',
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS1oZWFkZXItdGVtcGxhdGUuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vaW9uaWMtc2VsZWN0YWJsZS8iLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9pb25pYy1zZWxlY3RhYmxlL2lvbmljLXNlbGVjdGFibGUtaGVhZGVyLXRlbXBsYXRlLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUsxQyxNQUFNLE9BQU8sc0NBQXNDOzs7WUFIbEQsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxpQ0FBaUM7YUFDNUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW2lvbmljU2VsZWN0YWJsZUhlYWRlclRlbXBsYXRlXScsXG59KVxuZXhwb3J0IGNsYXNzIElvbmljU2VsZWN0YWJsZUhlYWRlclRlbXBsYXRlRGlyZWN0aXZlIHsgfVxuIl19

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-icon-template.directive.js":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-icon-template.directive.js ***!
  \*******************************************************************************************************************************/
/*! exports provided: IonicSelectableIconTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableIconTemplateDirective", function() { return IonicSelectableIconTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

class IonicSelectableIconTemplateDirective {
}
IonicSelectableIconTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ionicSelectableIconTemplate]'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS1pY29uLXRlbXBsYXRlLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2lvbmljLXNlbGVjdGFibGUvIiwic291cmNlcyI6WyJzcmMvYXBwL2NvbXBvbmVudHMvaW9uaWMtc2VsZWN0YWJsZS9pb25pYy1zZWxlY3RhYmxlLWljb24tdGVtcGxhdGUuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBSzFDLE1BQU0sT0FBTyxvQ0FBb0M7OztZQUhoRCxTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLCtCQUErQjthQUMxQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbaW9uaWNTZWxlY3RhYmxlSWNvblRlbXBsYXRlXSdcbn0pXG5leHBvcnQgY2xhc3MgSW9uaWNTZWxlY3RhYmxlSWNvblRlbXBsYXRlRGlyZWN0aXZlIHsgfVxuIl19

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-item-end-template.directive.js":
/*!***********************************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-item-end-template.directive.js ***!
  \***********************************************************************************************************************************/
/*! exports provided: IonicSelectableItemEndTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableItemEndTemplateDirective", function() { return IonicSelectableItemEndTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

class IonicSelectableItemEndTemplateDirective {
}
IonicSelectableItemEndTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ionicSelectableItemEndTemplate]',
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS1pdGVtLWVuZC10ZW1wbGF0ZS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9pb25pYy1zZWxlY3RhYmxlLyIsInNvdXJjZXMiOlsic3JjL2FwcC9jb21wb25lbnRzL2lvbmljLXNlbGVjdGFibGUvaW9uaWMtc2VsZWN0YWJsZS1pdGVtLWVuZC10ZW1wbGF0ZS5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFLMUMsTUFBTSxPQUFPLHVDQUF1Qzs7O1lBSG5ELFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsa0NBQWtDO2FBQzdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1tpb25pY1NlbGVjdGFibGVJdGVtRW5kVGVtcGxhdGVdJyxcbn0pXG5leHBvcnQgY2xhc3MgSW9uaWNTZWxlY3RhYmxlSXRlbUVuZFRlbXBsYXRlRGlyZWN0aXZlIHsgfVxuIl19

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-item-icon-template.directive.js":
/*!************************************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-item-icon-template.directive.js ***!
  \************************************************************************************************************************************/
/*! exports provided: IonicSelectableItemIconTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableItemIconTemplateDirective", function() { return IonicSelectableItemIconTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

class IonicSelectableItemIconTemplateDirective {
}
IonicSelectableItemIconTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ionicSelectableItemIconTemplate]'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS1pdGVtLWljb24tdGVtcGxhdGUuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vaW9uaWMtc2VsZWN0YWJsZS8iLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9pb25pYy1zZWxlY3RhYmxlL2lvbmljLXNlbGVjdGFibGUtaXRlbS1pY29uLXRlbXBsYXRlLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUsxQyxNQUFNLE9BQU8sd0NBQXdDOzs7WUFIcEQsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxtQ0FBbUM7YUFDOUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW2lvbmljU2VsZWN0YWJsZUl0ZW1JY29uVGVtcGxhdGVdJ1xufSlcbmV4cG9ydCBjbGFzcyBJb25pY1NlbGVjdGFibGVJdGVtSWNvblRlbXBsYXRlRGlyZWN0aXZlIHsgfVxuIl19

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-item-template.directive.js":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-item-template.directive.js ***!
  \*******************************************************************************************************************************/
/*! exports provided: IonicSelectableItemTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableItemTemplateDirective", function() { return IonicSelectableItemTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

class IonicSelectableItemTemplateDirective {
}
IonicSelectableItemTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ionicSelectableItemTemplate]'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS1pdGVtLXRlbXBsYXRlLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2lvbmljLXNlbGVjdGFibGUvIiwic291cmNlcyI6WyJzcmMvYXBwL2NvbXBvbmVudHMvaW9uaWMtc2VsZWN0YWJsZS9pb25pYy1zZWxlY3RhYmxlLWl0ZW0tdGVtcGxhdGUuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBSzFDLE1BQU0sT0FBTyxvQ0FBb0M7OztZQUhoRCxTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLCtCQUErQjthQUMxQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbaW9uaWNTZWxlY3RhYmxlSXRlbVRlbXBsYXRlXSdcbn0pXG5leHBvcnQgY2xhc3MgSW9uaWNTZWxlY3RhYmxlSXRlbVRlbXBsYXRlRGlyZWN0aXZlIHsgfVxuIl19

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-message-template.directive.js":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-message-template.directive.js ***!
  \**********************************************************************************************************************************/
/*! exports provided: IonicSelectableMessageTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableMessageTemplateDirective", function() { return IonicSelectableMessageTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

class IonicSelectableMessageTemplateDirective {
}
IonicSelectableMessageTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ionicSelectableMessageTemplate]',
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS1tZXNzYWdlLXRlbXBsYXRlLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2lvbmljLXNlbGVjdGFibGUvIiwic291cmNlcyI6WyJzcmMvYXBwL2NvbXBvbmVudHMvaW9uaWMtc2VsZWN0YWJsZS9pb25pYy1zZWxlY3RhYmxlLW1lc3NhZ2UtdGVtcGxhdGUuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBSzFDLE1BQU0sT0FBTyx1Q0FBdUM7OztZQUhuRCxTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGtDQUFrQzthQUM3QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbaW9uaWNTZWxlY3RhYmxlTWVzc2FnZVRlbXBsYXRlXScsXG59KVxuZXhwb3J0IGNsYXNzIElvbmljU2VsZWN0YWJsZU1lc3NhZ2VUZW1wbGF0ZURpcmVjdGl2ZSB7IH1cbiJdfQ==

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-modal.component.js":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-modal.component.js ***!
  \***********************************************************************************************************************/
/*! exports provided: IonicSelectableModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableModalComponent", function() { return IonicSelectableModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */


class IonicSelectableModalComponent {
    /**
     * @param {?} navParams
     * @param {?} _element
     */
    constructor(navParams, _element) {
        this.navParams = navParams;
        this._element = _element;
        this._cssClass = true;
        this.selectComponent = this.navParams.get('selectComponent');
        this.selectComponent._modalComponent = this;
        this.selectComponent._selectedItems = [];
        if (!this.selectComponent._isNullOrWhiteSpace(this.selectComponent.value)) {
            if (this.selectComponent.isMultiple) {
                this.selectComponent.value.forEach((/**
                 * @param {?} item
                 * @return {?}
                 */
                item => {
                    this.selectComponent._selectedItems.push(item);
                }));
            }
            else {
                this.selectComponent._selectedItems.push(this.selectComponent.value);
            }
        }
        this.selectComponent._setItemsToConfirm(this.selectComponent._selectedItems);
    }
    /**
     * @return {?}
     */
    get _canClearCssClass() {
        return this.selectComponent.canClear;
    }
    /**
     * @return {?}
     */
    get _isMultipleCssClass() {
        return this.selectComponent.isMultiple;
    }
    /**
     * @return {?}
     */
    get _isSearchingCssClass() {
        return this.selectComponent._isSearching;
    }
    /**
     * @return {?}
     */
    get _isIos() {
        return this.selectComponent._isIos;
    }
    /**
     * @return {?}
     */
    _isMD() {
        return this.selectComponent._isMD;
    }
    /**
     * @return {?}
     */
    get _isAddItemTemplateVisibleCssClass() {
        return this.selectComponent._isAddItemTemplateVisible;
    }
    /**
     * @return {?}
     */
    onResize() {
        // ion-footer inside the template might change its height when
        // device orientation changes.
        this.selectComponent._positionAddItemTemplate();
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this._header = this._element.nativeElement.querySelector('ion-header');
        if (this._searchbarComponent && this.selectComponent.shouldFocusSearchbar) {
            // Focus after a delay because focus doesn't work without it.
            setTimeout((/**
             * @return {?}
             */
            () => {
                this._searchbarComponent.setFocus();
            }), 1000);
        }
    }
}
IonicSelectableModalComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: 'ionic-selectable-modal',
                template: "<ion-header>\n  <ion-toolbar *ngIf=\"!selectComponent.headerTemplate\"\n    [color]=\"selectComponent.headerColor ? selectComponent.headerColor : null\">\n    <ion-buttons [slot]=\"selectComponent.closeButtonSlot\">\n      <ion-button (click)=\"selectComponent._close()\">\n        <span *ngIf=\"selectComponent.closeButtonTemplate\"\n          [ngTemplateOutlet]=\"selectComponent.closeButtonTemplate\">\n        </span>\n        <span *ngIf=\"!selectComponent.closeButtonTemplate\">\n          {{selectComponent.closeButtonText}}\n        </span>\n      </ion-button>\n    </ion-buttons>\n    <ion-title>\n      <!-- Need span for for text ellipsis. -->\n      <span *ngIf=\"selectComponent.titleTemplate\"\n        [ngTemplateOutlet]=\"selectComponent.titleTemplate\">\n      </span>\n      <span *ngIf=\"!selectComponent.titleTemplate\">\n        {{selectComponent.label}}\n      </span>\n    </ion-title>\n  </ion-toolbar>\n  <div *ngIf=\"selectComponent.headerTemplate\"\n    [ngTemplateOutlet]=\"selectComponent.headerTemplate\">\n  </div>\n  <ion-toolbar\n    *ngIf=\"selectComponent.canSearch || selectComponent.messageTemplate\">\n    <ion-searchbar *ngIf=\"selectComponent.canSearch\" #searchbarComponent\n      [(ngModel)]=\"selectComponent._searchText\"\n      (ionChange)=\"selectComponent._filterItems()\"\n      (ionClear)=\"selectComponent._onSearchbarClear()\"\n      [placeholder]=\"selectComponent.searchPlaceholder\"\n      [debounce]=\"selectComponent.searchDebounce\">\n    </ion-searchbar>\n    <div class=\"ionic-selectable-message\"\n      *ngIf=\"selectComponent.messageTemplate\">\n      <div [ngTemplateOutlet]=\"selectComponent.messageTemplate\">\n      </div>\n    </div>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <div class=\"ionic-selectable-spinner\" *ngIf=\"selectComponent._isSearching\">\n    <div class=\"ionic-selectable-spinner-background\"></div>\n    <ion-spinner></ion-spinner>\n  </div>\n  <ion-list class=\"ion-no-margin\"\n    *ngIf=\"!selectComponent.hasVirtualScroll && selectComponent._hasFilteredItems\">\n    <ion-item-group *ngFor=\"let group of selectComponent._filteredGroups\"\n      class=\"ionic-selectable-group\">\n      <ion-item-divider *ngIf=\"selectComponent._hasGroups\"\n        [color]=\"selectComponent.groupColor ? selectComponent.groupColor : null\">\n        <!-- Need span for for text ellipsis. -->\n        <span *ngIf=\"selectComponent.groupTemplate\"\n          [ngTemplateOutlet]=\"selectComponent.groupTemplate\"\n          [ngTemplateOutletContext]=\"{ group: group }\">\n        </span>\n        <!-- Need ion-label for text ellipsis. -->\n        <ion-label *ngIf=\"!selectComponent.groupTemplate\">\n          {{group.text}}\n        </ion-label>\n        <div *ngIf=\"selectComponent.groupEndTemplate\" slot=\"end\">\n          <div [ngTemplateOutlet]=\"selectComponent.groupEndTemplate\"\n            [ngTemplateOutletContext]=\"{ group: group }\">\n          </div>\n        </div>\n      </ion-item-divider>\n      <ion-item button=\"true\" detail=\"false\" *ngFor=\"let item of group.items\"\n        (click)=\"selectComponent._select(item)\" class=\"ionic-selectable-item\"\n        [ngClass]=\"{\n          'ionic-selectable-item-is-selected': selectComponent._isItemSelected(item),\n          'ionic-selectable-item-is-disabled': selectComponent._isItemDisabled(item)\n        }\" [disabled]=\"selectComponent._isItemDisabled(item)\">\n        <!-- Need span for text ellipsis. -->\n        <span *ngIf=\"selectComponent.itemTemplate\"\n          [ngTemplateOutlet]=\"selectComponent.itemTemplate\"\n          [ngTemplateOutletContext]=\"{ item: item, isItemSelected: selectComponent._isItemSelected(item) }\">\n        </span>\n        <!-- Need ion-label for text ellipsis. -->\n        <ion-label *ngIf=\"!selectComponent.itemTemplate\">\n          {{selectComponent._formatItem(item)}}\n        </ion-label>\n        <div *ngIf=\"selectComponent.itemEndTemplate\" slot=\"end\">\n          <div [ngTemplateOutlet]=\"selectComponent.itemEndTemplate\"\n            [ngTemplateOutletContext]=\"{ item: item, isItemSelected: selectComponent._isItemSelected(item) }\">\n          </div>\n        </div>\n        <span *ngIf=\"selectComponent.itemIconTemplate\"\n          [ngTemplateOutlet]=\"selectComponent.itemIconTemplate\"\n          [ngTemplateOutletContext]=\"{ item: item, isItemSelected: selectComponent._isItemSelected(item) }\">\n        </span>\n        <ion-icon *ngIf=\"!selectComponent.itemIconTemplate\"\n          [name]=\"selectComponent._isItemSelected(item) ? 'checkmark-circle' : 'radio-button-off'\"\n          [color]=\"selectComponent._isItemSelected(item) ? 'primary' : null\"\n          [slot]=\"selectComponent.itemIconSlot\">\n        </ion-icon>\n        <ion-button *ngIf=\"selectComponent.canSaveItem\"\n          class=\"ionic-selectable-item-button\" slot=\"end\" fill=\"outline\"\n          (click)=\"selectComponent._saveItem($event, item)\">\n          <ion-icon slot=\"icon-only\" name=\"md-create\"></ion-icon>\n        </ion-button>\n        <ion-button *ngIf=\"selectComponent.canDeleteItem\"\n          class=\"ionic-selectable-item-button\" slot=\"end\" fill=\"outline\"\n          (click)=\"selectComponent._deleteItemClick($event, item)\">\n          <ion-icon slot=\"icon-only\" name=\"md-trash\"></ion-icon>\n        </ion-button>\n      </ion-item>\n    </ion-item-group>\n  </ion-list>\n  <!-- Fail text should be above InfiniteScroll to avoid a gap when no items are found. -->\n  <div *ngIf=\"!selectComponent._hasFilteredItems\">\n    <span *ngIf=\"selectComponent.searchFailTemplate\"\n      [ngTemplateOutlet]=\"selectComponent.searchFailTemplate\">\n    </span>\n    <div *ngIf=\"!selectComponent.searchFailTemplate\" class=\"ion-margin\">\n      {{selectComponent.searchFailText}}\n    </div>\n  </div>\n  <ion-infinite-scroll *ngIf=\"!selectComponent.hasVirtualScroll\"\n    [disabled]=\"!selectComponent.hasInfiniteScroll\"\n    (ionInfinite)=\"selectComponent._getMoreItems()\">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n  <ion-virtual-scroll class=\"ion-no-margin\"\n    *ngIf=\"selectComponent.hasVirtualScroll && selectComponent._hasFilteredItems\"\n    [items]=\"selectComponent._filteredGroups[0].items\"\n    [headerFn]=\"selectComponent.virtualScrollHeaderFn\"\n    [approxItemHeight]=\"selectComponent.virtualScrollApproxItemHeight\">\n    <ion-item-divider *virtualHeader=\"let header\"\n      [color]=\"selectComponent.groupColor ? selectComponent.groupColor : null\">\n      {{header}}\n    </ion-item-divider>\n    <ion-item button=\"true\" detail=\"false\" *virtualItem=\"let item\"\n      (click)=\"selectComponent._select(item)\" class=\"ionic-selectable-item\"\n      [ngClass]=\"{\n        'ionic-selectable-item-is-selected': selectComponent._isItemSelected(item),\n        'ionic-selectable-item-is-disabled': selectComponent._isItemDisabled(item)\n      }\" [disabled]=\"selectComponent._isItemDisabled(item)\">\n      <!-- Need span for text ellipsis. -->\n      <span *ngIf=\"selectComponent.itemTemplate\"\n        [ngTemplateOutlet]=\"selectComponent.itemTemplate\"\n        [ngTemplateOutletContext]=\"{ item: item, isItemSelected: selectComponent._isItemSelected(item) }\">\n      </span>\n      <!-- Need ion-label for text ellipsis. -->\n      <ion-label *ngIf=\"!selectComponent.itemTemplate\">\n        {{selectComponent._formatItem(item)}}\n      </ion-label>\n      <div *ngIf=\"selectComponent.itemEndTemplate\" slot=\"end\">\n        <div [ngTemplateOutlet]=\"selectComponent.itemEndTemplate\"\n          [ngTemplateOutletContext]=\"{ item: item, isItemSelected: selectComponent._isItemSelected(item) }\">\n        </div>\n      </div>\n      <span *ngIf=\"selectComponent.itemIconTemplate\"\n        [ngTemplateOutlet]=\"selectComponent.itemIconTemplate\"\n        [ngTemplateOutletContext]=\"{ item: item, isItemSelected: selectComponent._isItemSelected(item) }\">\n      </span>\n      <ion-icon *ngIf=\"!selectComponent.itemIconTemplate\"\n        [name]=\"selectComponent._isItemSelected(item) ? 'checkmark-circle' : 'radio-button-off'\"\n        [color]=\"selectComponent._isItemSelected(item) ? 'primary' : null\"\n        [slot]=\"selectComponent.itemIconSlot\">\n      </ion-icon>\n      <ion-button *ngIf=\"selectComponent.canSaveItem\"\n        class=\"ionic-selectable-item-button\" slot=\"end\" fill=\"outline\"\n        (click)=\"selectComponent._saveItem($event, item)\">\n        <ion-icon slot=\"icon-only\" name=\"md-create\"></ion-icon>\n      </ion-button>\n      <ion-button *ngIf=\"selectComponent.canDeleteItem\"\n        class=\"ionic-selectable-item-button\" slot=\"end\" fill=\"outline\"\n        (click)=\"selectComponent._deleteItemClick($event, item)\">\n        <ion-icon slot=\"icon-only\" name=\"md-trash\"></ion-icon>\n      </ion-button>\n    </ion-item>\n  </ion-virtual-scroll>\n</ion-content>\n<div class=\"ionic-selectable-add-item-template\"\n  *ngIf=\"selectComponent._isAddItemTemplateVisible\"\n  [ngStyle]=\"{ 'top.px': _header.offsetHeight }\">\n  <div class=\"ionic-selectable-add-item-template-inner\"\n    [ngStyle]=\"{ 'height': selectComponent._addItemTemplateFooterHeight }\">\n    <span [ngTemplateOutlet]=\"selectComponent.addItemTemplate\"\n      [ngTemplateOutletContext]=\"{ item: selectComponent._itemToAdd, isAdd: selectComponent._itemToAdd === null }\">\n    </span>\n  </div>\n</div>\n<ion-footer\n  *ngIf=\"selectComponent._footerButtonsCount > 0 || selectComponent.footerTemplate\"\n  [ngStyle]=\"{ 'visibility': selectComponent._isFooterVisible ? 'initial' : 'hidden' }\">\n  <ion-toolbar *ngIf=\"!selectComponent.footerTemplate\">\n    <ion-row>\n      <ion-col *ngIf=\"selectComponent.canClear\">\n        <ion-button expand=\"full\" (click)=\"selectComponent._clear()\"\n          [disabled]=\"!selectComponent._selectedItems.length\">\n          {{selectComponent.clearButtonText}}\n        </ion-button>\n      </ion-col>\n      <ion-col *ngIf=\"selectComponent.canAddItem\">\n        <ion-button expand=\"full\" (click)=\"selectComponent._addItemClick()\">\n          {{selectComponent.addButtonText}}\n        </ion-button>\n      </ion-col>\n      <ion-col\n        *ngIf=\"selectComponent.isMultiple || selectComponent.hasConfirmButton\">\n        <ion-button expand=\"full\" (click)=\"selectComponent._confirm()\"\n          [disabled]=\"!selectComponent.isConfirmButtonEnabled\">\n          {{selectComponent.confirmButtonText}}\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n  <div *ngIf=\"selectComponent.footerTemplate\"\n    [ngTemplateOutlet]=\"selectComponent.footerTemplate\">\n  </div>\n</ion-footer>\n"
            }] }
];
/** @nocollapse */
IonicSelectableModalComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavParams"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] }
];
IonicSelectableModalComponent.propDecorators = {
    _content: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonContent"], { static: false },] }],
    _searchbarComponent: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['searchbarComponent', { static: false },] }],
    _infiniteScroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonInfiniteScroll"], { static: false },] }],
    _cssClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-modal',] }],
    _canClearCssClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-modal-can-clear',] }],
    _isMultipleCssClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-modal-is-multiple',] }],
    _isSearchingCssClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-modal-is-searching',] }],
    _isIos: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-modal-ios',] }],
    _isMD: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-modal-md',] }],
    _isAddItemTemplateVisibleCssClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-modal-is-add-item-template-visible',] }],
    onResize: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['window:resize',] }]
};
if (false) {}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS1tb2RhbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9pb25pYy1zZWxlY3RhYmxlLyIsInNvdXJjZXMiOlsic3JjL2FwcC9jb21wb25lbnRzL2lvbmljLXNlbGVjdGFibGUvaW9uaWMtc2VsZWN0YWJsZS1tb2RhbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBaUIsU0FBUyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzRyxPQUFPLEVBQUUsVUFBVSxFQUFFLGlCQUFpQixFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQU94RixNQUFNLE9BQU8sNkJBQTZCOzs7OztJQTBDeEMsWUFDVSxTQUFvQixFQUNyQixRQUFvQjtRQURuQixjQUFTLEdBQVQsU0FBUyxDQUFXO1FBQ3JCLGFBQVEsR0FBUixRQUFRLENBQVk7UUFsQzdCLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFvQ2YsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQzdELElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUM1QyxJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7UUFFekMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN6RSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxFQUFFO2dCQUNuQyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxPQUFPOzs7O2dCQUFDLElBQUksQ0FBQyxFQUFFO29CQUN4QyxJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2pELENBQUMsRUFBQyxDQUFDO2FBQ0o7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDdEU7U0FDRjtRQUVELElBQUksQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUMvRSxDQUFDOzs7O0lBbERELElBQ0ksaUJBQWlCO1FBQ25CLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUM7SUFDdkMsQ0FBQzs7OztJQUNELElBQ0ksbUJBQW1CO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUM7SUFDekMsQ0FBQzs7OztJQUNELElBQ0ksb0JBQW9CO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUM7SUFDM0MsQ0FBQzs7OztJQUNELElBQ0ksTUFBTTtRQUNSLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUM7SUFDckMsQ0FBQzs7OztJQUVELEtBQUs7UUFDSCxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDO0lBQ3BDLENBQUM7Ozs7SUFDRCxJQUNJLGlDQUFpQztRQUNuQyxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMseUJBQXlCLENBQUM7SUFDeEQsQ0FBQzs7OztJQUVELFFBQVE7UUFDTiw4REFBOEQ7UUFDOUQsOEJBQThCO1FBQzlCLElBQUksQ0FBQyxlQUFlLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztJQUNsRCxDQUFDOzs7O0lBdUJELGVBQWU7UUFDYixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUV2RSxJQUFJLElBQUksQ0FBQyxtQkFBbUIsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLG9CQUFvQixFQUFFO1lBQ3pFLDZEQUE2RDtZQUM3RCxVQUFVOzs7WUFBQyxHQUFHLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RDLENBQUMsR0FBRSxJQUFJLENBQUMsQ0FBQztTQUNWO0lBQ0gsQ0FBQzs7O1lBNUVGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsd0JBQXdCO2dCQUNsQywwZ1ZBQXNEO2FBQ3ZEOzs7O1lBTnFELFNBQVM7WUFENUIsVUFBVTs7O3VCQVMxQyxTQUFTLFNBQUMsVUFBVSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTtrQ0FJdkMsU0FBUyxTQUFDLG9CQUFvQixFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTs4QkFFakQsU0FBUyxTQUFDLGlCQUFpQixFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTt3QkFFOUMsV0FBVyxTQUFDLDhCQUE4QjtnQ0FFMUMsV0FBVyxTQUFDLHdDQUF3QztrQ0FJcEQsV0FBVyxTQUFDLDBDQUEwQzttQ0FJdEQsV0FBVyxTQUFDLDJDQUEyQztxQkFJdkQsV0FBVyxTQUFDLGtDQUFrQztvQkFJOUMsV0FBVyxTQUFDLGlDQUFpQztnREFJN0MsV0FBVyxTQUFDLDJEQUEyRDt1QkFJdkUsWUFBWSxTQUFDLGVBQWU7Ozs7SUFsQzdCLGlEQUNxQjs7SUFDckIsZ0RBQXFCOztJQUNyQix3REFBMEM7O0lBQzFDLDREQUNrQzs7SUFDbEMsd0RBQ21DOztJQUNuQyxrREFDaUI7Ozs7O0lBaUNmLGtEQUE0Qjs7SUFDNUIsaURBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWZ0ZXJWaWV3SW5pdCwgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBIb3N0QmluZGluZywgSG9zdExpc3RlbmVyLCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IElvbkNvbnRlbnQsIElvbkluZmluaXRlU2Nyb2xsLCBJb25TZWFyY2hiYXIsIE5hdlBhcmFtcyB9IGZyb20gJ0Bpb25pYy9hbmd1bGFyJztcbmltcG9ydCB7IElvbmljU2VsZWN0YWJsZUNvbXBvbmVudCB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS5jb21wb25lbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdpb25pYy1zZWxlY3RhYmxlLW1vZGFsJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2lvbmljLXNlbGVjdGFibGUtbW9kYWwuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIElvbmljU2VsZWN0YWJsZU1vZGFsQ29tcG9uZW50IGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCB7XG4gIEBWaWV3Q2hpbGQoSW9uQ29udGVudCwgeyBzdGF0aWM6IGZhbHNlIH0pXG4gIF9jb250ZW50OiBJb25Db250ZW50O1xuICBfaGVhZGVyOiBIVE1MRWxlbWVudDtcbiAgc2VsZWN0Q29tcG9uZW50OiBJb25pY1NlbGVjdGFibGVDb21wb25lbnQ7XG4gIEBWaWV3Q2hpbGQoJ3NlYXJjaGJhckNvbXBvbmVudCcsIHsgc3RhdGljOiBmYWxzZSB9KVxuICBfc2VhcmNoYmFyQ29tcG9uZW50OiBJb25TZWFyY2hiYXI7XG4gIEBWaWV3Q2hpbGQoSW9uSW5maW5pdGVTY3JvbGwsIHsgc3RhdGljOiBmYWxzZSB9KVxuICBfaW5maW5pdGVTY3JvbGw6IElvbkluZmluaXRlU2Nyb2xsO1xuICBASG9zdEJpbmRpbmcoJ2NsYXNzLmlvbmljLXNlbGVjdGFibGUtbW9kYWwnKVxuICBfY3NzQ2xhc3MgPSB0cnVlO1xuICBASG9zdEJpbmRpbmcoJ2NsYXNzLmlvbmljLXNlbGVjdGFibGUtbW9kYWwtY2FuLWNsZWFyJylcbiAgZ2V0IF9jYW5DbGVhckNzc0NsYXNzKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLnNlbGVjdENvbXBvbmVudC5jYW5DbGVhcjtcbiAgfVxuICBASG9zdEJpbmRpbmcoJ2NsYXNzLmlvbmljLXNlbGVjdGFibGUtbW9kYWwtaXMtbXVsdGlwbGUnKVxuICBnZXQgX2lzTXVsdGlwbGVDc3NDbGFzcygpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5zZWxlY3RDb21wb25lbnQuaXNNdWx0aXBsZTtcbiAgfVxuICBASG9zdEJpbmRpbmcoJ2NsYXNzLmlvbmljLXNlbGVjdGFibGUtbW9kYWwtaXMtc2VhcmNoaW5nJylcbiAgZ2V0IF9pc1NlYXJjaGluZ0Nzc0NsYXNzKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLnNlbGVjdENvbXBvbmVudC5faXNTZWFyY2hpbmc7XG4gIH1cbiAgQEhvc3RCaW5kaW5nKCdjbGFzcy5pb25pYy1zZWxlY3RhYmxlLW1vZGFsLWlvcycpXG4gIGdldCBfaXNJb3MoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuc2VsZWN0Q29tcG9uZW50Ll9pc0lvcztcbiAgfVxuICBASG9zdEJpbmRpbmcoJ2NsYXNzLmlvbmljLXNlbGVjdGFibGUtbW9kYWwtbWQnKVxuICBfaXNNRCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5zZWxlY3RDb21wb25lbnQuX2lzTUQ7XG4gIH1cbiAgQEhvc3RCaW5kaW5nKCdjbGFzcy5pb25pYy1zZWxlY3RhYmxlLW1vZGFsLWlzLWFkZC1pdGVtLXRlbXBsYXRlLXZpc2libGUnKVxuICBnZXQgX2lzQWRkSXRlbVRlbXBsYXRlVmlzaWJsZUNzc0NsYXNzKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLnNlbGVjdENvbXBvbmVudC5faXNBZGRJdGVtVGVtcGxhdGVWaXNpYmxlO1xuICB9XG4gIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzpyZXNpemUnKVxuICBvblJlc2l6ZSgpIHtcbiAgICAvLyBpb24tZm9vdGVyIGluc2lkZSB0aGUgdGVtcGxhdGUgbWlnaHQgY2hhbmdlIGl0cyBoZWlnaHQgd2hlblxuICAgIC8vIGRldmljZSBvcmllbnRhdGlvbiBjaGFuZ2VzLlxuICAgIHRoaXMuc2VsZWN0Q29tcG9uZW50Ll9wb3NpdGlvbkFkZEl0ZW1UZW1wbGF0ZSgpO1xuICB9XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBuYXZQYXJhbXM6IE5hdlBhcmFtcyxcbiAgICBwdWJsaWMgX2VsZW1lbnQ6IEVsZW1lbnRSZWYsXG4gICkge1xuICAgIHRoaXMuc2VsZWN0Q29tcG9uZW50ID0gdGhpcy5uYXZQYXJhbXMuZ2V0KCdzZWxlY3RDb21wb25lbnQnKTtcbiAgICB0aGlzLnNlbGVjdENvbXBvbmVudC5fbW9kYWxDb21wb25lbnQgPSB0aGlzO1xuICAgIHRoaXMuc2VsZWN0Q29tcG9uZW50Ll9zZWxlY3RlZEl0ZW1zID0gW107XG5cbiAgICBpZiAoIXRoaXMuc2VsZWN0Q29tcG9uZW50Ll9pc051bGxPcldoaXRlU3BhY2UodGhpcy5zZWxlY3RDb21wb25lbnQudmFsdWUpKSB7XG4gICAgICBpZiAodGhpcy5zZWxlY3RDb21wb25lbnQuaXNNdWx0aXBsZSkge1xuICAgICAgICB0aGlzLnNlbGVjdENvbXBvbmVudC52YWx1ZS5mb3JFYWNoKGl0ZW0gPT4ge1xuICAgICAgICAgIHRoaXMuc2VsZWN0Q29tcG9uZW50Ll9zZWxlY3RlZEl0ZW1zLnB1c2goaXRlbSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5zZWxlY3RDb21wb25lbnQuX3NlbGVjdGVkSXRlbXMucHVzaCh0aGlzLnNlbGVjdENvbXBvbmVudC52YWx1ZSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdGhpcy5zZWxlY3RDb21wb25lbnQuX3NldEl0ZW1zVG9Db25maXJtKHRoaXMuc2VsZWN0Q29tcG9uZW50Ll9zZWxlY3RlZEl0ZW1zKTtcbiAgfVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcbiAgICB0aGlzLl9oZWFkZXIgPSB0aGlzLl9lbGVtZW50Lm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignaW9uLWhlYWRlcicpO1xuXG4gICAgaWYgKHRoaXMuX3NlYXJjaGJhckNvbXBvbmVudCAmJiB0aGlzLnNlbGVjdENvbXBvbmVudC5zaG91bGRGb2N1c1NlYXJjaGJhcikge1xuICAgICAgLy8gRm9jdXMgYWZ0ZXIgYSBkZWxheSBiZWNhdXNlIGZvY3VzIGRvZXNuJ3Qgd29yayB3aXRob3V0IGl0LlxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIHRoaXMuX3NlYXJjaGJhckNvbXBvbmVudC5zZXRGb2N1cygpO1xuICAgICAgfSwgMTAwMCk7XG4gICAgfVxuICB9XG59XG4iXX0=

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-placeholder-template.directive.js":
/*!**************************************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-placeholder-template.directive.js ***!
  \**************************************************************************************************************************************/
/*! exports provided: IonicSelectablePlaceholderTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectablePlaceholderTemplateDirective", function() { return IonicSelectablePlaceholderTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

class IonicSelectablePlaceholderTemplateDirective {
}
IonicSelectablePlaceholderTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ionicSelectablePlaceholderTemplate]',
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS1wbGFjZWhvbGRlci10ZW1wbGF0ZS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9pb25pYy1zZWxlY3RhYmxlLyIsInNvdXJjZXMiOlsic3JjL2FwcC9jb21wb25lbnRzL2lvbmljLXNlbGVjdGFibGUvaW9uaWMtc2VsZWN0YWJsZS1wbGFjZWhvbGRlci10ZW1wbGF0ZS5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFLMUMsTUFBTSxPQUFPLDJDQUEyQzs7O1lBSHZELFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsc0NBQXNDO2FBQ2pEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1tpb25pY1NlbGVjdGFibGVQbGFjZWhvbGRlclRlbXBsYXRlXScsXG59KVxuZXhwb3J0IGNsYXNzIElvbmljU2VsZWN0YWJsZVBsYWNlaG9sZGVyVGVtcGxhdGVEaXJlY3RpdmUgeyB9XG4iXX0=

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-search-fail-template.directive.js":
/*!**************************************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-search-fail-template.directive.js ***!
  \**************************************************************************************************************************************/
/*! exports provided: IonicSelectableSearchFailTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableSearchFailTemplateDirective", function() { return IonicSelectableSearchFailTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

class IonicSelectableSearchFailTemplateDirective {
}
IonicSelectableSearchFailTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ionicSelectableSearchFailTemplate]',
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS1zZWFyY2gtZmFpbC10ZW1wbGF0ZS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9pb25pYy1zZWxlY3RhYmxlLyIsInNvdXJjZXMiOlsic3JjL2FwcC9jb21wb25lbnRzL2lvbmljLXNlbGVjdGFibGUvaW9uaWMtc2VsZWN0YWJsZS1zZWFyY2gtZmFpbC10ZW1wbGF0ZS5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFLMUMsTUFBTSxPQUFPLDBDQUEwQzs7O1lBSHRELFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUscUNBQXFDO2FBQ2hEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1tpb25pY1NlbGVjdGFibGVTZWFyY2hGYWlsVGVtcGxhdGVdJyxcbn0pXG5leHBvcnQgY2xhc3MgSW9uaWNTZWxlY3RhYmxlU2VhcmNoRmFpbFRlbXBsYXRlRGlyZWN0aXZlIHsgfVxuIl19

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-title-template.directive.js":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-title-template.directive.js ***!
  \********************************************************************************************************************************/
/*! exports provided: IonicSelectableTitleTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableTitleTemplateDirective", function() { return IonicSelectableTitleTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

class IonicSelectableTitleTemplateDirective {
}
IonicSelectableTitleTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ionicSelectableTitleTemplate]',
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS10aXRsZS10ZW1wbGF0ZS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9pb25pYy1zZWxlY3RhYmxlLyIsInNvdXJjZXMiOlsic3JjL2FwcC9jb21wb25lbnRzL2lvbmljLXNlbGVjdGFibGUvaW9uaWMtc2VsZWN0YWJsZS10aXRsZS10ZW1wbGF0ZS5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFLMUMsTUFBTSxPQUFPLHFDQUFxQzs7O1lBSGpELFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZ0NBQWdDO2FBQzNDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1tpb25pY1NlbGVjdGFibGVUaXRsZVRlbXBsYXRlXScsXG59KVxuZXhwb3J0IGNsYXNzIElvbmljU2VsZWN0YWJsZVRpdGxlVGVtcGxhdGVEaXJlY3RpdmUgeyB9XG4iXX0=

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-value-template.directive.js":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-value-template.directive.js ***!
  \********************************************************************************************************************************/
/*! exports provided: IonicSelectableValueTemplateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableValueTemplateDirective", function() { return IonicSelectableValueTemplateDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

class IonicSelectableValueTemplateDirective {
}
IonicSelectableValueTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ionicSelectableValueTemplate]',
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS12YWx1ZS10ZW1wbGF0ZS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9pb25pYy1zZWxlY3RhYmxlLyIsInNvdXJjZXMiOlsic3JjL2FwcC9jb21wb25lbnRzL2lvbmljLXNlbGVjdGFibGUvaW9uaWMtc2VsZWN0YWJsZS12YWx1ZS10ZW1wbGF0ZS5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFLMUMsTUFBTSxPQUFPLHFDQUFxQzs7O1lBSGpELFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZ0NBQWdDO2FBQzNDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1tpb25pY1NlbGVjdGFibGVWYWx1ZVRlbXBsYXRlXScsXG59KVxuZXhwb3J0IGNsYXNzIElvbmljU2VsZWN0YWJsZVZhbHVlVGVtcGxhdGVEaXJlY3RpdmUgeyB9XG4iXX0=

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable.component.js":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable.component.js ***!
  \*****************************************************************************************************************/
/*! exports provided: IonicSelectableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableComponent", function() { return IonicSelectableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_selectable_add_item_template_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ionic-selectable-add-item-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-add-item-template.directive.js");
/* harmony import */ var _ionic_selectable_close_button_template_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ionic-selectable-close-button-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-close-button-template.directive.js");
/* harmony import */ var _ionic_selectable_footer_template_directive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ionic-selectable-footer-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-footer-template.directive.js");
/* harmony import */ var _ionic_selectable_group_end_template_directive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ionic-selectable-group-end-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-group-end-template.directive.js");
/* harmony import */ var _ionic_selectable_group_template_directive__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ionic-selectable-group-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-group-template.directive.js");
/* harmony import */ var _ionic_selectable_header_template_directive__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ionic-selectable-header-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-header-template.directive.js");
/* harmony import */ var _ionic_selectable_item_end_template_directive__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./ionic-selectable-item-end-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-item-end-template.directive.js");
/* harmony import */ var _ionic_selectable_item_icon_template_directive__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./ionic-selectable-item-icon-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-item-icon-template.directive.js");
/* harmony import */ var _ionic_selectable_item_template_directive__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./ionic-selectable-item-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-item-template.directive.js");
/* harmony import */ var _ionic_selectable_message_template_directive__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./ionic-selectable-message-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-message-template.directive.js");
/* harmony import */ var _ionic_selectable_modal_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./ionic-selectable-modal.component */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-modal.component.js");
/* harmony import */ var _ionic_selectable_placeholder_template_directive__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./ionic-selectable-placeholder-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-placeholder-template.directive.js");
/* harmony import */ var _ionic_selectable_search_fail_template_directive__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./ionic-selectable-search-fail-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-search-fail-template.directive.js");
/* harmony import */ var _ionic_selectable_title_template_directive__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./ionic-selectable-title-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-title-template.directive.js");
/* harmony import */ var _ionic_selectable_value_template_directive__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./ionic-selectable-value-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-value-template.directive.js");
/* harmony import */ var _ionic_selectable_icon_template_directive__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./ionic-selectable-icon-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-icon-template.directive.js");
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// tslint:disable-next-line:max-line-length



















class IonicSelectableComponent {
    /**
     * @param {?} _modalController
     * @param {?} _platform
     * @param {?} ionItem
     * @param {?} _iterableDiffers
     * @param {?} _element
     * @param {?} _renderer
     */
    constructor(_modalController, _platform, ionItem, _iterableDiffers, _element, _renderer) {
        this._modalController = _modalController;
        this._platform = _platform;
        this.ionItem = ionItem;
        this._iterableDiffers = _iterableDiffers;
        this._element = _element;
        this._renderer = _renderer;
        this._cssClass = true;
        this._isOnSearchEnabled = true;
        this._isEnabled = true;
        this._shouldBackdropClose = true;
        this._isOpened = false;
        this._value = null;
        this._canClear = false;
        this._hasConfirmButton = false;
        this._isMultiple = false;
        this._canAddItem = false;
        this.onItemsChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this._hasIonLabel = false;
        this._ionLabelPosition = null;
        this._label = null;
        this._valueItems = [];
        this._searchText = '';
        this._hasSearchText = false;
        this._groups = [];
        this._itemsToConfirm = [];
        this._selectedItems = [];
        this._filteredGroups = [];
        this._isAddItemTemplateVisible = false;
        this._isFooterVisible = true;
        this._itemToAdd = null;
        this._footerButtonsCount = 0;
        this._hasFilteredItems = false;
        /**
         * A list of items.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#items).
         *
         * \@default []
         * \@memberof IonicSelectableComponent
         */
        this.items = [];
        this.itemsChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Modal CSS class.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#modalcssclass).
         *
         * \@default null
         * \@memberof IonicSelectableComponent
         */
        this.modalCssClass = null;
        /**
         * Modal enter animation.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#modalenteranimation).
         *
         * \@default null
         * \@memberof IonicSelectableComponent
         */
        this.modalEnterAnimation = null;
        /**
         * Modal leave animation.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#modalleaveanimation).
         *
         * \@default null
         * \@memberof IonicSelectableComponent
         */
        this.modalLeaveAnimation = null;
        /**
         * Determines whether Confirm button is enabled.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#isconfirmbuttonenabled).
         *
         * \@default true
         * \@memberof IonicSelectableComponent
         */
        this.isConfirmButtonEnabled = true;
        /**
         * Item property to use as a unique identifier, e.g, `'id'`.
         * **Note**: `items` should be an object array.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#itemvaluefield).
         *
         * \@default null
         * \@memberof IonicSelectableComponent
         */
        this.itemValueField = null;
        /**
         * Item property to display, e.g, `'name'`.
         * **Note**: `items` should be an object array.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#itemtextfield).
         *
         * \@default false
         * \@memberof IonicSelectableComponent
         */
        this.itemTextField = null;
        /**
         *
         * Group property to use as a unique identifier to group items, e.g. `'country.id'`.
         * **Note**: `items` should be an object array.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#groupvaluefield).
         *
         * \@default null
         * \@memberof IonicSelectableComponent
         */
        this.groupValueField = null;
        /**
         * Group property to display, e.g. `'country.name'`.
         * **Note**: `items` should be an object array.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#grouptextfield).
         *
         * \@default null
         * \@memberof IonicSelectableComponent
         */
        this.groupTextField = null;
        /**
         * Determines whether to show Searchbar.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#cansearch).
         *
         * \@default false
         * \@memberof IonicSelectableComponent
         */
        this.canSearch = false;
        /**
         * Determines whether Ionic [InfiniteScroll](https://ionicframework.com/docs/api/components/infinite-scroll/InfiniteScroll/) is enabled.
         * **Note**: Infinite scroll cannot be used together with virtual scroll.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#hasinfinitescroll).
         *
         * \@default false
         * \@memberof IonicSelectableComponent
         */
        this.hasInfiniteScroll = false;
        /**
         * Determines whether Ionic [VirtualScroll](https://ionicframework.com/docs/api/components/virtual-scroll/VirtualScroll/) is enabled.
         * **Note**: Virtual scroll cannot be used together with infinite scroll.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#hasvirtualscroll).
         *
         * \@default false
         * \@memberof IonicSelectableComponent
         */
        this.hasVirtualScroll = false;
        /**
         * See Ionic VirtualScroll [approxItemHeight](https://ionicframework.com/docs/api/components/virtual-scroll/VirtualScroll/).
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#virtualscrollapproxitemheight).
         *
         * \@default '40px'
         * \@memberof IonicSelectableComponent
         */
        this.virtualScrollApproxItemHeight = '40px';
        /**
         * A placeholder for Searchbar.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#searchplaceholder).
         *
         * \@default 'Search'
         * \@memberof IonicSelectableComponent
         */
        this.searchPlaceholder = 'Search';
        /**
         * A placeholder.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#placeholder).
         *
         * \@default null
         * \@memberof IonicSelectableComponent
         */
        this.placeholder = null;
        /**
         * Text to display when no items have been found during search.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#searchfailtext).
         *
         * \@default 'No items found.'
         * \@memberof IonicSelectableComponent
         */
        this.searchFailText = 'No items found.';
        /**
         * Clear button text.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#clearbuttontext).
         *
         * \@default 'Clear'
         * \@memberof IonicSelectableComponent
         */
        this.clearButtonText = 'Clear';
        /**
         * Add button text.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#addbuttontext).
         *
         * \@default 'Add'
         * \@memberof IonicSelectableComponent
         */
        this.addButtonText = 'Add';
        /**
         * Confirm button text.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#confirmbuttontext).
         *
         * \@default 'OK'
         * \@memberof IonicSelectableComponent
         */
        this.confirmButtonText = 'OK';
        /**
         * Close button text.
         * The field is only applicable to **iOS** platform, on **Android** only Cross icon is displayed.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#closebuttontext).
         *
         * \@default 'Cancel'
         * \@memberof IonicSelectableComponent
         */
        this.closeButtonText = 'Cancel';
        /**
         * Determines whether Searchbar should receive focus when Modal is opened.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#shouldfocussearchbar).
         *
         * \@default false
         * \@memberof IonicSelectableComponent
         */
        this.shouldFocusSearchbar = false;
        /**
         * Header color. [Ionic colors](https://ionicframework.com/docs/theming/advanced#colors) are supported.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#headercolor).
         *
         * \@default null
         * \@memberof IonicSelectableComponent
         */
        this.headerColor = null;
        /**
         * Group color. [Ionic colors](https://ionicframework.com/docs/theming/advanced#colors) are supported.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#groupcolor).
         *
         * \@default null
         * \@memberof IonicSelectableComponent
         */
        this.groupColor = null;
        /**
         * Close button slot. [Ionic slots](https://ionicframework.com/docs/api/buttons) are supported.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#closebuttonslot).
         *
         * \@default 'start'
         * \@memberof IonicSelectableComponent
         */
        this.closeButtonSlot = 'start';
        /**
         * Item icon slot. [Ionic slots](https://ionicframework.com/docs/api/item) are supported.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#itemiconslot).
         *
         * \@default 'start'
         * \@memberof IonicSelectableComponent
         */
        this.itemIconSlot = 'start';
        /**
         * Fires when item/s has been selected and Modal closed.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#onchange).
         *
         * \@memberof IonicSelectableComponent
         */
        this.onChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires when the user is typing in Searchbar.
         * **Note**: `canSearch` and `isOnSearchEnabled` has to be enabled.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#onsearch).
         *
         * \@memberof IonicSelectableComponent
         */
        this.onSearch = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires when no items have been found.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#onsearchfail).
         *
         * \@memberof IonicSelectableComponent
         */
        this.onSearchFail = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires when some items have been found.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#onsearchsuccess).
         *
         * \@memberof IonicSelectableComponent
         */
        this.onSearchSuccess = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires when the user has scrolled to the end of the list.
         * **Note**: `hasInfiniteScroll` has to be enabled.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#oninfinitescroll).
         *
         * \@memberof IonicSelectableComponent
         */
        this.onInfiniteScroll = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires when Modal has been opened.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#onopen).
         *
         * \@memberof IonicSelectableComponent
         */
        this.onOpen = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires when Modal has been closed.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#onclose).
         *
         * \@memberof IonicSelectableComponent
         */
        this.onClose = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires when an item has been selected or unselected.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#onselect).
         *
         * \@memberof IonicSelectableComponent
         */
        this.onSelect = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires when Clear button has been clicked.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#onclear).
         *
         * \@memberof IonicSelectableComponent
         */
        this.onClear = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * How long, in milliseconds, to wait to filter items or to trigger `onSearch` event after each keystroke.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#searchdebounce).
         *
         * \@default 250
         * \@memberof IonicSelectableComponent
         */
        this.searchDebounce = 250;
        /**
         * A list of items to disable.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#disableditems).
         *
         * \@default []
         * \@memberof IonicSelectableComponent
         */
        this.disabledItems = [];
        /**
         * Determines whether item value only should be stored in `ngModel`, not the entire item.
         * **Note**: Item value is defined by `itemValueField`.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#shouldstoreitemvalue).
         *
         * \@default false
         * \@memberof IonicSelectableComponent
         */
        this.shouldStoreItemValue = false;
        /**
         * Determines whether to allow editing items.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#cansaveitem).
         *
         * \@default false
         * \@memberof IonicSelectableComponent
         */
        this.canSaveItem = false;
        /**
         * Determines whether to allow deleting items.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#candeleteitem).
         *
         * \@default false
         * \@memberof IonicSelectableComponent
         */
        this.canDeleteItem = false;
        /**
         * Fires when Edit item button has been clicked.
         * When the button has been clicked `ionicSelectableAddItemTemplate` will be shown. Use the template to create a form to edit item.
         * **Note**: `canSaveItem` has to be enabled.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#onsaveitem).
         *
         * \@memberof IonicSelectableComponent
         */
        this.onSaveItem = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires when Delete item button has been clicked.
         * **Note**: `canDeleteItem` has to be enabled.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#ondeleteitem).
         *
         * \@memberof IonicSelectableComponent
         */
        this.onDeleteItem = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * Fires when Add item button has been clicked.
         * When the button has been clicked `ionicSelectableAddItemTemplate` will be shown. Use the template to create a form to add item.
         * **Note**: `canAddItem` has to be enabled.
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#onadditem).
         *
         * \@memberof IonicSelectableComponent
         */
        this.onAddItem = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        /**
         * See Ionic VirtualScroll [headerFn](https://ionicframework.com/docs/api/components/virtual-scroll/VirtualScroll/).
         * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#virtualscrollheaderfn).
         *
         * \@memberof IonicSelectableComponent
         */
        this.virtualScrollHeaderFn = (/**
         * @return {?}
         */
        () => {
            return null;
        });
        this.propagateOnChange = (/**
         * @param {?} _
         * @return {?}
         */
        (_) => { });
        this.propagateOnTouched = (/**
         * @return {?}
         */
        () => { });
        if (!this.items || !this.items.length) {
            this.items = [];
        }
        this._itemsDiffer = this._iterableDiffers.find(this.items).create();
    }
    /**
     * @return {?}
     */
    get _isMultipleCssClass() {
        return this.isMultiple;
    }
    /**
     * @return {?}
     */
    get _hasValueCssClass() {
        return this.hasValue();
    }
    /**
     * @return {?}
     */
    get _hasPlaceholderCssClass() {
        return this._hasPlaceholder;
    }
    /**
     * @return {?}
     */
    get _hasIonLabelCssClass() {
        return this._hasIonLabel;
    }
    /**
     * @return {?}
     */
    get _hasDefaultIonLabelCssClass() {
        return this._ionLabelPosition === 'default';
    }
    /**
     * @return {?}
     */
    get _hasFixedIonLabelCssClass() {
        return this._ionLabelPosition === 'fixed';
    }
    /**
     * @return {?}
     */
    get _hasStackedIonLabelCssClass() {
        return this._ionLabelPosition === 'stacked';
    }
    /**
     * @return {?}
     */
    get _hasFloatingIonLabelCssClass() {
        return this._ionLabelPosition === 'floating';
    }
    /**
     * @private
     * @return {?}
     */
    get _hasInfiniteScroll() {
        return this.isEnabled && this._modalComponent &&
            this._modalComponent._infiniteScroll ? true : false;
    }
    /**
     * @return {?}
     */
    get _shouldStoreItemValue() {
        return this.shouldStoreItemValue && this._hasObjects;
    }
    /**
     * Text of [Ionic Label](https://ionicframework.com/docs/api/label).
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#label).
     *
     * \@readonly
     * \@default null
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    get label() {
        return this._label;
    }
    /**
     * Text that the user has typed in Searchbar.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#searchtext).
     *
     * \@readonly
     * \@default ''
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    get searchText() {
        return this._searchText;
    }
    /**
     * @param {?} searchText
     * @return {?}
     */
    set searchText(searchText) {
        this._searchText = searchText;
        this._setHasSearchText();
    }
    /**
     * Determines whether search is running.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#issearching).
     *
     * \@default false
     * \@readonly
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    get isSearching() {
        return this._isSearching;
    }
    /**
     * Determines whether user has typed anything in Searchbar.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#hassearchtext).
     *
     * \@default false
     * \@readonly
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    get hasSearchText() {
        return this._hasSearchText;
    }
    /**
     * @return {?}
     */
    get value() {
        return this._value;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set value(value) {
        this._value = value;
        // Set value items.
        this._valueItems.splice(0, this._valueItems.length);
        if (this.isMultiple) {
            if (value && value.length) {
                Array.prototype.push.apply(this._valueItems, value);
            }
        }
        else {
            if (!this._isNullOrWhiteSpace(value)) {
                this._valueItems.push(value);
            }
        }
        this._setIonItemHasValue();
        this._setHasPlaceholder();
    }
    /**
     * Determines whether the component is enabled.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#isenabled).
     *
     * \@default true
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    get isEnabled() {
        return this._isEnabled;
    }
    /**
     * @param {?} isEnabled
     * @return {?}
     */
    set isEnabled(isEnabled) {
        this._isEnabled = !!isEnabled;
        this.enableIonItem(this._isEnabled);
    }
    /**
     * Determines whether Modal should be closed when backdrop is clicked.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#shouldbackdropclose).
     *
     * \@default true
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    get shouldBackdropClose() {
        return this._shouldBackdropClose;
    }
    /**
     * @param {?} shouldBackdropClose
     * @return {?}
     */
    set shouldBackdropClose(shouldBackdropClose) {
        this._shouldBackdropClose = !!shouldBackdropClose;
    }
    /**
     * Determines whether Modal is opened.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#isopened).
     *
     * \@default false
     * \@readonly
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    get isOpened() {
        return this._isOpened;
    }
    /**
     * Determines whether Confirm button is visible for single selection.
     * By default Confirm button is visible only for multiple selection.
     * **Note**: It is always true for multiple selection and cannot be changed.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#hasconfirmbutton).
     *
     * \@default true
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    get hasConfirmButton() {
        return this._hasConfirmButton;
    }
    /**
     * @param {?} hasConfirmButton
     * @return {?}
     */
    set hasConfirmButton(hasConfirmButton) {
        this._hasConfirmButton = !!hasConfirmButton;
        this._countFooterButtons();
    }
    /**
     * Determines whether `onSearch` event is enabled.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#isonsearchenabled).
     *
     * \@default true
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    get isOnSearchEnabled() {
        return this._isOnSearchEnabled;
    }
    /**
     * @param {?} isOnSearchEnabled
     * @return {?}
     */
    set isOnSearchEnabled(isOnSearchEnabled) {
        this._isOnSearchEnabled = !!isOnSearchEnabled;
    }
    /**
     * Determines whether to show Clear button.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#canclear).
     *
     * \@default false
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    get canClear() {
        return this._canClear;
    }
    /**
     * @param {?} canClear
     * @return {?}
     */
    set canClear(canClear) {
        this._canClear = !!canClear;
        this._countFooterButtons();
    }
    /**
     * Determines whether multiple items can be selected.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#ismultiple).
     *
     * \@default false
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    get isMultiple() {
        return this._isMultiple;
    }
    /**
     * @param {?} isMultiple
     * @return {?}
     */
    set isMultiple(isMultiple) {
        this._isMultiple = !!isMultiple;
        this._countFooterButtons();
    }
    /**
     * A list of items that are selected and awaiting confirmation by user, when he has clicked Confirm button.
     * After the user has clicked Confirm button items to confirm are cleared.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#itemstoconfirm).
     *
     * \@default []
     * \@readonly
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    get itemsToConfirm() {
        return this._itemsToConfirm;
    }
    /**
     * Determines whether to allow adding items.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#canadditem).
     *
     * \@default false
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    get canAddItem() {
        return this._canAddItem;
    }
    /**
     * @param {?} canAddItem
     * @return {?}
     */
    set canAddItem(canAddItem) {
        this._canAddItem = !!canAddItem;
        this._countFooterButtons();
    }
    /**
     * @return {?}
     */
    initFocus() { }
    /**
     * @param {?} isEnabled
     * @return {?}
     */
    enableIonItem(isEnabled) {
        if (!this.ionItem) {
            return;
        }
        this.ionItem.disabled = !isEnabled;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    _isNullOrWhiteSpace(value) {
        if (value === null || value === undefined) {
            return true;
        }
        // Convert value to string in case if it's not.
        return value.toString().replace(/\s/g, '').length < 1;
    }
    /**
     * @return {?}
     */
    _setHasSearchText() {
        this._hasSearchText = !this._isNullOrWhiteSpace(this._searchText);
    }
    /**
     * @return {?}
     */
    _hasOnSearch() {
        return this.isOnSearchEnabled && this.onSearch.observers.length > 0;
    }
    /**
     * @return {?}
     */
    _hasOnSaveItem() {
        return this.canSaveItem && this.onSaveItem.observers.length > 0;
    }
    /**
     * @return {?}
     */
    _hasOnAddItem() {
        return this.canAddItem && this.onAddItem.observers.length > 0;
    }
    /**
     * @return {?}
     */
    _hasOnDeleteItem() {
        return this.canDeleteItem && this.onDeleteItem.observers.length > 0;
    }
    /**
     * @return {?}
     */
    _emitValueChange() {
        this.propagateOnChange(this.value);
        this.onChange.emit({
            component: this,
            value: this.value
        });
    }
    /**
     * @return {?}
     */
    _emitSearch() {
        if (!this.canSearch) {
            return;
        }
        this.onSearch.emit({
            component: this,
            text: this._searchText
        });
    }
    /**
     * @param {?} item
     * @param {?} isSelected
     * @return {?}
     */
    _emitOnSelect(item, isSelected) {
        this.onSelect.emit({
            component: this,
            item: item,
            isSelected: isSelected
        });
    }
    /**
     * @param {?} items
     * @return {?}
     */
    _emitOnClear(items) {
        this.onClear.emit({
            component: this,
            items: items
        });
    }
    /**
     * @param {?} isSuccess
     * @return {?}
     */
    _emitOnSearchSuccessOrFail(isSuccess) {
        /** @type {?} */
        const eventData = {
            component: this,
            text: this._searchText
        };
        if (isSuccess) {
            this.onSearchSuccess.emit(eventData);
        }
        else {
            this.onSearchFail.emit(eventData);
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    _formatItem(item) {
        if (this._isNullOrWhiteSpace(item)) {
            return null;
        }
        return this.itemTextField ? item[this.itemTextField] : item.toString();
    }
    /**
     * @param {?} item
     * @return {?}
     */
    _formatValueItem(item) {
        if (this._shouldStoreItemValue) {
            // Get item text from the list as we store it's value only.
            /** @type {?} */
            const selectedItem = this.items.find((/**
             * @param {?} _item
             * @return {?}
             */
            _item => {
                return _item[this.itemValueField] === item;
            }));
            return this._formatItem(selectedItem);
        }
        else {
            return this._formatItem(item);
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    _getItemValue(item) {
        if (!this._hasObjects) {
            return item;
        }
        return item[this.itemValueField];
    }
    /**
     * @param {?} item
     * @return {?}
     */
    _getStoredItemValue(item) {
        if (!this._hasObjects) {
            return item;
        }
        return this._shouldStoreItemValue ? item : item[this.itemValueField];
    }
    /**
     * @return {?}
     */
    _onSearchbarClear() {
        // Ionic Searchbar doesn't clear bind with ngModel value.
        // Do it ourselves.
        this._searchText = '';
    }
    /**
     * @return {?}
     */
    _filterItems() {
        this._setHasSearchText();
        if (this._hasOnSearch()) {
            // Delegate filtering to the event.
            this._emitSearch();
        }
        else {
            // Default filtering.
            /** @type {?} */
            let groups = [];
            if (!this._searchText || !this._searchText.trim()) {
                groups = this._groups;
            }
            else {
                /** @type {?} */
                const filterText = this._searchText.trim().toLowerCase();
                this._groups.forEach((/**
                 * @param {?} group
                 * @return {?}
                 */
                group => {
                    /** @type {?} */
                    const items = group.items.filter((/**
                     * @param {?} item
                     * @return {?}
                     */
                    item => {
                        /** @type {?} */
                        const itemText = (this.itemTextField ?
                            item[this.itemTextField] : item).toString().toLowerCase();
                        return itemText.indexOf(filterText) !== -1;
                    }));
                    if (items.length) {
                        groups.push({
                            value: group.value,
                            text: group.text,
                            items: items
                        });
                    }
                }));
                // No items found.
                if (!groups.length) {
                    groups.push({
                        items: []
                    });
                }
            }
            this._filteredGroups = groups;
            this._hasFilteredItems = !this._areGroupsEmpty(groups);
            this._emitOnSearchSuccessOrFail(this._hasFilteredItems);
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    _isItemDisabled(item) {
        if (!this.disabledItems) {
            return;
        }
        return this.disabledItems.some((/**
         * @param {?} _item
         * @return {?}
         */
        _item => {
            return this._getItemValue(_item) === this._getItemValue(item);
        }));
    }
    /**
     * @param {?} item
     * @return {?}
     */
    _isItemSelected(item) {
        return this._selectedItems.find((/**
         * @param {?} selectedItem
         * @return {?}
         */
        selectedItem => {
            return this._getItemValue(item) === this._getStoredItemValue(selectedItem);
        })) !== undefined;
    }
    /**
     * @param {?} item
     * @return {?}
     */
    _addSelectedItem(item) {
        if (this._shouldStoreItemValue) {
            this._selectedItems.push(this._getItemValue(item));
        }
        else {
            this._selectedItems.push(item);
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    _deleteSelectedItem(item) {
        /** @type {?} */
        let itemToDeleteIndex;
        this._selectedItems.forEach((/**
         * @param {?} selectedItem
         * @param {?} itemIndex
         * @return {?}
         */
        (selectedItem, itemIndex) => {
            if (this._getItemValue(item) ===
                this._getStoredItemValue(selectedItem)) {
                itemToDeleteIndex = itemIndex;
            }
        }));
        this._selectedItems.splice(itemToDeleteIndex, 1);
    }
    /**
     * @return {?}
     */
    _click() {
        if (!this.isEnabled) {
            return;
        }
        this._label = this._getLabelText();
        this.open().then((/**
         * @return {?}
         */
        () => {
            this.onOpen.emit({
                component: this
            });
        }));
    }
    /**
     * @param {?} event
     * @param {?} item
     * @return {?}
     */
    _saveItem(event, item) {
        event.stopPropagation();
        this._itemToAdd = item;
        if (this._hasOnSaveItem()) {
            this.onSaveItem.emit({
                component: this,
                item: this._itemToAdd
            });
        }
        else {
            this.showAddItemTemplate();
        }
    }
    /**
     * @param {?} event
     * @param {?} item
     * @return {?}
     */
    _deleteItemClick(event, item) {
        event.stopPropagation();
        this._itemToAdd = item;
        if (this._hasOnDeleteItem()) {
            // Delegate logic to event.
            this.onDeleteItem.emit({
                component: this,
                item: this._itemToAdd
            });
        }
        else {
            this.deleteItem(this._itemToAdd);
        }
    }
    /**
     * @return {?}
     */
    _addItemClick() {
        if (this._hasOnAddItem()) {
            this.onAddItem.emit({
                component: this
            });
        }
        else {
            this.showAddItemTemplate();
        }
    }
    /**
     * @return {?}
     */
    _positionAddItemTemplate() {
        // Wait for the template to render.
        setTimeout((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const footer = this._modalComponent._element.nativeElement
                .querySelector('.ionic-selectable-add-item-template ion-footer');
            this._addItemTemplateFooterHeight = footer ? `calc(100% - ${footer.offsetHeight}px)` : '100%';
        }), 100);
    }
    /**
     * @return {?}
     */
    _close() {
        this.close().then((/**
         * @return {?}
         */
        () => {
            this.onClose.emit({
                component: this
            });
        }));
        if (!this._hasOnSearch()) {
            this._searchText = '';
            this._setHasSearchText();
        }
    }
    /**
     * @return {?}
     */
    _clear() {
        /** @type {?} */
        const selectedItems = this._selectedItems;
        this.clear();
        this._emitValueChange();
        this._emitOnClear(selectedItems);
        this.close().then((/**
         * @return {?}
         */
        () => {
            this.onClose.emit({
                component: this
            });
        }));
    }
    /**
     * @return {?}
     */
    _getMoreItems() {
        this.onInfiniteScroll.emit({
            component: this,
            text: this._searchText
        });
    }
    /**
     * @param {?} items
     * @return {?}
     */
    _setItemsToConfirm(items) {
        // Return a copy of original array, so it couldn't be changed from outside.
        this._itemsToConfirm = [].concat(items);
    }
    /**
     * @param {?} selectedItem
     * @return {?}
     */
    _doSelect(selectedItem) {
        this.value = selectedItem;
        this._emitValueChange();
    }
    /**
     * @param {?} item
     * @return {?}
     */
    _select(item) {
        /** @type {?} */
        const isItemSelected = this._isItemSelected(item);
        if (this.isMultiple) {
            if (isItemSelected) {
                this._deleteSelectedItem(item);
            }
            else {
                this._addSelectedItem(item);
            }
            this._setItemsToConfirm(this._selectedItems);
            // Emit onSelect event after setting items to confirm so they could be used
            // inside the event.
            this._emitOnSelect(item, !isItemSelected);
        }
        else {
            if (this.hasConfirmButton || this.footerTemplate) {
                // Don't close Modal and keep track on items to confirm.
                // When footer template is used it's up to developer to close Modal.
                this._selectedItems = [];
                if (isItemSelected) {
                    this._deleteSelectedItem(item);
                }
                else {
                    this._addSelectedItem(item);
                }
                this._setItemsToConfirm(this._selectedItems);
                // Emit onSelect event after setting items to confirm so they could be used
                // inside the event.
                this._emitOnSelect(item, !isItemSelected);
            }
            else {
                if (!isItemSelected) {
                    this._selectedItems = [];
                    this._addSelectedItem(item);
                    // Emit onSelect before onChange.
                    this._emitOnSelect(item, true);
                    if (this._shouldStoreItemValue) {
                        this._doSelect(this._getItemValue(item));
                    }
                    else {
                        this._doSelect(item);
                    }
                }
                this._close();
            }
        }
    }
    /**
     * @return {?}
     */
    _confirm() {
        this.confirm();
        this._close();
    }
    /**
     * @private
     * @return {?}
     */
    _getLabelText() {
        return this._ionLabelElement ? this._ionLabelElement.textContent : null;
    }
    /**
     * @private
     * @param {?} groups
     * @return {?}
     */
    _areGroupsEmpty(groups) {
        return groups.length === 0 || groups.every((/**
         * @param {?} group
         * @return {?}
         */
        group => {
            return !group.items || group.items.length === 0;
        }));
    }
    /**
     * @private
     * @return {?}
     */
    _countFooterButtons() {
        /** @type {?} */
        let footerButtonsCount = 0;
        if (this.canClear) {
            footerButtonsCount++;
        }
        if (this.isMultiple || this._hasConfirmButton) {
            footerButtonsCount++;
        }
        if (this.canAddItem) {
            footerButtonsCount++;
        }
        this._footerButtonsCount = footerButtonsCount;
    }
    /**
     * @private
     * @param {?} items
     * @return {?}
     */
    _setItems(items) {
        // It's important to have an empty starting group with empty items (groups[0].items),
        // because we bind to it when using VirtualScroll.
        // See https://github.com/eakoriakin/ionic-selectable/issues/70.
        /** @type {?} */
        let groups = [{
                items: items || []
            }];
        if (items && items.length) {
            if (this._hasGroups) {
                groups = [];
                items.forEach((/**
                 * @param {?} item
                 * @return {?}
                 */
                item => {
                    /** @type {?} */
                    const groupValue = this._getPropertyValue(item, this.groupValueField);
                    /** @type {?} */
                    const group = groups.find((/**
                     * @param {?} _group
                     * @return {?}
                     */
                    _group => _group.value === groupValue));
                    if (group) {
                        group.items.push(item);
                    }
                    else {
                        groups.push({
                            value: groupValue,
                            text: this._getPropertyValue(item, this.groupTextField),
                            items: [item]
                        });
                    }
                }));
            }
        }
        this._groups = groups;
        this._filteredGroups = this._groups;
        this._hasFilteredItems = !this._areGroupsEmpty(this._filteredGroups);
    }
    /**
     * @private
     * @param {?} object
     * @param {?} property
     * @return {?}
     */
    _getPropertyValue(object, property) {
        if (!property) {
            return null;
        }
        return property.split('.').reduce((/**
         * @param {?} _object
         * @param {?} _property
         * @return {?}
         */
        (_object, _property) => {
            return _object ? _object[_property] : null;
        }), object);
    }
    /**
     * @private
     * @param {?} hasFocus
     * @return {?}
     */
    _setIonItemHasFocus(hasFocus) {
        if (!this.ionItem) {
            return;
        }
        // Apply focus CSS class for proper stylying of ion-item/ion-label.
        this._setIonItemCssClass('item-has-focus', hasFocus);
    }
    /**
     * @private
     * @return {?}
     */
    _setIonItemHasValue() {
        if (!this.ionItem) {
            return;
        }
        // Apply value CSS class for proper stylying of ion-item/ion-label.
        this._setIonItemCssClass('item-has-value', this.hasValue());
    }
    /**
     * @private
     * @return {?}
     */
    _setHasPlaceholder() {
        this._hasPlaceholder = !this.hasValue() &&
            (!this._isNullOrWhiteSpace(this.placeholder) || this.placeholderTemplate) ?
            true : false;
    }
    /**
     * @private
     * @param {?} cssClass
     * @param {?} shouldAdd
     * @return {?}
     */
    _setIonItemCssClass(cssClass, shouldAdd) {
        if (!this._ionItemElement) {
            return;
        }
        // Change to Renderer2
        if (shouldAdd) {
            this._renderer.addClass(this._ionItemElement, cssClass);
        }
        else {
            this._renderer.removeClass(this._ionItemElement, cssClass);
        }
    }
    /**
     * @private
     * @param {?} isVisible
     * @return {?}
     */
    _toggleAddItemTemplate(isVisible) {
        // It should be possible to show/hide the template regardless
        // canAddItem or canSaveItem parameters, so we could implement some
        // custom behavior. E.g. adding item when search fails using onSearchFail event.
        if (!this.addItemTemplate) {
            return;
        }
        // To make SaveItemTemplate visible we just position it over list using CSS.
        // We don't hide list with *ngIf or [hidden] to prevent its scroll position.
        this._isAddItemTemplateVisible = isVisible;
        this._isFooterVisible = !isVisible;
    }
    /* ControlValueAccessor */
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this.value = value;
    }
    /**
     * @param {?} method
     * @return {?}
     */
    registerOnChange(method) {
        this.propagateOnChange = method;
    }
    /**
     * @param {?} method
     * @return {?}
     */
    registerOnTouched(method) {
        this.propagateOnTouched = method;
    }
    /**
     * @param {?} isDisabled
     * @return {?}
     */
    setDisabledState(isDisabled) {
        this.isEnabled = !isDisabled;
    }
    /* .ControlValueAccessor */
    /**
     * @return {?}
     */
    ngOnInit() {
        this._isIos = this._platform.is('ios');
        this._isMD = !this._isIos;
        this._hasObjects = !this._isNullOrWhiteSpace(this.itemValueField);
        // Grouping is supported for objects only.
        // Ionic VirtualScroll has it's own implementation of grouping.
        this._hasGroups = Boolean(this._hasObjects && this.groupValueField && !this.hasVirtualScroll);
        if (this.ionItem) {
            this._ionItemElement = this._element.nativeElement.closest('ion-item');
            this._setIonItemCssClass('item-interactive', true);
            this._setIonItemCssClass('item-ionic-selectable', true);
            if (this._ionItemElement) {
                this._ionLabelElement = this._ionItemElement.querySelector('ion-label');
                if (this._ionLabelElement) {
                    this._hasIonLabel = true;
                    this._ionLabelPosition = this._ionLabelElement.getAttribute('position') || 'default';
                }
            }
        }
        this.enableIonItem(this.isEnabled);
    }
    /**
     * @return {?}
     */
    ngDoCheck() {
        /** @type {?} */
        const itemsChanges = this._itemsDiffer.diff(this.items);
        if (itemsChanges) {
            this._setItems(this.items);
            this.value = this.value;
            this.onItemsChange.emit({
                component: this
            });
        }
    }
    /**
     * Adds item.
     * **Note**: If you want an item to be added to the original array as well use two-way data binding syntax on `[(items)]` field.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#additem).
     *
     * \@memberof IonicSelectableComponent
     * @param {?} item Item to add.
     * @return {?} Promise that resolves when item has been added.
     */
    addItem(item) {
        /** @type {?} */
        const self = this;
        // Adding item triggers onItemsChange.
        // Return a promise that resolves when onItemsChange finishes.
        // We need a promise or user could do something after item has been added,
        // e.g. use search() method to find the added item.
        this.items.unshift(item);
        // Close any running subscription.
        if (this._addItemObservable) {
            this._addItemObservable.unsubscribe();
        }
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            // Complete callback isn't fired for some reason,
            // so unsubscribe in both success and fail cases.
            self._addItemObservable = self.onItemsChange.asObservable().subscribe((/**
             * @return {?}
             */
            () => {
                self._addItemObservable.unsubscribe();
                resolve();
            }), (/**
             * @return {?}
             */
            () => {
                self._addItemObservable.unsubscribe();
                reject();
            }));
        }));
    }
    /**
     * Deletes item.
     * **Note**: If you want an item to be deleted from the original array as well use two-way data binding syntax on `[(items)]` field.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#deleteitem).
     *
     * \@memberof IonicSelectableComponent
     * @param {?} item Item to delete.
     * @return {?} Promise that resolves when item has been deleted.
     */
    deleteItem(item) {
        /** @type {?} */
        const self = this;
        /** @type {?} */
        let hasValueChanged = false;
        // Remove deleted item from selected items.
        if (this._selectedItems) {
            this._selectedItems = this._selectedItems.filter((/**
             * @param {?} _item
             * @return {?}
             */
            _item => {
                return this._getItemValue(item) !== this._getStoredItemValue(_item);
            }));
        }
        // Remove deleted item from value.
        if (this.value) {
            if (this.isMultiple) {
                /** @type {?} */
                const values = this.value.filter((/**
                 * @param {?} value
                 * @return {?}
                 */
                value => {
                    return value.id !== item.id;
                }));
                if (values.length !== this.value.length) {
                    this.value = values;
                    hasValueChanged = true;
                }
            }
            else {
                if (item === this.value) {
                    this.value = null;
                    hasValueChanged = true;
                }
            }
        }
        if (hasValueChanged) {
            this._emitValueChange();
        }
        // Remove deleted item from list.
        /** @type {?} */
        const items = this.items.filter((/**
         * @param {?} _item
         * @return {?}
         */
        _item => {
            return _item.id !== item.id;
        }));
        // Refresh items on parent component.
        this.itemsChange.emit(items);
        // Refresh list.
        this._setItems(items);
        this.onItemsChange.emit({
            component: this
        });
        // Close any running subscription.
        if (this._deleteItemObservable) {
            this._deleteItemObservable.unsubscribe();
        }
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            // Complete callback isn't fired for some reason,
            // so unsubscribe in both success and fail cases.
            self._deleteItemObservable = self.onItemsChange.asObservable().subscribe((/**
             * @return {?}
             */
            () => {
                self._deleteItemObservable.unsubscribe();
                resolve();
            }), (/**
             * @return {?}
             */
            () => {
                self._deleteItemObservable.unsubscribe();
                reject();
            }));
        }));
    }
    /**
     * Determines whether any item has been selected.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#hasvalue).
     *
     * \@memberof IonicSelectableComponent
     * @return {?} A boolean determining whether any item has been selected.
     */
    hasValue() {
        if (this.isMultiple) {
            return this._valueItems.length !== 0;
        }
        else {
            return this._valueItems.length !== 0 && !this._isNullOrWhiteSpace(this._valueItems[0]);
        }
    }
    /**
     * Opens Modal.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#open).
     *
     * \@memberof IonicSelectableComponent
     * @return {?} Promise that resolves when Modal has been opened.
     */
    open() {
        /** @type {?} */
        const self = this;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            if (!self._isEnabled || self._isOpened) {
                reject('IonicSelectable is disabled or already opened.');
                return;
            }
            self._filterItems();
            self._isOpened = true;
            /** @type {?} */
            const modalOptions = {
                component: _ionic_selectable_modal_component__WEBPACK_IMPORTED_MODULE_13__["IonicSelectableModalComponent"],
                componentProps: { selectComponent: self },
                backdropDismiss: self._shouldBackdropClose
            };
            if (self.modalCssClass) {
                modalOptions.cssClass = self.modalCssClass;
            }
            if (self.modalEnterAnimation) {
                modalOptions.enterAnimation = self.modalEnterAnimation;
            }
            if (self.modalLeaveAnimation) {
                modalOptions.leaveAnimation = self.modalLeaveAnimation;
            }
            self._modalController.create(modalOptions).then((/**
             * @param {?} modal
             * @return {?}
             */
            modal => {
                self._modal = modal;
                modal.present().then((/**
                 * @return {?}
                 */
                () => {
                    // Set focus after Modal has opened to avoid flickering of focus highlighting
                    // before Modal opening.
                    self._setIonItemHasFocus(true);
                    resolve();
                }));
                modal.onWillDismiss().then((/**
                 * @return {?}
                 */
                () => {
                    self._setIonItemHasFocus(false);
                }));
                modal.onDidDismiss().then((/**
                 * @param {?} event
                 * @return {?}
                 */
                event => {
                    self._isOpened = false;
                    self._itemsToConfirm = [];
                    // Closed by clicking on backdrop outside modal.
                    if (event.role === 'backdrop') {
                        self.onClose.emit({
                            component: self
                        });
                    }
                }));
            }));
        }));
    }
    /**
     * Closes Modal.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#close).
     *
     * \@memberof IonicSelectableComponent
     * @return {?} Promise that resolves when Modal has been closed.
     */
    close() {
        /** @type {?} */
        const self = this;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            if (!self._isEnabled || !self._isOpened) {
                reject('IonicSelectable is disabled or already closed.');
                return;
            }
            self.propagateOnTouched();
            self._isOpened = false;
            self._itemToAdd = null;
            self._modal.dismiss().then((/**
             * @return {?}
             */
            () => {
                self._setIonItemHasFocus(false);
                self.hideAddItemTemplate();
                resolve();
            }));
        }));
    }
    /**
     * Clears value.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#clear).
     *
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    clear() {
        this.value = this.isMultiple ? [] : null;
        this._itemsToConfirm = [];
        this.propagateOnChange(this.value);
    }
    /**
     * Confirms selected items by updating value.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#confirm).
     *
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    confirm() {
        if (this.isMultiple) {
            this._doSelect(this._selectedItems);
        }
        else if (this.hasConfirmButton || this.footerTemplate) {
            this._doSelect(this._selectedItems[0] || null);
        }
    }
    /**
     * Selects or deselects all or specific items.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#toggleitems).
     *
     * \@memberof IonicSelectableComponent
     * @param {?} isSelect Determines whether to select or deselect items.
     * @param {?=} items
     * @return {?}
     */
    toggleItems(isSelect, items) {
        if (isSelect) {
            /** @type {?} */
            const hasItems = items && items.length;
            /** @type {?} */
            let itemsToToggle = this._groups.reduce((/**
             * @param {?} allItems
             * @param {?} group
             * @return {?}
             */
            (allItems, group) => {
                return allItems.concat(group.items);
            }), []);
            // Don't allow to select all items in single mode.
            if (!this.isMultiple && !hasItems) {
                itemsToToggle = [];
            }
            // Toggle specific items.
            if (hasItems) {
                itemsToToggle = itemsToToggle.filter((/**
                 * @param {?} itemToToggle
                 * @return {?}
                 */
                itemToToggle => {
                    return items.find((/**
                     * @param {?} item
                     * @return {?}
                     */
                    item => {
                        return this._getItemValue(itemToToggle) === this._getItemValue(item);
                    })) !== undefined;
                }));
                // Take the first item for single mode.
                if (!this.isMultiple) {
                    itemsToToggle.splice(0, 1);
                }
            }
            itemsToToggle.forEach((/**
             * @param {?} item
             * @return {?}
             */
            item => {
                this._addSelectedItem(item);
            }));
        }
        else {
            this._selectedItems = [];
        }
        this._setItemsToConfirm(this._selectedItems);
    }
    /**
     * Scrolls to the top of Modal content.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#scrolltotop).
     *
     * \@memberof IonicSelectableComponent
     * @return {?} Promise that resolves when scroll has been completed.
     */
    scrollToTop() {
        /** @type {?} */
        const self = this;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            if (!self._isOpened) {
                reject('IonicSelectable content cannot be scrolled.');
                return;
            }
            self._modalComponent._content.scrollToTop().then((/**
             * @return {?}
             */
            () => {
                resolve();
            }));
        }));
    }
    /**
     * Scrolls to the bottom of Modal content.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#scrolltobottom).
     *
     * \@memberof IonicSelectableComponent
     * @return {?} Promise that resolves when scroll has been completed.
     */
    scrollToBottom() {
        /** @type {?} */
        const self = this;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            if (!self._isOpened) {
                reject('IonicSelectable content cannot be scrolled.');
                return;
            }
            self._modalComponent._content.scrollToBottom().then((/**
             * @return {?}
             */
            () => {
                resolve();
            }));
        }));
    }
    /**
     * Starts search process by showing Loading spinner.
     * Use it together with `onSearch` event to indicate search start.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#startsearch).
     *
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    startSearch() {
        if (!this._isEnabled) {
            return;
        }
        this.showLoading();
    }
    /**
     * Ends search process by hiding Loading spinner and refreshing items.
     * Use it together with `onSearch` event to indicate search end.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#endsearch).
     *
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    endSearch() {
        if (!this._isEnabled) {
            return;
        }
        this.hideLoading();
        // When inside Ionic Modal and onSearch event is used,
        // ngDoCheck() doesn't work as _itemsDiffer fails to detect changes.
        // See https://github.com/eakoriakin/ionic-selectable/issues/44.
        // Refresh items manually.
        this._setItems(this.items);
        this._emitOnSearchSuccessOrFail(this._hasFilteredItems);
    }
    /**
     * Enables infinite scroll.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#enableinfinitescroll).
     *
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    enableInfiniteScroll() {
        if (!this._hasInfiniteScroll) {
            return;
        }
        this._modalComponent._infiniteScroll.disabled = false;
    }
    /**
     * Disables infinite scroll.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#disableinfinitescroll).
     *
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    disableInfiniteScroll() {
        if (!this._hasInfiniteScroll) {
            return;
        }
        this._modalComponent._infiniteScroll.disabled = true;
    }
    /**
     * Ends infinite scroll.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#endinfinitescroll).
     *
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    endInfiniteScroll() {
        if (!this._hasInfiniteScroll) {
            return;
        }
        this._modalComponent._infiniteScroll.complete();
        this._setItems(this.items);
    }
    /**
     * Triggers search of items.
     * **Note**: `canSearch` has to be enabled.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#search).
     *
     * \@memberof IonicSelectableComponent
     * @param {?} text Text to search items by.
     * @return {?}
     */
    search(text) {
        if (!this._isEnabled || !this._isOpened || !this.canSearch) {
            return;
        }
        this._searchText = text;
        this._setHasSearchText();
        this._filterItems();
    }
    /**
     * Shows Loading spinner.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#showloading).
     *
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    showLoading() {
        if (!this._isEnabled) {
            return;
        }
        this._isSearching = true;
    }
    /**
     * Hides Loading spinner.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#hideloading).
     *
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    hideLoading() {
        if (!this._isEnabled) {
            return;
        }
        this._isSearching = false;
    }
    /**
     * Shows `ionicSelectableAddItemTemplate`.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#showadditemtemplate).
     *
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    showAddItemTemplate() {
        this._toggleAddItemTemplate(true);
        // Position the template only when it shous up.
        this._positionAddItemTemplate();
    }
    /**
     * Hides `ionicSelectableAddItemTemplate`.
     * See more on [GitHub](https://github.com/eakoriakin/ionic-selectable/wiki/Documentation#hideadditemtemplate).
     *
     * \@memberof IonicSelectableComponent
     * @return {?}
     */
    hideAddItemTemplate() {
        // Clean item to add as it's no longer needed once Add Item Modal has been closed.
        this._itemToAdd = null;
        this._toggleAddItemTemplate(false);
    }
}
IonicSelectableComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: 'ionic-selectable',
                template: "<div class=\"ionic-selectable-inner\">\n  <div class=\"ionic-selectable-value\">\n    <div *ngIf=\"valueTemplate && _valueItems.length && isMultiple\"\n      [ngTemplateOutlet]=\"valueTemplate\"\n      [ngTemplateOutletContext]=\"{ value: _valueItems }\">\n    </div>\n    <div class=\"ionic-selectable-value-item\"\n      *ngIf=\"valueTemplate && _valueItems.length && !isMultiple\">\n      <div [ngTemplateOutlet]=\"valueTemplate\"\n        [ngTemplateOutletContext]=\"{ value: _valueItems[0] }\">\n      </div>\n    </div>\n    <span *ngIf=\"!valueTemplate && _valueItems.length\">\n      <div class=\"ionic-selectable-value-item\"\n        *ngFor=\"let valueItem of _valueItems\">\n        {{_formatValueItem(valueItem)}}\n      </div>\n    </span>\n    <div *ngIf=\"_hasPlaceholder && placeholderTemplate\"\n      class=\"ionic-selectable-value-item\">\n      <div [ngTemplateOutlet]=\"placeholderTemplate\">\n      </div>\n    </div>\n    <div class=\"ionic-selectable-value-item\"\n      *ngIf=\"_hasPlaceholder && !placeholderTemplate\">\n      {{placeholder}}\n    </div>\n    <!-- Fix icon allignment when there's no value or placeholder. -->\n    <span *ngIf=\"!_valueItems.length && !_hasPlaceholder\">&nbsp;</span>\n  </div>\n  <div *ngIf=\"iconTemplate\" class=\"ionic-selectable-icon-template\">\n      <div [ngTemplateOutlet]=\"iconTemplate\"></div>\n  </div>\n  <div *ngIf=\"!iconTemplate\" class=\"ionic-selectable-icon\">\n    <div class=\"ionic-selectable-icon-inner\"></div>\n  </div>\n  <!-- Need to be type=\"button\" otherwise click event triggers form ngSubmit. -->\n  <button class=\"ionic-selectable-cover\" [disabled]=\"!isEnabled\"\n    (click)=\"_click()\" type=\"button\">\n  </button>\n</div>\n",
                encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
                providers: [{
                        provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"],
                        useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])((/**
                         * @return {?}
                         */
                        () => IonicSelectableComponent)),
                        multi: true
                    }],
                styles: [".item-ionic-selectable .item-inner .input-wrapper{align-items:normal}.item-ionic-selectable ion-label{flex:1;max-width:initial}.ionic-selectable{display:block;max-width:45%}.ionic-selectable-inner{display:flex;flex-wrap:wrap;flex-direction:row;justify-content:flex-end}.ionic-selectable-has-placeholder .ionic-selectable-value-item{color:var(--placeholder-color,#999)}.ionic-selectable-value{flex:1;padding-top:13px;padding-bottom:13px;overflow:hidden}.ionic-selectable-value-item{text-overflow:ellipsis;overflow:hidden;white-space:nowrap}.ionic-selectable-value-item:not(:last-child){margin-bottom:5px}.ionic-selectable-icon{position:relative;width:20px}.ionic-selectable-icon-inner{position:absolute;top:20px;left:5px;border-top:5px solid;border-right:5px solid transparent;border-left:5px solid transparent;pointer-events:none;color:var(--icon-color,#999)}.ionic-selectable-icon-template{-ms-grid-row-align:center;align-self:center;margin-left:5px}.ionic-selectable-ios .ionic-selectable-value{padding-top:11px;padding-bottom:11px}.ionic-selectable-ios .ionic-selectable-icon-inner{top:19px}.ionic-selectable-spinner{position:fixed;bottom:0;top:0;left:0;right:0;z-index:1}.ionic-selectable-spinner-background{top:0;bottom:0;left:0;right:0;position:absolute;background-color:#000;opacity:.05}.ionic-selectable-spinner ion-spinner{position:absolute;top:50%;left:50%;z-index:10;margin-top:-14px;margin-left:-14px}.ionic-selectable-cover{left:0;top:0;margin:0;position:absolute;width:100%;height:100%;border:0;background:0 0;cursor:pointer;-webkit-appearance:none;-moz-appearance:none;appearance:none;outline:0}.ionic-selectable-add-item-template{position:fixed;bottom:0;left:0;right:0;background-color:#fff}.ionic-selectable-add-item-template-inner{overflow-y:auto}.ionic-selectable-add-item-template-inner>ion-footer{bottom:0;position:absolute}.ionic-selectable:not(.ionic-selectable-has-label){max-width:100%;width:100%}.ionic-selectable:not(.ionic-selectable-has-label)-value-item{text-align:right}.ionic-selectable-label-floating,.ionic-selectable-label-stacked{-ms-grid-row-align:stretch;align-self:stretch;max-width:100%;padding-left:0;padding-top:8px;padding-bottom:8px}.ionic-selectable-label-floating .ionic-selectable-value,.ionic-selectable-label-stacked .ionic-selectable-value{padding-top:0;padding-bottom:0;min-height:19px}.ionic-selectable-label-floating .ionic-selectable-icon-inner,.ionic-selectable-label-stacked .ionic-selectable-icon-inner{top:7px}.ionic-selectable-label-floating.ionic-selectable-ios .ionic-selectable-value,.ionic-selectable-label-stacked.ionic-selectable-ios .ionic-selectable-value{padding-top:0;padding-bottom:0;min-height:20px}.ionic-selectable-label-floating.ionic-selectable-ios .ionic-selectable-icon-inner,.ionic-selectable-label-stacked.ionic-selectable-ios .ionic-selectable-icon-inner{top:8px}.ionic-selectable-label-default .ionic-selectable-value,.ionic-selectable-label-fixed .ionic-selectable-value{padding-left:var(--padding-start,16px)}.ionic-selectable-label-fixed:not(.ionic-selectable-has-value) .ionic-selectable-value{padding-left:calc(var(--padding-start,$padding) + 11px)}.ionic-selectable-modal .ionic-selectable-group ion-item-divider{padding-right:16px}.ionic-selectable-modal .ionic-selectable-item-button{margin-left:8px;margin-right:8px}.ionic-selectable-modal-ios .ionic-selectable-message{padding:8px}.ionic-selectable-modal-ios .ionic-selectable-group ion-item-divider{padding-right:8px}.ionic-selectable-modal-md .ionic-selectable-message{padding:8px 12px}.ionic-selectable-modal.ionic-selectable-modal-can-clear.ionic-selectable-modal-is-multiple .footer .col:first-child{padding-right:8px}.ionic-selectable-modal.ionic-selectable-modal-can-clear.ionic-selectable-modal-is-multiple .footer .col:last-child{padding-left:8px}.ionic-selectable-modal.ionic-selectable-modal-is-add-item-template-visible>.content>.scroll-content,.ionic-selectable-modal.ionic-selectable-modal-is-searching .scroll-content{overflow-y:hidden}.ionic-selectable-modal ion-header ion-toolbar:first-of-type{padding-top:var(--ion-safe-area-top,0)}"]
            }] }
];
/** @nocollapse */
IonicSelectableComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonItem"], decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"] }] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"] }
];
IonicSelectableComponent.propDecorators = {
    _cssClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable',] }],
    _isIos: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-ios',] }],
    _isMD: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-md',] }],
    _isMultipleCssClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-is-multiple',] }],
    _hasValueCssClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-has-value',] }],
    _hasPlaceholderCssClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-has-placeholder',] }],
    _hasIonLabelCssClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-has-label',] }],
    _hasDefaultIonLabelCssClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-label-default',] }],
    _hasFixedIonLabelCssClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-label-fixed',] }],
    _hasStackedIonLabelCssClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-label-stacked',] }],
    _hasFloatingIonLabelCssClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-label-floating',] }],
    items: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    itemsChange: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    isEnabled: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-is-enabled',] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['isEnabled',] }],
    shouldBackdropClose: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['shouldBackdropClose',] }],
    modalCssClass: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    modalEnterAnimation: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    modalLeaveAnimation: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    isConfirmButtonEnabled: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    hasConfirmButton: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['hasConfirmButton',] }],
    itemValueField: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    itemTextField: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    groupValueField: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    groupTextField: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    canSearch: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    isOnSearchEnabled: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['isOnSearchEnabled',] }],
    canClear: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ['class.ionic-selectable-can-clear',] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['canClear',] }],
    hasInfiniteScroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    hasVirtualScroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    virtualScrollApproxItemHeight: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    searchPlaceholder: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    placeholder: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    isMultiple: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['isMultiple',] }],
    searchFailText: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    clearButtonText: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    addButtonText: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    confirmButtonText: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    closeButtonText: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    shouldFocusSearchbar: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    headerColor: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    groupColor: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    closeButtonSlot: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    itemIconSlot: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    onChange: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    onSearch: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    onSearchFail: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    onSearchSuccess: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    onInfiniteScroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    onOpen: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    onClose: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    onSelect: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    onClear: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    searchDebounce: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    disabledItems: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    shouldStoreItemValue: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    canSaveItem: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    canDeleteItem: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    canAddItem: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['canAddItem',] }],
    onSaveItem: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    onDeleteItem: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    onAddItem: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    valueTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_ionic_selectable_value_template_directive__WEBPACK_IMPORTED_MODULE_17__["IonicSelectableValueTemplateDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], static: false },] }],
    itemTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_ionic_selectable_item_template_directive__WEBPACK_IMPORTED_MODULE_11__["IonicSelectableItemTemplateDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], static: false },] }],
    itemEndTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_ionic_selectable_item_end_template_directive__WEBPACK_IMPORTED_MODULE_9__["IonicSelectableItemEndTemplateDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], static: false },] }],
    titleTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_ionic_selectable_title_template_directive__WEBPACK_IMPORTED_MODULE_16__["IonicSelectableTitleTemplateDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], static: false },] }],
    placeholderTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_ionic_selectable_placeholder_template_directive__WEBPACK_IMPORTED_MODULE_14__["IonicSelectablePlaceholderTemplateDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], static: false },] }],
    messageTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_ionic_selectable_message_template_directive__WEBPACK_IMPORTED_MODULE_12__["IonicSelectableMessageTemplateDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], static: false },] }],
    groupTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_ionic_selectable_group_template_directive__WEBPACK_IMPORTED_MODULE_7__["IonicSelectableGroupTemplateDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], static: false },] }],
    groupEndTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_ionic_selectable_group_end_template_directive__WEBPACK_IMPORTED_MODULE_6__["IonicSelectableGroupEndTemplateDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], static: false },] }],
    closeButtonTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_ionic_selectable_close_button_template_directive__WEBPACK_IMPORTED_MODULE_4__["IonicSelectableCloseButtonTemplateDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], static: false },] }],
    searchFailTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_ionic_selectable_search_fail_template_directive__WEBPACK_IMPORTED_MODULE_15__["IonicSelectableSearchFailTemplateDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], static: false },] }],
    addItemTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_ionic_selectable_add_item_template_directive__WEBPACK_IMPORTED_MODULE_3__["IonicSelectableAddItemTemplateDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], static: false },] }],
    footerTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_ionic_selectable_footer_template_directive__WEBPACK_IMPORTED_MODULE_5__["IonicSelectableFooterTemplateDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], static: false },] }],
    headerTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_ionic_selectable_header_template_directive__WEBPACK_IMPORTED_MODULE_8__["IonicSelectableHeaderTemplateDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], static: false },] }],
    itemIconTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_ionic_selectable_item_icon_template_directive__WEBPACK_IMPORTED_MODULE_10__["IonicSelectableItemIconTemplateDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], static: false },] }],
    iconTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [_ionic_selectable_icon_template_directive__WEBPACK_IMPORTED_MODULE_18__["IonicSelectableIconTemplateDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], static: false },] }],
    virtualScrollHeaderFn: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }]
};
if (false) {}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9pb25pYy1zZWxlY3RhYmxlLyIsInNvdXJjZXMiOlsic3JjL2FwcC9jb21wb25lbnRzL2lvbmljLXNlbGVjdGFibGUvaW9uaWMtc2VsZWN0YWJsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFDQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBVyxVQUFVLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFrQixlQUFlLEVBQVUsUUFBUSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pPLE9BQU8sRUFBd0IsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN6RSxPQUFPLEVBQUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxRQUFRLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUdwRSxPQUFPLEVBQUUsdUNBQXVDLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUN6RyxPQUFPLEVBQUUsMkNBQTJDLEVBQUUsTUFBTSxvREFBb0QsQ0FBQztBQUNqSCxPQUFPLEVBQUUsc0NBQXNDLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUN0RyxPQUFPLEVBQUUsd0NBQXdDLEVBQUUsTUFBTSxpREFBaUQsQ0FBQztBQUMzRyxPQUFPLEVBQUUscUNBQXFDLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUNwRyxPQUFPLEVBQUUsc0NBQXNDLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUN0RyxPQUFPLEVBQUUsdUNBQXVDLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUN6RyxPQUFPLEVBQUUsd0NBQXdDLEVBQUUsTUFBTSxpREFBaUQsQ0FBQztBQUMzRyxPQUFPLEVBQUUsb0NBQW9DLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNsRyxPQUFPLEVBQUUsdUNBQXVDLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUN4RyxPQUFPLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUNuRixPQUFPLEVBQUUsMkNBQTJDLEVBQUUsTUFBTSxtREFBbUQsQ0FBQztBQUNoSCxPQUFPLEVBQUUsMENBQTBDLEVBQUUsTUFBTSxtREFBbUQsQ0FBQztBQUMvRyxPQUFPLEVBQUUscUNBQXFDLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUNwRyxPQUFPLEVBQUUscUNBQXFDLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUNwRyxPQUFPLEVBQUUsb0NBQW9DLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQWFsRyxNQUFNLE9BQU8sd0JBQXdCOzs7Ozs7Ozs7SUE4dkJuQyxZQUNVLGdCQUFpQyxFQUNqQyxTQUFtQixFQUNQLE9BQWdCLEVBQzVCLGdCQUFpQyxFQUNqQyxRQUFvQixFQUNwQixTQUFvQjtRQUxwQixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWlCO1FBQ2pDLGNBQVMsR0FBVCxTQUFTLENBQVU7UUFDUCxZQUFPLEdBQVAsT0FBTyxDQUFTO1FBQzVCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBaUI7UUFDakMsYUFBUSxHQUFSLFFBQVEsQ0FBWTtRQUNwQixjQUFTLEdBQVQsU0FBUyxDQUFXO1FBbHdCOUIsY0FBUyxHQUFHLElBQUksQ0FBQztRQXFDVCx1QkFBa0IsR0FBRyxJQUFJLENBQUM7UUFDMUIsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQix5QkFBb0IsR0FBRyxJQUFJLENBQUM7UUFDNUIsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNsQixXQUFNLEdBQVEsSUFBSSxDQUFDO1FBSW5CLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDbEIsc0JBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQzFCLGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBR3BCLGtCQUFhLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7UUFHdEQsaUJBQVksR0FBRyxLQUFLLENBQUM7UUFDckIsc0JBQWlCLEdBQXdELElBQUksQ0FBQztRQUM5RSxXQUFNLEdBQVcsSUFBSSxDQUFDO1FBUTlCLGdCQUFXLEdBQVUsRUFBRSxDQUFDO1FBQ3hCLGdCQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLFlBQU8sR0FBVSxFQUFFLENBQUM7UUFDcEIsb0JBQWUsR0FBVSxFQUFFLENBQUM7UUFDNUIsbUJBQWMsR0FBVSxFQUFFLENBQUM7UUFFM0Isb0JBQWUsR0FBVSxFQUFFLENBQUM7UUFJNUIsOEJBQXlCLEdBQUcsS0FBSyxDQUFDO1FBQ2xDLHFCQUFnQixHQUFHLElBQUksQ0FBQztRQUN4QixlQUFVLEdBQVEsSUFBSSxDQUFDO1FBQ3ZCLHdCQUFtQixHQUFHLENBQUMsQ0FBQztRQUN4QixzQkFBaUIsR0FBRyxLQUFLLENBQUM7Ozs7Ozs7O1FBcUYxQixVQUFLLEdBQVUsRUFBRSxDQUFDO1FBRWxCLGdCQUFXLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7Ozs7Ozs7O1FBMENwRCxrQkFBYSxHQUFXLElBQUksQ0FBQzs7Ozs7Ozs7UUFVN0Isd0JBQW1CLEdBQXFCLElBQUksQ0FBQzs7Ozs7Ozs7UUFVN0Msd0JBQW1CLEdBQXFCLElBQUksQ0FBQzs7Ozs7Ozs7UUFzQjdDLDJCQUFzQixHQUFHLElBQUksQ0FBQzs7Ozs7Ozs7O1FBNkI5QixtQkFBYyxHQUFXLElBQUksQ0FBQzs7Ozs7Ozs7O1FBVzlCLGtCQUFhLEdBQVcsSUFBSSxDQUFDOzs7Ozs7Ozs7O1FBWTdCLG9CQUFlLEdBQVcsSUFBSSxDQUFDOzs7Ozs7Ozs7UUFXL0IsbUJBQWMsR0FBVyxJQUFJLENBQUM7Ozs7Ozs7O1FBVTlCLGNBQVMsR0FBRyxLQUFLLENBQUM7Ozs7Ozs7OztRQTJDbEIsc0JBQWlCLEdBQUcsS0FBSyxDQUFDOzs7Ozs7Ozs7UUFXMUIscUJBQWdCLEdBQUcsS0FBSyxDQUFDOzs7Ozs7OztRQVV6QixrQ0FBNkIsR0FBRyxNQUFNLENBQUM7Ozs7Ozs7O1FBVXZDLHNCQUFpQixHQUFHLFFBQVEsQ0FBQzs7Ozs7Ozs7UUFVN0IsZ0JBQVcsR0FBVyxJQUFJLENBQUM7Ozs7Ozs7O1FBMEIzQixtQkFBYyxHQUFHLGlCQUFpQixDQUFDOzs7Ozs7OztRQVVuQyxvQkFBZSxHQUFHLE9BQU8sQ0FBQzs7Ozs7Ozs7UUFVMUIsa0JBQWEsR0FBRyxLQUFLLENBQUM7Ozs7Ozs7O1FBVXRCLHNCQUFpQixHQUFHLElBQUksQ0FBQzs7Ozs7Ozs7O1FBV3pCLG9CQUFlLEdBQUcsUUFBUSxDQUFDOzs7Ozs7OztRQVUzQix5QkFBb0IsR0FBRyxLQUFLLENBQUM7Ozs7Ozs7O1FBVTdCLGdCQUFXLEdBQVcsSUFBSSxDQUFDOzs7Ozs7OztRQVUzQixlQUFVLEdBQVcsSUFBSSxDQUFDOzs7Ozs7OztRQVUxQixvQkFBZSxHQUFHLE9BQU8sQ0FBQzs7Ozs7Ozs7UUFVMUIsaUJBQVksR0FBRyxPQUFPLENBQUM7Ozs7Ozs7UUFTdkIsYUFBUSxHQUFzRSxJQUFJLFlBQVksRUFBRSxDQUFDOzs7Ozs7OztRQVVqRyxhQUFRLEdBQXdFLElBQUksWUFBWSxFQUFFLENBQUM7Ozs7Ozs7UUFTbkcsaUJBQVksR0FBd0UsSUFBSSxZQUFZLEVBQUUsQ0FBQzs7Ozs7OztRQVN2RyxvQkFBZSxHQUF3RSxJQUFJLFlBQVksRUFBRSxDQUFDOzs7Ozs7OztRQVUxRyxxQkFBZ0IsR0FBd0UsSUFBSSxZQUFZLEVBQUUsQ0FBQzs7Ozs7OztRQVMzRyxXQUFNLEdBQTBELElBQUksWUFBWSxFQUFFLENBQUM7Ozs7Ozs7UUFTbkYsWUFBTyxHQUEwRCxJQUFJLFlBQVksRUFBRSxDQUFDOzs7Ozs7O1FBU3BGLGFBQVEsR0FBMEYsSUFBSSxZQUFZLEVBQUUsQ0FBQzs7Ozs7OztRQVNySCxZQUFPLEdBQXdFLElBQUksWUFBWSxFQUFFLENBQUM7Ozs7Ozs7O1FBdUJsRyxtQkFBYyxHQUFXLEdBQUcsQ0FBQzs7Ozs7Ozs7UUFVN0Isa0JBQWEsR0FBVSxFQUFFLENBQUM7Ozs7Ozs7OztRQVcxQix5QkFBb0IsR0FBRyxLQUFLLENBQUM7Ozs7Ozs7O1FBVTdCLGdCQUFXLEdBQUcsS0FBSyxDQUFDOzs7Ozs7OztRQVVwQixrQkFBYSxHQUFHLEtBQUssQ0FBQzs7Ozs7Ozs7O1FBMkJ0QixlQUFVLEdBQXFFLElBQUksWUFBWSxFQUFFLENBQUM7Ozs7Ozs7O1FBVWxHLGlCQUFZLEdBQXFFLElBQUksWUFBWSxFQUFFLENBQUM7Ozs7Ozs7OztRQVdwRyxjQUFTLEdBQTBELElBQUksWUFBWSxFQUFFLENBQUM7Ozs7Ozs7UUF5Q3RGLDBCQUFxQjs7O1FBQUcsR0FBRyxFQUFFO1lBQzNCLE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxFQUFBO1FBeWVPLHNCQUFpQjs7OztRQUFHLENBQUMsQ0FBTSxFQUFFLEVBQUUsR0FBRyxDQUFDLEVBQUM7UUFDcEMsdUJBQWtCOzs7UUFBRyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUM7UUFoZXJDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUU7WUFDckMsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7U0FDakI7UUFFRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ3RFLENBQUM7Ozs7SUFwd0JELElBQ0ksbUJBQW1CO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUN6QixDQUFDOzs7O0lBQ0QsSUFDSSxpQkFBaUI7UUFDbkIsT0FBTyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDekIsQ0FBQzs7OztJQUNELElBQ0ksdUJBQXVCO1FBQ3pCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztJQUM5QixDQUFDOzs7O0lBQ0QsSUFDSSxvQkFBb0I7UUFDdEIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzNCLENBQUM7Ozs7SUFDRCxJQUNJLDJCQUEyQjtRQUM3QixPQUFPLElBQUksQ0FBQyxpQkFBaUIsS0FBSyxTQUFTLENBQUM7SUFDOUMsQ0FBQzs7OztJQUNELElBQ0kseUJBQXlCO1FBQzNCLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixLQUFLLE9BQU8sQ0FBQztJQUM1QyxDQUFDOzs7O0lBQ0QsSUFDSSwyQkFBMkI7UUFDN0IsT0FBTyxJQUFJLENBQUMsaUJBQWlCLEtBQUssU0FBUyxDQUFDO0lBQzlDLENBQUM7Ozs7SUFDRCxJQUNJLDRCQUE0QjtRQUM5QixPQUFPLElBQUksQ0FBQyxpQkFBaUIsS0FBSyxVQUFVLENBQUM7SUFDL0MsQ0FBQzs7Ozs7SUFxQkQsSUFBWSxrQkFBa0I7UUFDNUIsT0FBTyxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxlQUFlO1lBQzNDLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUN4RCxDQUFDOzs7O0lBQ0QsSUFBSSxxQkFBcUI7UUFDdkIsT0FBTyxJQUFJLENBQUMsb0JBQW9CLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUN2RCxDQUFDOzs7Ozs7Ozs7O0lBMEJELElBQUksS0FBSztRQUNQLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNyQixDQUFDOzs7Ozs7Ozs7O0lBVUQsSUFBSSxVQUFVO1FBQ1osT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzFCLENBQUM7Ozs7O0lBQ0QsSUFBSSxVQUFVLENBQUMsVUFBa0I7UUFDL0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUM7UUFDOUIsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7SUFDM0IsQ0FBQzs7Ozs7Ozs7OztJQVVELElBQUksV0FBVztRQUNiLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztJQUMzQixDQUFDOzs7Ozs7Ozs7O0lBVUQsSUFBSSxhQUFhO1FBQ2YsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO0lBQzdCLENBQUM7Ozs7SUFFRCxJQUFJLEtBQUs7UUFDUCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFDRCxJQUFJLEtBQUssQ0FBQyxLQUFVO1FBQ2xCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBRXBCLG1CQUFtQjtRQUNuQixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUVwRCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbkIsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE1BQU0sRUFBRTtnQkFDekIsS0FBSyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDckQ7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDOUI7U0FDRjtRQUVELElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO0lBQzVCLENBQUM7Ozs7Ozs7OztJQXFCRCxJQUVJLFNBQVM7UUFDWCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDekIsQ0FBQzs7Ozs7SUFDRCxJQUFJLFNBQVMsQ0FBQyxTQUFrQjtRQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUM7UUFDOUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7Ozs7Ozs7O0lBU0QsSUFDSSxtQkFBbUI7UUFDckIsT0FBTyxJQUFJLENBQUMsb0JBQW9CLENBQUM7SUFDbkMsQ0FBQzs7Ozs7SUFDRCxJQUFJLG1CQUFtQixDQUFDLG1CQUE0QjtRQUNsRCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsQ0FBQyxDQUFDLG1CQUFtQixDQUFDO0lBQ3BELENBQUM7Ozs7Ozs7Ozs7SUF3Q0QsSUFBSSxRQUFRO1FBQ1YsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3hCLENBQUM7Ozs7Ozs7Ozs7O0lBcUJELElBQ0ksZ0JBQWdCO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDO0lBQ2hDLENBQUM7Ozs7O0lBQ0QsSUFBSSxnQkFBZ0IsQ0FBQyxnQkFBeUI7UUFDNUMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQztRQUM1QyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUM3QixDQUFDOzs7Ozs7Ozs7SUFnRUQsSUFDSSxpQkFBaUI7UUFDbkIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFDRCxJQUFJLGlCQUFpQixDQUFDLGlCQUEwQjtRQUM5QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDLGlCQUFpQixDQUFDO0lBQ2hELENBQUM7Ozs7Ozs7OztJQVNELElBRUksUUFBUTtRQUNWLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN4QixDQUFDOzs7OztJQUNELElBQUksUUFBUSxDQUFDLFFBQWlCO1FBQzVCLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQztRQUM1QixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUM3QixDQUFDOzs7Ozs7Ozs7SUE2REQsSUFDSSxVQUFVO1FBQ1osT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzFCLENBQUM7Ozs7O0lBQ0QsSUFBSSxVQUFVLENBQUMsVUFBbUI7UUFDaEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUMsVUFBVSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQzdCLENBQUM7Ozs7Ozs7Ozs7O0lBbU1ELElBQUksY0FBYztRQUNoQixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7SUFDOUIsQ0FBQzs7Ozs7Ozs7O0lBNERELElBQ0ksVUFBVTtRQUNaLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUMxQixDQUFDOzs7OztJQUNELElBQUksVUFBVSxDQUFDLFVBQW1CO1FBQ2hDLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLFVBQVUsQ0FBQztRQUNoQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUM3QixDQUFDOzs7O0lBNEZELFNBQVMsS0FBSyxDQUFDOzs7OztJQUVmLGFBQWEsQ0FBQyxTQUFrQjtRQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNqQixPQUFPO1NBQ1I7UUFFRCxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxDQUFDLFNBQVMsQ0FBQztJQUNyQyxDQUFDOzs7OztJQUVELG1CQUFtQixDQUFDLEtBQVU7UUFDNUIsSUFBSSxLQUFLLEtBQUssSUFBSSxJQUFJLEtBQUssS0FBSyxTQUFTLEVBQUU7WUFDekMsT0FBTyxJQUFJLENBQUM7U0FDYjtRQUVELCtDQUErQztRQUMvQyxPQUFPLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDeEQsQ0FBQzs7OztJQUVELGlCQUFpQjtRQUNmLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7Ozs7SUFFRCxZQUFZO1FBQ1YsT0FBTyxJQUFJLENBQUMsaUJBQWlCLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUN0RSxDQUFDOzs7O0lBRUQsY0FBYztRQUNaLE9BQU8sSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7Ozs7SUFFRCxhQUFhO1FBQ1gsT0FBTyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDaEUsQ0FBQzs7OztJQUVELGdCQUFnQjtRQUNkLE9BQU8sSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ3RFLENBQUM7Ozs7SUFFRCxnQkFBZ0I7UUFDZCxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRW5DLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO1lBQ2pCLFNBQVMsRUFBRSxJQUFJO1lBQ2YsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1NBQ2xCLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbkIsT0FBTztTQUNSO1FBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7WUFDakIsU0FBUyxFQUFFLElBQUk7WUFDZixJQUFJLEVBQUUsSUFBSSxDQUFDLFdBQVc7U0FDdkIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7O0lBRUQsYUFBYSxDQUFDLElBQVMsRUFBRSxVQUFtQjtRQUMxQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztZQUNqQixTQUFTLEVBQUUsSUFBSTtZQUNmLElBQUksRUFBRSxJQUFJO1lBQ1YsVUFBVSxFQUFFLFVBQVU7U0FDdkIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsS0FBWTtRQUN2QixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUNoQixTQUFTLEVBQUUsSUFBSTtZQUNmLEtBQUssRUFBRSxLQUFLO1NBQ2IsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCwwQkFBMEIsQ0FBQyxTQUFrQjs7Y0FDckMsU0FBUyxHQUFHO1lBQ2hCLFNBQVMsRUFBRSxJQUFJO1lBQ2YsSUFBSSxFQUFFLElBQUksQ0FBQyxXQUFXO1NBQ3ZCO1FBRUQsSUFBSSxTQUFTLEVBQUU7WUFDYixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUN0QzthQUFNO1lBQ0wsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDbkM7SUFDSCxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxJQUFTO1FBQ25CLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2xDLE9BQU8sSUFBSSxDQUFDO1NBQ2I7UUFFRCxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUN6RSxDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLElBQVM7UUFDeEIsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7OztrQkFFeEIsWUFBWSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSTs7OztZQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUMzQyxPQUFPLEtBQUssQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssSUFBSSxDQUFDO1lBQzdDLENBQUMsRUFBQztZQUVGLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUN2QzthQUFNO1lBQ0wsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQy9CO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsSUFBUztRQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNyQixPQUFPLElBQUksQ0FBQztTQUNiO1FBRUQsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQsbUJBQW1CLENBQUMsSUFBUztRQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNyQixPQUFPLElBQUksQ0FBQztTQUNiO1FBRUQsT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUN2RSxDQUFDOzs7O0lBRUQsaUJBQWlCO1FBQ2YseURBQXlEO1FBQ3pELG1CQUFtQjtRQUNuQixJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztJQUN4QixDQUFDOzs7O0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBRXpCLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRSxFQUFFO1lBQ3ZCLG1DQUFtQztZQUNuQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDcEI7YUFBTTs7O2dCQUVELE1BQU0sR0FBRyxFQUFFO1lBRWYsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxFQUFFO2dCQUNqRCxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQzthQUN2QjtpQkFBTTs7c0JBQ0MsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFO2dCQUV4RCxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU87Ozs7Z0JBQUMsS0FBSyxDQUFDLEVBQUU7OzBCQUNyQixLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNOzs7O29CQUFDLElBQUksQ0FBQyxFQUFFOzs4QkFDaEMsUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDOzRCQUNwQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxXQUFXLEVBQUU7d0JBQzNELE9BQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDN0MsQ0FBQyxFQUFDO29CQUVGLElBQUksS0FBSyxDQUFDLE1BQU0sRUFBRTt3QkFDaEIsTUFBTSxDQUFDLElBQUksQ0FBQzs0QkFDVixLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUs7NEJBQ2xCLElBQUksRUFBRSxLQUFLLENBQUMsSUFBSTs0QkFDaEIsS0FBSyxFQUFFLEtBQUs7eUJBQ2IsQ0FBQyxDQUFDO3FCQUNKO2dCQUNILENBQUMsRUFBQyxDQUFDO2dCQUVILGtCQUFrQjtnQkFDbEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0JBQ2xCLE1BQU0sQ0FBQyxJQUFJLENBQUM7d0JBQ1YsS0FBSyxFQUFFLEVBQUU7cUJBQ1YsQ0FBQyxDQUFDO2lCQUNKO2FBQ0Y7WUFFRCxJQUFJLENBQUMsZUFBZSxHQUFHLE1BQU0sQ0FBQztZQUM5QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3ZELElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUN6RDtJQUNILENBQUM7Ozs7O0lBRUQsZUFBZSxDQUFDLElBQVM7UUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdkIsT0FBTztTQUNSO1FBRUQsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUk7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUNyQyxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoRSxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsZUFBZSxDQUFDLElBQVM7UUFDdkIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUk7Ozs7UUFBQyxZQUFZLENBQUMsRUFBRTtZQUM3QyxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzdFLENBQUMsRUFBQyxLQUFLLFNBQVMsQ0FBQztJQUNuQixDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLElBQVM7UUFDeEIsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDOUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1NBQ3BEO2FBQU07WUFDTCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNoQztJQUNILENBQUM7Ozs7O0lBRUQsbUJBQW1CLENBQUMsSUFBUzs7WUFDdkIsaUJBQWlCO1FBRXJCLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTzs7Ozs7UUFBQyxDQUFDLFlBQVksRUFBRSxTQUFTLEVBQUUsRUFBRTtZQUN0RCxJQUNFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO2dCQUN4QixJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxDQUFDLEVBQ3RDO2dCQUNBLGlCQUFpQixHQUFHLFNBQVMsQ0FBQzthQUMvQjtRQUNILENBQUMsRUFBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDbkQsQ0FBQzs7OztJQUVELE1BQU07UUFDSixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNuQixPQUFPO1NBQ1I7UUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNuQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSTs7O1FBQUMsR0FBRyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUNmLFNBQVMsRUFBRSxJQUFJO2FBQ2hCLENBQUMsQ0FBQztRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7O0lBRUQsU0FBUyxDQUFDLEtBQVksRUFBRSxJQUFTO1FBQy9CLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUV2QixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUUsRUFBRTtZQUN6QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztnQkFDbkIsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsSUFBSSxFQUFFLElBQUksQ0FBQyxVQUFVO2FBQ3RCLENBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztTQUM1QjtJQUNILENBQUM7Ozs7OztJQUVELGdCQUFnQixDQUFDLEtBQVksRUFBRSxJQUFTO1FBQ3RDLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUV2QixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFO1lBQzNCLDJCQUEyQjtZQUMzQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQztnQkFDckIsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsSUFBSSxFQUFFLElBQUksQ0FBQyxVQUFVO2FBQ3RCLENBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUNsQztJQUNILENBQUM7Ozs7SUFFRCxhQUFhO1FBQ1gsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQUU7WUFDeEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7Z0JBQ2xCLFNBQVMsRUFBRSxJQUFJO2FBQ2hCLENBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztTQUM1QjtJQUNILENBQUM7Ozs7SUFFRCx3QkFBd0I7UUFDdEIsbUNBQW1DO1FBQ25DLFVBQVU7OztRQUFDLEdBQUcsRUFBRTs7a0JBQ1IsTUFBTSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGFBQWE7aUJBQ3ZELGFBQWEsQ0FBQyxnREFBZ0QsQ0FBQztZQUVsRSxJQUFJLENBQUMsNEJBQTRCLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxlQUFlLE1BQU0sQ0FBQyxZQUFZLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQ2hHLENBQUMsR0FBRSxHQUFHLENBQUMsQ0FBQztJQUNWLENBQUM7Ozs7SUFFRCxNQUFNO1FBQ0osSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLElBQUk7OztRQUFDLEdBQUcsRUFBRTtZQUNyQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztnQkFDaEIsU0FBUyxFQUFFLElBQUk7YUFDaEIsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxFQUFFO1lBQ3hCLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1NBQzFCO0lBQ0gsQ0FBQzs7OztJQUVELE1BQU07O2NBQ0UsYUFBYSxHQUFHLElBQUksQ0FBQyxjQUFjO1FBRXpDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNiLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDakMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLElBQUk7OztRQUFDLEdBQUcsRUFBRTtZQUNyQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztnQkFDaEIsU0FBUyxFQUFFLElBQUk7YUFDaEIsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQsYUFBYTtRQUNYLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7WUFDekIsU0FBUyxFQUFFLElBQUk7WUFDZixJQUFJLEVBQUUsSUFBSSxDQUFDLFdBQVc7U0FDdkIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxrQkFBa0IsQ0FBQyxLQUFZO1FBQzdCLDJFQUEyRTtRQUMzRSxJQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7Ozs7SUFFRCxTQUFTLENBQUMsWUFBaUI7UUFDekIsSUFBSSxDQUFDLEtBQUssR0FBRyxZQUFZLENBQUM7UUFDMUIsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDMUIsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsSUFBUzs7Y0FDVCxjQUFjLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUM7UUFFakQsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLElBQUksY0FBYyxFQUFFO2dCQUNsQixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDaEM7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzdCO1lBRUQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUU3QywyRUFBMkU7WUFDM0Usb0JBQW9CO1lBQ3BCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDM0M7YUFBTTtZQUNMLElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ2hELHdEQUF3RDtnQkFDeEQsb0VBQW9FO2dCQUNwRSxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztnQkFFekIsSUFBSSxjQUFjLEVBQUU7b0JBQ2xCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDaEM7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUM3QjtnQkFFRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUU3QywyRUFBMkU7Z0JBQzNFLG9CQUFvQjtnQkFDcEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxjQUFjLENBQUMsQ0FBQzthQUMzQztpQkFBTTtnQkFDTCxJQUFJLENBQUMsY0FBYyxFQUFFO29CQUNuQixJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztvQkFDekIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO29CQUU1QixpQ0FBaUM7b0JBQ2pDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUUvQixJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRTt3QkFDOUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7cUJBQzFDO3lCQUFNO3dCQUNMLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ3RCO2lCQUNGO2dCQUVELElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQzthQUNmO1NBQ0Y7SUFDSCxDQUFDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUNmLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNoQixDQUFDOzs7OztJQUVPLGFBQWE7UUFDbkIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUMxRSxDQUFDOzs7Ozs7SUFFTyxlQUFlLENBQUMsTUFBTTtRQUM1QixPQUFPLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxJQUFJLE1BQU0sQ0FBQyxLQUFLOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDakQsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO1FBQ2xELENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFTyxtQkFBbUI7O1lBQ3JCLGtCQUFrQixHQUFHLENBQUM7UUFFMUIsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pCLGtCQUFrQixFQUFFLENBQUM7U0FDdEI7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQzdDLGtCQUFrQixFQUFFLENBQUM7U0FDdEI7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbkIsa0JBQWtCLEVBQUUsQ0FBQztTQUN0QjtRQUVELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxrQkFBa0IsQ0FBQztJQUNoRCxDQUFDOzs7Ozs7SUFFTyxTQUFTLENBQUMsS0FBWTs7Ozs7WUFJeEIsTUFBTSxHQUFVLENBQUM7Z0JBQ25CLEtBQUssRUFBRSxLQUFLLElBQUksRUFBRTthQUNuQixDQUFDO1FBRUYsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUN6QixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ25CLE1BQU0sR0FBRyxFQUFFLENBQUM7Z0JBRVosS0FBSyxDQUFDLE9BQU87Ozs7Z0JBQUMsSUFBSSxDQUFDLEVBQUU7OzBCQUNiLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUM7OzBCQUNuRSxLQUFLLEdBQUcsTUFBTSxDQUFDLElBQUk7Ozs7b0JBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxLQUFLLFVBQVUsRUFBQztvQkFFNUQsSUFBSSxLQUFLLEVBQUU7d0JBQ1QsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ3hCO3lCQUFNO3dCQUNMLE1BQU0sQ0FBQyxJQUFJLENBQUM7NEJBQ1YsS0FBSyxFQUFFLFVBQVU7NEJBQ2pCLElBQUksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUM7NEJBQ3ZELEtBQUssRUFBRSxDQUFDLElBQUksQ0FBQzt5QkFDZCxDQUFDLENBQUM7cUJBQ0o7Z0JBQ0gsQ0FBQyxFQUFDLENBQUM7YUFDSjtTQUNGO1FBRUQsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDdEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7Ozs7Ozs7SUFFTyxpQkFBaUIsQ0FBQyxNQUFXLEVBQUUsUUFBZ0I7UUFDckQsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNiLE9BQU8sSUFBSSxDQUFDO1NBQ2I7UUFFRCxPQUFPLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTTs7Ozs7UUFBQyxDQUFDLE9BQU8sRUFBRSxTQUFTLEVBQUUsRUFBRTtZQUN2RCxPQUFPLE9BQU8sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDN0MsQ0FBQyxHQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2IsQ0FBQzs7Ozs7O0lBRU8sbUJBQW1CLENBQUMsUUFBaUI7UUFDM0MsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDakIsT0FBTztTQUNSO1FBRUQsbUVBQW1FO1FBQ25FLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUN2RCxDQUFDOzs7OztJQUVPLG1CQUFtQjtRQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNqQixPQUFPO1NBQ1I7UUFFRCxtRUFBbUU7UUFDbkUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0lBQzlELENBQUM7Ozs7O0lBRU8sa0JBQWtCO1FBQ3hCLElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ3JDLENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7WUFDM0UsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7Ozs7OztJQUtPLG1CQUFtQixDQUFDLFFBQWdCLEVBQUUsU0FBa0I7UUFDOUQsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDekIsT0FBTztTQUNSO1FBRUQsc0JBQXNCO1FBQ3RCLElBQUksU0FBUyxFQUFFO1lBQ2IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxRQUFRLENBQUMsQ0FBQztTQUN6RDthQUFNO1lBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxRQUFRLENBQUMsQ0FBQztTQUM1RDtJQUNILENBQUM7Ozs7OztJQUVPLHNCQUFzQixDQUFDLFNBQWtCO1FBQy9DLDZEQUE2RDtRQUM3RCxtRUFBbUU7UUFDbkUsZ0ZBQWdGO1FBQ2hGLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3pCLE9BQU87U0FDUjtRQUVELDRFQUE0RTtRQUM1RSw0RUFBNEU7UUFDNUUsSUFBSSxDQUFDLHlCQUF5QixHQUFHLFNBQVMsQ0FBQztRQUMzQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxTQUFTLENBQUM7SUFDckMsQ0FBQzs7Ozs7O0lBR0QsVUFBVSxDQUFDLEtBQVU7UUFDbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxNQUFXO1FBQzFCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUM7SUFDbEMsQ0FBQzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxNQUFrQjtRQUNsQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsTUFBTSxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsVUFBbUI7UUFDbEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLFVBQVUsQ0FBQztJQUMvQixDQUFDOzs7OztJQUdELFFBQVE7UUFDTixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzFCLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2xFLDBDQUEwQztRQUMxQywrREFBK0Q7UUFDL0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFFOUYsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3ZFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNuRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsdUJBQXVCLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFeEQsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO2dCQUN4QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBRXhFLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO29CQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztvQkFDekIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksU0FBUyxDQUFDO2lCQUN0RjthQUNGO1NBQ0Y7UUFFRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNyQyxDQUFDOzs7O0lBRUQsU0FBUzs7Y0FDRCxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUV2RCxJQUFJLFlBQVksRUFBRTtZQUNoQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMzQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7WUFFeEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUM7Z0JBQ3RCLFNBQVMsRUFBRSxJQUFJO2FBQ2hCLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7Ozs7Ozs7OztJQVdELE9BQU8sQ0FBQyxJQUFTOztjQUNULElBQUksR0FBRyxJQUFJO1FBRWpCLHNDQUFzQztRQUN0Qyw4REFBOEQ7UUFDOUQsMEVBQTBFO1FBQzFFLG1EQUFtRDtRQUNuRCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUV6QixrQ0FBa0M7UUFDbEMsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDM0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3ZDO1FBRUQsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUMsVUFBVSxPQUFPLEVBQUUsTUFBTTtZQUMxQyxpREFBaUQ7WUFDakQsaURBQWlEO1lBQ2pELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksRUFBRSxDQUFDLFNBQVM7OztZQUFDLEdBQUcsRUFBRTtnQkFDekUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUN0QyxPQUFPLEVBQUUsQ0FBQztZQUNaLENBQUM7OztZQUFFLEdBQUcsRUFBRTtnQkFDTixJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ3RDLE1BQU0sRUFBRSxDQUFDO1lBQ1gsQ0FBQyxFQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7Ozs7Ozs7SUFXRCxVQUFVLENBQUMsSUFBUzs7Y0FDWixJQUFJLEdBQUcsSUFBSTs7WUFDYixlQUFlLEdBQUcsS0FBSztRQUUzQiwyQ0FBMkM7UUFDM0MsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNOzs7O1lBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ3ZELE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdEUsQ0FBQyxFQUFDLENBQUM7U0FDSjtRQUVELGtDQUFrQztRQUNsQyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDZCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7O3NCQUNiLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU07Ozs7Z0JBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQ3ZDLE9BQU8sS0FBSyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsRUFBRSxDQUFDO2dCQUM5QixDQUFDLEVBQUM7Z0JBRUYsSUFBSSxNQUFNLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFO29CQUN2QyxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQztvQkFDcEIsZUFBZSxHQUFHLElBQUksQ0FBQztpQkFDeEI7YUFDRjtpQkFBTTtnQkFDTCxJQUFJLElBQUksS0FBSyxJQUFJLENBQUMsS0FBSyxFQUFFO29CQUN2QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztvQkFDbEIsZUFBZSxHQUFHLElBQUksQ0FBQztpQkFDeEI7YUFDRjtTQUNGO1FBRUQsSUFBSSxlQUFlLEVBQUU7WUFDbkIsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7U0FDekI7OztjQUdLLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU07Ozs7UUFBQyxLQUFLLENBQUMsRUFBRTtZQUN0QyxPQUFPLEtBQUssQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUM5QixDQUFDLEVBQUM7UUFFRixxQ0FBcUM7UUFDckMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFN0IsZ0JBQWdCO1FBQ2hCLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFdEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUM7WUFDdEIsU0FBUyxFQUFFLElBQUk7U0FDaEIsQ0FBQyxDQUFDO1FBRUgsa0NBQWtDO1FBQ2xDLElBQUksSUFBSSxDQUFDLHFCQUFxQixFQUFFO1lBQzlCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUMxQztRQUVELE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLFVBQVUsT0FBTyxFQUFFLE1BQU07WUFDMUMsaURBQWlEO1lBQ2pELGlEQUFpRDtZQUNqRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxTQUFTOzs7WUFBQyxHQUFHLEVBQUU7Z0JBQzVFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDekMsT0FBTyxFQUFFLENBQUM7WUFDWixDQUFDOzs7WUFBRSxHQUFHLEVBQUU7Z0JBQ04sSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUN6QyxNQUFNLEVBQUUsQ0FBQztZQUNYLENBQUMsRUFBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7Ozs7OztJQVNELFFBQVE7UUFDTixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbkIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUM7U0FDdEM7YUFBTTtZQUNMLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN4RjtJQUNILENBQUM7Ozs7Ozs7O0lBU0QsSUFBSTs7Y0FDSSxJQUFJLEdBQUcsSUFBSTtRQUVqQixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxVQUFVLE9BQU8sRUFBRSxNQUFNO1lBQzFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ3RDLE1BQU0sQ0FBQyxnREFBZ0QsQ0FBQyxDQUFDO2dCQUN6RCxPQUFPO2FBQ1I7WUFFRCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7WUFDcEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7O2tCQUVoQixZQUFZLEdBQWlCO2dCQUNqQyxTQUFTLEVBQUUsNkJBQTZCO2dCQUN4QyxjQUFjLEVBQUUsRUFBRSxlQUFlLEVBQUUsSUFBSSxFQUFFO2dCQUN6QyxlQUFlLEVBQUUsSUFBSSxDQUFDLG9CQUFvQjthQUMzQztZQUVELElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtnQkFDdEIsWUFBWSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO2FBQzVDO1lBRUQsSUFBSSxJQUFJLENBQUMsbUJBQW1CLEVBQUU7Z0JBQzVCLFlBQVksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDO2FBQ3hEO1lBRUQsSUFBSSxJQUFJLENBQUMsbUJBQW1CLEVBQUU7Z0JBQzVCLFlBQVksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDO2FBQ3hEO1lBRUQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxJQUFJOzs7O1lBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ3RELElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO2dCQUNwQixLQUFLLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSTs7O2dCQUFDLEdBQUcsRUFBRTtvQkFDeEIsNkVBQTZFO29CQUM3RSx3QkFBd0I7b0JBQ3hCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDL0IsT0FBTyxFQUFFLENBQUM7Z0JBQ1osQ0FBQyxFQUFDLENBQUM7Z0JBRUgsS0FBSyxDQUFDLGFBQWEsRUFBRSxDQUFDLElBQUk7OztnQkFBQyxHQUFHLEVBQUU7b0JBQzlCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDbEMsQ0FBQyxFQUFDLENBQUM7Z0JBRUgsS0FBSyxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUk7Ozs7Z0JBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQ2hDLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO29CQUN2QixJQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQztvQkFFMUIsZ0RBQWdEO29CQUNoRCxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssVUFBVSxFQUFFO3dCQUM3QixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQzs0QkFDaEIsU0FBUyxFQUFFLElBQUk7eUJBQ2hCLENBQUMsQ0FBQztxQkFDSjtnQkFDSCxDQUFDLEVBQUMsQ0FBQztZQUNMLENBQUMsRUFBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7Ozs7OztJQVNELEtBQUs7O2NBQ0csSUFBSSxHQUFHLElBQUk7UUFFakIsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUMsVUFBVSxPQUFPLEVBQUUsTUFBTTtZQUMxQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ3ZDLE1BQU0sQ0FBQyxnREFBZ0QsQ0FBQyxDQUFDO2dCQUN6RCxPQUFPO2FBQ1I7WUFFRCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztZQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztZQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUk7OztZQUFDLEdBQUcsRUFBRTtnQkFDOUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNoQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztnQkFDM0IsT0FBTyxFQUFFLENBQUM7WUFDWixDQUFDLEVBQUMsQ0FBQztRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7Ozs7SUFRRCxLQUFLO1FBQ0gsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUN6QyxJQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JDLENBQUM7Ozs7Ozs7O0lBUUQsT0FBTztRQUNMLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNuQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztTQUNyQzthQUFNLElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdkQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDO1NBQ2hEO0lBQ0gsQ0FBQzs7Ozs7Ozs7OztJQVVELFdBQVcsQ0FBQyxRQUFpQixFQUFFLEtBQWE7UUFDMUMsSUFBSSxRQUFRLEVBQUU7O2tCQUNOLFFBQVEsR0FBRyxLQUFLLElBQUksS0FBSyxDQUFDLE1BQU07O2dCQUNsQyxhQUFhLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNOzs7OztZQUFDLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxFQUFFO2dCQUMxRCxPQUFPLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3RDLENBQUMsR0FBRSxFQUFFLENBQUM7WUFFTixrREFBa0Q7WUFDbEQsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2pDLGFBQWEsR0FBRyxFQUFFLENBQUM7YUFDcEI7WUFFRCx5QkFBeUI7WUFDekIsSUFBSSxRQUFRLEVBQUU7Z0JBQ1osYUFBYSxHQUFHLGFBQWEsQ0FBQyxNQUFNOzs7O2dCQUFDLFlBQVksQ0FBQyxFQUFFO29CQUNsRCxPQUFPLEtBQUssQ0FBQyxJQUFJOzs7O29CQUFDLElBQUksQ0FBQyxFQUFFO3dCQUN2QixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLEtBQUssSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDdkUsQ0FBQyxFQUFDLEtBQUssU0FBUyxDQUFDO2dCQUNuQixDQUFDLEVBQUMsQ0FBQztnQkFFSCx1Q0FBdUM7Z0JBQ3ZDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO29CQUNwQixhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztpQkFDNUI7YUFDRjtZQUVELGFBQWEsQ0FBQyxPQUFPOzs7O1lBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzNCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5QixDQUFDLEVBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztTQUMxQjtRQUVELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDL0MsQ0FBQzs7Ozs7Ozs7SUFTRCxXQUFXOztjQUNILElBQUksR0FBRyxJQUFJO1FBRWpCLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLFVBQVUsT0FBTyxFQUFFLE1BQU07WUFDMUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ25CLE1BQU0sQ0FBQyw2Q0FBNkMsQ0FBQyxDQUFDO2dCQUN0RCxPQUFPO2FBQ1I7WUFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJOzs7WUFBQyxHQUFHLEVBQUU7Z0JBQ3BELE9BQU8sRUFBRSxDQUFDO1lBQ1osQ0FBQyxFQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7Ozs7O0lBU0QsY0FBYzs7Y0FDTixJQUFJLEdBQUcsSUFBSTtRQUVqQixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxVQUFVLE9BQU8sRUFBRSxNQUFNO1lBQzFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO2dCQUNuQixNQUFNLENBQUMsNkNBQTZDLENBQUMsQ0FBQztnQkFDdEQsT0FBTzthQUNSO1lBRUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsY0FBYyxFQUFFLENBQUMsSUFBSTs7O1lBQUMsR0FBRyxFQUFFO2dCQUN2RCxPQUFPLEVBQUUsQ0FBQztZQUNaLENBQUMsRUFBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7Ozs7Ozs7SUFTRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDcEIsT0FBTztTQUNSO1FBRUQsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7Ozs7Ozs7OztJQVNELFNBQVM7UUFDUCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNwQixPQUFPO1NBQ1I7UUFFRCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFFbkIsc0RBQXNEO1FBQ3RELG9FQUFvRTtRQUNwRSxnRUFBZ0U7UUFDaEUsMEJBQTBCO1FBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUMxRCxDQUFDOzs7Ozs7OztJQVFELG9CQUFvQjtRQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQzVCLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDeEQsQ0FBQzs7Ozs7Ozs7SUFRRCxxQkFBcUI7UUFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUM1QixPQUFPO1NBQ1I7UUFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO0lBQ3ZELENBQUM7Ozs7Ozs7O0lBUUQsaUJBQWlCO1FBQ2YsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUM1QixPQUFPO1NBQ1I7UUFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoRCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM3QixDQUFDOzs7Ozs7Ozs7O0lBVUQsTUFBTSxDQUFDLElBQVk7UUFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUMxRCxPQUFPO1NBQ1I7UUFFRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUN4QixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7Ozs7Ozs7SUFRRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDcEIsT0FBTztTQUNSO1FBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7SUFDM0IsQ0FBQzs7Ozs7Ozs7SUFRRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDcEIsT0FBTztTQUNSO1FBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7SUFDNUIsQ0FBQzs7Ozs7Ozs7SUFRRCxtQkFBbUI7UUFDakIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBRWxDLCtDQUErQztRQUMvQyxJQUFJLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztJQUNsQyxDQUFDOzs7Ozs7OztJQVFELG1CQUFtQjtRQUNqQixrRkFBa0Y7UUFDbEYsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JDLENBQUM7OztZQXp5REYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxrQkFBa0I7Z0JBQzVCLDBzREFBZ0Q7Z0JBRWhELGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO2dCQUNyQyxTQUFTLEVBQUUsQ0FBQzt3QkFDVixPQUFPLEVBQUUsaUJBQWlCO3dCQUMxQixXQUFXLEVBQUUsVUFBVTs7O3dCQUFDLEdBQUcsRUFBRSxDQUFDLHdCQUF3QixFQUFDO3dCQUN2RCxLQUFLLEVBQUUsSUFBSTtxQkFDWixDQUFDOzthQUNIOzs7O1lBOUJpQixlQUFlO1lBQUUsUUFBUTtZQUFsQyxPQUFPLHVCQWd5QlgsUUFBUTtZQWx5QndHLGVBQWU7WUFBekYsVUFBVTtZQUEyRyxTQUFTOzs7d0JBa0N0SyxXQUFXLFNBQUMsd0JBQXdCO3FCQUVwQyxXQUFXLFNBQUMsNEJBQTRCO29CQUV4QyxXQUFXLFNBQUMsMkJBQTJCO2tDQUV2QyxXQUFXLFNBQUMsb0NBQW9DO2dDQUloRCxXQUFXLFNBQUMsa0NBQWtDO3NDQUk5QyxXQUFXLFNBQUMsd0NBQXdDO21DQUlwRCxXQUFXLFNBQUMsa0NBQWtDOzBDQUk5QyxXQUFXLFNBQUMsc0NBQXNDO3dDQUlsRCxXQUFXLFNBQUMsb0NBQW9DOzBDQUloRCxXQUFXLFNBQUMsc0NBQXNDOzJDQUlsRCxXQUFXLFNBQUMsdUNBQXVDO29CQWtJbkQsS0FBSzswQkFFTCxNQUFNO3dCQVVOLFdBQVcsU0FBQyxtQ0FBbUMsY0FDL0MsS0FBSyxTQUFDLFdBQVc7a0NBZ0JqQixLQUFLLFNBQUMscUJBQXFCOzRCQWUzQixLQUFLO2tDQVVMLEtBQUs7a0NBVUwsS0FBSztxQ0FzQkwsS0FBSzsrQkFZTCxLQUFLLFNBQUMsa0JBQWtCOzZCQWlCeEIsS0FBSzs0QkFXTCxLQUFLOzhCQVlMLEtBQUs7NkJBV0wsS0FBSzt3QkFVTCxLQUFLO2dDQVVMLEtBQUssU0FBQyxtQkFBbUI7dUJBZXpCLFdBQVcsU0FBQyxrQ0FBa0MsY0FDOUMsS0FBSyxTQUFDLFVBQVU7Z0NBaUJoQixLQUFLOytCQVdMLEtBQUs7NENBVUwsS0FBSztnQ0FVTCxLQUFLOzBCQVVMLEtBQUs7eUJBVUwsS0FBSyxTQUFDLFlBQVk7NkJBZ0JsQixLQUFLOzhCQVVMLEtBQUs7NEJBVUwsS0FBSztnQ0FVTCxLQUFLOzhCQVdMLEtBQUs7bUNBVUwsS0FBSzswQkFVTCxLQUFLO3lCQVVMLEtBQUs7OEJBVUwsS0FBSzsyQkFVTCxLQUFLO3VCQVNMLE1BQU07dUJBVU4sTUFBTTsyQkFTTixNQUFNOzhCQVNOLE1BQU07K0JBVU4sTUFBTTtxQkFTTixNQUFNO3NCQVNOLE1BQU07dUJBU04sTUFBTTtzQkFTTixNQUFNOzZCQXVCTixLQUFLOzRCQVVMLEtBQUs7bUNBV0wsS0FBSzswQkFVTCxLQUFLOzRCQVVMLEtBQUs7eUJBVUwsS0FBSyxTQUFDLFlBQVk7eUJBaUJsQixNQUFNOzJCQVVOLE1BQU07d0JBV04sTUFBTTs0QkFHTixZQUFZLFNBQUMscUNBQXFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUU7MkJBRXhGLFlBQVksU0FBQyxvQ0FBb0MsRUFBRSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTs4QkFFdkYsWUFBWSxTQUFDLHVDQUF1QyxFQUFFLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFOzRCQUUxRixZQUFZLFNBQUMscUNBQXFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUU7a0NBRXhGLFlBQVksU0FBQywyQ0FBMkMsRUFBRSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTs4QkFFOUYsWUFBWSxTQUFDLHVDQUF1QyxFQUFFLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFOzRCQUUxRixZQUFZLFNBQUMscUNBQXFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUU7K0JBRXhGLFlBQVksU0FBQyx3Q0FBd0MsRUFBRSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTtrQ0FFM0YsWUFBWSxTQUFDLDJDQUEyQyxFQUFFLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFO2lDQUU5RixZQUFZLFNBQUMsMENBQTBDLEVBQUUsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUU7OEJBRTdGLFlBQVksU0FBQyx1Q0FBdUMsRUFBRSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTs2QkFFMUYsWUFBWSxTQUFDLHNDQUFzQyxFQUFFLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFOzZCQUd6RixZQUFZLFNBQUMsc0NBQXNDLEVBQUUsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUU7K0JBRXpGLFlBQVksU0FBQyx3Q0FBd0MsRUFBRSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTsyQkFFM0YsWUFBWSxTQUFDLG9DQUFvQyxFQUFFLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFO29DQVN2RixLQUFLOzs7O0lBeHZCTiw2Q0FDaUI7O0lBQ2pCLDBDQUNnQjs7SUFDaEIseUNBQ2U7Ozs7O0lBaUNmLHNEQUFrQzs7Ozs7SUFDbEMsOENBQTBCOzs7OztJQUMxQix3REFBb0M7Ozs7O0lBQ3BDLDZDQUEwQjs7Ozs7SUFDMUIsMENBQTJCOzs7OztJQUMzQiwwQ0FBb0M7Ozs7O0lBQ3BDLGdEQUEwQzs7Ozs7SUFDMUMsK0NBQTZCOzs7OztJQUM3Qiw2Q0FBMEI7Ozs7O0lBQzFCLHFEQUFrQzs7Ozs7SUFDbEMsK0NBQTRCOzs7OztJQUM1QiwrQ0FBNEI7Ozs7O0lBQzVCLHNEQUF5Qzs7Ozs7SUFDekMseURBQTRDOzs7OztJQUM1QyxpREFBOEQ7Ozs7O0lBQzlELG1EQUE2Qjs7Ozs7SUFDN0Isb0RBQThCOzs7OztJQUM5QixnREFBNkI7Ozs7O0lBQzdCLHFEQUFzRjs7Ozs7SUFDdEYsMENBQThCOztJQVE5QiwrQ0FBd0I7O0lBQ3hCLCtDQUFpQjs7SUFDakIsa0RBQXVCOztJQUN2QiwyQ0FBb0I7O0lBQ3BCLG1EQUE0Qjs7SUFDNUIsa0RBQTJCOztJQUMzQixtREFBK0M7O0lBQy9DLG1EQUE0Qjs7SUFDNUIsOENBQW9COztJQUNwQixnREFBc0I7O0lBQ3RCLG1EQUF5Qjs7SUFDekIsNkRBQWtDOztJQUNsQyxvREFBd0I7O0lBQ3hCLDhDQUF1Qjs7SUFDdkIsdURBQXdCOztJQUN4QixxREFBMEI7Ozs7Ozs7OztJQW9GMUIseUNBQ2tCOztJQUNsQiwrQ0FDb0Q7Ozs7Ozs7OztJQXlDcEQsaURBQzZCOzs7Ozs7Ozs7SUFTN0IsdURBQzZDOzs7Ozs7Ozs7SUFTN0MsdURBQzZDOzs7Ozs7Ozs7SUFxQjdDLDBEQUM4Qjs7Ozs7Ozs7OztJQTRCOUIsa0RBQzhCOzs7Ozs7Ozs7O0lBVTlCLGlEQUM2Qjs7Ozs7Ozs7Ozs7SUFXN0IsbURBQytCOzs7Ozs7Ozs7O0lBVS9CLGtEQUM4Qjs7Ozs7Ozs7O0lBUzlCLDZDQUNrQjs7Ozs7Ozs7OztJQTBDbEIscURBQzBCOzs7Ozs7Ozs7O0lBVTFCLG9EQUN5Qjs7Ozs7Ozs7O0lBU3pCLGlFQUN1Qzs7Ozs7Ozs7O0lBU3ZDLHFEQUM2Qjs7Ozs7Ozs7O0lBUzdCLCtDQUMyQjs7Ozs7Ozs7O0lBeUIzQixrREFDbUM7Ozs7Ozs7OztJQVNuQyxtREFDMEI7Ozs7Ozs7OztJQVMxQixpREFDc0I7Ozs7Ozs7OztJQVN0QixxREFDeUI7Ozs7Ozs7Ozs7SUFVekIsbURBQzJCOzs7Ozs7Ozs7SUFTM0Isd0RBQzZCOzs7Ozs7Ozs7SUFTN0IsK0NBQzJCOzs7Ozs7Ozs7SUFTM0IsOENBQzBCOzs7Ozs7Ozs7SUFTMUIsbURBQzBCOzs7Ozs7Ozs7SUFTMUIsZ0RBQ3VCOzs7Ozs7OztJQVF2Qiw0Q0FDaUc7Ozs7Ozs7OztJQVNqRyw0Q0FDbUc7Ozs7Ozs7O0lBUW5HLGdEQUN1Rzs7Ozs7Ozs7SUFRdkcsbURBQzBHOzs7Ozs7Ozs7SUFTMUcsb0RBQzJHOzs7Ozs7OztJQVEzRywwQ0FDbUY7Ozs7Ozs7O0lBUW5GLDJDQUNvRjs7Ozs7Ozs7SUFRcEYsNENBQ3FIOzs7Ozs7OztJQVFySCwyQ0FDa0c7Ozs7Ozs7OztJQXNCbEcsa0RBQzZCOzs7Ozs7Ozs7SUFTN0IsaURBQzBCOzs7Ozs7Ozs7O0lBVTFCLHdEQUM2Qjs7Ozs7Ozs7O0lBUzdCLCtDQUNvQjs7Ozs7Ozs7O0lBU3BCLGlEQUNzQjs7Ozs7Ozs7OztJQTBCdEIsOENBQ2tHOzs7Ozs7Ozs7SUFTbEcsZ0RBQ29HOzs7Ozs7Ozs7O0lBVXBHLDZDQUNzRjs7SUFFdEYsaURBQ2dDOztJQUNoQyxnREFDK0I7O0lBQy9CLG1EQUNrQzs7SUFDbEMsaURBQ2dDOztJQUNoQyx1REFDc0M7O0lBQ3RDLG1EQUNrQzs7SUFDbEMsaURBQ2dDOztJQUNoQyxvREFDbUM7O0lBQ25DLHVEQUNzQzs7SUFDdEMsc0RBQ3FDOztJQUNyQyxtREFDa0M7O0lBQ2xDLGtEQUNpQzs7SUFDakMsZ0VBQXFDOztJQUNyQyxrREFDaUM7O0lBQ2pDLG9EQUNtQzs7SUFDbkMsZ0RBQytCOzs7Ozs7OztJQVEvQix5REFHQzs7Ozs7SUF5ZUQscURBQTRDOzs7OztJQUM1QyxzREFBdUM7Ozs7O0lBdmVyQyxvREFBeUM7Ozs7O0lBQ3pDLDZDQUEyQjs7Ozs7SUFDM0IsMkNBQW9DOzs7OztJQUNwQyxvREFBeUM7Ozs7O0lBQ3pDLDRDQUE0Qjs7Ozs7SUFDNUIsNkNBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm1heC1saW5lLWxlbmd0aFxuaW1wb3J0IHsgQ29tcG9uZW50LCBDb250ZW50Q2hpbGQsIERvQ2hlY2ssIEVsZW1lbnRSZWYsIEV2ZW50RW1pdHRlciwgZm9yd2FyZFJlZiwgSG9zdEJpbmRpbmcsIElucHV0LCBJdGVyYWJsZURpZmZlciwgSXRlcmFibGVEaWZmZXJzLCBPbkluaXQsIE9wdGlvbmFsLCBPdXRwdXQsIFJlbmRlcmVyMiwgVGVtcGxhdGVSZWYsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb250cm9sVmFsdWVBY2Nlc3NvciwgTkdfVkFMVUVfQUNDRVNTT1IgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBJb25JdGVtLCBNb2RhbENvbnRyb2xsZXIsIFBsYXRmb3JtIH0gZnJvbSAnQGlvbmljL2FuZ3VsYXInO1xuaW1wb3J0IHsgQW5pbWF0aW9uQnVpbGRlciwgTW9kYWxPcHRpb25zIH0gZnJvbSAnQGlvbmljL2NvcmUnO1xuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBJb25pY1NlbGVjdGFibGVBZGRJdGVtVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtYWRkLWl0ZW0tdGVtcGxhdGUuZGlyZWN0aXZlJztcbmltcG9ydCB7IElvbmljU2VsZWN0YWJsZUNsb3NlQnV0dG9uVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtY2xvc2UtYnV0dG9uLXRlbXBsYXRlLmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBJb25pY1NlbGVjdGFibGVGb290ZXJUZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS1mb290ZXItdGVtcGxhdGUuZGlyZWN0aXZlJztcbmltcG9ydCB7IElvbmljU2VsZWN0YWJsZUdyb3VwRW5kVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtZ3JvdXAtZW5kLXRlbXBsYXRlLmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBJb25pY1NlbGVjdGFibGVHcm91cFRlbXBsYXRlRGlyZWN0aXZlIH0gZnJvbSAnLi9pb25pYy1zZWxlY3RhYmxlLWdyb3VwLXRlbXBsYXRlLmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBJb25pY1NlbGVjdGFibGVIZWFkZXJUZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS1oZWFkZXItdGVtcGxhdGUuZGlyZWN0aXZlJztcbmltcG9ydCB7IElvbmljU2VsZWN0YWJsZUl0ZW1FbmRUZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS1pdGVtLWVuZC10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgSW9uaWNTZWxlY3RhYmxlSXRlbUljb25UZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS1pdGVtLWljb24tdGVtcGxhdGUuZGlyZWN0aXZlJztcbmltcG9ydCB7IElvbmljU2VsZWN0YWJsZUl0ZW1UZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS1pdGVtLXRlbXBsYXRlLmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBJb25pY1NlbGVjdGFibGVNZXNzYWdlVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtbWVzc2FnZS10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgSW9uaWNTZWxlY3RhYmxlTW9kYWxDb21wb25lbnQgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtbW9kYWwuY29tcG9uZW50JztcbmltcG9ydCB7IElvbmljU2VsZWN0YWJsZVBsYWNlaG9sZGVyVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtcGxhY2Vob2xkZXItdGVtcGxhdGUuZGlyZWN0aXZlJztcbmltcG9ydCB7IElvbmljU2VsZWN0YWJsZVNlYXJjaEZhaWxUZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS1zZWFyY2gtZmFpbC10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgSW9uaWNTZWxlY3RhYmxlVGl0bGVUZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS10aXRsZS10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgSW9uaWNTZWxlY3RhYmxlVmFsdWVUZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS12YWx1ZS10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgSW9uaWNTZWxlY3RhYmxlSWNvblRlbXBsYXRlRGlyZWN0aXZlIH0gZnJvbSAnLi9pb25pYy1zZWxlY3RhYmxlLWljb24tdGVtcGxhdGUuZGlyZWN0aXZlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnaW9uaWMtc2VsZWN0YWJsZScsXG4gIHRlbXBsYXRlVXJsOiAnLi9pb25pYy1zZWxlY3RhYmxlLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vaW9uaWMtc2VsZWN0YWJsZS5jb21wb25lbnQuc2NzcyddLFxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxuICBwcm92aWRlcnM6IFt7XG4gICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXG4gICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50KSxcbiAgICBtdWx0aTogdHJ1ZVxuICB9XVxufSlcbmV4cG9ydCBjbGFzcyBJb25pY1NlbGVjdGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBDb250cm9sVmFsdWVBY2Nlc3NvciwgT25Jbml0LCBEb0NoZWNrIHtcbiAgQEhvc3RCaW5kaW5nKCdjbGFzcy5pb25pYy1zZWxlY3RhYmxlJylcbiAgX2Nzc0NsYXNzID0gdHJ1ZTtcbiAgQEhvc3RCaW5kaW5nKCdjbGFzcy5pb25pYy1zZWxlY3RhYmxlLWlvcycpXG4gIF9pc0lvczogYm9vbGVhbjtcbiAgQEhvc3RCaW5kaW5nKCdjbGFzcy5pb25pYy1zZWxlY3RhYmxlLW1kJylcbiAgX2lzTUQ6IGJvb2xlYW47XG4gIEBIb3N0QmluZGluZygnY2xhc3MuaW9uaWMtc2VsZWN0YWJsZS1pcy1tdWx0aXBsZScpXG4gIGdldCBfaXNNdWx0aXBsZUNzc0NsYXNzKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmlzTXVsdGlwbGU7XG4gIH1cbiAgQEhvc3RCaW5kaW5nKCdjbGFzcy5pb25pYy1zZWxlY3RhYmxlLWhhcy12YWx1ZScpXG4gIGdldCBfaGFzVmFsdWVDc3NDbGFzcygpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5oYXNWYWx1ZSgpO1xuICB9XG4gIEBIb3N0QmluZGluZygnY2xhc3MuaW9uaWMtc2VsZWN0YWJsZS1oYXMtcGxhY2Vob2xkZXInKVxuICBnZXQgX2hhc1BsYWNlaG9sZGVyQ3NzQ2xhc3MoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuX2hhc1BsYWNlaG9sZGVyO1xuICB9XG4gIEBIb3N0QmluZGluZygnY2xhc3MuaW9uaWMtc2VsZWN0YWJsZS1oYXMtbGFiZWwnKVxuICBnZXQgX2hhc0lvbkxhYmVsQ3NzQ2xhc3MoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuX2hhc0lvbkxhYmVsO1xuICB9XG4gIEBIb3N0QmluZGluZygnY2xhc3MuaW9uaWMtc2VsZWN0YWJsZS1sYWJlbC1kZWZhdWx0JylcbiAgZ2V0IF9oYXNEZWZhdWx0SW9uTGFiZWxDc3NDbGFzcygpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5faW9uTGFiZWxQb3NpdGlvbiA9PT0gJ2RlZmF1bHQnO1xuICB9XG4gIEBIb3N0QmluZGluZygnY2xhc3MuaW9uaWMtc2VsZWN0YWJsZS1sYWJlbC1maXhlZCcpXG4gIGdldCBfaGFzRml4ZWRJb25MYWJlbENzc0NsYXNzKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLl9pb25MYWJlbFBvc2l0aW9uID09PSAnZml4ZWQnO1xuICB9XG4gIEBIb3N0QmluZGluZygnY2xhc3MuaW9uaWMtc2VsZWN0YWJsZS1sYWJlbC1zdGFja2VkJylcbiAgZ2V0IF9oYXNTdGFja2VkSW9uTGFiZWxDc3NDbGFzcygpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5faW9uTGFiZWxQb3NpdGlvbiA9PT0gJ3N0YWNrZWQnO1xuICB9XG4gIEBIb3N0QmluZGluZygnY2xhc3MuaW9uaWMtc2VsZWN0YWJsZS1sYWJlbC1mbG9hdGluZycpXG4gIGdldCBfaGFzRmxvYXRpbmdJb25MYWJlbENzc0NsYXNzKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLl9pb25MYWJlbFBvc2l0aW9uID09PSAnZmxvYXRpbmcnO1xuICB9XG4gIHByaXZhdGUgX2lzT25TZWFyY2hFbmFibGVkID0gdHJ1ZTtcbiAgcHJpdmF0ZSBfaXNFbmFibGVkID0gdHJ1ZTtcbiAgcHJpdmF0ZSBfc2hvdWxkQmFja2Ryb3BDbG9zZSA9IHRydWU7XG4gIHByaXZhdGUgX2lzT3BlbmVkID0gZmFsc2U7XG4gIHByaXZhdGUgX3ZhbHVlOiBhbnkgPSBudWxsO1xuICBwcml2YXRlIF9tb2RhbDogSFRNTElvbk1vZGFsRWxlbWVudDtcbiAgcHJpdmF0ZSBfaXRlbXNEaWZmZXI6IEl0ZXJhYmxlRGlmZmVyPGFueT47XG4gIHByaXZhdGUgX2hhc09iamVjdHM6IGJvb2xlYW47XG4gIHByaXZhdGUgX2NhbkNsZWFyID0gZmFsc2U7XG4gIHByaXZhdGUgX2hhc0NvbmZpcm1CdXR0b24gPSBmYWxzZTtcbiAgcHJpdmF0ZSBfaXNNdWx0aXBsZSA9IGZhbHNlO1xuICBwcml2YXRlIF9jYW5BZGRJdGVtID0gZmFsc2U7XG4gIHByaXZhdGUgX2FkZEl0ZW1PYnNlcnZhYmxlOiBTdWJzY3JpcHRpb247XG4gIHByaXZhdGUgX2RlbGV0ZUl0ZW1PYnNlcnZhYmxlOiBTdWJzY3JpcHRpb247XG4gIHByaXZhdGUgb25JdGVtc0NoYW5nZTogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIHByaXZhdGUgX2lvbkl0ZW1FbGVtZW50OiBhbnk7XG4gIHByaXZhdGUgX2lvbkxhYmVsRWxlbWVudDogYW55O1xuICBwcml2YXRlIF9oYXNJb25MYWJlbCA9IGZhbHNlO1xuICBwcml2YXRlIF9pb25MYWJlbFBvc2l0aW9uOiAnZml4ZWQnIHwgJ3N0YWNrZWQnIHwgJ2Zsb2F0aW5nJyB8ICdkZWZhdWx0JyB8IG51bGwgPSBudWxsO1xuICBwcml2YXRlIF9sYWJlbDogc3RyaW5nID0gbnVsbDtcbiAgcHJpdmF0ZSBnZXQgX2hhc0luZmluaXRlU2Nyb2xsKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmlzRW5hYmxlZCAmJiB0aGlzLl9tb2RhbENvbXBvbmVudCAmJlxuICAgICAgdGhpcy5fbW9kYWxDb21wb25lbnQuX2luZmluaXRlU2Nyb2xsID8gdHJ1ZSA6IGZhbHNlO1xuICB9XG4gIGdldCBfc2hvdWxkU3RvcmVJdGVtVmFsdWUoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuc2hvdWxkU3RvcmVJdGVtVmFsdWUgJiYgdGhpcy5faGFzT2JqZWN0cztcbiAgfVxuICBfdmFsdWVJdGVtczogYW55W10gPSBbXTtcbiAgX3NlYXJjaFRleHQgPSAnJztcbiAgX2hhc1NlYXJjaFRleHQgPSBmYWxzZTtcbiAgX2dyb3VwczogYW55W10gPSBbXTtcbiAgX2l0ZW1zVG9Db25maXJtOiBhbnlbXSA9IFtdO1xuICBfc2VsZWN0ZWRJdGVtczogYW55W10gPSBbXTtcbiAgX21vZGFsQ29tcG9uZW50OiBJb25pY1NlbGVjdGFibGVNb2RhbENvbXBvbmVudDtcbiAgX2ZpbHRlcmVkR3JvdXBzOiBhbnlbXSA9IFtdO1xuICBfaGFzR3JvdXBzOiBib29sZWFuO1xuICBfaXNTZWFyY2hpbmc6IGJvb2xlYW47XG4gIF9oYXNQbGFjZWhvbGRlcjogYm9vbGVhbjtcbiAgX2lzQWRkSXRlbVRlbXBsYXRlVmlzaWJsZSA9IGZhbHNlO1xuICBfaXNGb290ZXJWaXNpYmxlID0gdHJ1ZTtcbiAgX2l0ZW1Ub0FkZDogYW55ID0gbnVsbDtcbiAgX2Zvb3RlckJ1dHRvbnNDb3VudCA9IDA7XG4gIF9oYXNGaWx0ZXJlZEl0ZW1zID0gZmFsc2U7XG5cbiAgLyoqXG4gICAqIFRleHQgb2YgW0lvbmljIExhYmVsXShodHRwczovL2lvbmljZnJhbWV3b3JrLmNvbS9kb2NzL2FwaS9sYWJlbCkuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI2xhYmVsKS5cbiAgICpcbiAgICogQHJlYWRvbmx5XG4gICAqIEBkZWZhdWx0IG51bGxcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgZ2V0IGxhYmVsKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuX2xhYmVsO1xuICB9XG5cbiAgLyoqXG4gICAqIFRleHQgdGhhdCB0aGUgdXNlciBoYXMgdHlwZWQgaW4gU2VhcmNoYmFyLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNzZWFyY2h0ZXh0KS5cbiAgICpcbiAgICogQHJlYWRvbmx5XG4gICAqIEBkZWZhdWx0ICcnXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIGdldCBzZWFyY2hUZXh0KCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuX3NlYXJjaFRleHQ7XG4gIH1cbiAgc2V0IHNlYXJjaFRleHQoc2VhcmNoVGV4dDogc3RyaW5nKSB7XG4gICAgdGhpcy5fc2VhcmNoVGV4dCA9IHNlYXJjaFRleHQ7XG4gICAgdGhpcy5fc2V0SGFzU2VhcmNoVGV4dCgpO1xuICB9XG5cbiAgLyoqXG4gICAqIERldGVybWluZXMgd2hldGhlciBzZWFyY2ggaXMgcnVubmluZy5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jaXNzZWFyY2hpbmcpLlxuICAgKlxuICAgKiBAZGVmYXVsdCBmYWxzZVxuICAgKiBAcmVhZG9ubHlcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgZ2V0IGlzU2VhcmNoaW5nKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLl9pc1NlYXJjaGluZztcbiAgfVxuXG4gIC8qKlxuICAgKiBEZXRlcm1pbmVzIHdoZXRoZXIgdXNlciBoYXMgdHlwZWQgYW55dGhpbmcgaW4gU2VhcmNoYmFyLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNoYXNzZWFyY2h0ZXh0KS5cbiAgICpcbiAgICogQGRlZmF1bHQgZmFsc2VcbiAgICogQHJlYWRvbmx5XG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIGdldCBoYXNTZWFyY2hUZXh0KCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLl9oYXNTZWFyY2hUZXh0O1xuICB9XG5cbiAgZ2V0IHZhbHVlKCk6IGFueSB7XG4gICAgcmV0dXJuIHRoaXMuX3ZhbHVlO1xuICB9XG4gIHNldCB2YWx1ZSh2YWx1ZTogYW55KSB7XG4gICAgdGhpcy5fdmFsdWUgPSB2YWx1ZTtcblxuICAgIC8vIFNldCB2YWx1ZSBpdGVtcy5cbiAgICB0aGlzLl92YWx1ZUl0ZW1zLnNwbGljZSgwLCB0aGlzLl92YWx1ZUl0ZW1zLmxlbmd0aCk7XG5cbiAgICBpZiAodGhpcy5pc011bHRpcGxlKSB7XG4gICAgICBpZiAodmFsdWUgJiYgdmFsdWUubGVuZ3RoKSB7XG4gICAgICAgIEFycmF5LnByb3RvdHlwZS5wdXNoLmFwcGx5KHRoaXMuX3ZhbHVlSXRlbXMsIHZhbHVlKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKCF0aGlzLl9pc051bGxPcldoaXRlU3BhY2UodmFsdWUpKSB7XG4gICAgICAgIHRoaXMuX3ZhbHVlSXRlbXMucHVzaCh2YWx1ZSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdGhpcy5fc2V0SW9uSXRlbUhhc1ZhbHVlKCk7XG4gICAgdGhpcy5fc2V0SGFzUGxhY2Vob2xkZXIoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBBIGxpc3Qgb2YgaXRlbXMuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI2l0ZW1zKS5cbiAgICpcbiAgICogQGRlZmF1bHQgW11cbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgQElucHV0KClcbiAgaXRlbXM6IGFueVtdID0gW107XG4gIEBPdXRwdXQoKVxuICBpdGVtc0NoYW5nZTogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgLyoqXG4gICAqIERldGVybWluZXMgd2hldGhlciB0aGUgY29tcG9uZW50IGlzIGVuYWJsZWQuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI2lzZW5hYmxlZCkuXG4gICAqXG4gICAqIEBkZWZhdWx0IHRydWVcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgQEhvc3RCaW5kaW5nKCdjbGFzcy5pb25pYy1zZWxlY3RhYmxlLWlzLWVuYWJsZWQnKVxuICBASW5wdXQoJ2lzRW5hYmxlZCcpXG4gIGdldCBpc0VuYWJsZWQoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuX2lzRW5hYmxlZDtcbiAgfVxuICBzZXQgaXNFbmFibGVkKGlzRW5hYmxlZDogYm9vbGVhbikge1xuICAgIHRoaXMuX2lzRW5hYmxlZCA9ICEhaXNFbmFibGVkO1xuICAgIHRoaXMuZW5hYmxlSW9uSXRlbSh0aGlzLl9pc0VuYWJsZWQpO1xuICB9XG5cbiAgLyoqXG4gICAqIERldGVybWluZXMgd2hldGhlciBNb2RhbCBzaG91bGQgYmUgY2xvc2VkIHdoZW4gYmFja2Ryb3AgaXMgY2xpY2tlZC5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jc2hvdWxkYmFja2Ryb3BjbG9zZSkuXG4gICAqXG4gICAqIEBkZWZhdWx0IHRydWVcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgQElucHV0KCdzaG91bGRCYWNrZHJvcENsb3NlJylcbiAgZ2V0IHNob3VsZEJhY2tkcm9wQ2xvc2UoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuX3Nob3VsZEJhY2tkcm9wQ2xvc2U7XG4gIH1cbiAgc2V0IHNob3VsZEJhY2tkcm9wQ2xvc2Uoc2hvdWxkQmFja2Ryb3BDbG9zZTogYm9vbGVhbikge1xuICAgIHRoaXMuX3Nob3VsZEJhY2tkcm9wQ2xvc2UgPSAhIXNob3VsZEJhY2tkcm9wQ2xvc2U7XG4gIH1cblxuICAvKipcbiAgICogTW9kYWwgQ1NTIGNsYXNzLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNtb2RhbGNzc2NsYXNzKS5cbiAgICpcbiAgICogQGRlZmF1bHQgbnVsbFxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBASW5wdXQoKVxuICBtb2RhbENzc0NsYXNzOiBzdHJpbmcgPSBudWxsO1xuXG4gIC8qKlxuICAgKiBNb2RhbCBlbnRlciBhbmltYXRpb24uXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI21vZGFsZW50ZXJhbmltYXRpb24pLlxuICAgKlxuICAgKiBAZGVmYXVsdCBudWxsXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBJbnB1dCgpXG4gIG1vZGFsRW50ZXJBbmltYXRpb246IEFuaW1hdGlvbkJ1aWxkZXIgPSBudWxsO1xuXG4gIC8qKlxuICAgKiBNb2RhbCBsZWF2ZSBhbmltYXRpb24uXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI21vZGFsbGVhdmVhbmltYXRpb24pLlxuICAgKlxuICAgKiBAZGVmYXVsdCBudWxsXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBJbnB1dCgpXG4gIG1vZGFsTGVhdmVBbmltYXRpb246IEFuaW1hdGlvbkJ1aWxkZXIgPSBudWxsO1xuXG4gIC8qKlxuICAgKiBEZXRlcm1pbmVzIHdoZXRoZXIgTW9kYWwgaXMgb3BlbmVkLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNpc29wZW5lZCkuXG4gICAqXG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqIEByZWFkb25seVxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBnZXQgaXNPcGVuZWQoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuX2lzT3BlbmVkO1xuICB9XG5cbiAgLyoqXG4gICAqIERldGVybWluZXMgd2hldGhlciBDb25maXJtIGJ1dHRvbiBpcyBlbmFibGVkLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNpc2NvbmZpcm1idXR0b25lbmFibGVkKS5cbiAgICpcbiAgICogQGRlZmF1bHQgdHJ1ZVxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBASW5wdXQoKVxuICBpc0NvbmZpcm1CdXR0b25FbmFibGVkID0gdHJ1ZTtcblxuICAvKipcbiAqIERldGVybWluZXMgd2hldGhlciBDb25maXJtIGJ1dHRvbiBpcyB2aXNpYmxlIGZvciBzaW5nbGUgc2VsZWN0aW9uLlxuICogQnkgZGVmYXVsdCBDb25maXJtIGJ1dHRvbiBpcyB2aXNpYmxlIG9ubHkgZm9yIG11bHRpcGxlIHNlbGVjdGlvbi5cbiAqICoqTm90ZSoqOiBJdCBpcyBhbHdheXMgdHJ1ZSBmb3IgbXVsdGlwbGUgc2VsZWN0aW9uIGFuZCBjYW5ub3QgYmUgY2hhbmdlZC5cbiAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI2hhc2NvbmZpcm1idXR0b24pLlxuICpcbiAqIEBkZWZhdWx0IHRydWVcbiAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAqL1xuICBASW5wdXQoJ2hhc0NvbmZpcm1CdXR0b24nKVxuICBnZXQgaGFzQ29uZmlybUJ1dHRvbigpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5faGFzQ29uZmlybUJ1dHRvbjtcbiAgfVxuICBzZXQgaGFzQ29uZmlybUJ1dHRvbihoYXNDb25maXJtQnV0dG9uOiBib29sZWFuKSB7XG4gICAgdGhpcy5faGFzQ29uZmlybUJ1dHRvbiA9ICEhaGFzQ29uZmlybUJ1dHRvbjtcbiAgICB0aGlzLl9jb3VudEZvb3RlckJ1dHRvbnMoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBJdGVtIHByb3BlcnR5IHRvIHVzZSBhcyBhIHVuaXF1ZSBpZGVudGlmaWVyLCBlLmcsIGAnaWQnYC5cbiAgICogKipOb3RlKio6IGBpdGVtc2Agc2hvdWxkIGJlIGFuIG9iamVjdCBhcnJheS5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jaXRlbXZhbHVlZmllbGQpLlxuICAgKlxuICAgKiBAZGVmYXVsdCBudWxsXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBJbnB1dCgpXG4gIGl0ZW1WYWx1ZUZpZWxkOiBzdHJpbmcgPSBudWxsO1xuXG4gIC8qKlxuICAgKiBJdGVtIHByb3BlcnR5IHRvIGRpc3BsYXksIGUuZywgYCduYW1lJ2AuXG4gICAqICoqTm90ZSoqOiBgaXRlbXNgIHNob3VsZCBiZSBhbiBvYmplY3QgYXJyYXkuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI2l0ZW10ZXh0ZmllbGQpLlxuICAgKlxuICAgKiBAZGVmYXVsdCBmYWxzZVxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBASW5wdXQoKVxuICBpdGVtVGV4dEZpZWxkOiBzdHJpbmcgPSBudWxsO1xuXG4gIC8qKlxuICAgKlxuICAgKiBHcm91cCBwcm9wZXJ0eSB0byB1c2UgYXMgYSB1bmlxdWUgaWRlbnRpZmllciB0byBncm91cCBpdGVtcywgZS5nLiBgJ2NvdW50cnkuaWQnYC5cbiAgICogKipOb3RlKio6IGBpdGVtc2Agc2hvdWxkIGJlIGFuIG9iamVjdCBhcnJheS5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jZ3JvdXB2YWx1ZWZpZWxkKS5cbiAgICpcbiAgICogQGRlZmF1bHQgbnVsbFxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBASW5wdXQoKVxuICBncm91cFZhbHVlRmllbGQ6IHN0cmluZyA9IG51bGw7XG5cbiAgLyoqXG4qIEdyb3VwIHByb3BlcnR5IHRvIGRpc3BsYXksIGUuZy4gYCdjb3VudHJ5Lm5hbWUnYC5cbiogKipOb3RlKio6IGBpdGVtc2Agc2hvdWxkIGJlIGFuIG9iamVjdCBhcnJheS5cbiogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jZ3JvdXB0ZXh0ZmllbGQpLlxuKlxuKiBAZGVmYXVsdCBudWxsXG4qIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiovXG4gIEBJbnB1dCgpXG4gIGdyb3VwVGV4dEZpZWxkOiBzdHJpbmcgPSBudWxsO1xuXG4gIC8qKlxuICAgKiBEZXRlcm1pbmVzIHdoZXRoZXIgdG8gc2hvdyBTZWFyY2hiYXIuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI2NhbnNlYXJjaCkuXG4gICAqXG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBJbnB1dCgpXG4gIGNhblNlYXJjaCA9IGZhbHNlO1xuXG4gIC8qKlxuICAgKiBEZXRlcm1pbmVzIHdoZXRoZXIgYG9uU2VhcmNoYCBldmVudCBpcyBlbmFibGVkLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNpc29uc2VhcmNoZW5hYmxlZCkuXG4gICAqXG4gICAqIEBkZWZhdWx0IHRydWVcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgQElucHV0KCdpc09uU2VhcmNoRW5hYmxlZCcpXG4gIGdldCBpc09uU2VhcmNoRW5hYmxlZCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5faXNPblNlYXJjaEVuYWJsZWQ7XG4gIH1cbiAgc2V0IGlzT25TZWFyY2hFbmFibGVkKGlzT25TZWFyY2hFbmFibGVkOiBib29sZWFuKSB7XG4gICAgdGhpcy5faXNPblNlYXJjaEVuYWJsZWQgPSAhIWlzT25TZWFyY2hFbmFibGVkO1xuICB9XG5cbiAgLyoqXG4gICAqIERldGVybWluZXMgd2hldGhlciB0byBzaG93IENsZWFyIGJ1dHRvbi5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jY2FuY2xlYXIpLlxuICAgKlxuICAgKiBAZGVmYXVsdCBmYWxzZVxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBASG9zdEJpbmRpbmcoJ2NsYXNzLmlvbmljLXNlbGVjdGFibGUtY2FuLWNsZWFyJylcbiAgQElucHV0KCdjYW5DbGVhcicpXG4gIGdldCBjYW5DbGVhcigpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5fY2FuQ2xlYXI7XG4gIH1cbiAgc2V0IGNhbkNsZWFyKGNhbkNsZWFyOiBib29sZWFuKSB7XG4gICAgdGhpcy5fY2FuQ2xlYXIgPSAhIWNhbkNsZWFyO1xuICAgIHRoaXMuX2NvdW50Rm9vdGVyQnV0dG9ucygpO1xuICB9XG5cbiAgLyoqXG4gICAqIERldGVybWluZXMgd2hldGhlciBJb25pYyBbSW5maW5pdGVTY3JvbGxdKGh0dHBzOi8vaW9uaWNmcmFtZXdvcmsuY29tL2RvY3MvYXBpL2NvbXBvbmVudHMvaW5maW5pdGUtc2Nyb2xsL0luZmluaXRlU2Nyb2xsLykgaXMgZW5hYmxlZC5cbiAgICogKipOb3RlKio6IEluZmluaXRlIHNjcm9sbCBjYW5ub3QgYmUgdXNlZCB0b2dldGhlciB3aXRoIHZpcnR1YWwgc2Nyb2xsLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNoYXNpbmZpbml0ZXNjcm9sbCkuXG4gICAqXG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBJbnB1dCgpXG4gIGhhc0luZmluaXRlU2Nyb2xsID0gZmFsc2U7XG5cbiAgLyoqXG4gICAqIERldGVybWluZXMgd2hldGhlciBJb25pYyBbVmlydHVhbFNjcm9sbF0oaHR0cHM6Ly9pb25pY2ZyYW1ld29yay5jb20vZG9jcy9hcGkvY29tcG9uZW50cy92aXJ0dWFsLXNjcm9sbC9WaXJ0dWFsU2Nyb2xsLykgaXMgZW5hYmxlZC5cbiAgICogKipOb3RlKio6IFZpcnR1YWwgc2Nyb2xsIGNhbm5vdCBiZSB1c2VkIHRvZ2V0aGVyIHdpdGggaW5maW5pdGUgc2Nyb2xsLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNoYXN2aXJ0dWFsc2Nyb2xsKS5cbiAgICpcbiAgICogQGRlZmF1bHQgZmFsc2VcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgQElucHV0KClcbiAgaGFzVmlydHVhbFNjcm9sbCA9IGZhbHNlO1xuXG4gIC8qKlxuICAgKiBTZWUgSW9uaWMgVmlydHVhbFNjcm9sbCBbYXBwcm94SXRlbUhlaWdodF0oaHR0cHM6Ly9pb25pY2ZyYW1ld29yay5jb20vZG9jcy9hcGkvY29tcG9uZW50cy92aXJ0dWFsLXNjcm9sbC9WaXJ0dWFsU2Nyb2xsLykuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI3ZpcnR1YWxzY3JvbGxhcHByb3hpdGVtaGVpZ2h0KS5cbiAgICpcbiAgICogQGRlZmF1bHQgJzQwcHgnXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBJbnB1dCgpXG4gIHZpcnR1YWxTY3JvbGxBcHByb3hJdGVtSGVpZ2h0ID0gJzQwcHgnO1xuXG4gIC8qKlxuICAgKiBBIHBsYWNlaG9sZGVyIGZvciBTZWFyY2hiYXIuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI3NlYXJjaHBsYWNlaG9sZGVyKS5cbiAgICpcbiAgICogQGRlZmF1bHQgJ1NlYXJjaCdcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgQElucHV0KClcbiAgc2VhcmNoUGxhY2Vob2xkZXIgPSAnU2VhcmNoJztcblxuICAvKipcbiAgICogQSBwbGFjZWhvbGRlci5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jcGxhY2Vob2xkZXIpLlxuICAgKlxuICAgKiBAZGVmYXVsdCBudWxsXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBJbnB1dCgpXG4gIHBsYWNlaG9sZGVyOiBzdHJpbmcgPSBudWxsO1xuXG4gIC8qKlxuICAgKiBEZXRlcm1pbmVzIHdoZXRoZXIgbXVsdGlwbGUgaXRlbXMgY2FuIGJlIHNlbGVjdGVkLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNpc211bHRpcGxlKS5cbiAgICpcbiAgICogQGRlZmF1bHQgZmFsc2VcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgQElucHV0KCdpc011bHRpcGxlJylcbiAgZ2V0IGlzTXVsdGlwbGUoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuX2lzTXVsdGlwbGU7XG4gIH1cbiAgc2V0IGlzTXVsdGlwbGUoaXNNdWx0aXBsZTogYm9vbGVhbikge1xuICAgIHRoaXMuX2lzTXVsdGlwbGUgPSAhIWlzTXVsdGlwbGU7XG4gICAgdGhpcy5fY291bnRGb290ZXJCdXR0b25zKCk7XG4gIH1cblxuICAvKipcbiAgICogVGV4dCB0byBkaXNwbGF5IHdoZW4gbm8gaXRlbXMgaGF2ZSBiZWVuIGZvdW5kIGR1cmluZyBzZWFyY2guXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI3NlYXJjaGZhaWx0ZXh0KS5cbiAgICpcbiAgICogQGRlZmF1bHQgJ05vIGl0ZW1zIGZvdW5kLidcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgQElucHV0KClcbiAgc2VhcmNoRmFpbFRleHQgPSAnTm8gaXRlbXMgZm91bmQuJztcblxuICAvKipcbiAgICogQ2xlYXIgYnV0dG9uIHRleHQuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI2NsZWFyYnV0dG9udGV4dCkuXG4gICAqXG4gICAqIEBkZWZhdWx0ICdDbGVhcidcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgQElucHV0KClcbiAgY2xlYXJCdXR0b25UZXh0ID0gJ0NsZWFyJztcblxuICAvKipcbiAgICogQWRkIGJ1dHRvbiB0ZXh0LlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNhZGRidXR0b250ZXh0KS5cbiAgICpcbiAgICogQGRlZmF1bHQgJ0FkZCdcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgQElucHV0KClcbiAgYWRkQnV0dG9uVGV4dCA9ICdBZGQnO1xuXG4gIC8qKlxuICAgKiBDb25maXJtIGJ1dHRvbiB0ZXh0LlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNjb25maXJtYnV0dG9udGV4dCkuXG4gICAqXG4gICAqIEBkZWZhdWx0ICdPSydcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgQElucHV0KClcbiAgY29uZmlybUJ1dHRvblRleHQgPSAnT0snO1xuXG4gIC8qKlxuICAgKiBDbG9zZSBidXR0b24gdGV4dC5cbiAgICogVGhlIGZpZWxkIGlzIG9ubHkgYXBwbGljYWJsZSB0byAqKmlPUyoqIHBsYXRmb3JtLCBvbiAqKkFuZHJvaWQqKiBvbmx5IENyb3NzIGljb24gaXMgZGlzcGxheWVkLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNjbG9zZWJ1dHRvbnRleHQpLlxuICAgKlxuICAgKiBAZGVmYXVsdCAnQ2FuY2VsJ1xuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBASW5wdXQoKVxuICBjbG9zZUJ1dHRvblRleHQgPSAnQ2FuY2VsJztcblxuICAvKipcbiAgICogRGV0ZXJtaW5lcyB3aGV0aGVyIFNlYXJjaGJhciBzaG91bGQgcmVjZWl2ZSBmb2N1cyB3aGVuIE1vZGFsIGlzIG9wZW5lZC5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jc2hvdWxkZm9jdXNzZWFyY2hiYXIpLlxuICAgKlxuICAgKiBAZGVmYXVsdCBmYWxzZVxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBASW5wdXQoKVxuICBzaG91bGRGb2N1c1NlYXJjaGJhciA9IGZhbHNlO1xuXG4gIC8qKlxuICAgKiBIZWFkZXIgY29sb3IuIFtJb25pYyBjb2xvcnNdKGh0dHBzOi8vaW9uaWNmcmFtZXdvcmsuY29tL2RvY3MvdGhlbWluZy9hZHZhbmNlZCNjb2xvcnMpIGFyZSBzdXBwb3J0ZWQuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI2hlYWRlcmNvbG9yKS5cbiAgICpcbiAgICogQGRlZmF1bHQgbnVsbFxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBASW5wdXQoKVxuICBoZWFkZXJDb2xvcjogc3RyaW5nID0gbnVsbDtcblxuICAvKipcbiAgICogR3JvdXAgY29sb3IuIFtJb25pYyBjb2xvcnNdKGh0dHBzOi8vaW9uaWNmcmFtZXdvcmsuY29tL2RvY3MvdGhlbWluZy9hZHZhbmNlZCNjb2xvcnMpIGFyZSBzdXBwb3J0ZWQuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI2dyb3VwY29sb3IpLlxuICAgKlxuICAgKiBAZGVmYXVsdCBudWxsXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBJbnB1dCgpXG4gIGdyb3VwQ29sb3I6IHN0cmluZyA9IG51bGw7XG5cbiAgLyoqXG4gICAqIENsb3NlIGJ1dHRvbiBzbG90LiBbSW9uaWMgc2xvdHNdKGh0dHBzOi8vaW9uaWNmcmFtZXdvcmsuY29tL2RvY3MvYXBpL2J1dHRvbnMpIGFyZSBzdXBwb3J0ZWQuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI2Nsb3NlYnV0dG9uc2xvdCkuXG4gICAqXG4gICAqIEBkZWZhdWx0ICdzdGFydCdcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgQElucHV0KClcbiAgY2xvc2VCdXR0b25TbG90ID0gJ3N0YXJ0JztcblxuICAvKipcbiAgICogSXRlbSBpY29uIHNsb3QuIFtJb25pYyBzbG90c10oaHR0cHM6Ly9pb25pY2ZyYW1ld29yay5jb20vZG9jcy9hcGkvaXRlbSkgYXJlIHN1cHBvcnRlZC5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jaXRlbWljb25zbG90KS5cbiAgICpcbiAgICogQGRlZmF1bHQgJ3N0YXJ0J1xuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBASW5wdXQoKVxuICBpdGVtSWNvblNsb3QgPSAnc3RhcnQnO1xuXG4gIC8qKlxuICAgKiBGaXJlcyB3aGVuIGl0ZW0vcyBoYXMgYmVlbiBzZWxlY3RlZCBhbmQgTW9kYWwgY2xvc2VkLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNvbmNoYW5nZSkuXG4gICAqXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBPdXRwdXQoKVxuICBvbkNoYW5nZTogRXZlbnRFbWl0dGVyPHsgY29tcG9uZW50OiBJb25pY1NlbGVjdGFibGVDb21wb25lbnQsIHZhbHVlOiBhbnkgfT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgLyoqXG4gICAqIEZpcmVzIHdoZW4gdGhlIHVzZXIgaXMgdHlwaW5nIGluIFNlYXJjaGJhci5cbiAgICogKipOb3RlKio6IGBjYW5TZWFyY2hgIGFuZCBgaXNPblNlYXJjaEVuYWJsZWRgIGhhcyB0byBiZSBlbmFibGVkLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNvbnNlYXJjaCkuXG4gICAqXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBPdXRwdXQoKVxuICBvblNlYXJjaDogRXZlbnRFbWl0dGVyPHsgY29tcG9uZW50OiBJb25pY1NlbGVjdGFibGVDb21wb25lbnQsIHRleHQ6IHN0cmluZyB9PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAvKipcbiAgICogRmlyZXMgd2hlbiBubyBpdGVtcyBoYXZlIGJlZW4gZm91bmQuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI29uc2VhcmNoZmFpbCkuXG4gICAqXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBPdXRwdXQoKVxuICBvblNlYXJjaEZhaWw6IEV2ZW50RW1pdHRlcjx7IGNvbXBvbmVudDogSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50LCB0ZXh0OiBzdHJpbmcgfT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgLyoqXG4gICAqIEZpcmVzIHdoZW4gc29tZSBpdGVtcyBoYXZlIGJlZW4gZm91bmQuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI29uc2VhcmNoc3VjY2VzcykuXG4gICAqXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBPdXRwdXQoKVxuICBvblNlYXJjaFN1Y2Nlc3M6IEV2ZW50RW1pdHRlcjx7IGNvbXBvbmVudDogSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50LCB0ZXh0OiBzdHJpbmcgfT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgLyoqXG4gICAqIEZpcmVzIHdoZW4gdGhlIHVzZXIgaGFzIHNjcm9sbGVkIHRvIHRoZSBlbmQgb2YgdGhlIGxpc3QuXG4gICAqICoqTm90ZSoqOiBgaGFzSW5maW5pdGVTY3JvbGxgIGhhcyB0byBiZSBlbmFibGVkLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNvbmluZmluaXRlc2Nyb2xsKS5cbiAgICpcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgQE91dHB1dCgpXG4gIG9uSW5maW5pdGVTY3JvbGw6IEV2ZW50RW1pdHRlcjx7IGNvbXBvbmVudDogSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50LCB0ZXh0OiBzdHJpbmcgfT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgLyoqXG4gICAqIEZpcmVzIHdoZW4gTW9kYWwgaGFzIGJlZW4gb3BlbmVkLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNvbm9wZW4pLlxuICAgKlxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBAT3V0cHV0KClcbiAgb25PcGVuOiBFdmVudEVtaXR0ZXI8eyBjb21wb25lbnQ6IElvbmljU2VsZWN0YWJsZUNvbXBvbmVudCB9PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAvKipcbiAgICogRmlyZXMgd2hlbiBNb2RhbCBoYXMgYmVlbiBjbG9zZWQuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI29uY2xvc2UpLlxuICAgKlxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBAT3V0cHV0KClcbiAgb25DbG9zZTogRXZlbnRFbWl0dGVyPHsgY29tcG9uZW50OiBJb25pY1NlbGVjdGFibGVDb21wb25lbnQgfT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgLyoqXG4gICAqIEZpcmVzIHdoZW4gYW4gaXRlbSBoYXMgYmVlbiBzZWxlY3RlZCBvciB1bnNlbGVjdGVkLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNvbnNlbGVjdCkuXG4gICAqXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBPdXRwdXQoKVxuICBvblNlbGVjdDogRXZlbnRFbWl0dGVyPHsgY29tcG9uZW50OiBJb25pY1NlbGVjdGFibGVDb21wb25lbnQsIGl0ZW06IGFueSwgaXNTZWxlY3RlZDogYm9vbGVhbiB9PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAvKipcbiAgICogRmlyZXMgd2hlbiBDbGVhciBidXR0b24gaGFzIGJlZW4gY2xpY2tlZC5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jb25jbGVhcikuXG4gICAqXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBPdXRwdXQoKVxuICBvbkNsZWFyOiBFdmVudEVtaXR0ZXI8eyBjb21wb25lbnQ6IElvbmljU2VsZWN0YWJsZUNvbXBvbmVudCwgaXRlbXM6IGFueVtdIH0+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIC8qKlxuICAgKiBBIGxpc3Qgb2YgaXRlbXMgdGhhdCBhcmUgc2VsZWN0ZWQgYW5kIGF3YWl0aW5nIGNvbmZpcm1hdGlvbiBieSB1c2VyLCB3aGVuIGhlIGhhcyBjbGlja2VkIENvbmZpcm0gYnV0dG9uLlxuICAgKiBBZnRlciB0aGUgdXNlciBoYXMgY2xpY2tlZCBDb25maXJtIGJ1dHRvbiBpdGVtcyB0byBjb25maXJtIGFyZSBjbGVhcmVkLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNpdGVtc3RvY29uZmlybSkuXG4gICAqXG4gICAqIEBkZWZhdWx0IFtdXG4gICAqIEByZWFkb25seVxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBnZXQgaXRlbXNUb0NvbmZpcm0oKTogYW55W10ge1xuICAgIHJldHVybiB0aGlzLl9pdGVtc1RvQ29uZmlybTtcbiAgfVxuXG4gIC8qKlxuICAgKiBIb3cgbG9uZywgaW4gbWlsbGlzZWNvbmRzLCB0byB3YWl0IHRvIGZpbHRlciBpdGVtcyBvciB0byB0cmlnZ2VyIGBvblNlYXJjaGAgZXZlbnQgYWZ0ZXIgZWFjaCBrZXlzdHJva2UuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI3NlYXJjaGRlYm91bmNlKS5cbiAgICpcbiAgICogQGRlZmF1bHQgMjUwXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBJbnB1dCgpXG4gIHNlYXJjaERlYm91bmNlOiBOdW1iZXIgPSAyNTA7XG5cbiAgLyoqXG4gICAqIEEgbGlzdCBvZiBpdGVtcyB0byBkaXNhYmxlLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNkaXNhYmxlZGl0ZW1zKS5cbiAgICpcbiAgICogQGRlZmF1bHQgW11cbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgQElucHV0KClcbiAgZGlzYWJsZWRJdGVtczogYW55W10gPSBbXTtcblxuICAvKipcbiAgICogRGV0ZXJtaW5lcyB3aGV0aGVyIGl0ZW0gdmFsdWUgb25seSBzaG91bGQgYmUgc3RvcmVkIGluIGBuZ01vZGVsYCwgbm90IHRoZSBlbnRpcmUgaXRlbS5cbiAgICogKipOb3RlKio6IEl0ZW0gdmFsdWUgaXMgZGVmaW5lZCBieSBgaXRlbVZhbHVlRmllbGRgLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNzaG91bGRzdG9yZWl0ZW12YWx1ZSkuXG4gICAqXG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBJbnB1dCgpXG4gIHNob3VsZFN0b3JlSXRlbVZhbHVlID0gZmFsc2U7XG5cbiAgLyoqXG4gICAqIERldGVybWluZXMgd2hldGhlciB0byBhbGxvdyBlZGl0aW5nIGl0ZW1zLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNjYW5zYXZlaXRlbSkuXG4gICAqXG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBJbnB1dCgpXG4gIGNhblNhdmVJdGVtID0gZmFsc2U7XG5cbiAgLyoqXG4gICAqIERldGVybWluZXMgd2hldGhlciB0byBhbGxvdyBkZWxldGluZyBpdGVtcy5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jY2FuZGVsZXRlaXRlbSkuXG4gICAqXG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBJbnB1dCgpXG4gIGNhbkRlbGV0ZUl0ZW0gPSBmYWxzZTtcblxuICAvKipcbiAgICogRGV0ZXJtaW5lcyB3aGV0aGVyIHRvIGFsbG93IGFkZGluZyBpdGVtcy5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jY2FuYWRkaXRlbSkuXG4gICAqXG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBJbnB1dCgnY2FuQWRkSXRlbScpXG4gIGdldCBjYW5BZGRJdGVtKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLl9jYW5BZGRJdGVtO1xuICB9XG4gIHNldCBjYW5BZGRJdGVtKGNhbkFkZEl0ZW06IGJvb2xlYW4pIHtcbiAgICB0aGlzLl9jYW5BZGRJdGVtID0gISFjYW5BZGRJdGVtO1xuICAgIHRoaXMuX2NvdW50Rm9vdGVyQnV0dG9ucygpO1xuICB9XG5cbiAgLyoqXG4gICAqIEZpcmVzIHdoZW4gRWRpdCBpdGVtIGJ1dHRvbiBoYXMgYmVlbiBjbGlja2VkLlxuICAgKiBXaGVuIHRoZSBidXR0b24gaGFzIGJlZW4gY2xpY2tlZCBgaW9uaWNTZWxlY3RhYmxlQWRkSXRlbVRlbXBsYXRlYCB3aWxsIGJlIHNob3duLiBVc2UgdGhlIHRlbXBsYXRlIHRvIGNyZWF0ZSBhIGZvcm0gdG8gZWRpdCBpdGVtLlxuICAgKiAqKk5vdGUqKjogYGNhblNhdmVJdGVtYCBoYXMgdG8gYmUgZW5hYmxlZC5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jb25zYXZlaXRlbSkuXG4gICAqXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBPdXRwdXQoKVxuICBvblNhdmVJdGVtOiBFdmVudEVtaXR0ZXI8eyBjb21wb25lbnQ6IElvbmljU2VsZWN0YWJsZUNvbXBvbmVudCwgaXRlbTogYW55IH0+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIC8qKlxuICAgKiBGaXJlcyB3aGVuIERlbGV0ZSBpdGVtIGJ1dHRvbiBoYXMgYmVlbiBjbGlja2VkLlxuICAgKiAqKk5vdGUqKjogYGNhbkRlbGV0ZUl0ZW1gIGhhcyB0byBiZSBlbmFibGVkLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNvbmRlbGV0ZWl0ZW0pLlxuICAgKlxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBAT3V0cHV0KClcbiAgb25EZWxldGVJdGVtOiBFdmVudEVtaXR0ZXI8eyBjb21wb25lbnQ6IElvbmljU2VsZWN0YWJsZUNvbXBvbmVudCwgaXRlbTogYW55IH0+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIC8qKlxuICAgKiBGaXJlcyB3aGVuIEFkZCBpdGVtIGJ1dHRvbiBoYXMgYmVlbiBjbGlja2VkLlxuICAgKiBXaGVuIHRoZSBidXR0b24gaGFzIGJlZW4gY2xpY2tlZCBgaW9uaWNTZWxlY3RhYmxlQWRkSXRlbVRlbXBsYXRlYCB3aWxsIGJlIHNob3duLiBVc2UgdGhlIHRlbXBsYXRlIHRvIGNyZWF0ZSBhIGZvcm0gdG8gYWRkIGl0ZW0uXG4gICAqICoqTm90ZSoqOiBgY2FuQWRkSXRlbWAgaGFzIHRvIGJlIGVuYWJsZWQuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI29uYWRkaXRlbSkuXG4gICAqXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBPdXRwdXQoKVxuICBvbkFkZEl0ZW06IEV2ZW50RW1pdHRlcjx7IGNvbXBvbmVudDogSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50IH0+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIEBDb250ZW50Q2hpbGQoSW9uaWNTZWxlY3RhYmxlVmFsdWVUZW1wbGF0ZURpcmVjdGl2ZSwgeyByZWFkOiBUZW1wbGF0ZVJlZiwgc3RhdGljOiBmYWxzZSB9KVxuICB2YWx1ZVRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xuICBAQ29udGVudENoaWxkKElvbmljU2VsZWN0YWJsZUl0ZW1UZW1wbGF0ZURpcmVjdGl2ZSwgeyByZWFkOiBUZW1wbGF0ZVJlZiwgc3RhdGljOiBmYWxzZSB9KVxuICBpdGVtVGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XG4gIEBDb250ZW50Q2hpbGQoSW9uaWNTZWxlY3RhYmxlSXRlbUVuZFRlbXBsYXRlRGlyZWN0aXZlLCB7IHJlYWQ6IFRlbXBsYXRlUmVmLCBzdGF0aWM6IGZhbHNlIH0pXG4gIGl0ZW1FbmRUZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcbiAgQENvbnRlbnRDaGlsZChJb25pY1NlbGVjdGFibGVUaXRsZVRlbXBsYXRlRGlyZWN0aXZlLCB7IHJlYWQ6IFRlbXBsYXRlUmVmLCBzdGF0aWM6IGZhbHNlIH0pXG4gIHRpdGxlVGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XG4gIEBDb250ZW50Q2hpbGQoSW9uaWNTZWxlY3RhYmxlUGxhY2Vob2xkZXJUZW1wbGF0ZURpcmVjdGl2ZSwgeyByZWFkOiBUZW1wbGF0ZVJlZiwgc3RhdGljOiBmYWxzZSB9KVxuICBwbGFjZWhvbGRlclRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xuICBAQ29udGVudENoaWxkKElvbmljU2VsZWN0YWJsZU1lc3NhZ2VUZW1wbGF0ZURpcmVjdGl2ZSwgeyByZWFkOiBUZW1wbGF0ZVJlZiwgc3RhdGljOiBmYWxzZSB9KVxuICBtZXNzYWdlVGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XG4gIEBDb250ZW50Q2hpbGQoSW9uaWNTZWxlY3RhYmxlR3JvdXBUZW1wbGF0ZURpcmVjdGl2ZSwgeyByZWFkOiBUZW1wbGF0ZVJlZiwgc3RhdGljOiBmYWxzZSB9KVxuICBncm91cFRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xuICBAQ29udGVudENoaWxkKElvbmljU2VsZWN0YWJsZUdyb3VwRW5kVGVtcGxhdGVEaXJlY3RpdmUsIHsgcmVhZDogVGVtcGxhdGVSZWYsIHN0YXRpYzogZmFsc2UgfSlcbiAgZ3JvdXBFbmRUZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcbiAgQENvbnRlbnRDaGlsZChJb25pY1NlbGVjdGFibGVDbG9zZUJ1dHRvblRlbXBsYXRlRGlyZWN0aXZlLCB7IHJlYWQ6IFRlbXBsYXRlUmVmLCBzdGF0aWM6IGZhbHNlIH0pXG4gIGNsb3NlQnV0dG9uVGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XG4gIEBDb250ZW50Q2hpbGQoSW9uaWNTZWxlY3RhYmxlU2VhcmNoRmFpbFRlbXBsYXRlRGlyZWN0aXZlLCB7IHJlYWQ6IFRlbXBsYXRlUmVmLCBzdGF0aWM6IGZhbHNlIH0pXG4gIHNlYXJjaEZhaWxUZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcbiAgQENvbnRlbnRDaGlsZChJb25pY1NlbGVjdGFibGVBZGRJdGVtVGVtcGxhdGVEaXJlY3RpdmUsIHsgcmVhZDogVGVtcGxhdGVSZWYsIHN0YXRpYzogZmFsc2UgfSlcbiAgYWRkSXRlbVRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xuICBAQ29udGVudENoaWxkKElvbmljU2VsZWN0YWJsZUZvb3RlclRlbXBsYXRlRGlyZWN0aXZlLCB7IHJlYWQ6IFRlbXBsYXRlUmVmLCBzdGF0aWM6IGZhbHNlIH0pXG4gIGZvb3RlclRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xuICBfYWRkSXRlbVRlbXBsYXRlRm9vdGVySGVpZ2h0OiBzdHJpbmc7XG4gIEBDb250ZW50Q2hpbGQoSW9uaWNTZWxlY3RhYmxlSGVhZGVyVGVtcGxhdGVEaXJlY3RpdmUsIHsgcmVhZDogVGVtcGxhdGVSZWYsIHN0YXRpYzogZmFsc2UgfSlcbiAgaGVhZGVyVGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XG4gIEBDb250ZW50Q2hpbGQoSW9uaWNTZWxlY3RhYmxlSXRlbUljb25UZW1wbGF0ZURpcmVjdGl2ZSwgeyByZWFkOiBUZW1wbGF0ZVJlZiwgc3RhdGljOiBmYWxzZSB9KVxuICBpdGVtSWNvblRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xuICBAQ29udGVudENoaWxkKElvbmljU2VsZWN0YWJsZUljb25UZW1wbGF0ZURpcmVjdGl2ZSwgeyByZWFkOiBUZW1wbGF0ZVJlZiwgc3RhdGljOiBmYWxzZSB9KVxuICBpY29uVGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XG5cbiAgLyoqXG4gICAqIFNlZSBJb25pYyBWaXJ0dWFsU2Nyb2xsIFtoZWFkZXJGbl0oaHR0cHM6Ly9pb25pY2ZyYW1ld29yay5jb20vZG9jcy9hcGkvY29tcG9uZW50cy92aXJ0dWFsLXNjcm9sbC9WaXJ0dWFsU2Nyb2xsLykuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI3ZpcnR1YWxzY3JvbGxoZWFkZXJmbikuXG4gICAqXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIEBJbnB1dCgpXG4gIHZpcnR1YWxTY3JvbGxIZWFkZXJGbiA9ICgpID0+IHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgX21vZGFsQ29udHJvbGxlcjogTW9kYWxDb250cm9sbGVyLFxuICAgIHByaXZhdGUgX3BsYXRmb3JtOiBQbGF0Zm9ybSxcbiAgICBAT3B0aW9uYWwoKSBwcml2YXRlIGlvbkl0ZW06IElvbkl0ZW0sXG4gICAgcHJpdmF0ZSBfaXRlcmFibGVEaWZmZXJzOiBJdGVyYWJsZURpZmZlcnMsXG4gICAgcHJpdmF0ZSBfZWxlbWVudDogRWxlbWVudFJlZixcbiAgICBwcml2YXRlIF9yZW5kZXJlcjogUmVuZGVyZXIyXG4gICkge1xuICAgIGlmICghdGhpcy5pdGVtcyB8fCAhdGhpcy5pdGVtcy5sZW5ndGgpIHtcbiAgICAgIHRoaXMuaXRlbXMgPSBbXTtcbiAgICB9XG5cbiAgICB0aGlzLl9pdGVtc0RpZmZlciA9IHRoaXMuX2l0ZXJhYmxlRGlmZmVycy5maW5kKHRoaXMuaXRlbXMpLmNyZWF0ZSgpO1xuICB9XG5cbiAgaW5pdEZvY3VzKCkgeyB9XG5cbiAgZW5hYmxlSW9uSXRlbShpc0VuYWJsZWQ6IGJvb2xlYW4pIHtcbiAgICBpZiAoIXRoaXMuaW9uSXRlbSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMuaW9uSXRlbS5kaXNhYmxlZCA9ICFpc0VuYWJsZWQ7XG4gIH1cblxuICBfaXNOdWxsT3JXaGl0ZVNwYWNlKHZhbHVlOiBhbnkpOiBib29sZWFuIHtcbiAgICBpZiAodmFsdWUgPT09IG51bGwgfHwgdmFsdWUgPT09IHVuZGVmaW5lZCkge1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgLy8gQ29udmVydCB2YWx1ZSB0byBzdHJpbmcgaW4gY2FzZSBpZiBpdCdzIG5vdC5cbiAgICByZXR1cm4gdmFsdWUudG9TdHJpbmcoKS5yZXBsYWNlKC9cXHMvZywgJycpLmxlbmd0aCA8IDE7XG4gIH1cblxuICBfc2V0SGFzU2VhcmNoVGV4dCgpIHtcbiAgICB0aGlzLl9oYXNTZWFyY2hUZXh0ID0gIXRoaXMuX2lzTnVsbE9yV2hpdGVTcGFjZSh0aGlzLl9zZWFyY2hUZXh0KTtcbiAgfVxuXG4gIF9oYXNPblNlYXJjaCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5pc09uU2VhcmNoRW5hYmxlZCAmJiB0aGlzLm9uU2VhcmNoLm9ic2VydmVycy5sZW5ndGggPiAwO1xuICB9XG5cbiAgX2hhc09uU2F2ZUl0ZW0oKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuY2FuU2F2ZUl0ZW0gJiYgdGhpcy5vblNhdmVJdGVtLm9ic2VydmVycy5sZW5ndGggPiAwO1xuICB9XG5cbiAgX2hhc09uQWRkSXRlbSgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5jYW5BZGRJdGVtICYmIHRoaXMub25BZGRJdGVtLm9ic2VydmVycy5sZW5ndGggPiAwO1xuICB9XG5cbiAgX2hhc09uRGVsZXRlSXRlbSgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5jYW5EZWxldGVJdGVtICYmIHRoaXMub25EZWxldGVJdGVtLm9ic2VydmVycy5sZW5ndGggPiAwO1xuICB9XG5cbiAgX2VtaXRWYWx1ZUNoYW5nZSgpIHtcbiAgICB0aGlzLnByb3BhZ2F0ZU9uQ2hhbmdlKHRoaXMudmFsdWUpO1xuXG4gICAgdGhpcy5vbkNoYW5nZS5lbWl0KHtcbiAgICAgIGNvbXBvbmVudDogdGhpcyxcbiAgICAgIHZhbHVlOiB0aGlzLnZhbHVlXG4gICAgfSk7XG4gIH1cblxuICBfZW1pdFNlYXJjaCgpIHtcbiAgICBpZiAoIXRoaXMuY2FuU2VhcmNoKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5vblNlYXJjaC5lbWl0KHtcbiAgICAgIGNvbXBvbmVudDogdGhpcyxcbiAgICAgIHRleHQ6IHRoaXMuX3NlYXJjaFRleHRcbiAgICB9KTtcbiAgfVxuXG4gIF9lbWl0T25TZWxlY3QoaXRlbTogYW55LCBpc1NlbGVjdGVkOiBib29sZWFuKSB7XG4gICAgdGhpcy5vblNlbGVjdC5lbWl0KHtcbiAgICAgIGNvbXBvbmVudDogdGhpcyxcbiAgICAgIGl0ZW06IGl0ZW0sXG4gICAgICBpc1NlbGVjdGVkOiBpc1NlbGVjdGVkXG4gICAgfSk7XG4gIH1cblxuICBfZW1pdE9uQ2xlYXIoaXRlbXM6IGFueVtdKSB7XG4gICAgdGhpcy5vbkNsZWFyLmVtaXQoe1xuICAgICAgY29tcG9uZW50OiB0aGlzLFxuICAgICAgaXRlbXM6IGl0ZW1zXG4gICAgfSk7XG4gIH1cblxuICBfZW1pdE9uU2VhcmNoU3VjY2Vzc09yRmFpbChpc1N1Y2Nlc3M6IGJvb2xlYW4pIHtcbiAgICBjb25zdCBldmVudERhdGEgPSB7XG4gICAgICBjb21wb25lbnQ6IHRoaXMsXG4gICAgICB0ZXh0OiB0aGlzLl9zZWFyY2hUZXh0XG4gICAgfTtcblxuICAgIGlmIChpc1N1Y2Nlc3MpIHtcbiAgICAgIHRoaXMub25TZWFyY2hTdWNjZXNzLmVtaXQoZXZlbnREYXRhKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5vblNlYXJjaEZhaWwuZW1pdChldmVudERhdGEpO1xuICAgIH1cbiAgfVxuXG4gIF9mb3JtYXRJdGVtKGl0ZW06IGFueSk6IHN0cmluZyB7XG4gICAgaWYgKHRoaXMuX2lzTnVsbE9yV2hpdGVTcGFjZShpdGVtKSkge1xuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMuaXRlbVRleHRGaWVsZCA/IGl0ZW1bdGhpcy5pdGVtVGV4dEZpZWxkXSA6IGl0ZW0udG9TdHJpbmcoKTtcbiAgfVxuXG4gIF9mb3JtYXRWYWx1ZUl0ZW0oaXRlbTogYW55KTogc3RyaW5nIHtcbiAgICBpZiAodGhpcy5fc2hvdWxkU3RvcmVJdGVtVmFsdWUpIHtcbiAgICAgIC8vIEdldCBpdGVtIHRleHQgZnJvbSB0aGUgbGlzdCBhcyB3ZSBzdG9yZSBpdCdzIHZhbHVlIG9ubHkuXG4gICAgICBjb25zdCBzZWxlY3RlZEl0ZW0gPSB0aGlzLml0ZW1zLmZpbmQoX2l0ZW0gPT4ge1xuICAgICAgICByZXR1cm4gX2l0ZW1bdGhpcy5pdGVtVmFsdWVGaWVsZF0gPT09IGl0ZW07XG4gICAgICB9KTtcblxuICAgICAgcmV0dXJuIHRoaXMuX2Zvcm1hdEl0ZW0oc2VsZWN0ZWRJdGVtKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHRoaXMuX2Zvcm1hdEl0ZW0oaXRlbSk7XG4gICAgfVxuICB9XG5cbiAgX2dldEl0ZW1WYWx1ZShpdGVtOiBhbnkpOiBhbnkge1xuICAgIGlmICghdGhpcy5faGFzT2JqZWN0cykge1xuICAgICAgcmV0dXJuIGl0ZW07XG4gICAgfVxuXG4gICAgcmV0dXJuIGl0ZW1bdGhpcy5pdGVtVmFsdWVGaWVsZF07XG4gIH1cblxuICBfZ2V0U3RvcmVkSXRlbVZhbHVlKGl0ZW06IGFueSk6IGFueSB7XG4gICAgaWYgKCF0aGlzLl9oYXNPYmplY3RzKSB7XG4gICAgICByZXR1cm4gaXRlbTtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcy5fc2hvdWxkU3RvcmVJdGVtVmFsdWUgPyBpdGVtIDogaXRlbVt0aGlzLml0ZW1WYWx1ZUZpZWxkXTtcbiAgfVxuXG4gIF9vblNlYXJjaGJhckNsZWFyKCkge1xuICAgIC8vIElvbmljIFNlYXJjaGJhciBkb2Vzbid0IGNsZWFyIGJpbmQgd2l0aCBuZ01vZGVsIHZhbHVlLlxuICAgIC8vIERvIGl0IG91cnNlbHZlcy5cbiAgICB0aGlzLl9zZWFyY2hUZXh0ID0gJyc7XG4gIH1cblxuICBfZmlsdGVySXRlbXMoKSB7XG4gICAgdGhpcy5fc2V0SGFzU2VhcmNoVGV4dCgpO1xuXG4gICAgaWYgKHRoaXMuX2hhc09uU2VhcmNoKCkpIHtcbiAgICAgIC8vIERlbGVnYXRlIGZpbHRlcmluZyB0byB0aGUgZXZlbnQuXG4gICAgICB0aGlzLl9lbWl0U2VhcmNoKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIERlZmF1bHQgZmlsdGVyaW5nLlxuICAgICAgbGV0IGdyb3VwcyA9IFtdO1xuXG4gICAgICBpZiAoIXRoaXMuX3NlYXJjaFRleHQgfHwgIXRoaXMuX3NlYXJjaFRleHQudHJpbSgpKSB7XG4gICAgICAgIGdyb3VwcyA9IHRoaXMuX2dyb3VwcztcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnN0IGZpbHRlclRleHQgPSB0aGlzLl9zZWFyY2hUZXh0LnRyaW0oKS50b0xvd2VyQ2FzZSgpO1xuXG4gICAgICAgIHRoaXMuX2dyb3Vwcy5mb3JFYWNoKGdyb3VwID0+IHtcbiAgICAgICAgICBjb25zdCBpdGVtcyA9IGdyb3VwLml0ZW1zLmZpbHRlcihpdGVtID0+IHtcbiAgICAgICAgICAgIGNvbnN0IGl0ZW1UZXh0ID0gKHRoaXMuaXRlbVRleHRGaWVsZCA/XG4gICAgICAgICAgICAgIGl0ZW1bdGhpcy5pdGVtVGV4dEZpZWxkXSA6IGl0ZW0pLnRvU3RyaW5nKCkudG9Mb3dlckNhc2UoKTtcbiAgICAgICAgICAgIHJldHVybiBpdGVtVGV4dC5pbmRleE9mKGZpbHRlclRleHQpICE9PSAtMTtcbiAgICAgICAgICB9KTtcblxuICAgICAgICAgIGlmIChpdGVtcy5sZW5ndGgpIHtcbiAgICAgICAgICAgIGdyb3Vwcy5wdXNoKHtcbiAgICAgICAgICAgICAgdmFsdWU6IGdyb3VwLnZhbHVlLFxuICAgICAgICAgICAgICB0ZXh0OiBncm91cC50ZXh0LFxuICAgICAgICAgICAgICBpdGVtczogaXRlbXNcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gTm8gaXRlbXMgZm91bmQuXG4gICAgICAgIGlmICghZ3JvdXBzLmxlbmd0aCkge1xuICAgICAgICAgIGdyb3Vwcy5wdXNoKHtcbiAgICAgICAgICAgIGl0ZW1zOiBbXVxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHRoaXMuX2ZpbHRlcmVkR3JvdXBzID0gZ3JvdXBzO1xuICAgICAgdGhpcy5faGFzRmlsdGVyZWRJdGVtcyA9ICF0aGlzLl9hcmVHcm91cHNFbXB0eShncm91cHMpO1xuICAgICAgdGhpcy5fZW1pdE9uU2VhcmNoU3VjY2Vzc09yRmFpbCh0aGlzLl9oYXNGaWx0ZXJlZEl0ZW1zKTtcbiAgICB9XG4gIH1cblxuICBfaXNJdGVtRGlzYWJsZWQoaXRlbTogYW55KTogYm9vbGVhbiB7XG4gICAgaWYgKCF0aGlzLmRpc2FibGVkSXRlbXMpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcy5kaXNhYmxlZEl0ZW1zLnNvbWUoX2l0ZW0gPT4ge1xuICAgICAgcmV0dXJuIHRoaXMuX2dldEl0ZW1WYWx1ZShfaXRlbSkgPT09IHRoaXMuX2dldEl0ZW1WYWx1ZShpdGVtKTtcbiAgICB9KTtcbiAgfVxuXG4gIF9pc0l0ZW1TZWxlY3RlZChpdGVtOiBhbnkpIHtcbiAgICByZXR1cm4gdGhpcy5fc2VsZWN0ZWRJdGVtcy5maW5kKHNlbGVjdGVkSXRlbSA9PiB7XG4gICAgICByZXR1cm4gdGhpcy5fZ2V0SXRlbVZhbHVlKGl0ZW0pID09PSB0aGlzLl9nZXRTdG9yZWRJdGVtVmFsdWUoc2VsZWN0ZWRJdGVtKTtcbiAgICB9KSAhPT0gdW5kZWZpbmVkO1xuICB9XG5cbiAgX2FkZFNlbGVjdGVkSXRlbShpdGVtOiBhbnkpIHtcbiAgICBpZiAodGhpcy5fc2hvdWxkU3RvcmVJdGVtVmFsdWUpIHtcbiAgICAgIHRoaXMuX3NlbGVjdGVkSXRlbXMucHVzaCh0aGlzLl9nZXRJdGVtVmFsdWUoaXRlbSkpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLl9zZWxlY3RlZEl0ZW1zLnB1c2goaXRlbSk7XG4gICAgfVxuICB9XG5cbiAgX2RlbGV0ZVNlbGVjdGVkSXRlbShpdGVtOiBhbnkpIHtcbiAgICBsZXQgaXRlbVRvRGVsZXRlSW5kZXg7XG5cbiAgICB0aGlzLl9zZWxlY3RlZEl0ZW1zLmZvckVhY2goKHNlbGVjdGVkSXRlbSwgaXRlbUluZGV4KSA9PiB7XG4gICAgICBpZiAoXG4gICAgICAgIHRoaXMuX2dldEl0ZW1WYWx1ZShpdGVtKSA9PT1cbiAgICAgICAgdGhpcy5fZ2V0U3RvcmVkSXRlbVZhbHVlKHNlbGVjdGVkSXRlbSlcbiAgICAgICkge1xuICAgICAgICBpdGVtVG9EZWxldGVJbmRleCA9IGl0ZW1JbmRleDtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIHRoaXMuX3NlbGVjdGVkSXRlbXMuc3BsaWNlKGl0ZW1Ub0RlbGV0ZUluZGV4LCAxKTtcbiAgfVxuXG4gIF9jbGljaygpIHtcbiAgICBpZiAoIXRoaXMuaXNFbmFibGVkKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5fbGFiZWwgPSB0aGlzLl9nZXRMYWJlbFRleHQoKTtcbiAgICB0aGlzLm9wZW4oKS50aGVuKCgpID0+IHtcbiAgICAgIHRoaXMub25PcGVuLmVtaXQoe1xuICAgICAgICBjb21wb25lbnQ6IHRoaXNcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgX3NhdmVJdGVtKGV2ZW50OiBFdmVudCwgaXRlbTogYW55KSB7XG4gICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgdGhpcy5faXRlbVRvQWRkID0gaXRlbTtcblxuICAgIGlmICh0aGlzLl9oYXNPblNhdmVJdGVtKCkpIHtcbiAgICAgIHRoaXMub25TYXZlSXRlbS5lbWl0KHtcbiAgICAgICAgY29tcG9uZW50OiB0aGlzLFxuICAgICAgICBpdGVtOiB0aGlzLl9pdGVtVG9BZGRcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNob3dBZGRJdGVtVGVtcGxhdGUoKTtcbiAgICB9XG4gIH1cblxuICBfZGVsZXRlSXRlbUNsaWNrKGV2ZW50OiBFdmVudCwgaXRlbTogYW55KSB7XG4gICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgdGhpcy5faXRlbVRvQWRkID0gaXRlbTtcblxuICAgIGlmICh0aGlzLl9oYXNPbkRlbGV0ZUl0ZW0oKSkge1xuICAgICAgLy8gRGVsZWdhdGUgbG9naWMgdG8gZXZlbnQuXG4gICAgICB0aGlzLm9uRGVsZXRlSXRlbS5lbWl0KHtcbiAgICAgICAgY29tcG9uZW50OiB0aGlzLFxuICAgICAgICBpdGVtOiB0aGlzLl9pdGVtVG9BZGRcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmRlbGV0ZUl0ZW0odGhpcy5faXRlbVRvQWRkKTtcbiAgICB9XG4gIH1cblxuICBfYWRkSXRlbUNsaWNrKCkge1xuICAgIGlmICh0aGlzLl9oYXNPbkFkZEl0ZW0oKSkge1xuICAgICAgdGhpcy5vbkFkZEl0ZW0uZW1pdCh7XG4gICAgICAgIGNvbXBvbmVudDogdGhpc1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2hvd0FkZEl0ZW1UZW1wbGF0ZSgpO1xuICAgIH1cbiAgfVxuXG4gIF9wb3NpdGlvbkFkZEl0ZW1UZW1wbGF0ZSgpIHtcbiAgICAvLyBXYWl0IGZvciB0aGUgdGVtcGxhdGUgdG8gcmVuZGVyLlxuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgY29uc3QgZm9vdGVyID0gdGhpcy5fbW9kYWxDb21wb25lbnQuX2VsZW1lbnQubmF0aXZlRWxlbWVudFxuICAgICAgICAucXVlcnlTZWxlY3RvcignLmlvbmljLXNlbGVjdGFibGUtYWRkLWl0ZW0tdGVtcGxhdGUgaW9uLWZvb3RlcicpO1xuXG4gICAgICB0aGlzLl9hZGRJdGVtVGVtcGxhdGVGb290ZXJIZWlnaHQgPSBmb290ZXIgPyBgY2FsYygxMDAlIC0gJHtmb290ZXIub2Zmc2V0SGVpZ2h0fXB4KWAgOiAnMTAwJSc7XG4gICAgfSwgMTAwKTtcbiAgfVxuXG4gIF9jbG9zZSgpIHtcbiAgICB0aGlzLmNsb3NlKCkudGhlbigoKSA9PiB7XG4gICAgICB0aGlzLm9uQ2xvc2UuZW1pdCh7XG4gICAgICAgIGNvbXBvbmVudDogdGhpc1xuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICBpZiAoIXRoaXMuX2hhc09uU2VhcmNoKCkpIHtcbiAgICAgIHRoaXMuX3NlYXJjaFRleHQgPSAnJztcbiAgICAgIHRoaXMuX3NldEhhc1NlYXJjaFRleHQoKTtcbiAgICB9XG4gIH1cblxuICBfY2xlYXIoKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRJdGVtcyA9IHRoaXMuX3NlbGVjdGVkSXRlbXM7XG5cbiAgICB0aGlzLmNsZWFyKCk7XG4gICAgdGhpcy5fZW1pdFZhbHVlQ2hhbmdlKCk7XG4gICAgdGhpcy5fZW1pdE9uQ2xlYXIoc2VsZWN0ZWRJdGVtcyk7XG4gICAgdGhpcy5jbG9zZSgpLnRoZW4oKCkgPT4ge1xuICAgICAgdGhpcy5vbkNsb3NlLmVtaXQoe1xuICAgICAgICBjb21wb25lbnQ6IHRoaXNcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgX2dldE1vcmVJdGVtcygpIHtcbiAgICB0aGlzLm9uSW5maW5pdGVTY3JvbGwuZW1pdCh7XG4gICAgICBjb21wb25lbnQ6IHRoaXMsXG4gICAgICB0ZXh0OiB0aGlzLl9zZWFyY2hUZXh0XG4gICAgfSk7XG4gIH1cblxuICBfc2V0SXRlbXNUb0NvbmZpcm0oaXRlbXM6IGFueVtdKSB7XG4gICAgLy8gUmV0dXJuIGEgY29weSBvZiBvcmlnaW5hbCBhcnJheSwgc28gaXQgY291bGRuJ3QgYmUgY2hhbmdlZCBmcm9tIG91dHNpZGUuXG4gICAgdGhpcy5faXRlbXNUb0NvbmZpcm0gPSBbXS5jb25jYXQoaXRlbXMpO1xuICB9XG5cbiAgX2RvU2VsZWN0KHNlbGVjdGVkSXRlbTogYW55KSB7XG4gICAgdGhpcy52YWx1ZSA9IHNlbGVjdGVkSXRlbTtcbiAgICB0aGlzLl9lbWl0VmFsdWVDaGFuZ2UoKTtcbiAgfVxuXG4gIF9zZWxlY3QoaXRlbTogYW55KSB7XG4gICAgY29uc3QgaXNJdGVtU2VsZWN0ZWQgPSB0aGlzLl9pc0l0ZW1TZWxlY3RlZChpdGVtKTtcblxuICAgIGlmICh0aGlzLmlzTXVsdGlwbGUpIHtcbiAgICAgIGlmIChpc0l0ZW1TZWxlY3RlZCkge1xuICAgICAgICB0aGlzLl9kZWxldGVTZWxlY3RlZEl0ZW0oaXRlbSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLl9hZGRTZWxlY3RlZEl0ZW0oaXRlbSk7XG4gICAgICB9XG5cbiAgICAgIHRoaXMuX3NldEl0ZW1zVG9Db25maXJtKHRoaXMuX3NlbGVjdGVkSXRlbXMpO1xuXG4gICAgICAvLyBFbWl0IG9uU2VsZWN0IGV2ZW50IGFmdGVyIHNldHRpbmcgaXRlbXMgdG8gY29uZmlybSBzbyB0aGV5IGNvdWxkIGJlIHVzZWRcbiAgICAgIC8vIGluc2lkZSB0aGUgZXZlbnQuXG4gICAgICB0aGlzLl9lbWl0T25TZWxlY3QoaXRlbSwgIWlzSXRlbVNlbGVjdGVkKTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHRoaXMuaGFzQ29uZmlybUJ1dHRvbiB8fCB0aGlzLmZvb3RlclRlbXBsYXRlKSB7XG4gICAgICAgIC8vIERvbid0IGNsb3NlIE1vZGFsIGFuZCBrZWVwIHRyYWNrIG9uIGl0ZW1zIHRvIGNvbmZpcm0uXG4gICAgICAgIC8vIFdoZW4gZm9vdGVyIHRlbXBsYXRlIGlzIHVzZWQgaXQncyB1cCB0byBkZXZlbG9wZXIgdG8gY2xvc2UgTW9kYWwuXG4gICAgICAgIHRoaXMuX3NlbGVjdGVkSXRlbXMgPSBbXTtcblxuICAgICAgICBpZiAoaXNJdGVtU2VsZWN0ZWQpIHtcbiAgICAgICAgICB0aGlzLl9kZWxldGVTZWxlY3RlZEl0ZW0oaXRlbSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5fYWRkU2VsZWN0ZWRJdGVtKGl0ZW0pO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5fc2V0SXRlbXNUb0NvbmZpcm0odGhpcy5fc2VsZWN0ZWRJdGVtcyk7XG5cbiAgICAgICAgLy8gRW1pdCBvblNlbGVjdCBldmVudCBhZnRlciBzZXR0aW5nIGl0ZW1zIHRvIGNvbmZpcm0gc28gdGhleSBjb3VsZCBiZSB1c2VkXG4gICAgICAgIC8vIGluc2lkZSB0aGUgZXZlbnQuXG4gICAgICAgIHRoaXMuX2VtaXRPblNlbGVjdChpdGVtLCAhaXNJdGVtU2VsZWN0ZWQpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKCFpc0l0ZW1TZWxlY3RlZCkge1xuICAgICAgICAgIHRoaXMuX3NlbGVjdGVkSXRlbXMgPSBbXTtcbiAgICAgICAgICB0aGlzLl9hZGRTZWxlY3RlZEl0ZW0oaXRlbSk7XG5cbiAgICAgICAgICAvLyBFbWl0IG9uU2VsZWN0IGJlZm9yZSBvbkNoYW5nZS5cbiAgICAgICAgICB0aGlzLl9lbWl0T25TZWxlY3QoaXRlbSwgdHJ1ZSk7XG5cbiAgICAgICAgICBpZiAodGhpcy5fc2hvdWxkU3RvcmVJdGVtVmFsdWUpIHtcbiAgICAgICAgICAgIHRoaXMuX2RvU2VsZWN0KHRoaXMuX2dldEl0ZW1WYWx1ZShpdGVtKSk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuX2RvU2VsZWN0KGl0ZW0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuX2Nsb3NlKCk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgX2NvbmZpcm0oKSB7XG4gICAgdGhpcy5jb25maXJtKCk7XG4gICAgdGhpcy5fY2xvc2UoKTtcbiAgfVxuXG4gIHByaXZhdGUgX2dldExhYmVsVGV4dCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLl9pb25MYWJlbEVsZW1lbnQgPyB0aGlzLl9pb25MYWJlbEVsZW1lbnQudGV4dENvbnRlbnQgOiBudWxsO1xuICB9XG5cbiAgcHJpdmF0ZSBfYXJlR3JvdXBzRW1wdHkoZ3JvdXBzKSB7XG4gICAgcmV0dXJuIGdyb3Vwcy5sZW5ndGggPT09IDAgfHwgZ3JvdXBzLmV2ZXJ5KGdyb3VwID0+IHtcbiAgICAgIHJldHVybiAhZ3JvdXAuaXRlbXMgfHwgZ3JvdXAuaXRlbXMubGVuZ3RoID09PSAwO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBfY291bnRGb290ZXJCdXR0b25zKCkge1xuICAgIGxldCBmb290ZXJCdXR0b25zQ291bnQgPSAwO1xuXG4gICAgaWYgKHRoaXMuY2FuQ2xlYXIpIHtcbiAgICAgIGZvb3RlckJ1dHRvbnNDb3VudCsrO1xuICAgIH1cblxuICAgIGlmICh0aGlzLmlzTXVsdGlwbGUgfHwgdGhpcy5faGFzQ29uZmlybUJ1dHRvbikge1xuICAgICAgZm9vdGVyQnV0dG9uc0NvdW50Kys7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuY2FuQWRkSXRlbSkge1xuICAgICAgZm9vdGVyQnV0dG9uc0NvdW50Kys7XG4gICAgfVxuXG4gICAgdGhpcy5fZm9vdGVyQnV0dG9uc0NvdW50ID0gZm9vdGVyQnV0dG9uc0NvdW50O1xuICB9XG5cbiAgcHJpdmF0ZSBfc2V0SXRlbXMoaXRlbXM6IGFueVtdKSB7XG4gICAgLy8gSXQncyBpbXBvcnRhbnQgdG8gaGF2ZSBhbiBlbXB0eSBzdGFydGluZyBncm91cCB3aXRoIGVtcHR5IGl0ZW1zIChncm91cHNbMF0uaXRlbXMpLFxuICAgIC8vIGJlY2F1c2Ugd2UgYmluZCB0byBpdCB3aGVuIHVzaW5nIFZpcnR1YWxTY3JvbGwuXG4gICAgLy8gU2VlIGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvaXNzdWVzLzcwLlxuICAgIGxldCBncm91cHM6IGFueVtdID0gW3tcbiAgICAgIGl0ZW1zOiBpdGVtcyB8fCBbXVxuICAgIH1dO1xuXG4gICAgaWYgKGl0ZW1zICYmIGl0ZW1zLmxlbmd0aCkge1xuICAgICAgaWYgKHRoaXMuX2hhc0dyb3Vwcykge1xuICAgICAgICBncm91cHMgPSBbXTtcblxuICAgICAgICBpdGVtcy5mb3JFYWNoKGl0ZW0gPT4ge1xuICAgICAgICAgIGNvbnN0IGdyb3VwVmFsdWUgPSB0aGlzLl9nZXRQcm9wZXJ0eVZhbHVlKGl0ZW0sIHRoaXMuZ3JvdXBWYWx1ZUZpZWxkKSxcbiAgICAgICAgICAgIGdyb3VwID0gZ3JvdXBzLmZpbmQoX2dyb3VwID0+IF9ncm91cC52YWx1ZSA9PT0gZ3JvdXBWYWx1ZSk7XG5cbiAgICAgICAgICBpZiAoZ3JvdXApIHtcbiAgICAgICAgICAgIGdyb3VwLml0ZW1zLnB1c2goaXRlbSk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGdyb3Vwcy5wdXNoKHtcbiAgICAgICAgICAgICAgdmFsdWU6IGdyb3VwVmFsdWUsXG4gICAgICAgICAgICAgIHRleHQ6IHRoaXMuX2dldFByb3BlcnR5VmFsdWUoaXRlbSwgdGhpcy5ncm91cFRleHRGaWVsZCksXG4gICAgICAgICAgICAgIGl0ZW1zOiBbaXRlbV1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdGhpcy5fZ3JvdXBzID0gZ3JvdXBzO1xuICAgIHRoaXMuX2ZpbHRlcmVkR3JvdXBzID0gdGhpcy5fZ3JvdXBzO1xuICAgIHRoaXMuX2hhc0ZpbHRlcmVkSXRlbXMgPSAhdGhpcy5fYXJlR3JvdXBzRW1wdHkodGhpcy5fZmlsdGVyZWRHcm91cHMpO1xuICB9XG5cbiAgcHJpdmF0ZSBfZ2V0UHJvcGVydHlWYWx1ZShvYmplY3Q6IGFueSwgcHJvcGVydHk6IHN0cmluZyk6IGFueSB7XG4gICAgaWYgKCFwcm9wZXJ0eSkge1xuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgcmV0dXJuIHByb3BlcnR5LnNwbGl0KCcuJykucmVkdWNlKChfb2JqZWN0LCBfcHJvcGVydHkpID0+IHtcbiAgICAgIHJldHVybiBfb2JqZWN0ID8gX29iamVjdFtfcHJvcGVydHldIDogbnVsbDtcbiAgICB9LCBvYmplY3QpO1xuICB9XG5cbiAgcHJpdmF0ZSBfc2V0SW9uSXRlbUhhc0ZvY3VzKGhhc0ZvY3VzOiBib29sZWFuKSB7XG4gICAgaWYgKCF0aGlzLmlvbkl0ZW0pIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICAvLyBBcHBseSBmb2N1cyBDU1MgY2xhc3MgZm9yIHByb3BlciBzdHlseWluZyBvZiBpb24taXRlbS9pb24tbGFiZWwuXG4gICAgdGhpcy5fc2V0SW9uSXRlbUNzc0NsYXNzKCdpdGVtLWhhcy1mb2N1cycsIGhhc0ZvY3VzKTtcbiAgfVxuXG4gIHByaXZhdGUgX3NldElvbkl0ZW1IYXNWYWx1ZSgpIHtcbiAgICBpZiAoIXRoaXMuaW9uSXRlbSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIEFwcGx5IHZhbHVlIENTUyBjbGFzcyBmb3IgcHJvcGVyIHN0eWx5aW5nIG9mIGlvbi1pdGVtL2lvbi1sYWJlbC5cbiAgICB0aGlzLl9zZXRJb25JdGVtQ3NzQ2xhc3MoJ2l0ZW0taGFzLXZhbHVlJywgdGhpcy5oYXNWYWx1ZSgpKTtcbiAgfVxuXG4gIHByaXZhdGUgX3NldEhhc1BsYWNlaG9sZGVyKCkge1xuICAgIHRoaXMuX2hhc1BsYWNlaG9sZGVyID0gIXRoaXMuaGFzVmFsdWUoKSAmJlxuICAgICAgKCF0aGlzLl9pc051bGxPcldoaXRlU3BhY2UodGhpcy5wbGFjZWhvbGRlcikgfHwgdGhpcy5wbGFjZWhvbGRlclRlbXBsYXRlKSA/XG4gICAgICB0cnVlIDogZmFsc2U7XG4gIH1cblxuICBwcml2YXRlIHByb3BhZ2F0ZU9uQ2hhbmdlID0gKF86IGFueSkgPT4geyB9O1xuICBwcml2YXRlIHByb3BhZ2F0ZU9uVG91Y2hlZCA9ICgpID0+IHsgfTtcblxuICBwcml2YXRlIF9zZXRJb25JdGVtQ3NzQ2xhc3MoY3NzQ2xhc3M6IHN0cmluZywgc2hvdWxkQWRkOiBib29sZWFuKSB7XG4gICAgaWYgKCF0aGlzLl9pb25JdGVtRWxlbWVudCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIENoYW5nZSB0byBSZW5kZXJlcjJcbiAgICBpZiAoc2hvdWxkQWRkKSB7XG4gICAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLl9pb25JdGVtRWxlbWVudCwgY3NzQ2xhc3MpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLl9pb25JdGVtRWxlbWVudCwgY3NzQ2xhc3MpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgX3RvZ2dsZUFkZEl0ZW1UZW1wbGF0ZShpc1Zpc2libGU6IGJvb2xlYW4pIHtcbiAgICAvLyBJdCBzaG91bGQgYmUgcG9zc2libGUgdG8gc2hvdy9oaWRlIHRoZSB0ZW1wbGF0ZSByZWdhcmRsZXNzXG4gICAgLy8gY2FuQWRkSXRlbSBvciBjYW5TYXZlSXRlbSBwYXJhbWV0ZXJzLCBzbyB3ZSBjb3VsZCBpbXBsZW1lbnQgc29tZVxuICAgIC8vIGN1c3RvbSBiZWhhdmlvci4gRS5nLiBhZGRpbmcgaXRlbSB3aGVuIHNlYXJjaCBmYWlscyB1c2luZyBvblNlYXJjaEZhaWwgZXZlbnQuXG4gICAgaWYgKCF0aGlzLmFkZEl0ZW1UZW1wbGF0ZSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIFRvIG1ha2UgU2F2ZUl0ZW1UZW1wbGF0ZSB2aXNpYmxlIHdlIGp1c3QgcG9zaXRpb24gaXQgb3ZlciBsaXN0IHVzaW5nIENTUy5cbiAgICAvLyBXZSBkb24ndCBoaWRlIGxpc3Qgd2l0aCAqbmdJZiBvciBbaGlkZGVuXSB0byBwcmV2ZW50IGl0cyBzY3JvbGwgcG9zaXRpb24uXG4gICAgdGhpcy5faXNBZGRJdGVtVGVtcGxhdGVWaXNpYmxlID0gaXNWaXNpYmxlO1xuICAgIHRoaXMuX2lzRm9vdGVyVmlzaWJsZSA9ICFpc1Zpc2libGU7XG4gIH1cblxuICAvKiBDb250cm9sVmFsdWVBY2Nlc3NvciAqL1xuICB3cml0ZVZhbHVlKHZhbHVlOiBhbnkpIHtcbiAgICB0aGlzLnZhbHVlID0gdmFsdWU7XG4gIH1cblxuICByZWdpc3Rlck9uQ2hhbmdlKG1ldGhvZDogYW55KTogdm9pZCB7XG4gICAgdGhpcy5wcm9wYWdhdGVPbkNoYW5nZSA9IG1ldGhvZDtcbiAgfVxuXG4gIHJlZ2lzdGVyT25Ub3VjaGVkKG1ldGhvZDogKCkgPT4gdm9pZCkge1xuICAgIHRoaXMucHJvcGFnYXRlT25Ub3VjaGVkID0gbWV0aG9kO1xuICB9XG5cbiAgc2V0RGlzYWJsZWRTdGF0ZShpc0Rpc2FibGVkOiBib29sZWFuKSB7XG4gICAgdGhpcy5pc0VuYWJsZWQgPSAhaXNEaXNhYmxlZDtcbiAgfVxuICAvKiAuQ29udHJvbFZhbHVlQWNjZXNzb3IgKi9cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLl9pc0lvcyA9IHRoaXMuX3BsYXRmb3JtLmlzKCdpb3MnKTtcbiAgICB0aGlzLl9pc01EID0gIXRoaXMuX2lzSW9zO1xuICAgIHRoaXMuX2hhc09iamVjdHMgPSAhdGhpcy5faXNOdWxsT3JXaGl0ZVNwYWNlKHRoaXMuaXRlbVZhbHVlRmllbGQpO1xuICAgIC8vIEdyb3VwaW5nIGlzIHN1cHBvcnRlZCBmb3Igb2JqZWN0cyBvbmx5LlxuICAgIC8vIElvbmljIFZpcnR1YWxTY3JvbGwgaGFzIGl0J3Mgb3duIGltcGxlbWVudGF0aW9uIG9mIGdyb3VwaW5nLlxuICAgIHRoaXMuX2hhc0dyb3VwcyA9IEJvb2xlYW4odGhpcy5faGFzT2JqZWN0cyAmJiB0aGlzLmdyb3VwVmFsdWVGaWVsZCAmJiAhdGhpcy5oYXNWaXJ0dWFsU2Nyb2xsKTtcblxuICAgIGlmICh0aGlzLmlvbkl0ZW0pIHtcbiAgICAgIHRoaXMuX2lvbkl0ZW1FbGVtZW50ID0gdGhpcy5fZWxlbWVudC5uYXRpdmVFbGVtZW50LmNsb3Nlc3QoJ2lvbi1pdGVtJyk7XG4gICAgICB0aGlzLl9zZXRJb25JdGVtQ3NzQ2xhc3MoJ2l0ZW0taW50ZXJhY3RpdmUnLCB0cnVlKTtcbiAgICAgIHRoaXMuX3NldElvbkl0ZW1Dc3NDbGFzcygnaXRlbS1pb25pYy1zZWxlY3RhYmxlJywgdHJ1ZSk7XG5cbiAgICAgIGlmICh0aGlzLl9pb25JdGVtRWxlbWVudCkge1xuICAgICAgICB0aGlzLl9pb25MYWJlbEVsZW1lbnQgPSB0aGlzLl9pb25JdGVtRWxlbWVudC5xdWVyeVNlbGVjdG9yKCdpb24tbGFiZWwnKTtcblxuICAgICAgICBpZiAodGhpcy5faW9uTGFiZWxFbGVtZW50KSB7XG4gICAgICAgICAgdGhpcy5faGFzSW9uTGFiZWwgPSB0cnVlO1xuICAgICAgICAgIHRoaXMuX2lvbkxhYmVsUG9zaXRpb24gPSB0aGlzLl9pb25MYWJlbEVsZW1lbnQuZ2V0QXR0cmlidXRlKCdwb3NpdGlvbicpIHx8ICdkZWZhdWx0JztcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMuZW5hYmxlSW9uSXRlbSh0aGlzLmlzRW5hYmxlZCk7XG4gIH1cblxuICBuZ0RvQ2hlY2soKSB7XG4gICAgY29uc3QgaXRlbXNDaGFuZ2VzID0gdGhpcy5faXRlbXNEaWZmZXIuZGlmZih0aGlzLml0ZW1zKTtcblxuICAgIGlmIChpdGVtc0NoYW5nZXMpIHtcbiAgICAgIHRoaXMuX3NldEl0ZW1zKHRoaXMuaXRlbXMpO1xuICAgICAgdGhpcy52YWx1ZSA9IHRoaXMudmFsdWU7XG5cbiAgICAgIHRoaXMub25JdGVtc0NoYW5nZS5lbWl0KHtcbiAgICAgICAgY29tcG9uZW50OiB0aGlzXG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQWRkcyBpdGVtLlxuICAgKiAqKk5vdGUqKjogSWYgeW91IHdhbnQgYW4gaXRlbSB0byBiZSBhZGRlZCB0byB0aGUgb3JpZ2luYWwgYXJyYXkgYXMgd2VsbCB1c2UgdHdvLXdheSBkYXRhIGJpbmRpbmcgc3ludGF4IG9uIGBbKGl0ZW1zKV1gIGZpZWxkLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNhZGRpdGVtKS5cbiAgICpcbiAgICogQHBhcmFtIGl0ZW0gSXRlbSB0byBhZGQuXG4gICAqIEByZXR1cm5zIFByb21pc2UgdGhhdCByZXNvbHZlcyB3aGVuIGl0ZW0gaGFzIGJlZW4gYWRkZWQuXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIGFkZEl0ZW0oaXRlbTogYW55KTogUHJvbWlzZTxhbnk+IHtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcblxuICAgIC8vIEFkZGluZyBpdGVtIHRyaWdnZXJzIG9uSXRlbXNDaGFuZ2UuXG4gICAgLy8gUmV0dXJuIGEgcHJvbWlzZSB0aGF0IHJlc29sdmVzIHdoZW4gb25JdGVtc0NoYW5nZSBmaW5pc2hlcy5cbiAgICAvLyBXZSBuZWVkIGEgcHJvbWlzZSBvciB1c2VyIGNvdWxkIGRvIHNvbWV0aGluZyBhZnRlciBpdGVtIGhhcyBiZWVuIGFkZGVkLFxuICAgIC8vIGUuZy4gdXNlIHNlYXJjaCgpIG1ldGhvZCB0byBmaW5kIHRoZSBhZGRlZCBpdGVtLlxuICAgIHRoaXMuaXRlbXMudW5zaGlmdChpdGVtKTtcblxuICAgIC8vIENsb3NlIGFueSBydW5uaW5nIHN1YnNjcmlwdGlvbi5cbiAgICBpZiAodGhpcy5fYWRkSXRlbU9ic2VydmFibGUpIHtcbiAgICAgIHRoaXMuX2FkZEl0ZW1PYnNlcnZhYmxlLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgIC8vIENvbXBsZXRlIGNhbGxiYWNrIGlzbid0IGZpcmVkIGZvciBzb21lIHJlYXNvbixcbiAgICAgIC8vIHNvIHVuc3Vic2NyaWJlIGluIGJvdGggc3VjY2VzcyBhbmQgZmFpbCBjYXNlcy5cbiAgICAgIHNlbGYuX2FkZEl0ZW1PYnNlcnZhYmxlID0gc2VsZi5vbkl0ZW1zQ2hhbmdlLmFzT2JzZXJ2YWJsZSgpLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICAgIHNlbGYuX2FkZEl0ZW1PYnNlcnZhYmxlLnVuc3Vic2NyaWJlKCk7XG4gICAgICAgIHJlc29sdmUoKTtcbiAgICAgIH0sICgpID0+IHtcbiAgICAgICAgc2VsZi5fYWRkSXRlbU9ic2VydmFibGUudW5zdWJzY3JpYmUoKTtcbiAgICAgICAgcmVqZWN0KCk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICogRGVsZXRlcyBpdGVtLlxuICogKipOb3RlKio6IElmIHlvdSB3YW50IGFuIGl0ZW0gdG8gYmUgZGVsZXRlZCBmcm9tIHRoZSBvcmlnaW5hbCBhcnJheSBhcyB3ZWxsIHVzZSB0d28td2F5IGRhdGEgYmluZGluZyBzeW50YXggb24gYFsoaXRlbXMpXWAgZmllbGQuXG4gKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNkZWxldGVpdGVtKS5cbiAqXG4gKiBAcGFyYW0gaXRlbSBJdGVtIHRvIGRlbGV0ZS5cbiAqIEByZXR1cm5zIFByb21pc2UgdGhhdCByZXNvbHZlcyB3aGVuIGl0ZW0gaGFzIGJlZW4gZGVsZXRlZC5cbiAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAqL1xuICBkZWxldGVJdGVtKGl0ZW06IGFueSk6IFByb21pc2U8YW55PiB7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG4gICAgbGV0IGhhc1ZhbHVlQ2hhbmdlZCA9IGZhbHNlO1xuXG4gICAgLy8gUmVtb3ZlIGRlbGV0ZWQgaXRlbSBmcm9tIHNlbGVjdGVkIGl0ZW1zLlxuICAgIGlmICh0aGlzLl9zZWxlY3RlZEl0ZW1zKSB7XG4gICAgICB0aGlzLl9zZWxlY3RlZEl0ZW1zID0gdGhpcy5fc2VsZWN0ZWRJdGVtcy5maWx0ZXIoX2l0ZW0gPT4ge1xuICAgICAgICByZXR1cm4gdGhpcy5fZ2V0SXRlbVZhbHVlKGl0ZW0pICE9PSB0aGlzLl9nZXRTdG9yZWRJdGVtVmFsdWUoX2l0ZW0pO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgLy8gUmVtb3ZlIGRlbGV0ZWQgaXRlbSBmcm9tIHZhbHVlLlxuICAgIGlmICh0aGlzLnZhbHVlKSB7XG4gICAgICBpZiAodGhpcy5pc011bHRpcGxlKSB7XG4gICAgICAgIGNvbnN0IHZhbHVlcyA9IHRoaXMudmFsdWUuZmlsdGVyKHZhbHVlID0+IHtcbiAgICAgICAgICByZXR1cm4gdmFsdWUuaWQgIT09IGl0ZW0uaWQ7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmICh2YWx1ZXMubGVuZ3RoICE9PSB0aGlzLnZhbHVlLmxlbmd0aCkge1xuICAgICAgICAgIHRoaXMudmFsdWUgPSB2YWx1ZXM7XG4gICAgICAgICAgaGFzVmFsdWVDaGFuZ2VkID0gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKGl0ZW0gPT09IHRoaXMudmFsdWUpIHtcbiAgICAgICAgICB0aGlzLnZhbHVlID0gbnVsbDtcbiAgICAgICAgICBoYXNWYWx1ZUNoYW5nZWQgPSB0cnVlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKGhhc1ZhbHVlQ2hhbmdlZCkge1xuICAgICAgdGhpcy5fZW1pdFZhbHVlQ2hhbmdlKCk7XG4gICAgfVxuXG4gICAgLy8gUmVtb3ZlIGRlbGV0ZWQgaXRlbSBmcm9tIGxpc3QuXG4gICAgY29uc3QgaXRlbXMgPSB0aGlzLml0ZW1zLmZpbHRlcihfaXRlbSA9PiB7XG4gICAgICByZXR1cm4gX2l0ZW0uaWQgIT09IGl0ZW0uaWQ7XG4gICAgfSk7XG5cbiAgICAvLyBSZWZyZXNoIGl0ZW1zIG9uIHBhcmVudCBjb21wb25lbnQuXG4gICAgdGhpcy5pdGVtc0NoYW5nZS5lbWl0KGl0ZW1zKTtcblxuICAgIC8vIFJlZnJlc2ggbGlzdC5cbiAgICB0aGlzLl9zZXRJdGVtcyhpdGVtcyk7XG5cbiAgICB0aGlzLm9uSXRlbXNDaGFuZ2UuZW1pdCh7XG4gICAgICBjb21wb25lbnQ6IHRoaXNcbiAgICB9KTtcblxuICAgIC8vIENsb3NlIGFueSBydW5uaW5nIHN1YnNjcmlwdGlvbi5cbiAgICBpZiAodGhpcy5fZGVsZXRlSXRlbU9ic2VydmFibGUpIHtcbiAgICAgIHRoaXMuX2RlbGV0ZUl0ZW1PYnNlcnZhYmxlLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgIC8vIENvbXBsZXRlIGNhbGxiYWNrIGlzbid0IGZpcmVkIGZvciBzb21lIHJlYXNvbixcbiAgICAgIC8vIHNvIHVuc3Vic2NyaWJlIGluIGJvdGggc3VjY2VzcyBhbmQgZmFpbCBjYXNlcy5cbiAgICAgIHNlbGYuX2RlbGV0ZUl0ZW1PYnNlcnZhYmxlID0gc2VsZi5vbkl0ZW1zQ2hhbmdlLmFzT2JzZXJ2YWJsZSgpLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICAgIHNlbGYuX2RlbGV0ZUl0ZW1PYnNlcnZhYmxlLnVuc3Vic2NyaWJlKCk7XG4gICAgICAgIHJlc29sdmUoKTtcbiAgICAgIH0sICgpID0+IHtcbiAgICAgICAgc2VsZi5fZGVsZXRlSXRlbU9ic2VydmFibGUudW5zdWJzY3JpYmUoKTtcbiAgICAgICAgcmVqZWN0KCk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBEZXRlcm1pbmVzIHdoZXRoZXIgYW55IGl0ZW0gaGFzIGJlZW4gc2VsZWN0ZWQuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI2hhc3ZhbHVlKS5cbiAgICpcbiAgICogQHJldHVybnMgQSBib29sZWFuIGRldGVybWluaW5nIHdoZXRoZXIgYW55IGl0ZW0gaGFzIGJlZW4gc2VsZWN0ZWQuXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIGhhc1ZhbHVlKCk6IGJvb2xlYW4ge1xuICAgIGlmICh0aGlzLmlzTXVsdGlwbGUpIHtcbiAgICAgIHJldHVybiB0aGlzLl92YWx1ZUl0ZW1zLmxlbmd0aCAhPT0gMDtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHRoaXMuX3ZhbHVlSXRlbXMubGVuZ3RoICE9PSAwICYmICF0aGlzLl9pc051bGxPcldoaXRlU3BhY2UodGhpcy5fdmFsdWVJdGVtc1swXSk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIE9wZW5zIE1vZGFsLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNvcGVuKS5cbiAgICpcbiAgICogQHJldHVybnMgUHJvbWlzZSB0aGF0IHJlc29sdmVzIHdoZW4gTW9kYWwgaGFzIGJlZW4gb3BlbmVkLlxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBvcGVuKCk6IFByb21pc2U8dm9pZD4ge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgIGlmICghc2VsZi5faXNFbmFibGVkIHx8IHNlbGYuX2lzT3BlbmVkKSB7XG4gICAgICAgIHJlamVjdCgnSW9uaWNTZWxlY3RhYmxlIGlzIGRpc2FibGVkIG9yIGFscmVhZHkgb3BlbmVkLicpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHNlbGYuX2ZpbHRlckl0ZW1zKCk7XG4gICAgICBzZWxmLl9pc09wZW5lZCA9IHRydWU7XG5cbiAgICAgIGNvbnN0IG1vZGFsT3B0aW9uczogTW9kYWxPcHRpb25zID0ge1xuICAgICAgICBjb21wb25lbnQ6IElvbmljU2VsZWN0YWJsZU1vZGFsQ29tcG9uZW50LFxuICAgICAgICBjb21wb25lbnRQcm9wczogeyBzZWxlY3RDb21wb25lbnQ6IHNlbGYgfSxcbiAgICAgICAgYmFja2Ryb3BEaXNtaXNzOiBzZWxmLl9zaG91bGRCYWNrZHJvcENsb3NlXG4gICAgICB9O1xuXG4gICAgICBpZiAoc2VsZi5tb2RhbENzc0NsYXNzKSB7XG4gICAgICAgIG1vZGFsT3B0aW9ucy5jc3NDbGFzcyA9IHNlbGYubW9kYWxDc3NDbGFzcztcbiAgICAgIH1cblxuICAgICAgaWYgKHNlbGYubW9kYWxFbnRlckFuaW1hdGlvbikge1xuICAgICAgICBtb2RhbE9wdGlvbnMuZW50ZXJBbmltYXRpb24gPSBzZWxmLm1vZGFsRW50ZXJBbmltYXRpb247XG4gICAgICB9XG5cbiAgICAgIGlmIChzZWxmLm1vZGFsTGVhdmVBbmltYXRpb24pIHtcbiAgICAgICAgbW9kYWxPcHRpb25zLmxlYXZlQW5pbWF0aW9uID0gc2VsZi5tb2RhbExlYXZlQW5pbWF0aW9uO1xuICAgICAgfVxuXG4gICAgICBzZWxmLl9tb2RhbENvbnRyb2xsZXIuY3JlYXRlKG1vZGFsT3B0aW9ucykudGhlbihtb2RhbCA9PiB7XG4gICAgICAgIHNlbGYuX21vZGFsID0gbW9kYWw7XG4gICAgICAgIG1vZGFsLnByZXNlbnQoKS50aGVuKCgpID0+IHtcbiAgICAgICAgICAvLyBTZXQgZm9jdXMgYWZ0ZXIgTW9kYWwgaGFzIG9wZW5lZCB0byBhdm9pZCBmbGlja2VyaW5nIG9mIGZvY3VzIGhpZ2hsaWdodGluZ1xuICAgICAgICAgIC8vIGJlZm9yZSBNb2RhbCBvcGVuaW5nLlxuICAgICAgICAgIHNlbGYuX3NldElvbkl0ZW1IYXNGb2N1cyh0cnVlKTtcbiAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIG1vZGFsLm9uV2lsbERpc21pc3MoKS50aGVuKCgpID0+IHtcbiAgICAgICAgICBzZWxmLl9zZXRJb25JdGVtSGFzRm9jdXMoZmFsc2UpO1xuICAgICAgICB9KTtcblxuICAgICAgICBtb2RhbC5vbkRpZERpc21pc3MoKS50aGVuKGV2ZW50ID0+IHtcbiAgICAgICAgICBzZWxmLl9pc09wZW5lZCA9IGZhbHNlO1xuICAgICAgICAgIHNlbGYuX2l0ZW1zVG9Db25maXJtID0gW107XG5cbiAgICAgICAgICAvLyBDbG9zZWQgYnkgY2xpY2tpbmcgb24gYmFja2Ryb3Agb3V0c2lkZSBtb2RhbC5cbiAgICAgICAgICBpZiAoZXZlbnQucm9sZSA9PT0gJ2JhY2tkcm9wJykge1xuICAgICAgICAgICAgc2VsZi5vbkNsb3NlLmVtaXQoe1xuICAgICAgICAgICAgICBjb21wb25lbnQ6IHNlbGZcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBDbG9zZXMgTW9kYWwuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI2Nsb3NlKS5cbiAgICpcbiAgICogQHJldHVybnMgUHJvbWlzZSB0aGF0IHJlc29sdmVzIHdoZW4gTW9kYWwgaGFzIGJlZW4gY2xvc2VkLlxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBjbG9zZSgpOiBQcm9taXNlPHZvaWQ+IHtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcblxuICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICBpZiAoIXNlbGYuX2lzRW5hYmxlZCB8fCAhc2VsZi5faXNPcGVuZWQpIHtcbiAgICAgICAgcmVqZWN0KCdJb25pY1NlbGVjdGFibGUgaXMgZGlzYWJsZWQgb3IgYWxyZWFkeSBjbG9zZWQuJyk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgc2VsZi5wcm9wYWdhdGVPblRvdWNoZWQoKTtcbiAgICAgIHNlbGYuX2lzT3BlbmVkID0gZmFsc2U7XG4gICAgICBzZWxmLl9pdGVtVG9BZGQgPSBudWxsO1xuICAgICAgc2VsZi5fbW9kYWwuZGlzbWlzcygpLnRoZW4oKCkgPT4ge1xuICAgICAgICBzZWxmLl9zZXRJb25JdGVtSGFzRm9jdXMoZmFsc2UpO1xuICAgICAgICBzZWxmLmhpZGVBZGRJdGVtVGVtcGxhdGUoKTtcbiAgICAgICAgcmVzb2x2ZSgpO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogQ2xlYXJzIHZhbHVlLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNjbGVhcikuXG4gICAqXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIGNsZWFyKCkge1xuICAgIHRoaXMudmFsdWUgPSB0aGlzLmlzTXVsdGlwbGUgPyBbXSA6IG51bGw7XG4gICAgdGhpcy5faXRlbXNUb0NvbmZpcm0gPSBbXTtcbiAgICB0aGlzLnByb3BhZ2F0ZU9uQ2hhbmdlKHRoaXMudmFsdWUpO1xuICB9XG5cbiAgLyoqXG4gICAqIENvbmZpcm1zIHNlbGVjdGVkIGl0ZW1zIGJ5IHVwZGF0aW5nIHZhbHVlLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNjb25maXJtKS5cbiAgICpcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgY29uZmlybSgpIHtcbiAgICBpZiAodGhpcy5pc011bHRpcGxlKSB7XG4gICAgICB0aGlzLl9kb1NlbGVjdCh0aGlzLl9zZWxlY3RlZEl0ZW1zKTtcbiAgICB9IGVsc2UgaWYgKHRoaXMuaGFzQ29uZmlybUJ1dHRvbiB8fCB0aGlzLmZvb3RlclRlbXBsYXRlKSB7XG4gICAgICB0aGlzLl9kb1NlbGVjdCh0aGlzLl9zZWxlY3RlZEl0ZW1zWzBdIHx8IG51bGwpO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBTZWxlY3RzIG9yIGRlc2VsZWN0cyBhbGwgb3Igc3BlY2lmaWMgaXRlbXMuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI3RvZ2dsZWl0ZW1zKS5cbiAgICpcbiAgICogQHBhcmFtIGlzU2VsZWN0IERldGVybWluZXMgd2hldGhlciB0byBzZWxlY3Qgb3IgZGVzZWxlY3QgaXRlbXMuXG4gICAqIEBwYXJhbSBbaXRlbXNdIEl0ZW1zIHRvIHRvZ2dsZS4gSWYgaXRlbXMgYXJlIG5vdCBzZXQgYWxsIGl0ZW1zIHdpbGwgYmUgdG9nZ2xlZC5cbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgdG9nZ2xlSXRlbXMoaXNTZWxlY3Q6IGJvb2xlYW4sIGl0ZW1zPzogYW55W10pIHtcbiAgICBpZiAoaXNTZWxlY3QpIHtcbiAgICAgIGNvbnN0IGhhc0l0ZW1zID0gaXRlbXMgJiYgaXRlbXMubGVuZ3RoO1xuICAgICAgbGV0IGl0ZW1zVG9Ub2dnbGUgPSB0aGlzLl9ncm91cHMucmVkdWNlKChhbGxJdGVtcywgZ3JvdXApID0+IHtcbiAgICAgICAgcmV0dXJuIGFsbEl0ZW1zLmNvbmNhdChncm91cC5pdGVtcyk7XG4gICAgICB9LCBbXSk7XG5cbiAgICAgIC8vIERvbid0IGFsbG93IHRvIHNlbGVjdCBhbGwgaXRlbXMgaW4gc2luZ2xlIG1vZGUuXG4gICAgICBpZiAoIXRoaXMuaXNNdWx0aXBsZSAmJiAhaGFzSXRlbXMpIHtcbiAgICAgICAgaXRlbXNUb1RvZ2dsZSA9IFtdO1xuICAgICAgfVxuXG4gICAgICAvLyBUb2dnbGUgc3BlY2lmaWMgaXRlbXMuXG4gICAgICBpZiAoaGFzSXRlbXMpIHtcbiAgICAgICAgaXRlbXNUb1RvZ2dsZSA9IGl0ZW1zVG9Ub2dnbGUuZmlsdGVyKGl0ZW1Ub1RvZ2dsZSA9PiB7XG4gICAgICAgICAgcmV0dXJuIGl0ZW1zLmZpbmQoaXRlbSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fZ2V0SXRlbVZhbHVlKGl0ZW1Ub1RvZ2dsZSkgPT09IHRoaXMuX2dldEl0ZW1WYWx1ZShpdGVtKTtcbiAgICAgICAgICB9KSAhPT0gdW5kZWZpbmVkO1xuICAgICAgICB9KTtcblxuICAgICAgICAvLyBUYWtlIHRoZSBmaXJzdCBpdGVtIGZvciBzaW5nbGUgbW9kZS5cbiAgICAgICAgaWYgKCF0aGlzLmlzTXVsdGlwbGUpIHtcbiAgICAgICAgICBpdGVtc1RvVG9nZ2xlLnNwbGljZSgwLCAxKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpdGVtc1RvVG9nZ2xlLmZvckVhY2goaXRlbSA9PiB7XG4gICAgICAgIHRoaXMuX2FkZFNlbGVjdGVkSXRlbShpdGVtKTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLl9zZWxlY3RlZEl0ZW1zID0gW107XG4gICAgfVxuXG4gICAgdGhpcy5fc2V0SXRlbXNUb0NvbmZpcm0odGhpcy5fc2VsZWN0ZWRJdGVtcyk7XG4gIH1cblxuICAvKipcbiAgICogU2Nyb2xscyB0byB0aGUgdG9wIG9mIE1vZGFsIGNvbnRlbnQuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI3Njcm9sbHRvdG9wKS5cbiAgICpcbiAgICogQHJldHVybnMgUHJvbWlzZSB0aGF0IHJlc29sdmVzIHdoZW4gc2Nyb2xsIGhhcyBiZWVuIGNvbXBsZXRlZC5cbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgc2Nyb2xsVG9Ub3AoKTogUHJvbWlzZTxhbnk+IHtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcblxuICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICBpZiAoIXNlbGYuX2lzT3BlbmVkKSB7XG4gICAgICAgIHJlamVjdCgnSW9uaWNTZWxlY3RhYmxlIGNvbnRlbnQgY2Fubm90IGJlIHNjcm9sbGVkLicpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHNlbGYuX21vZGFsQ29tcG9uZW50Ll9jb250ZW50LnNjcm9sbFRvVG9wKCkudGhlbigoKSA9PiB7XG4gICAgICAgIHJlc29sdmUoKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIFNjcm9sbHMgdG8gdGhlIGJvdHRvbSBvZiBNb2RhbCBjb250ZW50LlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNzY3JvbGx0b2JvdHRvbSkuXG4gICAqXG4gICAqIEByZXR1cm5zIFByb21pc2UgdGhhdCByZXNvbHZlcyB3aGVuIHNjcm9sbCBoYXMgYmVlbiBjb21wbGV0ZWQuXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIHNjcm9sbFRvQm90dG9tKCk6IFByb21pc2U8YW55PiB7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG5cbiAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgaWYgKCFzZWxmLl9pc09wZW5lZCkge1xuICAgICAgICByZWplY3QoJ0lvbmljU2VsZWN0YWJsZSBjb250ZW50IGNhbm5vdCBiZSBzY3JvbGxlZC4nKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBzZWxmLl9tb2RhbENvbXBvbmVudC5fY29udGVudC5zY3JvbGxUb0JvdHRvbSgpLnRoZW4oKCkgPT4ge1xuICAgICAgICByZXNvbHZlKCk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTdGFydHMgc2VhcmNoIHByb2Nlc3MgYnkgc2hvd2luZyBMb2FkaW5nIHNwaW5uZXIuXG4gICAqIFVzZSBpdCB0b2dldGhlciB3aXRoIGBvblNlYXJjaGAgZXZlbnQgdG8gaW5kaWNhdGUgc2VhcmNoIHN0YXJ0LlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNzdGFydHNlYXJjaCkuXG4gICAqXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIHN0YXJ0U2VhcmNoKCkge1xuICAgIGlmICghdGhpcy5faXNFbmFibGVkKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5zaG93TG9hZGluZygpO1xuICB9XG5cbiAgLyoqXG4gICAqIEVuZHMgc2VhcmNoIHByb2Nlc3MgYnkgaGlkaW5nIExvYWRpbmcgc3Bpbm5lciBhbmQgcmVmcmVzaGluZyBpdGVtcy5cbiAgICogVXNlIGl0IHRvZ2V0aGVyIHdpdGggYG9uU2VhcmNoYCBldmVudCB0byBpbmRpY2F0ZSBzZWFyY2ggZW5kLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNlbmRzZWFyY2gpLlxuICAgKlxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBlbmRTZWFyY2goKSB7XG4gICAgaWYgKCF0aGlzLl9pc0VuYWJsZWQpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aGlzLmhpZGVMb2FkaW5nKCk7XG5cbiAgICAvLyBXaGVuIGluc2lkZSBJb25pYyBNb2RhbCBhbmQgb25TZWFyY2ggZXZlbnQgaXMgdXNlZCxcbiAgICAvLyBuZ0RvQ2hlY2soKSBkb2Vzbid0IHdvcmsgYXMgX2l0ZW1zRGlmZmVyIGZhaWxzIHRvIGRldGVjdCBjaGFuZ2VzLlxuICAgIC8vIFNlZSBodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL2lzc3Vlcy80NC5cbiAgICAvLyBSZWZyZXNoIGl0ZW1zIG1hbnVhbGx5LlxuICAgIHRoaXMuX3NldEl0ZW1zKHRoaXMuaXRlbXMpO1xuICAgIHRoaXMuX2VtaXRPblNlYXJjaFN1Y2Nlc3NPckZhaWwodGhpcy5faGFzRmlsdGVyZWRJdGVtcyk7XG4gIH1cblxuICAvKipcbiAgICogRW5hYmxlcyBpbmZpbml0ZSBzY3JvbGwuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI2VuYWJsZWluZmluaXRlc2Nyb2xsKS5cbiAgICpcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgZW5hYmxlSW5maW5pdGVTY3JvbGwoKSB7XG4gICAgaWYgKCF0aGlzLl9oYXNJbmZpbml0ZVNjcm9sbCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMuX21vZGFsQ29tcG9uZW50Ll9pbmZpbml0ZVNjcm9sbC5kaXNhYmxlZCA9IGZhbHNlO1xuICB9XG5cbiAgLyoqXG4gICAqIERpc2FibGVzIGluZmluaXRlIHNjcm9sbC5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jZGlzYWJsZWluZmluaXRlc2Nyb2xsKS5cbiAgICpcbiAgICogQG1lbWJlcm9mIElvbmljU2VsZWN0YWJsZUNvbXBvbmVudFxuICAgKi9cbiAgZGlzYWJsZUluZmluaXRlU2Nyb2xsKCkge1xuICAgIGlmICghdGhpcy5faGFzSW5maW5pdGVTY3JvbGwpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aGlzLl9tb2RhbENvbXBvbmVudC5faW5maW5pdGVTY3JvbGwuZGlzYWJsZWQgPSB0cnVlO1xuICB9XG5cbiAgLyoqXG4gICAqIEVuZHMgaW5maW5pdGUgc2Nyb2xsLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNlbmRpbmZpbml0ZXNjcm9sbCkuXG4gICAqXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIGVuZEluZmluaXRlU2Nyb2xsKCkge1xuICAgIGlmICghdGhpcy5faGFzSW5maW5pdGVTY3JvbGwpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aGlzLl9tb2RhbENvbXBvbmVudC5faW5maW5pdGVTY3JvbGwuY29tcGxldGUoKTtcbiAgICB0aGlzLl9zZXRJdGVtcyh0aGlzLml0ZW1zKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUcmlnZ2VycyBzZWFyY2ggb2YgaXRlbXMuXG4gICAqICoqTm90ZSoqOiBgY2FuU2VhcmNoYCBoYXMgdG8gYmUgZW5hYmxlZC5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jc2VhcmNoKS5cbiAgICpcbiAgICogQHBhcmFtIHRleHQgVGV4dCB0byBzZWFyY2ggaXRlbXMgYnkuXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIHNlYXJjaCh0ZXh0OiBzdHJpbmcpIHtcbiAgICBpZiAoIXRoaXMuX2lzRW5hYmxlZCB8fCAhdGhpcy5faXNPcGVuZWQgfHwgIXRoaXMuY2FuU2VhcmNoKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5fc2VhcmNoVGV4dCA9IHRleHQ7XG4gICAgdGhpcy5fc2V0SGFzU2VhcmNoVGV4dCgpO1xuICAgIHRoaXMuX2ZpbHRlckl0ZW1zKCk7XG4gIH1cblxuICAvKipcbiAgICogU2hvd3MgTG9hZGluZyBzcGlubmVyLlxuICAgKiBTZWUgbW9yZSBvbiBbR2l0SHViXShodHRwczovL2dpdGh1Yi5jb20vZWFrb3JpYWtpbi9pb25pYy1zZWxlY3RhYmxlL3dpa2kvRG9jdW1lbnRhdGlvbiNzaG93bG9hZGluZykuXG4gICAqXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIHNob3dMb2FkaW5nKCkge1xuICAgIGlmICghdGhpcy5faXNFbmFibGVkKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5faXNTZWFyY2hpbmcgPSB0cnVlO1xuICB9XG5cbiAgLyoqXG4gICAqIEhpZGVzIExvYWRpbmcgc3Bpbm5lci5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jaGlkZWxvYWRpbmcpLlxuICAgKlxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBoaWRlTG9hZGluZygpIHtcbiAgICBpZiAoIXRoaXMuX2lzRW5hYmxlZCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMuX2lzU2VhcmNoaW5nID0gZmFsc2U7XG4gIH1cblxuICAvKipcbiAgICogU2hvd3MgYGlvbmljU2VsZWN0YWJsZUFkZEl0ZW1UZW1wbGF0ZWAuXG4gICAqIFNlZSBtb3JlIG9uIFtHaXRIdWJdKGh0dHBzOi8vZ2l0aHViLmNvbS9lYWtvcmlha2luL2lvbmljLXNlbGVjdGFibGUvd2lraS9Eb2N1bWVudGF0aW9uI3Nob3dhZGRpdGVtdGVtcGxhdGUpLlxuICAgKlxuICAgKiBAbWVtYmVyb2YgSW9uaWNTZWxlY3RhYmxlQ29tcG9uZW50XG4gICAqL1xuICBzaG93QWRkSXRlbVRlbXBsYXRlKCkge1xuICAgIHRoaXMuX3RvZ2dsZUFkZEl0ZW1UZW1wbGF0ZSh0cnVlKTtcblxuICAgIC8vIFBvc2l0aW9uIHRoZSB0ZW1wbGF0ZSBvbmx5IHdoZW4gaXQgc2hvdXMgdXAuXG4gICAgdGhpcy5fcG9zaXRpb25BZGRJdGVtVGVtcGxhdGUoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBIaWRlcyBgaW9uaWNTZWxlY3RhYmxlQWRkSXRlbVRlbXBsYXRlYC5cbiAgICogU2VlIG1vcmUgb24gW0dpdEh1Yl0oaHR0cHM6Ly9naXRodWIuY29tL2Vha29yaWFraW4vaW9uaWMtc2VsZWN0YWJsZS93aWtpL0RvY3VtZW50YXRpb24jaGlkZWFkZGl0ZW10ZW1wbGF0ZSkuXG4gICAqXG4gICAqIEBtZW1iZXJvZiBJb25pY1NlbGVjdGFibGVDb21wb25lbnRcbiAgICovXG4gIGhpZGVBZGRJdGVtVGVtcGxhdGUoKSB7XG4gICAgLy8gQ2xlYW4gaXRlbSB0byBhZGQgYXMgaXQncyBubyBsb25nZXIgbmVlZGVkIG9uY2UgQWRkIEl0ZW0gTW9kYWwgaGFzIGJlZW4gY2xvc2VkLlxuICAgIHRoaXMuX2l0ZW1Ub0FkZCA9IG51bGw7XG4gICAgdGhpcy5fdG9nZ2xlQWRkSXRlbVRlbXBsYXRlKGZhbHNlKTtcbiAgfVxufVxuIl19

/***/ }),

/***/ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable.module.js":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable.module.js ***!
  \**************************************************************************************************************/
/*! exports provided: IonicSelectableAddItemTemplateDirective, IonicSelectableCloseButtonTemplateDirective, IonicSelectableFooterTemplateDirective, IonicSelectableGroupEndTemplateDirective, IonicSelectableGroupTemplateDirective, IonicSelectableHeaderTemplateDirective, IonicSelectableItemEndTemplateDirective, IonicSelectableItemIconTemplateDirective, IonicSelectableItemTemplateDirective, IonicSelectableMessageTemplateDirective, IonicSelectableModalComponent, IonicSelectablePlaceholderTemplateDirective, IonicSelectableSearchFailTemplateDirective, IonicSelectableTitleTemplateDirective, IonicSelectableValueTemplateDirective, IonicSelectableIconTemplateDirective, IonicSelectableComponent, IonicSelectableModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableModule", function() { return IonicSelectableModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_selectable_add_item_template_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ionic-selectable-add-item-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-add-item-template.directive.js");
/* harmony import */ var _ionic_selectable_close_button_template_directive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ionic-selectable-close-button-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-close-button-template.directive.js");
/* harmony import */ var _ionic_selectable_footer_template_directive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ionic-selectable-footer-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-footer-template.directive.js");
/* harmony import */ var _ionic_selectable_group_end_template_directive__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ionic-selectable-group-end-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-group-end-template.directive.js");
/* harmony import */ var _ionic_selectable_group_template_directive__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ionic-selectable-group-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-group-template.directive.js");
/* harmony import */ var _ionic_selectable_header_template_directive__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./ionic-selectable-header-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-header-template.directive.js");
/* harmony import */ var _ionic_selectable_item_end_template_directive__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./ionic-selectable-item-end-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-item-end-template.directive.js");
/* harmony import */ var _ionic_selectable_item_icon_template_directive__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./ionic-selectable-item-icon-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-item-icon-template.directive.js");
/* harmony import */ var _ionic_selectable_item_template_directive__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./ionic-selectable-item-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-item-template.directive.js");
/* harmony import */ var _ionic_selectable_message_template_directive__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./ionic-selectable-message-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-message-template.directive.js");
/* harmony import */ var _ionic_selectable_modal_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./ionic-selectable-modal.component */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-modal.component.js");
/* harmony import */ var _ionic_selectable_placeholder_template_directive__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./ionic-selectable-placeholder-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-placeholder-template.directive.js");
/* harmony import */ var _ionic_selectable_search_fail_template_directive__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./ionic-selectable-search-fail-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-search-fail-template.directive.js");
/* harmony import */ var _ionic_selectable_title_template_directive__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./ionic-selectable-title-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-title-template.directive.js");
/* harmony import */ var _ionic_selectable_value_template_directive__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./ionic-selectable-value-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-value-template.directive.js");
/* harmony import */ var _ionic_selectable_icon_template_directive__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./ionic-selectable-icon-template.directive */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable-icon-template.directive.js");
/* harmony import */ var _ionic_selectable_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./ionic-selectable.component */ "./node_modules/ionic-selectable/esm2015/src/app/components/ionic-selectable/ionic-selectable.component.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableAddItemTemplateDirective", function() { return _ionic_selectable_add_item_template_directive__WEBPACK_IMPORTED_MODULE_4__["IonicSelectableAddItemTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableCloseButtonTemplateDirective", function() { return _ionic_selectable_close_button_template_directive__WEBPACK_IMPORTED_MODULE_5__["IonicSelectableCloseButtonTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableFooterTemplateDirective", function() { return _ionic_selectable_footer_template_directive__WEBPACK_IMPORTED_MODULE_6__["IonicSelectableFooterTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableGroupEndTemplateDirective", function() { return _ionic_selectable_group_end_template_directive__WEBPACK_IMPORTED_MODULE_7__["IonicSelectableGroupEndTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableGroupTemplateDirective", function() { return _ionic_selectable_group_template_directive__WEBPACK_IMPORTED_MODULE_8__["IonicSelectableGroupTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableHeaderTemplateDirective", function() { return _ionic_selectable_header_template_directive__WEBPACK_IMPORTED_MODULE_9__["IonicSelectableHeaderTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableItemEndTemplateDirective", function() { return _ionic_selectable_item_end_template_directive__WEBPACK_IMPORTED_MODULE_10__["IonicSelectableItemEndTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableItemIconTemplateDirective", function() { return _ionic_selectable_item_icon_template_directive__WEBPACK_IMPORTED_MODULE_11__["IonicSelectableItemIconTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableItemTemplateDirective", function() { return _ionic_selectable_item_template_directive__WEBPACK_IMPORTED_MODULE_12__["IonicSelectableItemTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableMessageTemplateDirective", function() { return _ionic_selectable_message_template_directive__WEBPACK_IMPORTED_MODULE_13__["IonicSelectableMessageTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableModalComponent", function() { return _ionic_selectable_modal_component__WEBPACK_IMPORTED_MODULE_14__["IonicSelectableModalComponent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectablePlaceholderTemplateDirective", function() { return _ionic_selectable_placeholder_template_directive__WEBPACK_IMPORTED_MODULE_15__["IonicSelectablePlaceholderTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableSearchFailTemplateDirective", function() { return _ionic_selectable_search_fail_template_directive__WEBPACK_IMPORTED_MODULE_16__["IonicSelectableSearchFailTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableTitleTemplateDirective", function() { return _ionic_selectable_title_template_directive__WEBPACK_IMPORTED_MODULE_17__["IonicSelectableTitleTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableValueTemplateDirective", function() { return _ionic_selectable_value_template_directive__WEBPACK_IMPORTED_MODULE_18__["IonicSelectableValueTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableIconTemplateDirective", function() { return _ionic_selectable_icon_template_directive__WEBPACK_IMPORTED_MODULE_19__["IonicSelectableIconTemplateDirective"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IonicSelectableComponent", function() { return _ionic_selectable_component__WEBPACK_IMPORTED_MODULE_20__["IonicSelectableComponent"]; });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */






































/** @type {?} */
const components = [_ionic_selectable_component__WEBPACK_IMPORTED_MODULE_20__["IonicSelectableComponent"], _ionic_selectable_modal_component__WEBPACK_IMPORTED_MODULE_14__["IonicSelectableModalComponent"]];
/** @type {?} */
const directives = [
    _ionic_selectable_value_template_directive__WEBPACK_IMPORTED_MODULE_18__["IonicSelectableValueTemplateDirective"],
    _ionic_selectable_item_template_directive__WEBPACK_IMPORTED_MODULE_12__["IonicSelectableItemTemplateDirective"],
    _ionic_selectable_item_end_template_directive__WEBPACK_IMPORTED_MODULE_10__["IonicSelectableItemEndTemplateDirective"],
    _ionic_selectable_title_template_directive__WEBPACK_IMPORTED_MODULE_17__["IonicSelectableTitleTemplateDirective"],
    _ionic_selectable_placeholder_template_directive__WEBPACK_IMPORTED_MODULE_15__["IonicSelectablePlaceholderTemplateDirective"],
    _ionic_selectable_message_template_directive__WEBPACK_IMPORTED_MODULE_13__["IonicSelectableMessageTemplateDirective"],
    _ionic_selectable_group_template_directive__WEBPACK_IMPORTED_MODULE_8__["IonicSelectableGroupTemplateDirective"],
    _ionic_selectable_group_end_template_directive__WEBPACK_IMPORTED_MODULE_7__["IonicSelectableGroupEndTemplateDirective"],
    _ionic_selectable_close_button_template_directive__WEBPACK_IMPORTED_MODULE_5__["IonicSelectableCloseButtonTemplateDirective"],
    _ionic_selectable_search_fail_template_directive__WEBPACK_IMPORTED_MODULE_16__["IonicSelectableSearchFailTemplateDirective"],
    _ionic_selectable_add_item_template_directive__WEBPACK_IMPORTED_MODULE_4__["IonicSelectableAddItemTemplateDirective"],
    _ionic_selectable_footer_template_directive__WEBPACK_IMPORTED_MODULE_6__["IonicSelectableFooterTemplateDirective"],
    _ionic_selectable_header_template_directive__WEBPACK_IMPORTED_MODULE_9__["IonicSelectableHeaderTemplateDirective"],
    _ionic_selectable_item_icon_template_directive__WEBPACK_IMPORTED_MODULE_11__["IonicSelectableItemIconTemplateDirective"],
    _ionic_selectable_icon_template_directive__WEBPACK_IMPORTED_MODULE_19__["IonicSelectableIconTemplateDirective"]
];
class IonicSelectableModule {
}
IonicSelectableModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"], args: [{
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                    _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"]
                ],
                declarations: [
                    ...components,
                    ...directives
                ],
                exports: [
                    ...components,
                    ...directives
                ],
                entryComponents: components
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9uaWMtc2VsZWN0YWJsZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9pb25pYy1zZWxlY3RhYmxlLyIsInNvdXJjZXMiOlsic3JjL2FwcC9jb21wb25lbnRzL2lvbmljLXNlbGVjdGFibGUvaW9uaWMtc2VsZWN0YWJsZS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLHVDQUF1QyxFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDekcsT0FBTyxFQUFFLDJDQUEyQyxFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFDakgsT0FBTyxFQUFFLHNDQUFzQyxFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDdEcsT0FBTyxFQUFFLHdDQUF3QyxFQUFFLE1BQU0saURBQWlELENBQUM7QUFDM0csT0FBTyxFQUFFLHFDQUFxQyxFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDcEcsT0FBTyxFQUFFLHNDQUFzQyxFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDdEcsT0FBTyxFQUFFLHVDQUF1QyxFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDekcsT0FBTyxFQUFFLHdDQUF3QyxFQUFFLE1BQU0saURBQWlELENBQUM7QUFDM0csT0FBTyxFQUFFLG9DQUFvQyxFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDbEcsT0FBTyxFQUFFLHVDQUF1QyxFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDeEcsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDbkYsT0FBTyxFQUFFLDJDQUEyQyxFQUFFLE1BQU0sbURBQW1ELENBQUM7QUFDaEgsT0FBTyxFQUFFLDBDQUEwQyxFQUFFLE1BQU0sbURBQW1ELENBQUM7QUFDL0csT0FBTyxFQUFFLHFDQUFxQyxFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDcEcsT0FBTyxFQUFFLHFDQUFxQyxFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDcEcsT0FBTyxFQUFFLG9DQUFvQyxFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDbEcsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDeEUsT0FBTyxFQUFFLHVDQUF1QyxFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDekcsT0FBTyxFQUFFLDJDQUEyQyxFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFDakgsT0FBTyxFQUFFLHNDQUFzQyxFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDdEcsT0FBTyxFQUFFLHdDQUF3QyxFQUFFLE1BQU0saURBQWlELENBQUM7QUFDM0csT0FBTyxFQUFFLHFDQUFxQyxFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDcEcsT0FBTyxFQUFFLHNDQUFzQyxFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDdEcsT0FBTyxFQUFFLHVDQUF1QyxFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDekcsT0FBTyxFQUFFLHdDQUF3QyxFQUFFLE1BQU0saURBQWlELENBQUM7QUFDM0csT0FBTyxFQUFFLG9DQUFvQyxFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDbEcsT0FBTyxFQUFFLHVDQUF1QyxFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDeEcsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDbkYsT0FBTyxFQUFFLDJDQUEyQyxFQUFFLE1BQU0sbURBQW1ELENBQUM7QUFDaEgsT0FBTyxFQUFFLDBDQUEwQyxFQUFFLE1BQU0sbURBQW1ELENBQUM7QUFDL0csT0FBTyxFQUFFLHFDQUFxQyxFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDcEcsT0FBTyxFQUFFLHFDQUFxQyxFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDcEcsT0FBTyxFQUFFLG9DQUFvQyxFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDbEcsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sOEJBQThCLENBQUM7O01BRWxFLFVBQVUsR0FBRyxDQUFDLHdCQUF3QixFQUFFLDZCQUE2QixDQUFDOztNQUMxRSxVQUFVLEdBQUc7SUFDWCxxQ0FBcUM7SUFDckMsb0NBQW9DO0lBQ3BDLHVDQUF1QztJQUN2QyxxQ0FBcUM7SUFDckMsMkNBQTJDO0lBQzNDLHVDQUF1QztJQUN2QyxxQ0FBcUM7SUFDckMsd0NBQXdDO0lBQ3hDLDJDQUEyQztJQUMzQywwQ0FBMEM7SUFDMUMsdUNBQXVDO0lBQ3ZDLHNDQUFzQztJQUN0QyxzQ0FBc0M7SUFDdEMsd0NBQXdDO0lBQ3hDLG9DQUFvQztDQUNyQztBQWtCSCxNQUFNLE9BQU8scUJBQXFCOzs7WUFoQmpDLFFBQVEsU0FBQztnQkFDUixPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixXQUFXO29CQUNYLFdBQVc7aUJBQ1o7Z0JBQ0QsWUFBWSxFQUFFO29CQUNaLEdBQUcsVUFBVTtvQkFDYixHQUFHLFVBQVU7aUJBQ2Q7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLEdBQUcsVUFBVTtvQkFDYixHQUFHLFVBQVU7aUJBQ2Q7Z0JBQ0QsZUFBZSxFQUFFLFVBQVU7YUFDNUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgSW9uaWNNb2R1bGUgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5pbXBvcnQgeyBJb25pY1NlbGVjdGFibGVBZGRJdGVtVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtYWRkLWl0ZW0tdGVtcGxhdGUuZGlyZWN0aXZlJztcbmltcG9ydCB7IElvbmljU2VsZWN0YWJsZUNsb3NlQnV0dG9uVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtY2xvc2UtYnV0dG9uLXRlbXBsYXRlLmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBJb25pY1NlbGVjdGFibGVGb290ZXJUZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS1mb290ZXItdGVtcGxhdGUuZGlyZWN0aXZlJztcbmltcG9ydCB7IElvbmljU2VsZWN0YWJsZUdyb3VwRW5kVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtZ3JvdXAtZW5kLXRlbXBsYXRlLmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBJb25pY1NlbGVjdGFibGVHcm91cFRlbXBsYXRlRGlyZWN0aXZlIH0gZnJvbSAnLi9pb25pYy1zZWxlY3RhYmxlLWdyb3VwLXRlbXBsYXRlLmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBJb25pY1NlbGVjdGFibGVIZWFkZXJUZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS1oZWFkZXItdGVtcGxhdGUuZGlyZWN0aXZlJztcbmltcG9ydCB7IElvbmljU2VsZWN0YWJsZUl0ZW1FbmRUZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS1pdGVtLWVuZC10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgSW9uaWNTZWxlY3RhYmxlSXRlbUljb25UZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS1pdGVtLWljb24tdGVtcGxhdGUuZGlyZWN0aXZlJztcbmltcG9ydCB7IElvbmljU2VsZWN0YWJsZUl0ZW1UZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS1pdGVtLXRlbXBsYXRlLmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBJb25pY1NlbGVjdGFibGVNZXNzYWdlVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtbWVzc2FnZS10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgSW9uaWNTZWxlY3RhYmxlTW9kYWxDb21wb25lbnQgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtbW9kYWwuY29tcG9uZW50JztcbmltcG9ydCB7IElvbmljU2VsZWN0YWJsZVBsYWNlaG9sZGVyVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtcGxhY2Vob2xkZXItdGVtcGxhdGUuZGlyZWN0aXZlJztcbmltcG9ydCB7IElvbmljU2VsZWN0YWJsZVNlYXJjaEZhaWxUZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS1zZWFyY2gtZmFpbC10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgSW9uaWNTZWxlY3RhYmxlVGl0bGVUZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS10aXRsZS10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgSW9uaWNTZWxlY3RhYmxlVmFsdWVUZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS12YWx1ZS10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgSW9uaWNTZWxlY3RhYmxlSWNvblRlbXBsYXRlRGlyZWN0aXZlIH0gZnJvbSAnLi9pb25pYy1zZWxlY3RhYmxlLWljb24tdGVtcGxhdGUuZGlyZWN0aXZlJztcbmltcG9ydCB7IElvbmljU2VsZWN0YWJsZUNvbXBvbmVudCB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS5jb21wb25lbnQnO1xuZXhwb3J0IHsgSW9uaWNTZWxlY3RhYmxlQWRkSXRlbVRlbXBsYXRlRGlyZWN0aXZlIH0gZnJvbSAnLi9pb25pYy1zZWxlY3RhYmxlLWFkZC1pdGVtLXRlbXBsYXRlLmRpcmVjdGl2ZSc7XG5leHBvcnQgeyBJb25pY1NlbGVjdGFibGVDbG9zZUJ1dHRvblRlbXBsYXRlRGlyZWN0aXZlIH0gZnJvbSAnLi9pb25pYy1zZWxlY3RhYmxlLWNsb3NlLWJ1dHRvbi10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xuZXhwb3J0IHsgSW9uaWNTZWxlY3RhYmxlRm9vdGVyVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtZm9vdGVyLXRlbXBsYXRlLmRpcmVjdGl2ZSc7XG5leHBvcnQgeyBJb25pY1NlbGVjdGFibGVHcm91cEVuZFRlbXBsYXRlRGlyZWN0aXZlIH0gZnJvbSAnLi9pb25pYy1zZWxlY3RhYmxlLWdyb3VwLWVuZC10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xuZXhwb3J0IHsgSW9uaWNTZWxlY3RhYmxlR3JvdXBUZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS1ncm91cC10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xuZXhwb3J0IHsgSW9uaWNTZWxlY3RhYmxlSGVhZGVyVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtaGVhZGVyLXRlbXBsYXRlLmRpcmVjdGl2ZSc7XG5leHBvcnQgeyBJb25pY1NlbGVjdGFibGVJdGVtRW5kVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtaXRlbS1lbmQtdGVtcGxhdGUuZGlyZWN0aXZlJztcbmV4cG9ydCB7IElvbmljU2VsZWN0YWJsZUl0ZW1JY29uVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtaXRlbS1pY29uLXRlbXBsYXRlLmRpcmVjdGl2ZSc7XG5leHBvcnQgeyBJb25pY1NlbGVjdGFibGVJdGVtVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtaXRlbS10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xuZXhwb3J0IHsgSW9uaWNTZWxlY3RhYmxlTWVzc2FnZVRlbXBsYXRlRGlyZWN0aXZlIH0gZnJvbSAnLi9pb25pYy1zZWxlY3RhYmxlLW1lc3NhZ2UtdGVtcGxhdGUuZGlyZWN0aXZlJztcbmV4cG9ydCB7IElvbmljU2VsZWN0YWJsZU1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi9pb25pYy1zZWxlY3RhYmxlLW1vZGFsLmNvbXBvbmVudCc7XG5leHBvcnQgeyBJb25pY1NlbGVjdGFibGVQbGFjZWhvbGRlclRlbXBsYXRlRGlyZWN0aXZlIH0gZnJvbSAnLi9pb25pYy1zZWxlY3RhYmxlLXBsYWNlaG9sZGVyLXRlbXBsYXRlLmRpcmVjdGl2ZSc7XG5leHBvcnQgeyBJb25pY1NlbGVjdGFibGVTZWFyY2hGYWlsVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtc2VhcmNoLWZhaWwtdGVtcGxhdGUuZGlyZWN0aXZlJztcbmV4cG9ydCB7IElvbmljU2VsZWN0YWJsZVRpdGxlVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtdGl0bGUtdGVtcGxhdGUuZGlyZWN0aXZlJztcbmV4cG9ydCB7IElvbmljU2VsZWN0YWJsZVZhbHVlVGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUtdmFsdWUtdGVtcGxhdGUuZGlyZWN0aXZlJztcbmV4cG9ydCB7IElvbmljU2VsZWN0YWJsZUljb25UZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vaW9uaWMtc2VsZWN0YWJsZS1pY29uLXRlbXBsYXRlLmRpcmVjdGl2ZSc7XG5leHBvcnQgeyBJb25pY1NlbGVjdGFibGVDb21wb25lbnQgfSBmcm9tICcuL2lvbmljLXNlbGVjdGFibGUuY29tcG9uZW50JztcblxuY29uc3QgY29tcG9uZW50cyA9IFtJb25pY1NlbGVjdGFibGVDb21wb25lbnQsIElvbmljU2VsZWN0YWJsZU1vZGFsQ29tcG9uZW50XSxcbiAgZGlyZWN0aXZlcyA9IFtcbiAgICBJb25pY1NlbGVjdGFibGVWYWx1ZVRlbXBsYXRlRGlyZWN0aXZlLFxuICAgIElvbmljU2VsZWN0YWJsZUl0ZW1UZW1wbGF0ZURpcmVjdGl2ZSxcbiAgICBJb25pY1NlbGVjdGFibGVJdGVtRW5kVGVtcGxhdGVEaXJlY3RpdmUsXG4gICAgSW9uaWNTZWxlY3RhYmxlVGl0bGVUZW1wbGF0ZURpcmVjdGl2ZSxcbiAgICBJb25pY1NlbGVjdGFibGVQbGFjZWhvbGRlclRlbXBsYXRlRGlyZWN0aXZlLFxuICAgIElvbmljU2VsZWN0YWJsZU1lc3NhZ2VUZW1wbGF0ZURpcmVjdGl2ZSxcbiAgICBJb25pY1NlbGVjdGFibGVHcm91cFRlbXBsYXRlRGlyZWN0aXZlLFxuICAgIElvbmljU2VsZWN0YWJsZUdyb3VwRW5kVGVtcGxhdGVEaXJlY3RpdmUsXG4gICAgSW9uaWNTZWxlY3RhYmxlQ2xvc2VCdXR0b25UZW1wbGF0ZURpcmVjdGl2ZSxcbiAgICBJb25pY1NlbGVjdGFibGVTZWFyY2hGYWlsVGVtcGxhdGVEaXJlY3RpdmUsXG4gICAgSW9uaWNTZWxlY3RhYmxlQWRkSXRlbVRlbXBsYXRlRGlyZWN0aXZlLFxuICAgIElvbmljU2VsZWN0YWJsZUZvb3RlclRlbXBsYXRlRGlyZWN0aXZlLFxuICAgIElvbmljU2VsZWN0YWJsZUhlYWRlclRlbXBsYXRlRGlyZWN0aXZlLFxuICAgIElvbmljU2VsZWN0YWJsZUl0ZW1JY29uVGVtcGxhdGVEaXJlY3RpdmUsXG4gICAgSW9uaWNTZWxlY3RhYmxlSWNvblRlbXBsYXRlRGlyZWN0aXZlXG4gIF07XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gICAgRm9ybXNNb2R1bGUsXG4gICAgSW9uaWNNb2R1bGVcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgLi4uY29tcG9uZW50cyxcbiAgICAuLi5kaXJlY3RpdmVzXG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICAuLi5jb21wb25lbnRzLFxuICAgIC4uLmRpcmVjdGl2ZXNcbiAgXSxcbiAgZW50cnlDb21wb25lbnRzOiBjb21wb25lbnRzXG59KVxuZXhwb3J0IGNsYXNzIElvbmljU2VsZWN0YWJsZU1vZHVsZSB7IH1cbiJdfQ==

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/history/history.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/history/history.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>History</ion-title>\n    <ion-buttons *ngIf=\"dataArrayCoords.length > 1\" slot=\"primary\">\n      <ion-button slot=\"icon-only\" fill=\"clear\">\n        <ion-icon style=\"font-size: 2em;\" name=\"arrow-dropleft\"></ion-icon>\n      </ion-button>\n      <ion-button *ngIf=\"!playPause\" slot=\"icon-only\" fill=\"clear\" color=\"secondary\" (click)=\"animateHistory()\">\n        <ion-icon style=\"font-size: 2em;\" name=\"play-circle\"></ion-icon>\n      </ion-button>\n      <ion-button *ngIf=\"playPause\" slot=\"icon-only\" fill=\"clear\" color=\"secondary\" (click)=\"animateHistory()\">\n        <ion-icon style=\"font-size: 2em;\" name=\"pause\"></ion-icon>\n      </ion-button>\n      <ion-button slot=\"icon-only\" fill=\"clear\">\n        <ion-icon style=\"font-size: 2em;\" name=\"arrow-dropright\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n  <ion-item>\n    <ion-label>Select Person</ion-label>\n    <ionic-selectable [(ngModel)]=\"selectedVehicle\" [items]=\"portstemp\" itemValueField=\"Device_Name\"\n      itemTextField=\"Device_Name\" [canSearch]=\"true\" (onChange)=\"onChangedSelect(selectedVehicle)\">\n    </ionic-selectable>\n  </ion-item>\n  <ion-row>\n    <ion-col size=\"5\" class=\"ion-padding-left\" class=\"col1\">\n      <ion-row>\n        <ion-col size=\"3\" class=\"ion-no-padding\">\n          <ion-icon style=\"font-size: 1.9em;\" name=\"clock\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"9\" class=\"ion-no-padding\">\n          <ion-row>\n            <ion-col class=\"ion-no-padding\" size=\"12\">\n              <ion-label style=\"margin-top: 1px;\"><span style=\"font-size: 11px\">From Date</span></ion-label>\n            </ion-col>\n            <ion-col class=\"ion-no-padding\" size=\"12\">\n              <ion-datetime displayFormat=\"DD/MM/YYYY hh:mm a\" pickerFormat=\"DD/MM/YY hh:mm a\"\n                [(ngModel)]=\"datetimeStart\" style=\"font-size: 10px; padding: 0px;\" (ionChange)=\"onChangeDatetime()\">\n              </ion-datetime>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n    </ion-col>\n    <ion-col size=\"5\" class=\"col1\">\n      <ion-row>\n        <ion-col size=\"3\" class=\"ion-no-padding\">\n          <ion-icon style=\"font-size: 1.9em;\" name=\"clock\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"9\" class=\"ion-no-padding\">\n          <ion-row>\n            <ion-col class=\"ion-no-padding\" size=\"12\">\n              <ion-label style=\"margin-top: 1px;\"><span style=\"font-size: 11px\">From Date</span></ion-label>\n            </ion-col>\n            <ion-col class=\"ion-no-padding\" size=\"12\">\n              <ion-datetime displayFormat=\"DD/MM/YYYY hh:mm a\" pickerFormat=\"DD/MM/YY hh:mm a\" [(ngModel)]=\"datetimeEnd\"\n                style=\"font-size: 10px; padding: 0px;\" (ionChange)=\"onChangeDatetime()\"></ion-datetime>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n      <!-- <ion-label style=\"margin-top: 1px;\">\n        <span style=\"font-size: 11px\">To Date</span>\n        <ion-datetime displayFormat=\"DD/MM/YYYY hh:mm a\" pickerFormat=\"DD/MM/YY hh:mm a\" [(ngModel)]=\"datetimeEnd\"\n          style=\"font-size: 10px;\"></ion-datetime>\n      </ion-label> -->\n    </ion-col>\n    <ion-col size=\"2\" ion-text text-right padding-right>\n      <ion-icon ios=\"ios-search\" md=\"md-search\" style=\"font-size:30px;\">\n      </ion-icon>\n    </ion-col>\n  </ion-row>\n</ion-header>\n\n<!-- <ion-content fullscreen no-bounce>\n  <div class=\"map\" #map></div>\n</ion-content> -->\n<ion-content fullscreen no-bounce>\n  <!-- <div\n    style=\"height: 100%; width: 100%;background-color: transparent; opacity: 0; transition: opacity 150ms ease-in; margin-top: -100px; position: absolute;\"\n    id=\"map_canvas\">\n    \n  </div> -->\n  <div id=\"map_canvas\"></div>\n  <!-- <div class=\"map\" #map *ngIf=\"mapData.length == 0\"></div> -->\n</ion-content>\n<ion-bottom-drawer *ngIf=\"selectedVehicle\" [(state)]=\"drawerState\" [minimumHeight]=\"minimumHeight\"\n  [dockedHeight]=\"dockedHeight\" [shouldBounce]=\"shouldBounce\" [distanceTop]=\"distanceTop\" (click)=\"setDocHeight()\">\n  <div class=\"drawer-content\">\n    <ion-row padding>\n      <ion-col size=\"3\">\n        <ion-img src=\"assets/Images/dummy-profile-pic.png\" style=\"border-radius: 12px;\"></ion-img>\n      </ion-col>\n      <ion-col size=\"9\">\n        <ion-row>\n          <ion-col size=\"12\">\n            <h4 style=\"margin: 0px;\">{{selectedVehicle.Device_Name | titlecase}}</h4>\n          </ion-col>\n          <ion-col size=\"12\" style=\"padding: 0px 0px 0px 5px\">\n            <p style=\"padding: 0px; margin: 0px; font-size: 12px;\">Near 216 1st cross road(Home)</p>\n          </ion-col>\n          <ion-col size=\"12\" style=\"padding: 5px 0px 0px 5px\">\n            <p style=\"color: gray; padding: 0px; margin: 0px; font-size: 12px;\">Since 8:38pm</p>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-padding\" *ngIf=\"geofenceData.length === 0\">\n      <ion-col size=\"12\">\n        Geofence not found...!!\n      </ion-col>\n    </ion-row>\n    <!-- <ion-list class=\"ion-padding\" *ngIf=\"geofenceData.length === 0\">\n      <ion-item>\n        Geofence not found...!!\n      </ion-item>\n    </ion-list> -->\n    <!-- <ion-list class=\"ion-padding\" *ngIf=\"geofenceData.length > 0\">\n      <ion-item *ngFor=\"let item of geofenceData\"> -->\n    <ion-row style=\"padding: 0px 16px;\" *ngIf=\"geofenceData.length > 0\">\n      <ion-col size=\"12\">\n        <ion-row *ngFor=\"let item of geofenceData\">\n          <ion-col size=\"12\">\n            <ion-row style=\"border-bottom: 1px solid #f0f5f5;\">\n              <ion-col size=\"2\" style=\"margin: auto;\">\n                <ion-icon style=\"font-size: 2em; color: #329cd1;\" name=\"pin\"></ion-icon>\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-row>\n                  <ion-col size=\"12\" ion-no-padding>\n                    <p style=\"font-size: 18px; margin: 0px;\">{{item.poiname | titlecase}}</p>\n                  </ion-col>\n                  <ion-col size=\"12\" ion-no-padding>\n                    <!-- <p class=\"para\">{{item.direction}} at {{item.timestamp | date:'shortTime'}}</p> -->\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n\n    </ion-row>\n    <!--</ion-item>\n       <ion-item>\n        <ion-row>\n          <ion-col size=\"3\" style=\"margin: auto;\">\n            <ion-icon style=\"font-size: 2em; color: #329cd1;\" name=\"pin\"></ion-icon>\n          </ion-col>\n          <ion-col size=\"9\">\n            <ion-row>\n              <ion-col size=\"12\" ion-no-padding>\n                <h4 style=\"margin: 0px;\">Tusion class</h4>\n              </ion-col>\n              <ion-col size=\"12\" ion-no-padding>\n                <p class=\"para\">6:00 pm</p>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n      <ion-item>\n        <ion-row>\n          <ion-col size=\"3\" style=\"margin: auto;\">\n            <ion-icon style=\"font-size: 2em; color: #329cd1;\" name=\"pin\"></ion-icon>\n          </ion-col>\n          <ion-col size=\"9\">\n            <ion-row>\n              <ion-col size=\"12\" ion-no-padding>\n                <h4 style=\"margin: 0px;\">Book store</h4>\n              </ion-col>\n              <ion-col size=\"12\" ion-no-padding>\n                <p class=\"para\">6:45 pm</p>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n      <ion-item>\n        <ion-row>\n          <ion-col size=\"3\" style=\"margin: auto;\">\n            <ion-icon style=\"font-size: 2em; color: #329cd1;\" name=\"pin\"></ion-icon>\n          </ion-col>\n          <ion-col size=\"9\">\n            <ion-row>\n              <ion-col size=\"12\" ion-no-padding>\n                <h4 style=\"margin: 0px;\">Coaching class</h4>\n              </ion-col>\n              <ion-col size=\"12\" ion-no-padding>\n                <p class=\"para\">7:30 pm</p>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n      <ion-item>\n        <ion-row>\n          <ion-col size=\"3\" style=\"margin: auto;\">\n            <ion-icon style=\"font-size: 2em; color: #329cd1;\" name=\"pin\"></ion-icon>\n          </ion-col>\n          <ion-col size=\"9\">\n            <ion-row>\n              <ion-col size=\"12\" ion-no-padding>\n                <h4 style=\"margin: 0px;\">Home</h4>\n              </ion-col>\n              <ion-col size=\"12\" ion-no-padding>\n                <p class=\"para\">8:00 pm</p>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-item> \n    </ion-list>-->\n  </div>\n</ion-bottom-drawer>"

/***/ }),

/***/ "./src/app/history/history.module.ts":
/*!*******************************************!*\
  !*** ./src/app/history/history.module.ts ***!
  \*******************************************/
/*! exports provided: HistoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPageModule", function() { return HistoryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var ion_bottom_drawer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ion-bottom-drawer */ "./node_modules/ion-bottom-drawer/fesm2015/ion-bottom-drawer.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ionic-selectable */ "./node_modules/ionic-selectable/esm2015/ionic-selectable.min.js");
/* harmony import */ var _history_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./history.page */ "./src/app/history/history.page.ts");









const routes = [
    {
        path: '',
        component: _history_page__WEBPACK_IMPORTED_MODULE_8__["HistoryPage"]
    }
];
let HistoryPageModule = class HistoryPageModule {
};
HistoryPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            ion_bottom_drawer__WEBPACK_IMPORTED_MODULE_5__["IonBottomDrawerModule"],
            ionic_selectable__WEBPACK_IMPORTED_MODULE_7__["IonicSelectableModule"]
        ],
        declarations: [_history_page__WEBPACK_IMPORTED_MODULE_8__["HistoryPage"]]
    })
], HistoryPageModule);



/***/ }),

/***/ "./src/app/history/history.page.scss":
/*!*******************************************!*\
  !*** ./src/app/history/history.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-toolbar {\n  --color: #ffffff;\n  --background: linear-gradient(to right, #329cd1, #008dd3, #007cd2, #1c6ace, #4055c5);\n}\n\n.map {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  background-color: transparent;\n  opacity: 0;\n  -webkit-transition: opacity 150ms ease-in;\n  transition: opacity 150ms ease-in;\n}\n\n.map.visible {\n  opacity: 1;\n}\n\n.para {\n  padding: 0px 0px 11px;\n  margin: 0px;\n  color: #aaa8a9;\n}\n\n#map_canvas {\n  height: 100%;\n  width: 100%;\n}\n\n.datetime-md,\n.datetime-ios {\n  padding: 0px;\n}\n\n.text-input-md,\n.text-input-ios {\n  margin: -1px;\n  padding: 0;\n  width: auto;\n}\n\n.avtar img {\n  width: 20px;\n  height: 20px;\n  margin-right: 2.5%;\n  margin-top: 4%;\n}\n\n.col1 {\n  min-width: 150px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvdHJhY2tpbmctZ2F1cmRpYW4vc3JjL2FwcC9oaXN0b3J5L2hpc3RvcnkucGFnZS5zY3NzIiwic3JjL2FwcC9oaXN0b3J5L2hpc3RvcnkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQUE7RUFDQSxvRkFBQTtBQ0NGOztBREVBO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLDZCQUFBO0VBQ0EsVUFBQTtFQUNBLHlDQUFBO0VBQUEsaUNBQUE7QUNDRjs7QURHQTtFQUNFLFVBQUE7QUNBRjs7QURFQTtFQUNFLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7QUNDRjs7QURFQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0FDQ0Y7O0FERUE7O0VBR0UsWUFBQTtBQ0FGOztBRElBOztFQUVFLFlBQUE7RUFDQSxVQUFBO0VBRUEsV0FBQTtBQ0ZGOztBREtBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUNGRjs7QURJQTtFQUNFLGdCQUFBO0FDREYiLCJmaWxlIjoic3JjL2FwcC9oaXN0b3J5L2hpc3RvcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXIge1xuICAtLWNvbG9yOiAjZmZmZmZmO1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzMyOWNkMSwgIzAwOGRkMywgIzAwN2NkMiwgIzFjNmFjZSwgIzQwNTVjNSk7XG59XG5cbi5tYXAge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBvcGFjaXR5OiAwO1xuICB0cmFuc2l0aW9uOiBvcGFjaXR5IDE1MG1zIGVhc2UtaW47XG4vLyAgIG1hcmdpbi10b3A6IC0xMDBweDtcbn1cblxuLm1hcC52aXNpYmxlIHtcbiAgb3BhY2l0eTogMTtcbn1cbi5wYXJhIHtcbiAgcGFkZGluZzogMHB4IDBweCAxMXB4O1xuICBtYXJnaW46IDBweDtcbiAgY29sb3I6ICNhYWE4YTk7XG59XG5cbiNtYXBfY2FudmFzIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmRhdGV0aW1lLW1kLCBcbi5kYXRldGltZS1pb3Mge1xuICAvLyBwYWRkaW5nOiAxM3B4IDhweCAwcHggMHB4O1xuICBwYWRkaW5nOiAwcHg7XG4gIC8vIG1hcmdpbjogLTglO1xufVxuXG4udGV4dC1pbnB1dC1tZCAsXG4udGV4dC1pbnB1dC1pb3N7XG4gIG1hcmdpbjogLTFweDtcbiAgcGFkZGluZzogMDtcbiAgLy8gd2lkdGg6IGNhbGMoMTAwJSAtIDhweCAtIDhweCk7XG4gIHdpZHRoOiBhdXRvO1xufVxuXG4uYXZ0YXIgaW1ne1xuICB3aWR0aDogMjBweDtcbiAgaGVpZ2h0OiAyMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDIuNSU7XG4gIG1hcmdpbi10b3A6IDQlO1xufVxuLmNvbDF7XG4gIG1pbi13aWR0aDogMTUwcHg7XG59XG4iLCJpb24tdG9vbGJhciB7XG4gIC0tY29sb3I6ICNmZmZmZmY7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMzI5Y2QxLCAjMDA4ZGQzLCAjMDA3Y2QyLCAjMWM2YWNlLCAjNDA1NWM1KTtcbn1cblxuLm1hcCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIG9wYWNpdHk6IDA7XG4gIHRyYW5zaXRpb246IG9wYWNpdHkgMTUwbXMgZWFzZS1pbjtcbn1cblxuLm1hcC52aXNpYmxlIHtcbiAgb3BhY2l0eTogMTtcbn1cblxuLnBhcmEge1xuICBwYWRkaW5nOiAwcHggMHB4IDExcHg7XG4gIG1hcmdpbjogMHB4O1xuICBjb2xvcjogI2FhYThhOTtcbn1cblxuI21hcF9jYW52YXMge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uZGF0ZXRpbWUtbWQsXG4uZGF0ZXRpbWUtaW9zIHtcbiAgcGFkZGluZzogMHB4O1xufVxuXG4udGV4dC1pbnB1dC1tZCxcbi50ZXh0LWlucHV0LWlvcyB7XG4gIG1hcmdpbjogLTFweDtcbiAgcGFkZGluZzogMDtcbiAgd2lkdGg6IGF1dG87XG59XG5cbi5hdnRhciBpbWcge1xuICB3aWR0aDogMjBweDtcbiAgaGVpZ2h0OiAyMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDIuNSU7XG4gIG1hcmdpbi10b3A6IDQlO1xufVxuXG4uY29sMSB7XG4gIG1pbi13aWR0aDogMTUwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/history/history.page.ts":
/*!*****************************************!*\
  !*** ./src/app/history/history.page.ts ***!
  \*****************************************/
/*! exports provided: HistoryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryPage", function() { return HistoryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var ion_bottom_drawer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ion-bottom-drawer */ "./node_modules/ion-bottom-drawer/fesm2015/ion-bottom-drawer.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);







let HistoryPage = class HistoryPage {
    constructor(
    // private renderer: Renderer2,
    apiCall, loadingController, alertController, toastCtrl, plt) {
        this.apiCall = apiCall;
        this.loadingController = loadingController;
        this.alertController = alertController;
        this.toastCtrl = toastCtrl;
        this.plt = plt;
        this.shouldBounce = true;
        // dockedHeight = 250;
        this.dockedHeight = 100;
        this.distanceTop = 378;
        this.drawerState = ion_bottom_drawer__WEBPACK_IMPORTED_MODULE_2__["DrawerState"].Docked;
        this.states = ion_bottom_drawer__WEBPACK_IMPORTED_MODULE_2__["DrawerState"];
        this.minimumHeight = 100;
        this.dataArrayCoords = [];
        this.mapData = [];
        this.portstemp = [];
        this.allData = {};
        this.geofenceData = [];
        this.geoShape = [];
        this.geodata = [];
        this.target = 0;
        this.speed = 0;
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = moment__WEBPACK_IMPORTED_MODULE_6__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = moment__WEBPACK_IMPORTED_MODULE_6__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }
    ngOnInit() {
        localStorage.removeItem("markerTarget");
        this.getdevices();
        this.plt.ready().then(() => {
            this.loadMap();
        });
    }
    setDocHeight() {
        let that = this;
        that.distanceTop = 150;
        that.drawerState = ion_bottom_drawer__WEBPACK_IMPORTED_MODULE_2__["DrawerState"].Top;
    }
    ngAfterViewInit() {
        // this.getGoogleMap().then(googleMaps => {
        //   const mapEl = this.mapElementRef.nativeElement;
        //   const map = new googleMaps.Map(mapEl, {
        //     center: { lat: 18.5204, lng: 73.8567 },
        //     zoom: 10
        //   });
        //   googleMaps.event.addListenerOnce(map, 'idle', () => {
        //     this.renderer.addClass(mapEl, 'visible');
        //   });
        // }).catch(err => {
        //   console.log(err);
        // });
    }
    loadMap() {
        _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["LocationService"].getMyLocation().then((myLocation) => {
            let options = {
                camera: {
                    target: myLocation.latLng
                },
                preferences: {
                    zoom: {
                        minZoom: 5,
                        maxZoom: 50
                    },
                    padding: {
                        left: 10,
                        top: 10,
                        bottom: 10,
                        right: 10
                    },
                    building: true
                }
            };
            this.map = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["GoogleMaps"].create('map_canvas', options);
        });
    }
    // private getGoogleMap(): Promise<any> {
    //   const win = window as any;
    //   const googleModule = win.google;
    //   if (googleModule && googleModule.maps) {
    //     return Promise.resolve(googleModule.maps);
    //   }
    //   return new Promise((resolve, reject) => {
    //     const script = document.createElement('script');
    //     script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8';
    //     script.async = true;
    //     script.defer = true;
    //     document.body.appendChild(script);
    //     script.onload = () => {
    //       const loadedGoogleMpdule = win.google;
    //       if (loadedGoogleMpdule && loadedGoogleMpdule.maps) {
    //         resolve(loadedGoogleMpdule.maps);
    //       } else {
    //         reject('Google maps SDK not available.');
    //       }
    //     };
    //   });
    // }
    getdevices() {
        var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.loadingController.create({
            message: 'Loading vehicle list...'
        }).then((loadEl => {
            loadEl.present();
            this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
                .subscribe(data => {
                loadEl.dismiss();
                this.portstemp = JSON.parse(JSON.stringify(data)).devices;
            }, error => {
                loadEl.dismiss();
                console.log(error);
            });
        }));
    }
    checkIfMapExist() {
        if (this.map != undefined) {
            this.map.remove();
            this.loadMap();
        }
    }
    onChangedSelect(item) {
        debugger;
        let that = this;
        console.log("item: ", item);
        this.checkIfMapExist();
        this.vehObj = item;
        console.log("selected vehicl: ", that.selectedVehicle);
        that.getHistoryOfSelectedVehicle();
    }
    onChangeDatetime() {
        debugger;
        this.checkIfMapExist();
        if (this.vehObj.Device_ID !== undefined) {
            this.getHistoryOfSelectedVehicle();
        }
    }
    // btnClicked(timeStart, timeEnd) {
    //   this.geoFence();
    //   if (localStorage.getItem("MainHistory") != null) {
    //     if (this.selectedVehicle == undefined) {
    //       this.alertController.create({
    //         message: "Please select the vehicle first!!",
    //         buttons: ['OK']
    //       }).then((alertEl => {
    //         alertEl.present();
    //       }))
    //     } else {
    //       this.maphistory(timeStart, timeEnd);
    //     }
    //   } else {
    //     this.maphistory(timeStart, timeEnd);
    //   }
    // }
    getHistoryOfSelectedVehicle() {
        if (new Date(this.datetimeEnd).toISOString() <= new Date(this.datetimeStart).toISOString()) {
            this.toastCtrl.create({
                message: 'Please select valid time and try again..(to time always greater than from Time).',
                duration: 3000,
                position: 'bottom'
            }).then((toastEl) => {
                toastEl.present();
            });
            return;
        }
        // this.callGeofence();
        this.geoFence();
        this.loadingController.create({
            message: 'Loading history data...',
        }).then((loadEl) => {
            loadEl.present();
            this.getHistoryData(loadEl);
        });
    }
    getDistanceAndSpeed(loadEl) {
        var url = this.apiCall.mainUrl + "gps/getDistanceSpeed?imei=" + this.vehObj.Device_ID + "&from=" + new Date(this.datetimeStart).toISOString() + "&to=" + new Date(this.datetimeEnd).toISOString();
        this.apiCall.getdevicesForAllVehiclesApi(url)
            .subscribe(respData => {
            loadEl.dismiss();
            if (respData != {}) {
                var res = JSON.parse(JSON.stringify(respData));
                console.log("check history: " + res.Distance);
            }
        }, err => {
            console.log(err);
            loadEl.dismiss();
        });
    }
    getHistoryData(loadEl) {
        var url1 = this.apiCall.mainUrl + "gps/v2?id=" + this.vehObj.Device_ID + "&from=" + new Date(this.datetimeStart).toISOString() + "&to=" + new Date(this.datetimeEnd).toISOString();
        this.apiCall.getdevicesForAllVehiclesApi(url1)
            .subscribe(respData => {
            loadEl.dismiss();
            if (respData != undefined) {
                var res = JSON.parse(JSON.stringify(respData));
                console.log("res gps data: ", res);
                if (res.length > 1) {
                    this.plotHstory(res.reverse());
                }
                else {
                    this.alertController.create({
                        message: 'Oops.. history data not found for selected dates.',
                        buttons: ['Okay']
                    }).then((alertEl) => {
                        alertEl.present();
                    });
                }
            }
        }, err => {
            loadEl.dismiss();
            console.log(err);
        });
    }
    plotHstory(data) {
        let that = this;
        that.dataArrayCoords = [];
        for (var i = 0; i < data.length; i++) {
            if (data[i].lat && data[i].lng) {
                var arr = [];
                var cumulativeDistance = 0;
                var startdatetime = new Date(data[i].insertionTime);
                arr.push(data[i].lat);
                arr.push(data[i].lng);
                arr.push({ "time": startdatetime.toLocaleString() });
                arr.push({ "speed": data[i].speed });
                arr.push({ "imei": data[i].imei });
                if (data[i].isPastData != true) {
                    if (i === 0) {
                        cumulativeDistance += 0;
                    }
                    else {
                        cumulativeDistance += data[i].distanceFromPrevious ? parseFloat(data[i].distanceFromPrevious) : 0;
                    }
                    data[i]['cummulative_distance'] = (cumulativeDistance).toFixed(2);
                    arr.push({ "cumu_dist": data[i]['cummulative_distance'] });
                }
                else {
                    data[i]['cummulative_distance'] = (cumulativeDistance).toFixed(2);
                    arr.push({ "cumu_dist": data[i]['cummulative_distance'] });
                }
                that.dataArrayCoords.push(arr);
            }
        }
        that.mapData = [];
        that.mapData = data.map(function (d) {
            return { lat: d.lat, lng: d.lng };
        });
        that.mapData.reverse();
        this.drawOnMap();
    }
    drawOnMap() {
        let that = this;
        let bounds = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["LatLngBounds"](that.mapData);
        that.map.moveCamera({
            target: bounds
        });
        that.map.addMarker({
            title: 'D',
            position: that.mapData[0],
            icon: 'red',
            styles: {
                'text-align': 'center',
                'font-style': 'italic',
                'font-weight': 'bold',
                'color': 'red'
            },
        }).then((marker) => {
            marker.showInfoWindow();
            that.map.addMarker({
                title: 'S',
                position: that.mapData[that.mapData.length - 1],
                icon: 'green',
                styles: {
                    'text-align': 'center',
                    'font-style': 'italic',
                    'font-weight': 'bold',
                    'color': 'green'
                },
            }).then((marker) => {
                marker.showInfoWindow();
            });
        });
        that.map.addPolyline({
            points: that.mapData,
            color: '#635400',
            width: 3,
            geodesic: true
        });
    }
    geoFence() {
        var url = this.apiCall.mainUrl + "poi/getPois?user=" + this.islogin._id;
        this.geoShape = [];
        this.geofenceData = [];
        this.apiCall.getdevicesForAllVehiclesApi(url)
            .subscribe(respData => {
            var data = JSON.parse(JSON.stringify(respData));
            console.log("geofence data=> " + data.length);
            if (data.length > 0) {
                this.innerFunc(data);
            }
        }, err => {
            console.log(err);
        });
    }
    innerFunc(data) {
        var i = 0, howManyTimes = data.length;
        let that = this;
        function f() {
            that.geofenceData.push({
                "poiname": data[i].poi.poiname,
                "radius": data[i].radius ? data[i].radius : 'N/A',
                "_id": data[i]._id,
                "start_location": {
                    'lat': data[i].poi.location.coordinates[1],
                    'long': data[i].poi.location.coordinates[0]
                },
                "address": data[i].poi.address ? data[i].poi.address : 'N/A'
            });
            // let ltln: ILatLng = { "lat": that.geoShape[i].start_location.lat, "lng": that.geoShape[i].start_location.long };
            // let circle: Circle = that.map.addCircleSync({
            //   'center': ltln,
            //   'radius': that.geoShape[i].radius,
            //   'strokeColor': '#4dc4ec',
            //   'strokeWidth': 2,
            //   'fillColor': 'rgba(77, 196, 236, 0.2)'
            // });
            // that.cityCircle = circle;
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 200);
            }
        }
        f();
    }
    animateHistory() {
        let that = this;
        // that.showZoom = true;
        if (localStorage.getItem("markerTarget") != null) {
            that.target = JSON.parse(localStorage.getItem("markerTarget"));
        }
        that.playPause = !that.playPause; // This would alternate the state each time
        var coord = that.dataArrayCoords[that.target];
        // that.coordreplaydata = coord;
        var lat = coord[0];
        var lng = coord[1];
        var startPos = [lat, lng];
        that.speed = 200; // km/h
        if (that.playPause) {
            that.map.setCameraTarget({ lat: lat, lng: lng });
            if (that.mark == undefined) {
                // debugger
                // var icicon;
                var fileUrl;
                // var imgUrl, str1;
                let canvas = document.createElement('canvas');
                canvas.width = 150;
                canvas.height = 150;
                var ctx = canvas.getContext('2d');
                var imageObj1 = new Image();
                imageObj1.crossOrigin = "anonymous";
                var imageObj2 = new Image();
                if (!fileUrl) {
                    imageObj1.src = "./assets/Images/dummy-profile-pic.png";
                }
                else {
                    imageObj1.src = fileUrl;
                }
                imageObj1.onload = function () {
                    ctx.drawImage(imageObj1, 36, 15, 78, 78);
                    imageObj2.src = "./assets/Images/32.png";
                    imageObj2.onload = function () {
                        ctx.drawImage(imageObj2, 0, 0, 150, 150);
                        var img = canvas.toDataURL("image/png");
                        that.map.addMarker({
                            icon: {
                                url: img,
                                size: {
                                    height: 90,
                                    width: 90
                                }
                            },
                            position: new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["LatLng"](startPos[0], startPos[1]),
                        }).then((marker) => {
                            that.mark = marker;
                            that.liveTrack(that.map, that.mark, that.dataArrayCoords, that.target, startPos, that.speed, 100);
                        });
                    };
                };
            }
            else {
                that.liveTrack(that.map, that.mark, that.dataArrayCoords, that.target, startPos, that.speed, 100);
            }
        }
        else {
            that.mark.setPosition(new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["LatLng"](startPos[0], startPos[1]));
        }
    }
    liveTrack(map, mark, coords, target, startPos, speed, delay) {
        let that = this;
        // that.events.subscribe("SpeedValue:Updated", (sdata) => {
        //   speed = sdata;
        // })
        var target = target;
        clearTimeout(that.ongoingGoToPoint[coords[target][4].imei]);
        clearTimeout(that.ongoingMoveMarker[coords[target][4].imei]);
        console.log("check coord imei: ", coords[target][4].imei);
        if (!startPos.length)
            coords.push([startPos[0], startPos[1]]);
        function _gotoPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000;
            if (coords[target] == undefined)
                return;
            var dest = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["LatLng"](coords[target][0], coords[target][1]);
            var distance = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["Spherical"].computeDistanceBetween(dest, mark.getPosition()); //in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target][0] - lat) / numStep;
            var deltaLng = (coords[target][1] - lng) / numStep;
            // function changeMarker(mark, deg) {
            //   mark.setRotation(deg);
            // }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["Spherical"].computeHeading(mark.getPosition(), new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["LatLng"](lat, lng));
                    // if ((head != 0) || (head == NaN)) {
                    //   changeMarker(mark, head);
                    // }
                    mark.setPosition(new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["LatLng"](lat, lng));
                    map.setCameraTarget(new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["LatLng"](lat, lng));
                    that.ongoingMoveMarker[coords[target][4].imei] = setTimeout(_moveMarker, delay);
                }
                else {
                    head = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["Spherical"].computeHeading(mark.getPosition(), dest);
                    // if ((head != 0) || (head == NaN)) {
                    //   changeMarker(mark, head);
                    // }
                    mark.setPosition(dest);
                    map.setCameraTarget(dest);
                    target++;
                    that.ongoingGoToPoint[coords[target][4].imei] = setTimeout(_gotoPoint, delay);
                }
            }
            a++;
            if (a > coords.length) {
            }
            else {
                // that.speedMarker = coords[target][3].speed;
                // that.updatetimedate = coords[target][2].time;
                // that.cumu_distance = coords[target][5].cumu_dist;
                if (that.playPause) {
                    _moveMarker();
                    target = target;
                    localStorage.setItem("markerTarget", target);
                }
                else { }
                // km_h = km_h;
            }
        }
        var a = 0;
        _gotoPoint();
    }
};
HistoryPage.ctorParameters = () => [
    { type: _app_service__WEBPACK_IMPORTED_MODULE_3__["AppService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('map', { static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], HistoryPage.prototype, "mapElementRef", void 0);
HistoryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-history',
        template: __webpack_require__(/*! raw-loader!./history.page.html */ "./node_modules/raw-loader/index.js!./src/app/history/history.page.html"),
        styles: [__webpack_require__(/*! ./history.page.scss */ "./src/app/history/history.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_service__WEBPACK_IMPORTED_MODULE_3__["AppService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"]])
], HistoryPage);



/***/ })

}]);
//# sourceMappingURL=history-history-module-es2015.js.map