(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-device-add-device-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/add-device/add-device.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/add-device/add-device.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"tool\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/device-list\"></ion-back-button>\n    </ion-buttons>\n    <!-- <ion-title>Add Device</ion-title> -->\n  </ion-toolbar>\n</ion-header>\n\n<ion-content fullscreen class=\"bgClass\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-card class=\"ion-padding\" style=\"background: white; border-radius: 10px;\">\n          <ion-row>\n            <ion-col size=\"12\" style=\"text-align: center;padding: 35px 5px 15px 5px;\">\n              <p style=\"color: #768fa3;\">To connect the watch to the app</p>\n            </ion-col>\n            <ion-col size=\"12\" style=\"text-align:center; padding: 0px 16px 25px 16px;\">\n              <h5 style=\"color: #15528b;\">Please enter the ID or IMEI on back of the watch</h5>\n            </ion-col>\n            <ion-col size=\"12\">\n              <ion-row style=\"border-radius: 10px; border: 1px solid #469a97;\">\n                <ion-col size=\"12\">\n                  <ion-input type=\"text\" [(ngModel)]=\"device_name\" required placeholder=\"Watch holder's Name\">\n                  </ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col></ion-col>\n              </ion-row>\n              <ion-row style=\"border-radius: 10px; border: 1px solid #469a97;\">\n                <ion-col size=\"12\">\n                  <ion-input type=\"number\" [(ngModel)]=\"imei\" required placeholder=\"ID or IMEI\">\n                  </ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col></ion-col>\n              </ion-row>\n              <ion-row style=\"border-radius: 10px; border: 1px solid #469a97;\">\n                <ion-col size=\"12\">\n                  <ion-input type=\"number\" [(ngModel)]=\"simno\" required placeholder=\"SIM Number\">\n                  </ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col></ion-col>\n              </ion-row>\n              <ion-row *ngIf=\"!respData\">\n                <ion-col size=\"12\">\n                  <ion-button class=\"custBtn\" expand=\"block\" shape=\"round\" (click)=\"selectImage()\">Upload watch holder's\n                    image\n                  </ion-button>\n                </ion-col>\n              </ion-row>\n              <ion-row *ngIf=\"device_name != undefined && simno != undefined && imei != undefined && respData != undefined\">\n                <ion-col size=\"12\">\n                  <ion-button expand=\"block\" class=\"custBtn\" (click)=\"saveDevice()\">Save Changes</ion-button>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/add-device/add-device.module.ts":
/*!*************************************************!*\
  !*** ./src/app/add-device/add-device.module.ts ***!
  \*************************************************/
/*! exports provided: AddDevicePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDevicePageModule", function() { return AddDevicePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_device_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-device.page */ "./src/app/add-device/add-device.page.ts");
/* harmony import */ var _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/crop/ngx */ "./node_modules/@ionic-native/crop/ngx/index.js");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "./node_modules/@ionic-native/file-transfer/ngx/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/Camera/ngx */ "./node_modules/@ionic-native/Camera/ngx/index.js");











var routes = [
    {
        path: '',
        component: _add_device_page__WEBPACK_IMPORTED_MODULE_6__["AddDevicePage"]
    }
];
var AddDevicePageModule = /** @class */ (function () {
    function AddDevicePageModule() {
    }
    AddDevicePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_add_device_page__WEBPACK_IMPORTED_MODULE_6__["AddDevicePage"]],
            providers: [
                _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_8__["FileTransfer"],
                _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_7__["Crop"],
                _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_9__["File"],
                _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_10__["Camera"]
            ]
        })
    ], AddDevicePageModule);
    return AddDevicePageModule;
}());



/***/ }),

/***/ "./src/app/add-device/add-device.page.scss":
/*!*************************************************!*\
  !*** ./src/app/add-device/add-device.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bgClass {\n  --background: #fff url('bgscreen.png') no-repeat center center / cover;\n}\n\n.tool {\n  --background: transparent;\n}\n\n.custBtn {\n  --background: #0aa360;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvdHJhY2tpbmctZ2F1cmRpYW4vc3JjL2FwcC9hZGQtZGV2aWNlL2FkZC1kZXZpY2UucGFnZS5zY3NzIiwic3JjL2FwcC9hZGQtZGV2aWNlL2FkZC1kZXZpY2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUUsc0VBQUE7QUNBRjs7QURHQTtFQUNJLHlCQUFBO0FDQUo7O0FER0E7RUFDSSxxQkFBQTtBQ0FKIiwiZmlsZSI6InNyYy9hcHAvYWRkLWRldmljZS9hZGQtZGV2aWNlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iZ0NsYXNzIHtcbiAgLy8gLS1iYWNrZ3JvdW5kOiB1cmwoLi4vYXNzZXRzL0ltYWdlcy9iZ3NjcmVlbi5wbmcpO1xuICAtLWJhY2tncm91bmQ6ICNmZmYgdXJsKFwiLi4vLi4vYXNzZXRzL0ltYWdlcy9iZ3NjcmVlbi5wbmdcIikgbm8tcmVwZWF0IGNlbnRlciBjZW50ZXIgLyBjb3Zlcjtcbn1cblxuLnRvb2wge1xuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG5cbi5jdXN0QnRuIHtcbiAgICAtLWJhY2tncm91bmQ6ICMwYWEzNjA7XG59XG5cbi8vIGlvbi1jb250ZW50IHtcbi8vICAgICAtLWJhY2tncm91bmQ6ICNmZmYgdXJsKFwiLi4vLi4vYXNzZXRzL0ltYWdlcy9MYXVuY2ggU2NyZWVuLmpwZ1wiKSBuby1yZXBlYXQgY2VudGVyIGNlbnRlciAvIGNvdmVyO1xuLy8gICB9XG4iLCIuYmdDbGFzcyB7XG4gIC0tYmFja2dyb3VuZDogI2ZmZiB1cmwoXCIuLi8uLi9hc3NldHMvSW1hZ2VzL2Jnc2NyZWVuLnBuZ1wiKSBuby1yZXBlYXQgY2VudGVyIGNlbnRlciAvIGNvdmVyO1xufVxuXG4udG9vbCB7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG5cbi5jdXN0QnRuIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMGFhMzYwO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/add-device/add-device.page.ts":
/*!***********************************************!*\
  !*** ./src/app/add-device/add-device.page.ts ***!
  \***********************************************/
/*! exports provided: AddDevicePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDevicePage", function() { return AddDevicePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/Camera/ngx */ "./node_modules/@ionic-native/Camera/ngx/index.js");
/* harmony import */ var _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/crop/ngx */ "./node_modules/@ionic-native/crop/ngx/index.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "./node_modules/@ionic-native/file-transfer/ngx/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);




// import { File } from '@ionic-native/file/ngx';




var AddDevicePage = /** @class */ (function () {
    function AddDevicePage(actionSheetController, camera, loadingController, crop, transfer, apiCall, taostCtrl) {
        this.actionSheetController = actionSheetController;
        this.camera = camera;
        this.loadingController = loadingController;
        this.crop = crop;
        this.transfer = transfer;
        this.apiCall = apiCall;
        this.taostCtrl = taostCtrl;
        this.devicedetails = {};
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    }
    AddDevicePage.prototype.ngOnInit = function () {
    };
    AddDevicePage.prototype.selectImage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: "Select Image source",
                            buttons: [{
                                    text: 'Load from Library',
                                    handler: function () {
                                        // this.openGallery();
                                        _this.pickImage(_this.camera.PictureSourceType.PHOTOLIBRARY);
                                        //SAVEDPHOTOALBUM
                                    }
                                },
                                {
                                    text: 'Use Camera',
                                    handler: function () {
                                        _this.pickImage(_this.camera.PictureSourceType.CAMERA);
                                    }
                                },
                                {
                                    text: 'Cancel',
                                    role: 'cancel'
                                }
                            ]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AddDevicePage.prototype.pickImage = function (sourceType) {
        var _this = this;
        // var url = this.apiCall.mainUrl + "devices/uploadDeviceImage";
        var url = "http://13.126.36.205/devices/uploadDeviceImage";
        var options = {
            quality: 100,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.crop.crop(imageData, { quality: 100 })
                .then(function (newImage) {
                var dlink123 = newImage.split('?');
                var wear = dlink123[0];
                var fileTransfer = _this.transfer.create();
                var uploadOpts = {
                    fileKey: 'photo',
                    // fileName: imageData.substr(imageData.lastIndexOf('/') + 1)
                    fileName: wear.substr(wear.lastIndexOf('/') + 1)
                };
                _this.loadingController.create({
                    message: 'Please wait....',
                }).then(function (loadEl) {
                    loadEl.present();
                    fileTransfer.upload(wear, url, uploadOpts)
                        .then(function (data) {
                        loadEl.dismiss();
                        _this.respData = JSON.parse(JSON.stringify(data)).response;
                        console.log("image data response: ", _this.respData);
                    }, function (err) {
                        loadEl.dismiss();
                        console.log(err);
                        _this.taostCtrl.create({
                            message: 'Something went wrong while uploading file... Please try after some time..',
                            duration: 2000,
                            position: 'bottom'
                        }).then(function (toastEl) {
                            toastEl.present();
                        });
                    });
                });
            });
        }, function (err) {
            console.log("imageData err: ", err);
            // Handle error
        });
    };
    AddDevicePage.prototype.dlUpdate = function (dllink) {
        var _this = this;
        // debugger
        // let that = this;
        var dlink123 = dllink.split('?');
        var wear = dlink123[0];
        console.log("new download link: ", wear);
        var _burl = this.apiCall.mainUrl + "users/updateImagePath";
        // var _burl = this.apiCall.mainUrl + "devices/uploadDeviceImage";
        var payload = {
            deviceImage: [dllink],
        };
        this.loadingController.create({
            message: 'Uploading image...'
        }).then(function (loadEl) {
            loadEl.present();
            _this.apiCall.urlWithdata(_burl, payload)
                .subscribe(function (respData) {
                loadEl.dismiss();
                console.log("check profile upload: ", respData);
                // this.getImgUrl();
            }, function (err) {
                loadEl.dismiss();
            });
        });
    };
    AddDevicePage.prototype.getImgUrl = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + "users/shareProfileImage?uid=" + this.userdetails._id;
        this.loadingController.create({
            message: 'Loading image...'
        }).then((function (loadEl) {
            loadEl.present();
            _this.apiCall.getdevicesForAllVehiclesApi(url)
                .subscribe(function (resp) {
                loadEl.dismiss();
                console.log("server image url=> ", resp);
                if (JSON.parse(JSON.stringify(resp)).imageDoc.length > 0) {
                    var imgUrl = JSON.parse(JSON.stringify(resp)).imageDoc[0];
                    var str1 = imgUrl.split('public/');
                    _this.fileUrl = _this.apiCall.mainUrl + str1[1];
                }
            }, function (err) {
                loadEl.dismiss();
            });
        }));
    };
    AddDevicePage.prototype.saveDevice = function () {
        var _this = this;
        // var devicedetails: any = {};
        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);
        var currentYear = moment__WEBPACK_IMPORTED_MODULE_7__(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        debugger;
        // if (this.userdetails.isSuperAdmin == true) {
        this.devicedetails = {
            "devicename": this.device_name,
            "deviceid": this.imei,
            // "driver_name": this.addvehicleForm.value.driver,
            // "contact_number": this.addvehicleForm.value.contact_num,
            "typdev": "Tracker",
            "sim_number": this.simno,
            "user": this.userdetails._id,
            "emailid": this.userdetails.email,
            "iconType": "",
            // "vehicleGroup": this.groupstaus_id,
            "device_model": "5e733cf65289634bd44dbf67",
            "expdate": new Date(currentYear).toISOString(),
            "supAdmin": this.userdetails._id
        };
        // } else {
        //   if (this.userdetails.isDealer == true) {
        //     this.devicedetails = {
        //       // "devicename": this.addvehicleForm.value.device_name,
        //       "deviceid": this.imei,
        //       // "driver_name": this.addvehicleForm.value.driver,
        //       // "contact_number": this.addvehicleForm.value.contact_num,
        //       "typdev": "Tracker",
        //       "sim_number": this.simno,
        //       "user": this.userdetails._id,
        //       "emailid": this.userdetails.email,
        //       "iconType": "",
        //       // "vehicleGroup": this.groupstaus_id,
        //       "device_model": "5e733cf65289634bd44dbf67",
        //       "expdate": new Date(currentYear).toISOString(),
        //       "supAdmin": this.userdetails.supAdmin,
        //       "Dealer": this.userdetails._id
        //     }
        //   }
        // }
        if (this.respData != undefined) {
            this.devicedetails.deviceImage = [this.respData];
        }
        debugger;
        this.loadingController.create({
            message: 'please wait we are adding device...'
        }).then(function (loadEL) {
            loadEL.present();
            var url1 = _this.apiCall.mainUrl + "devices/addDevice";
            _this.apiCall.urlWithdata(url1, _this.devicedetails)
                .subscribe(function (respData) {
                loadEL.dismiss();
                var res = JSON.parse(JSON.stringify(respData));
                console.log("check if device added or not: ", res);
                if (res) {
                    _this.taostCtrl.create({
                        message: 'Device added successfully.',
                        position: 'bottom',
                        duration: 2000
                    }).then(function (toastEl) {
                        toastEl.present();
                    });
                    _this.simno = undefined;
                    _this.imei = undefined;
                    _this.respData = undefined;
                }
            }, function (err) {
                console.log(err);
                loadEL.dismiss();
            });
        });
    };
    AddDevicePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
        { type: _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_3__["Camera"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
        { type: _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_4__["Crop"] },
        { type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_6__["FileTransfer"] },
        { type: _app_service__WEBPACK_IMPORTED_MODULE_5__["AppService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
    ]; };
    AddDevicePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add-device',
            template: __webpack_require__(/*! raw-loader!./add-device.page.html */ "./node_modules/raw-loader/index.js!./src/app/add-device/add-device.page.html"),
            styles: [__webpack_require__(/*! ./add-device.page.scss */ "./src/app/add-device/add-device.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"],
            _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_3__["Camera"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_4__["Crop"],
            _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_6__["FileTransfer"],
            _app_service__WEBPACK_IMPORTED_MODULE_5__["AppService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
    ], AddDevicePage);
    return AddDevicePage;
}());



/***/ })

}]);
//# sourceMappingURL=add-device-add-device-module-es5.js.map