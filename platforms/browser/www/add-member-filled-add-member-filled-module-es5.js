(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-member-filled-add-member-filled-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/add-member-filled/add-member-filled.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/add-member-filled/add-member-filled.page.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"tool\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\n    </ion-buttons>\n    <!-- <ion-title>Add Device</ion-title> -->\n  </ion-toolbar>\n</ion-header>\n\n<ion-content fullscreen class=\"bgClass\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-card class=\"ion-padding\" style=\"background: white; border-radius: 10px;\">\n          <ion-row>\n            <ion-col size=\"12\" style=\"text-align:center; padding: 0px 16px 25px 16px;\">\n              <h5 style=\"color: #15528b;\">Add Member</h5>\n            </ion-col>\n            <ion-col size=\"12\">\n              <ion-row style=\"border-radius: 10px; border: 1px solid #469a97;\">\n                <ion-col size=\"12\">\n                  <ion-input type=\"text\" [(ngModel)]=\"userId\" required placeholder=\"User Id\">\n                  </ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col></ion-col>\n              </ion-row>\n              <ion-row style=\"border-radius: 10px; border: 1px solid #469a97;\">\n                <ion-col size=\"12\">\n                  <ion-input type=\"text\" [(ngModel)]=\"fn\" required placeholder=\"First Name\">\n                  </ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col></ion-col>\n              </ion-row>\n              <ion-row style=\"border-radius: 10px; border: 1px solid #469a97;\">\n                <ion-col size=\"12\">\n                  <ion-input type=\"text\" [(ngModel)]=\"ln\" required placeholder=\"Last Name\">\n                  </ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col></ion-col>\n              </ion-row>\n              <ion-row style=\"border-radius: 10px; border: 1px solid #469a97;\">\n                <ion-col size=\"12\">\n                  <ion-input type=\"email\" [(ngModel)]=\"email\" required placeholder=\"E-mail Id\">\n                  </ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col></ion-col>\n              </ion-row>\n              <ion-row style=\"border-radius: 10px; border: 1px solid #469a97;\">\n                <ion-col size=\"12\">\n                  <ion-input type=\"number\" [(ngModel)]=\"phone\" required placeholder=\"Mobile Number\">\n                  </ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col></ion-col>\n              </ion-row>\n              <ion-row style=\"border-radius: 10px; border: 1px solid #469a97;\">\n                <ion-col size=\"12\">\n                  <ion-input type=\"password\" [(ngModel)]=\"pass\" required placeholder=\"Password\">\n                  </ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col></ion-col>\n              </ion-row>\n              <ion-row style=\"border-radius: 10px; border: 1px solid #469a97;\">\n                <ion-col size=\"12\">\n                  <ion-datetime displayFormat=\"MMM DD, YYYY HH:mm\" pickerFormat=\"MMM DD, YYYY HH:mm\"\n                    placeholder=\"Select expiry Date\" [(ngModel)]=\"exp_date\" min=\"2020\" max=\"2050\"></ion-datetime>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col></ion-col>\n              </ion-row>\n              <!-- <ion-row *ngIf=\"!respData\">\n                <ion-col size=\"4\">\n                  <p>Upload Image</p>\n                </ion-col>\n                <ion-col size=\"8\">\n                  <ion-button class=\"custBtn\" expand=\"block\" shape=\"round\" (click)=\"selectImage()\">Choose image\n                  </ion-button>\n                </ion-col>\n              </ion-row> -->\n              <ion-row\n                *ngIf=\"fn != undefined && ln != undefined && pass != undefined && exp_date != undefined && email != undefined && phone != undefined && userId != undefined\">\n                <ion-col size=\"12\">\n                  <ion-button expand=\"block\" class=\"custBtn\" (click)=\"submit()\">Submit</ion-button>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/add-member-filled/add-member-filled.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/add-member-filled/add-member-filled.module.ts ***!
  \***************************************************************/
/*! exports provided: AddMemberFilledPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddMemberFilledPageModule", function() { return AddMemberFilledPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_member_filled_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-member-filled.page */ "./src/app/add-member-filled/add-member-filled.page.ts");







var routes = [
    {
        path: '',
        component: _add_member_filled_page__WEBPACK_IMPORTED_MODULE_6__["AddMemberFilledPage"]
    }
];
var AddMemberFilledPageModule = /** @class */ (function () {
    function AddMemberFilledPageModule() {
    }
    AddMemberFilledPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_add_member_filled_page__WEBPACK_IMPORTED_MODULE_6__["AddMemberFilledPage"]]
        })
    ], AddMemberFilledPageModule);
    return AddMemberFilledPageModule;
}());



/***/ }),

/***/ "./src/app/add-member-filled/add-member-filled.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/add-member-filled/add-member-filled.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bgClass {\n  --background: #fff url('bgscreen.png') no-repeat center center / cover;\n}\n\n.tool {\n  --background: transparent;\n}\n\n.custBtn {\n  --background: #0aa360;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvdHJhY2tpbmctZ2F1cmRpYW4vc3JjL2FwcC9hZGQtbWVtYmVyLWZpbGxlZC9hZGQtbWVtYmVyLWZpbGxlZC5wYWdlLnNjc3MiLCJzcmMvYXBwL2FkZC1tZW1iZXItZmlsbGVkL2FkZC1tZW1iZXItZmlsbGVkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQTtFQUVFLHNFQUFBO0FDSEY7O0FES0E7RUFDRSx5QkFBQTtBQ0ZGOztBRHVCQTtFQUNFLHFCQUFBO0FDcEJGIiwiZmlsZSI6InNyYy9hcHAvYWRkLW1lbWJlci1maWxsZWQvYWRkLW1lbWJlci1maWxsZWQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gLnRleHQge1xuLy8gICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4vLyB9XG4uYmdDbGFzcyB7XG4gIC8vIC0tYmFja2dyb3VuZDogdXJsKC4uL2Fzc2V0cy9JbWFnZXMvYmdzY3JlZW4ucG5nKTtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmIHVybChcIi4uLy4uL2Fzc2V0cy9JbWFnZXMvYmdzY3JlZW4ucG5nXCIpIG5vLXJlcGVhdCBjZW50ZXIgY2VudGVyIC8gY292ZXI7XG59XG4udG9vbCB7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG5cblxuLy8gaW9uLWljb24ge1xuLy8gICBmb250LXNpemU6IDY0cHg7XG4vLyB9XG4vLyAuaW1hZ2Uge1xuLy8gICBoZWlnaHQ6IDMwcHg7XG4vLyAgIHdpZHRoOiBhdXRvO1xuLy8gICBtYXJnaW4tdG9wOiA1cHg7XG4vLyAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xuLy8gfVxuLy8gaW9uLWljb24ge1xuLy8gICBmb250LXNpemU6IDI1cHg7XG4vLyB9XG4vLyAuYnRuIHtcbi8vICAgd2lkdGg6IDI1M3B4O1xuLy8gICBtYXJnaW46IGF1dG87XG4vLyAgIG1hcmdpbi10b3A6IDM1cHg7XG4vLyB9XG4uY3VzdEJ0biB7XG4gIC0tYmFja2dyb3VuZDogIzBhYTM2MDtcbn1cbiIsIi5iZ0NsYXNzIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmIHVybChcIi4uLy4uL2Fzc2V0cy9JbWFnZXMvYmdzY3JlZW4ucG5nXCIpIG5vLXJlcGVhdCBjZW50ZXIgY2VudGVyIC8gY292ZXI7XG59XG5cbi50b29sIHtcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuLmN1c3RCdG4ge1xuICAtLWJhY2tncm91bmQ6ICMwYWEzNjA7XG59Il19 */"

/***/ }),

/***/ "./src/app/add-member-filled/add-member-filled.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/add-member-filled/add-member-filled.page.ts ***!
  \*************************************************************/
/*! exports provided: AddMemberFilledPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddMemberFilledPage", function() { return AddMemberFilledPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




var AddMemberFilledPage = /** @class */ (function () {
    function AddMemberFilledPage(
    // private actionSheetController: ActionSheetController,
    // private camera: Camera,
    loadingController, 
    // private crop: Crop,
    // private transfer: FileTransfer,
    apiCall, taostCtrl) {
        this.loadingController = loadingController;
        this.apiCall = apiCall;
        this.taostCtrl = taostCtrl;
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        // var currentDate = moment(new Date());
        // var tesmp = moment(new Date(currentDate), 'DD-MM-YYYY').format("YYYY-MM-DD")
        // var futureMonth = moment(tesmp).add(3, 'Y');
        // var futureMonthEnd = moment().add(3, 'years');
        // console.log("future year: ", (futureMonthEnd).format('dddd Do MMMM, YYYY'));
        // console.log(futureMonthEnd);
        // var temp = 
        // this.exp_date = new Date((futureMonthEnd).format('dddd Do MMMM, YYYY')).toISOString();
    }
    AddMemberFilledPage.prototype.ngOnInit = function () {
    };
    AddMemberFilledPage.prototype.submit = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + "users/createUserPeronalTracker";
        var payload = {
            first_name: this.fn ? this.fn : null,
            last_name: this.ln ? this.ln : null,
            email: this.email ? this.email : null,
            pass: this.pass ? this.pass : null,
            phone: this.phone ? this.phone : null,
            isDealer: false,
            expdate: new Date(this.exp_date).toISOString(),
            Dealer: this.userdetails._id,
            user_id: this.userId ? this.userId : null,
        };
        this.loadingController.create({
            message: 'Please wait, we are adding new member...'
        }).then(function (loadEl) {
            loadEl.present();
            _this.apiCall.urlWithdata(url, payload)
                .subscribe(function (respData) {
                loadEl.dismiss();
                if (respData) {
                    var res = JSON.parse(JSON.stringify(respData));
                    console.log("add member response: ", res);
                    if (res) {
                        _this.taostCtrl.create({
                            message: "member is added successfully.",
                            position: 'middle',
                            duration: 3000
                        }).then(function (toastEl) {
                            toastEl.present();
                            _this.fn = undefined;
                            _this.ln = undefined;
                            _this.phone = undefined;
                            _this.email = undefined;
                            _this.userId = undefined;
                            _this.exp_date = undefined;
                            _this.pass = undefined;
                        });
                    }
                }
            }, function (err) {
                loadEl.dismiss();
                console.log(err);
                if (err.error.message) {
                    _this.taostCtrl.create({
                        message: err.error.message,
                        position: 'middle',
                        duration: 2000
                    }).then(function (toastEl) {
                        toastEl.present();
                    });
                }
            });
        });
    };
    AddMemberFilledPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
        { type: _app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] }
    ]; };
    AddMemberFilledPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add-member-filled',
            template: __webpack_require__(/*! raw-loader!./add-member-filled.page.html */ "./node_modules/raw-loader/index.js!./src/app/add-member-filled/add-member-filled.page.html"),
            styles: [__webpack_require__(/*! ./add-member-filled.page.scss */ "./src/app/add-member-filled/add-member-filled.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]])
    ], AddMemberFilledPage);
    return AddMemberFilledPage;
}());



/***/ })

}]);
//# sourceMappingURL=add-member-filled-add-member-filled-module-es5.js.map