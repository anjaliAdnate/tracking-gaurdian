(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-sos-add-sos-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/add-sos/add-sos.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/add-sos/add-sos.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar class=\"tool\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\n    </ion-buttons>\n    <!-- <ion-title>Add Device</ion-title> -->\n  </ion-toolbar>\n</ion-header>\n\n<ion-content fullscreen class=\"bgClass\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-card class=\"ion-padding\" style=\"background: white; border-radius: 10px;\">\n          <ion-row>\n            <ion-col size=\"12\" style=\"text-align:center; padding: 0px 16px 25px 16px;\">\n              <h5 style=\"color: #15528b;\">Add Emergency Contact</h5>\n            </ion-col>\n            <ion-col size=\"12\">\n\n              <div *ngFor=\"let att of anArray; let idx = index\">\n                <ion-row style=\"border-radius: 10px; border: 1px solid #469a97;\">\n                  <ion-col size=\"12\">\n                    <ion-input type=\"tel\" maxlength=\"10\" [(ngModel)]=\"anArray[idx].value\" required\n                      placeholder=\"Contact Number {{idx+1}}\">\n                    </ion-input>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col></ion-col>\n                </ion-row>\n              </div>\n              <ion-row *ngIf=\"anArray.length < 3\">\n                <ion-col size=\"5\"></ion-col>\n                <ion-col size=\"2\">\n                  <ion-fab-button (click)=\"Add()\" color=\"secondary\">\n                    <ion-icon name=\"add\" expand=\"icon-only\"></ion-icon>\n                  </ion-fab-button>\n                </ion-col>\n                <ion-col size=\"5\"></ion-col>\n              </ion-row>\n              <ion-row *ngIf=\"(anArray.length > 0) && anArray[0].value !== ''\">\n                <ion-col size=\"12\">\n                  <ion-button expand=\"block\" class=\"custBtn\" (click)=\"submit()\">Submit</ion-button>\n                </ion-col>\n              </ion-row>\n              <div *ngIf=\"anArray.length === 3\" padding style=\"text-align: center;\n              color: lightgray;\">\n                <p>You can add only three SOS contact numbers.</p>\n              </div>\n\n              <ion-list *ngIf=\"user_details\" lines=\"none\">\n                <ion-item *ngFor=\"let item of user_details.alert.sos.phones\">\n                  <!-- <ion-avatar slot=\"start\"> -->\n                  <ion-icon slot=\"start\" name=\"call\" style=\"font-size: 1.1em;\"></ion-icon>\n                  <!-- <img src=\"assets/Images/call.png\"> -->\n                  <!-- </ion-avatar> -->\n                  <ion-label>\n                    {{item}}\n                    <!-- <p style=\"font-size: 1em;\">{{item}}</p> -->\n                  </ion-label>\n                  <!-- <ion-badge slot=\"end\"><ion-icon name=\"create\"></ion-icon></ion-badge> -->\n                  <ion-badge slot=\"end\" color=\"danger\">\n                    <ion-icon name=\"trash\"></ion-icon>\n                  </ion-badge>\n                </ion-item>\n              </ion-list>\n              <!-- <div *ngIf=\"(anArray.length > 0) && anArray[0].value !== ''\">\n                <p>The required fields can not be empty. You have to add atleast one contact number.</p>\n              </div> -->\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/add-sos/add-sos.module.ts":
/*!*******************************************!*\
  !*** ./src/app/add-sos/add-sos.module.ts ***!
  \*******************************************/
/*! exports provided: AddSosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddSosPageModule", function() { return AddSosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_sos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-sos.page */ "./src/app/add-sos/add-sos.page.ts");







var routes = [
    {
        path: '',
        component: _add_sos_page__WEBPACK_IMPORTED_MODULE_6__["AddSosPage"]
    }
];
var AddSosPageModule = /** @class */ (function () {
    function AddSosPageModule() {
    }
    AddSosPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_add_sos_page__WEBPACK_IMPORTED_MODULE_6__["AddSosPage"]]
        })
    ], AddSosPageModule);
    return AddSosPageModule;
}());



/***/ }),

/***/ "./src/app/add-sos/add-sos.page.scss":
/*!*******************************************!*\
  !*** ./src/app/add-sos/add-sos.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bgClass {\n  --background: #fff url('bgscreen.png') no-repeat center center / cover;\n}\n\n.tool {\n  --background: transparent;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvdHJhY2tpbmctZ2F1cmRpYW4vc3JjL2FwcC9hZGQtc29zL2FkZC1zb3MucGFnZS5zY3NzIiwic3JjL2FwcC9hZGQtc29zL2FkZC1zb3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUUsc0VBQUE7QUNBRjs7QURFQTtFQUNFLHlCQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9hZGQtc29zL2FkZC1zb3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJnQ2xhc3Mge1xuICAvLyAtLWJhY2tncm91bmQ6IHVybCguLi9hc3NldHMvSW1hZ2VzL2Jnc2NyZWVuLnBuZyk7XG4gIC0tYmFja2dyb3VuZDogI2ZmZiB1cmwoXCIuLi8uLi9hc3NldHMvSW1hZ2VzL2Jnc2NyZWVuLnBuZ1wiKSBuby1yZXBlYXQgY2VudGVyIGNlbnRlciAvIGNvdmVyO1xufVxuLnRvb2wge1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuIiwiLmJnQ2xhc3Mge1xuICAtLWJhY2tncm91bmQ6ICNmZmYgdXJsKFwiLi4vLi4vYXNzZXRzL0ltYWdlcy9iZ3NjcmVlbi5wbmdcIikgbm8tcmVwZWF0IGNlbnRlciBjZW50ZXIgLyBjb3Zlcjtcbn1cblxuLnRvb2wge1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/add-sos/add-sos.page.ts":
/*!*****************************************!*\
  !*** ./src/app/add-sos/add-sos.page.ts ***!
  \*****************************************/
/*! exports provided: AddSosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddSosPage", function() { return AddSosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




var AddSosPage = /** @class */ (function () {
    function AddSosPage(apiCall, alertController, loadingController, toastController) {
        this.apiCall = apiCall;
        this.alertController = alertController;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.anArray = [];
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    }
    AddSosPage.prototype.ngOnInit = function () {
    };
    AddSosPage.prototype.ionViewDidEnter = function () {
        this.getUserDetails();
    };
    AddSosPage.prototype.getUserDetails = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + "users/getuserDetail?uid=" + this.userdetails._id;
        this.loadingController.create({
            message: 'Please wait...'
        }).then(function (loadEl) {
            loadEl.present();
            _this.apiCall.getdevicesForAllVehiclesApi(url)
                .subscribe(function (respData) {
                loadEl.dismiss();
                if (respData) {
                    var res = JSON.parse(JSON.stringify(respData));
                    console.log("check user details: ", res);
                    _this.user_details = res;
                }
            }, function (err) {
                loadEl.dismiss();
                console.log("error: ", err);
            });
        });
    };
    AddSosPage.prototype.addMoreNumbers = function () {
    };
    AddSosPage.prototype.Add = function () {
        this.anArray.push({ 'value': '' });
    };
    AddSosPage.prototype.submit = function () {
        var _this = this;
        console.log('this.anArray', this.anArray);
        for (var i = 0; i < this.anArray.length; i++) {
            var val = this.anArray[i].value;
            if (/^\d{10}$/.test(val)) {
                this.user_details.alert.sos.phones.push(this.anArray[i].value);
                console.log(this.user_details.alert);
            }
            else {
                if (this.anArray.length === (i + 1)) {
                    if (this.anArray[i].value !== "") {
                        this.alertController.create({
                            message: 'Invalid numbers, must be ten digits',
                            buttons: ['Okay']
                        }).then(function (alertEl) {
                            alertEl.present();
                        });
                    }
                }
            }
        }
        var url = this.apiCall.mainUrl + "users/editUserDetails";
        var payload = {
            contactid: this.userdetails._id,
            alert: this.user_details.alert
        };
        this.loadingController.create({
            message: 'Please wait...'
        }).then(function (loadEl) {
            loadEl.present();
            _this.apiCall.urlWithdata(url, payload)
                .subscribe(function (respData) {
                loadEl.dismiss();
                if (respData) {
                    var res = JSON.parse(JSON.stringify(res));
                    console.log("check res edited user details: ", res);
                    if (res.message) {
                        if (res.message === 'Saved')
                            _this.toastController.create({
                                message: "Congratulations..!! Emergency contacts saved successfully.",
                                duration: 1500,
                                position: 'middle'
                            }).then(function (toastEl) {
                                toastEl.present();
                            });
                        _this.anArray = [];
                    }
                }
            }, function (err) {
                loadEl.dismiss();
                console.log(err);
            });
        });
    };
    AddSosPage.ctorParameters = function () { return [
        { type: _app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] }
    ]; };
    AddSosPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add-sos',
            template: __webpack_require__(/*! raw-loader!./add-sos.page.html */ "./node_modules/raw-loader/index.js!./src/app/add-sos/add-sos.page.html"),
            styles: [__webpack_require__(/*! ./add-sos.page.scss */ "./src/app/add-sos/add-sos.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]])
    ], AddSosPage);
    return AddSosPage;
}());



/***/ })

}]);
//# sourceMappingURL=add-sos-add-sos-module-es5.js.map