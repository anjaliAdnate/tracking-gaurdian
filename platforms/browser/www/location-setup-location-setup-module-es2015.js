(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["location-setup-location-setup-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/location-setup/location-setup.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/location-setup/location-setup.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Location Setup</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-row padding>\n          <ion-col size=\"9\">\n            <p style=\"text-align: left; color: #839cb7\">\n              Add your favourite places and get notified when\n              child arrive and leave.\n            </p>\n          </ion-col>\n          <ion-col size=\"3\" text-right style=\"margin: auto;\">\n            <ion-button style=\"color: #35587e;\" fill=\"clear\">\n              <ion-icon slot=\"start\" name=\"add\"></ion-icon>\n              ADD\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <!-- <ion-list>\n    <ion-item> -->\n  <ion-row *ngFor=\"let item of allDataArray\">\n    <ion-col size=\"12\">\n      <ion-row style=\"border-bottom: 1px solid #f0f5f5;\">\n        <ion-col size=\"2\">\n          <ion-avatar style=\"text-align: center;\">\n            <img src=\"{{item.imageSrc}}\" style=\"width: 30px; height: 30px;\" />\n          </ion-avatar>\n        </ion-col>\n        <ion-col size=\"10\" style=\"padding: 0px 5px 0px 5px;\">\n          <ion-row>\n            <ion-col size=\"12\" style=\"padding: 0px;\">\n              <p style=\"margin: 5px; font-size: 18px; color: rgb(97, 97, 97);\">{{item.title}}</p>\n            </ion-col>\n            <ion-col size=\"12\" *ngIf=\"item.count > 0\" style=\"padding: 0px;\">\n              <ion-row>\n                <ion-col size=\"5\">\n                  <p style=\"margin: 0px; color: #b3cccc;\">{{item.key}} added</p>\n                </ion-col>\n                <ion-col size=\"1\">\n                  <p style=\"margin: 0px;\">|</p>\n                </ion-col>\n                <ion-col size=\"6\">\n                  <ion-button fill=\"clear\" size=\"small\" color=\"danger\" style=\"margin-top: -1px;\"\n                    (click)=\"deletePOI(item.key)\">Delete\n                  </ion-button>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n            <ion-col size=\"12\" *ngIf=\"item.count == 0\" style=\"padding: 0px;\">\n              <ion-row>\n                <ion-col size=\"5\">\n                  <p style=\"margin: 0px; color: #b3cccc;\">{{item.subTitle}}</p>\n                </ion-col>\n                <ion-col size=\"1\">\n                  <p style=\"margin: 0px;\">|</p>\n                </ion-col>\n                <ion-col size=\"6\">\n                  <ion-button fill=\"clear\" size=\"small\" color=\"secondary\" style=\"margin-top: -1px;\"\n                    (click)=\"setLoaction(item.key)\">Add\n                  </ion-button>\n                </ion-col>\n              </ion-row>\n              <!-- <ion-row>\n                    <ion-col size=\"12\">\n                      {{item.subTitle}}\n                    </ion-col>\n                  </ion-row> -->\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n    </ion-col>\n\n  </ion-row>\n  <!-- </ion-item>\n  </ion-list> -->\n\n\n\n</ion-content>"

/***/ }),

/***/ "./src/app/location-setup/location-setup.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/location-setup/location-setup.module.ts ***!
  \*********************************************************/
/*! exports provided: LocationSetupPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocationSetupPageModule", function() { return LocationSetupPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _location_setup_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./location-setup.page */ "./src/app/location-setup/location-setup.page.ts");







const routes = [
    {
        path: '',
        component: _location_setup_page__WEBPACK_IMPORTED_MODULE_6__["LocationSetupPage"]
    }
];
let LocationSetupPageModule = class LocationSetupPageModule {
};
LocationSetupPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_location_setup_page__WEBPACK_IMPORTED_MODULE_6__["LocationSetupPage"]]
    })
], LocationSetupPageModule);



/***/ }),

/***/ "./src/app/location-setup/location-setup.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/location-setup/location-setup.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-toolbar {\n  --color: #ffffff;\n  --background: linear-gradient(to right, #329cd1, #008dd3, #007cd2, #1c6ace, #4055c5);\n}\n\n.para {\n  padding: 0px 0px 11px;\n  margin: 0px;\n  color: #aaa8a9;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvdHJhY2tpbmctZ2F1cmRpYW4vc3JjL2FwcC9sb2NhdGlvbi1zZXR1cC9sb2NhdGlvbi1zZXR1cC5wYWdlLnNjc3MiLCJzcmMvYXBwL2xvY2F0aW9uLXNldHVwL2xvY2F0aW9uLXNldHVwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFBO0VBQ0Esb0ZBQUE7QUNDRjs7QURDQTtFQUNFLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7QUNFRiIsImZpbGUiOiJzcmMvYXBwL2xvY2F0aW9uLXNldHVwL2xvY2F0aW9uLXNldHVwLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFyIHtcbiAgLS1jb2xvcjogI2ZmZmZmZjtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMzMjljZDEsICMwMDhkZDMsICMwMDdjZDIsICMxYzZhY2UsICM0MDU1YzUpO1xufVxuLnBhcmEge1xuICBwYWRkaW5nOiAwcHggMHB4IDExcHg7XG4gIG1hcmdpbjogMHB4O1xuICBjb2xvcjogI2FhYThhOTtcbn1cbiIsImlvbi10b29sYmFyIHtcbiAgLS1jb2xvcjogI2ZmZmZmZjtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMzMjljZDEsICMwMDhkZDMsICMwMDdjZDIsICMxYzZhY2UsICM0MDU1YzUpO1xufVxuXG4ucGFyYSB7XG4gIHBhZGRpbmc6IDBweCAwcHggMTFweDtcbiAgbWFyZ2luOiAwcHg7XG4gIGNvbG9yOiAjYWFhOGE5O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/location-setup/location-setup.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/location-setup/location-setup.page.ts ***!
  \*******************************************************/
/*! exports provided: LocationSetupPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocationSetupPage", function() { return LocationSetupPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





let LocationSetupPage = class LocationSetupPage {
    constructor(router, apiCall, alertCtrl, loadingController, toastCtrl) {
        this.router = router;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.loadingController = loadingController;
        this.toastCtrl = toastCtrl;
        this.homeCount = 0;
        this.workCount = 0;
        this.groceryCount = 0;
        this.schoolCount = 0;
        this.gymCount = 0;
        this.poiData = [];
        this.allDataArray = [
            {
                key: "home",
                title: "Add your home",
                subTitle: "add poi",
                count: this.homeCount,
                imageSrc: "assets/Images/home.png",
            },
            {
                key: "work",
                title: "Add your work",
                subTitle: "add poi",
                count: this.workCount,
                imageSrc: "assets/Images/suitcase.png",
            },
            {
                key: "school",
                title: "Add your school",
                subTitle: "add poi",
                count: this.schoolCount,
                imageSrc: "assets/Images/school.png",
            },
            {
                key: "gym",
                title: "Add your gym",
                subTitle: "add poi",
                count: this.gymCount,
                imageSrc: "assets/Images/weightlifting.png",
            },
            {
                key: "grocery_store",
                title: "Add your grocery store",
                subTitle: "add poi",
                count: this.groceryCount,
                imageSrc: "assets/Images/shop.png",
            }
        ];
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    }
    ngOnInit() {
    }
    setLoaction(key) {
        this.router.navigateByUrl('/add-muliple/' + key);
    }
    ionViewDidEnter() {
        this.callGeofence();
    }
    callGeofence() {
        debugger;
        this.homeCount = 0;
        this.workCount = 0;
        this.schoolCount = 0;
        this.gymCount = 0;
        this.groceryCount = 0;
        var url = this.apiCall.mainUrl + "poi/getPois?user=" + this.userdetails._id;
        // this.geoShape = [];
        // this.apiCall.getGeofenceCall(this.userdetails._id)
        this.apiCall.getdevicesForAllVehiclesApi(url)
            .subscribe(respData => {
            var data = JSON.parse(JSON.stringify(respData));
            console.log("geofence data=> " + data);
            if (data.length > 0) {
                this.poiData = data;
                // this.innerFunc(data);
                for (var i = 0; i < data.length; i++) {
                    if (data[i].poi.poi_type === 'home') {
                        this.homeCount += 1;
                    }
                    else if (data[i].poi.poi_type === 'work') {
                        this.workCount += 1;
                    }
                    else if (data[i].poi.poi_type === 'gym') {
                        this.gymCount += 1;
                    }
                    else if (data[i].poi.poi_type === 'school') {
                        this.schoolCount += 1;
                    }
                    else if (data[i].poi.poi_type === 'grocery_store') {
                        this.groceryCount += 1;
                    }
                }
                for (var v = 0; v < this.allDataArray.length; v++) {
                    if (this.allDataArray[v].key === 'home') {
                        this.allDataArray[v].count = this.homeCount;
                    }
                    else if (this.allDataArray[v].key === 'work') {
                        this.allDataArray[v].count = this.workCount;
                    }
                    else if (this.allDataArray[v].key === 'gym') {
                        this.allDataArray[v].count = this.gymCount;
                    }
                    else if (this.allDataArray[v].key === 'school') {
                        this.allDataArray[v].count = this.schoolCount;
                    }
                    else if (this.allDataArray[v].key === 'grocery_store') {
                        this.allDataArray[v].count = this.groceryCount;
                    }
                }
            }
            else {
                // this.toastCtrl.create({
                //   message: 'Geofence not found..!!',
                //   position: 'bottom',
                //   duration: 1500
                // }).then(toastEl => {
                //   toastEl.present();
                // })
            }
        }, err => {
            console.log(err);
        });
    }
    deletePOI(key) {
        debugger;
        // deletePOI(data) {
        var data;
        console.log("delete data: ", this.poiData[key]);
        for (var r = 0; r < this.poiData.length; r++) {
            if (this.poiData[r].poi.poi_type === key) {
                data = this.poiData[r];
            }
        }
        this.alertCtrl.create({
            message: 'Do you want to delete this POI?',
            buttons: [{
                    text: 'Cancel'
                }, {
                    text: 'YES PROCEED',
                    handler: () => {
                        this.loadingController.create({
                            message: 'please wait.. we are deleting POI..'
                        }).then((loadEl) => {
                            loadEl.present();
                            this.deleteCall(loadEl, data);
                        });
                    }
                }]
        }).then((alertEl) => {
            alertEl.present();
        });
        // alert.present();
    }
    deleteCall(loadEl, data) {
        debugger;
        var url = this.apiCall.mainUrl + "poi/deletePoi?_id=" + data._id;
        this.apiCall.getdevicesForAllVehiclesApi(url)
            .subscribe(data => {
            loadEl.dismiss();
            if (data) {
                var res = JSON.parse(JSON.stringify(data));
                console.log("deleted: " + res);
            }
            this.toastCtrl.create({
                message: 'deleted successfully.',
                position: "middle",
                duration: 1500
            }).then((toastEl) => {
                toastEl.present();
            });
            this.callGeofence();
        }, err => {
            loadEl.dismiss();
            console.log("error occured while deleting POI: ", err);
            this.toastCtrl.create({
                message: 'deleted successfully.',
                position: "middle",
                duration: 1500
            }).then((toastEl) => {
                toastEl.present();
            });
            this.callGeofence();
        });
    }
};
LocationSetupPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _app_service__WEBPACK_IMPORTED_MODULE_3__["AppService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] }
];
LocationSetupPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-location-setup',
        template: __webpack_require__(/*! raw-loader!./location-setup.page.html */ "./node_modules/raw-loader/index.js!./src/app/location-setup/location-setup.page.html"),
        styles: [__webpack_require__(/*! ./location-setup.page.scss */ "./src/app/location-setup/location-setup.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _app_service__WEBPACK_IMPORTED_MODULE_3__["AppService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]])
], LocationSetupPage);



/***/ })

}]);
//# sourceMappingURL=location-setup-location-setup-module-es2015.js.map