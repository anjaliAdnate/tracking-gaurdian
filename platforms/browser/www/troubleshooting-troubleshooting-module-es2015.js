(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["troubleshooting-troubleshooting-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/troubleshooting/troubleshooting.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/troubleshooting/troubleshooting.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n        <ion-buttons slot=\"start\">\n            <ion-back-button defaultHref=\"/settings\"></ion-back-button>\n        </ion-buttons>\n        <ion-title>Troubleshooting</ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-grid>\n        <ion-row>\n            <ion-col size-sm=\"6\" offset-sm=\"3\">\n                <ion-card>\n                    <ion-card-content>\n                        <ion-row>\n                            <ion-col size=\"2\" style=\"margin: auto;\">\n                                <ion-img src=\"assets/Images/usercircle.png\" style=\"width: 40px;\"></ion-img>\n                            </ion-col>\n                            <ion-col size=\"9\" style=\"padding-top: 10px; padding-left: 10px;\">\n                                <h2 style=\"margin: 0px; color: #476789\">Internet</h2>\n                            </ion-col>\n                            <ion-col size=\"1\">\n                                <ion-icon name=\"arrow-dropright\"></ion-icon>\n                            </ion-col>\n                        </ion-row>\n                    </ion-card-content>\n                </ion-card>\n\n                <ion-card>\n\n                    <ion-card-content>\n                        <ion-row>\n                            <ion-col size=\"2\" style=\"margin: auto;\">\n                                <ion-img src=\"assets/Images/usercircle.png\" style=\"width: 40px;\"></ion-img>\n                            </ion-col>\n                            <ion-col size=\"9\" style=\"padding-top: 10px; padding-left: 10px;\">\n                                <h4 style=\"margin: 0px; color: #476789\">KidsControl Server</h4>\n                            </ion-col>\n                            <ion-col size=\"1\">\n                                <ion-icon name=\"arrow-dropright\"></ion-icon>\n                            </ion-col>\n                        </ion-row>\n                    </ion-card-content>\n                </ion-card>\n                <ion-card>\n                    <ion-card-content>\n                        <ion-row>\n                            <ion-col size=\"2\" style=\"margin: auto;\">\n                                <ion-img src=\"assets/Images/usercircle.png\" style=\"width: 40px;\"></ion-img>\n                            </ion-col>\n                            <ion-col size=\"9\" style=\"padding-top: 10px; padding-left: 10px;\">\n                                <h4 style=\"margin: 0px; color: #476789\">Geolocation</h4>\n                            </ion-col>\n                            <ion-col size=\"1\">\n                                <ion-icon name=\"arrow-dropright\"></ion-icon>\n                            </ion-col>\n                        </ion-row>\n                    </ion-card-content>\n                </ion-card>\n                <ion-card>\n                    <ion-card-content>\n                        <ion-row>\n                            <ion-col size=\"2\" style=\"margin: auto;\">\n                                <ion-img src=\"assets/Images/usercircle.png\" style=\"width: 40px;\"></ion-img>\n                            </ion-col>\n                            <ion-col size=\"9\" style=\"padding-top: 10px; padding-left: 10px;\">\n                                <h4 style=\"margin: 0px; color: #476789\">Wi-Fi</h4>\n                            </ion-col>\n                            <ion-col size=\"1\">\n                                <ion-icon name=\"arrow-dropright\"></ion-icon>\n                            </ion-col>\n                        </ion-row>\n                    </ion-card-content>\n                </ion-card>\n                <ion-card>\n                    <ion-card-content>\n                        <ion-row>\n                            <ion-col size=\"2\" style=\"margin: auto;\">\n                                <ion-img src=\"assets/Images/usercircle.png\" style=\"width: 40px;\"></ion-img>\n                            </ion-col>\n                            <ion-col size=\"9\" style=\"padding-top: 10px; padding-left: 10px;\">\n                                <h4 style=\"margin: 0px; color: #476789\">Device Maintenance</h4>\n                            </ion-col>\n                            <ion-col size=\"1\">\n                                <ion-icon name=\"arrow-dropright\"></ion-icon>\n                            </ion-col>\n                        </ion-row>\n                    </ion-card-content>\n                </ion-card>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/troubleshooting/troubleshooting.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/troubleshooting/troubleshooting.module.ts ***!
  \***********************************************************/
/*! exports provided: TroubleshootingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TroubleshootingPageModule", function() { return TroubleshootingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _troubleshooting_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./troubleshooting.page */ "./src/app/troubleshooting/troubleshooting.page.ts");







const routes = [
    {
        path: '',
        component: _troubleshooting_page__WEBPACK_IMPORTED_MODULE_6__["TroubleshootingPage"]
    }
];
let TroubleshootingPageModule = class TroubleshootingPageModule {
};
TroubleshootingPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_troubleshooting_page__WEBPACK_IMPORTED_MODULE_6__["TroubleshootingPage"]]
    })
], TroubleshootingPageModule);



/***/ }),

/***/ "./src/app/troubleshooting/troubleshooting.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/troubleshooting/troubleshooting.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-icon {\n  font-size: xx-large;\n}\n\nion-toolbar {\n  --color: #ffffff;\n  --background: linear-gradient(to right, #329cd1, #008dd3, #007cd2, #1c6ace, #4055c5);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvdHJhY2tpbmctZ2F1cmRpYW4vc3JjL2FwcC90cm91Ymxlc2hvb3RpbmcvdHJvdWJsZXNob290aW5nLnBhZ2Uuc2NzcyIsInNyYy9hcHAvdHJvdWJsZXNob290aW5nL3Ryb3VibGVzaG9vdGluZy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxtQkFBQTtBQ0NGOztBRENBO0VBQ0UsZ0JBQUE7RUFDQSxvRkFBQTtBQ0VGIiwiZmlsZSI6InNyYy9hcHAvdHJvdWJsZXNob290aW5nL3Ryb3VibGVzaG9vdGluZy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogeHgtbGFyZ2U7XG59XG5pb24tdG9vbGJhciB7XG4gIC0tY29sb3I6ICNmZmZmZmY7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMzI5Y2QxLCAjMDA4ZGQzLCAjMDA3Y2QyLCAjMWM2YWNlLCAjNDA1NWM1KTtcbn1cbiIsImlvbi1pY29uIHtcbiAgZm9udC1zaXplOiB4eC1sYXJnZTtcbn1cblxuaW9uLXRvb2xiYXIge1xuICAtLWNvbG9yOiAjZmZmZmZmO1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzMyOWNkMSwgIzAwOGRkMywgIzAwN2NkMiwgIzFjNmFjZSwgIzQwNTVjNSk7XG59Il19 */"

/***/ }),

/***/ "./src/app/troubleshooting/troubleshooting.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/troubleshooting/troubleshooting.page.ts ***!
  \*********************************************************/
/*! exports provided: TroubleshootingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TroubleshootingPage", function() { return TroubleshootingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TroubleshootingPage = class TroubleshootingPage {
    constructor() { }
    ngOnInit() {
    }
};
TroubleshootingPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-troubleshooting',
        template: __webpack_require__(/*! raw-loader!./troubleshooting.page.html */ "./node_modules/raw-loader/index.js!./src/app/troubleshooting/troubleshooting.page.html"),
        styles: [__webpack_require__(/*! ./troubleshooting.page.scss */ "./src/app/troubleshooting/troubleshooting.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], TroubleshootingPage);



/***/ })

}]);
//# sourceMappingURL=troubleshooting-troubleshooting-module-es2015.js.map