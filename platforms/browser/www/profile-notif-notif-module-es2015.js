(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-notif-notif-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/profile/notif/notif.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/profile/notif/notif.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/profile\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Notifications</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <!-- <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\"> -->\n  <!-- <ion-button (click)=\"toggleInfiniteScroll()\" expand=\"block\">\n          Toggle Infinite Scroll\n        </ion-button> -->\n  <ion-list>\n    <ion-item *ngFor=\"let item of items\">\n      <!-- assets/Images/{{item.type}}.png -->\n      <ion-row>\n        <ion-col size=\"2\">\n          <!-- <ion-img [src]=\"'assets/Images/notification.png'\"></ion-img> -->\n          <ion-img src=\"assets/notification/{{item.type}}.png\"></ion-img>\n        </ion-col>\n        <ion-col size=\"6\">\n          <h6 style=\"margin: 0px;\">{{ item.item._type }}</h6>\n          <p style=\"margin: 0px; font-size: 12px;\">{{ item.item.sentence }}</p>\n          <p style=\"margin: 0px; font-size: 10px;\" *ngIf=\"item.type === 'IGN'\">Odo: <span\n              [ngClass]=\"{ 'switchOn': item.switch === 'ON','switchOff':   item.switch === 'OFF' }\">{{item.odo | number: \"1.0-2\"}}\n              km(s)</span></p>\n        </ion-col>\n        <ion-col size=\"4\" style=\"text-align: center; padding: 16px 0px;\">\n          <ion-row>\n            <ion-col size=\"12\" class=\"ion-no-padding\">\n              <p style=\"font-size: 10px; margin: auto;\n          font-weight: 600;\">\n                {{ item.timestamp | date: 'mediumDate' }}</p>\n            </ion-col>\n            <ion-col size=\"12\" class=\"ion-no-padding\">\n              <p style=\"font-size: 10px; margin: auto;\n          font-weight: 600;\">\n                {{ item.timestamp | date: 'shortTime' }}</p>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-list>\n\n  <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData($event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more data...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n  <!-- </ion-col>\n    </ion-row>\n  </ion-grid> -->\n\n</ion-content>"

/***/ }),

/***/ "./src/app/profile/notif/notif.module.ts":
/*!***********************************************!*\
  !*** ./src/app/profile/notif/notif.module.ts ***!
  \***********************************************/
/*! exports provided: NotifPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotifPageModule", function() { return NotifPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _notif_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./notif.page */ "./src/app/profile/notif/notif.page.ts");







const routes = [
    {
        path: '',
        component: _notif_page__WEBPACK_IMPORTED_MODULE_6__["NotifPage"]
    }
];
let NotifPageModule = class NotifPageModule {
};
NotifPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_notif_page__WEBPACK_IMPORTED_MODULE_6__["NotifPage"]]
    })
], NotifPageModule);



/***/ }),

/***/ "./src/app/profile/notif/notif.page.scss":
/*!***********************************************!*\
  !*** ./src/app/profile/notif/notif.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-toolbar {\n  --color: #ffffff;\n  --background: linear-gradient(to right, #329cd1, #008dd3, #007cd2, #1c6ace, #4055c5);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvdHJhY2tpbmctZ2F1cmRpYW4vc3JjL2FwcC9wcm9maWxlL25vdGlmL25vdGlmLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcHJvZmlsZS9ub3RpZi9ub3RpZi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtFQUNBLG9GQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9wcm9maWxlL25vdGlmL25vdGlmLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFyIHtcbiAgICAtLWNvbG9yOiAjZmZmZmZmO1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMzI5Y2QxLCAjMDA4ZGQzLCAjMDA3Y2QyLCAjMWM2YWNlLCAjNDA1NWM1KTtcbiAgfSIsImlvbi10b29sYmFyIHtcbiAgLS1jb2xvcjogI2ZmZmZmZjtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMzMjljZDEsICMwMDhkZDMsICMwMDdjZDIsICMxYzZhY2UsICM0MDU1YzUpO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/profile/notif/notif.page.ts":
/*!*********************************************!*\
  !*** ./src/app/profile/notif/notif.page.ts ***!
  \*********************************************/
/*! exports provided: NotifPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotifPage", function() { return NotifPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




// import { IonInfiniteScroll } from '@ionic/angular';
let NotifPage = class NotifPage {
    // @ViewChild(IonInfiniteScroll, {
    //   static: true
    // }) infiniteScroll: IonInfiniteScroll;
    constructor(apiCall, loadingController) {
        this.apiCall = apiCall;
        this.loadingController = loadingController;
        this.items = [];
        this.page = 1;
        this.limit = 15;
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    }
    ngOnInit() {
        this.getNotifications();
        // this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    }
    getNotifications() {
        this.page = 1;
        this.limit = 15;
        var url = this.apiCall.mainUrl + "notifs/getNotifiLimit?user=" + this.userdetails._id + "&pageNo=" + this.page + "&size=" + this.limit;
        this.loadingController.create({
            message: 'Loading notifications...'
        }).then(loadEl => {
            loadEl.present();
            this.apiCall.getdevicesForAllVehiclesApi(url)
                .subscribe(respData => {
                loadEl.dismiss();
                let variable = JSON.parse(JSON.stringify(respData));
                this.items = variable;
                console.log("notifs: ", this.items);
            }, err => {
                loadEl.dismiss();
            });
        });
    }
    loadData(event) {
        debugger;
        // this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
        let that = this;
        that.page = that.page + 1;
        var url = this.apiCall.mainUrl + "notifs/getNotifiLimit?user=" + this.userdetails._id + "&pageNo=" + that.page + "&size=" + that.limit;
        this.apiCall.getdevicesForAllVehiclesApi(url)
            .subscribe(respData => {
            let variable = JSON.parse(JSON.stringify(respData));
            for (var i = 0; i < variable.length; i++) {
                that.items.push(variable[i]);
            }
            // this.items = variable;
            console.log("notifs length: ", this.items.length);
            event.target.disabled = true;
            // this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
        }, err => {
        });
        // setTimeout(() => {
        //   console.log('Done');
        //   event.target.complete();
        //   // App logic to determine if all data is loaded
        //   // and disable the infinite scroll
        //   if (data.length == 1000) {
        //     event.target.disabled = true;
        //   }
        // }, 500);
    }
};
NotifPage.ctorParameters = () => [
    { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] }
];
NotifPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-notif',
        template: __webpack_require__(/*! raw-loader!./notif.page.html */ "./node_modules/raw-loader/index.js!./src/app/profile/notif/notif.page.html"),
        styles: [__webpack_require__(/*! ./notif.page.scss */ "./src/app/profile/notif/notif.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]])
], NotifPage);



/***/ })

}]);
//# sourceMappingURL=profile-notif-notif-module-es2015.js.map