(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["chat-chat-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/chat/chat.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/chat/chat.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      Chat Box\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content #IonContent fullscreen=\"false\">\n  <!-- <div class=\"messages-container\"> -->\n  <ion-list>\n    <div *ngFor=\"let chat of msgList; let i = index; \">\n      <ion-row *ngIf=\"chat.userId == User\">\n        <ion-col class=\"right\" no-padding\n          [ngClass]=\"{'clubbed':((msgList[i+1] && msgList[i+1].userId != chat.userId)|| !msgList[i+1])}\">\n          <div class=\"imageAvatarRight\">\n            <div class=\"imageAvatarBottom\">\n              <ion-avatar class=\"avatar\" [ngClass]=\"(msgList[i+1] && msgList[i+1].userId == chat.userId)?'hidden':''\">\n                <div class=\"imageAvatarBottomIcon\">\n                  <ion-icon name=\"add\" expand=\"icon-only\" color=\"light\"> </ion-icon>\n                </div>\n                <ion-img src=\"assets/chat1.jpg\"></ion-img>\n              </ion-avatar>\n            </div>\n            <ion-label color=\"light\">\n              <div class=\"chatDiv\" [ngClass]=\"{'sharper':((msgList[i+1] && msgList[i+1].userId == chat.userId) && \n              (msgList[i-1] && msgList[i-1].userId == chat.userId)),\n              'sharper-top':(msgList[i-1] && msgList[i-1].userId == chat.userId),\n              'sharper-bottom':(msgList[i+1] && msgList[i+1].userId == chat.userId)}\">\n                <p text-wrap padding>{{chat.message}}\n                </p>\n                <div class=\"corner-parent-right\">\n                  <div class=\"corner-child-right\">\n\n                  </div>\n                </div>\n              </div>\n            </ion-label>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row *ngIf=\"chat.userId == toUser\">\n        <ion-col class=\"left\" no-padding\n          [ngClass]=\"{'clubbed':((msgList[i+1] && msgList[i+1].userId != chat.userId)|| !msgList[i+1])}\">\n          <div class=\"imageAvatarLeft\">\n            <ion-label color=\"light\">\n              <div class=\"chatDiv\" [ngClass]=\"{'sharper':((msgList[i+1] && msgList[i+1].userId == chat.userId) && \n              (msgList[i-1] && msgList[i-1].userId == chat.userId)),\n              'sharper-top':(msgList[i-1] && msgList[i-1].userId == chat.userId),\n              'sharper-bottom':(msgList[i+1] && msgList[i+1].userId == chat.userId)}\">\n                <p text-wrap padding>{{chat.message}}</p>\n                <div class=\"corner-parent-left\">\n                  <div class=\"corner-child-left\">\n\n                  </div>\n                </div>\n              </div>\n            </ion-label>\n            <div class=\"imageAvatarBottom\">\n\n              <ion-avatar class=\"avatar\" [ngClass]=\"(msgList[i+1] && msgList[i+1].userId == chat.userId)?'hidden':''\">\n                <div class=\"imageAvatarBottomIcon\">\n                  <ion-icon name=\"add\" expand=\"icon-only\" color=\"light\"> </ion-icon>\n                </div>\n                <ion-img src=\"assets/chat6.jpg\"></ion-img>\n              </ion-avatar>\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row *ngIf=\"((msgList[i+1] && msgList[i+1].userId != chat.userId)|| !msgList[i+1])\">\n        <ion-col>\n          <ion-text>{{chat.time | date:'shortTime'}}</ion-text>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-list>\n\n  <!-- <ion-fab vertical=\"bottom\" horizontal=\"end\" edge slot=\"fixed\">\n    <ion-fab-button (click)=\"sendMsg()\" color=\"secondary\">\n      <ion-icon name=\"send\" expand=\"icon-only\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab> -->\n\n  <ion-row *ngIf=\"loader\">\n    <ion-col no-padding class=\"loading-col\">\n      <div class=\"imageAvatarRight\">\n        <div class=\"imageAvatarBottomLoader\">\n          <ion-avatar class=\"avatar\">\n            <div class=\"imageAvatarBottomIcon\">\n              <ion-icon name=\"add\" expand=\"icon-only\" color=\"light\"> </ion-icon>\n            </div>\n            <ion-img src=\"assets/chat6.jpg\"></ion-img>\n          </ion-avatar>\n        </div>\n        <ion-label>\n          <div class=\"chatDivLoader\">\n            <ion-spinner name=\"dots\" color=\"light\"></ion-spinner>\n            <!-- <ion-img src=\"../../assets/chat/loader.gif\"></ion-img> -->\n            <!-- <p text-wrap padding> {{paramData.name || 'Pam'}} is typing...</p> -->\n            <div class=\"corner-parent-right\">\n              <div class=\"corner-child-right\">\n\n              </div>\n            </div>\n          </div>\n        </ion-label>\n      </div>\n    </ion-col>\n  </ion-row>\n  <!-- </div> -->\n</ion-content>\n<ion-footer>\n  <ion-item>\n    <!-- <ion-icon slot=\"start\" name=\"camera\" expand=\"icon-only\" class=\"footerIcon\"></ion-icon> -->\n    <ion-textarea autoGrow autofocus placeholder=\"Write a message...\" rows=\"1\" [(ngModel)]=\"user_input\"\n      (ionFocus)=\"userTyping($event)\"></ion-textarea>\n    <!-- <ion-fab slot=\"end\"> -->\n    <ion-fab-button (click)=\"sendMsg()\" color=\"secondary\" slot=\"end\">\n      <ion-icon name=\"send\" expand=\"icon-only\"></ion-icon>\n    </ion-fab-button>\n    <!-- </ion-fab> -->\n  </ion-item>\n</ion-footer>"

/***/ }),

/***/ "./src/app/chat/chat.module.ts":
/*!*************************************!*\
  !*** ./src/app/chat/chat.module.ts ***!
  \*************************************/
/*! exports provided: ChatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageModule", function() { return ChatPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./chat.page */ "./src/app/chat/chat.page.ts");







// import { Keyboard } from '@ionic-native/keyboard/ngx';
var routes = [
    {
        path: '',
        component: _chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]
    }
];
var ChatPageModule = /** @class */ (function () {
    function ChatPageModule() {
    }
    ChatPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]],
        })
    ], ChatPageModule);
    return ChatPageModule;
}());



/***/ }),

/***/ "./src/app/chat/chat.page.scss":
/*!*************************************!*\
  !*** ./src/app/chat/chat.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-toolbar {\n  --color: #ffffff;\n  --background: linear-gradient(to right, #329cd1, #008dd3, #007cd2, #1c6ace, #4055c5);\n}\n\nion-list {\n  background: transparent;\n  padding-top: 20px;\n  margin-bottom: 0;\n}\n\n.imageAvatarRight {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: end;\n          align-items: flex-end;\n}\n\n.imageAvatarRight .imageAvatarBottom {\n  max-width: 214px;\n  -webkit-box-align: end;\n          align-items: flex-end;\n  display: -webkit-box;\n  display: flex;\n  border-radius: 50%;\n  position: relative;\n  top: 6px;\n  z-index: 11;\n}\n\n.imageAvatarRight .avatar {\n  width: 50px;\n  height: 50px;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n}\n\n.imageAvatarRight .imageAvatarBottomIcon {\n  width: 12px;\n  height: 12px;\n  border-radius: 50px;\n  background: var(--ion-color-tertiary-tint);\n  position: absolute;\n  top: 17%;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  right: 10.1%;\n  color: white;\n}\n\n.imageAvatarRight .chatDiv {\n  text-align: justify;\n  position: relative;\n  background: #9164ac7a;\n  border-radius: 6px;\n  border-bottom-left-radius: 0;\n  max-width: 72vw;\n  box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);\n}\n\n.imageAvatarRight ion-img {\n  width: 35px;\n  height: 35px;\n}\n\n.imageAvatarRight .sharper.chatDiv {\n  border-radius: 0;\n}\n\n.imageAvatarRight .sharper-bottom.chatDiv {\n  border-bottom-left-radius: 0;\n  border-bottom-right-radius: 0;\n}\n\n.imageAvatarRight .sharper-top.chatDiv {\n  border-top-left-radius: 0;\n  border-top-right-radius: 0;\n}\n\n.imageAvatarLeft {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n  -webkit-box-align: end;\n          align-items: flex-end;\n}\n\n.imageAvatarLeft .imageAvatarBottom {\n  max-width: 214px;\n  -webkit-box-align: end;\n          align-items: flex-end;\n  display: -webkit-box;\n  display: flex;\n  border-radius: 50%;\n  position: relative;\n  top: 6px;\n  z-index: 11;\n}\n\n.imageAvatarLeft .avatar {\n  width: 50px;\n  height: 50px;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n}\n\n.imageAvatarLeft .imageAvatarBottomIcon {\n  width: 12px;\n  height: 12px;\n  border-radius: 50px;\n  background: var(--ion-color-tertiary-tint);\n  position: absolute;\n  top: 17%;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  right: 10.1%;\n  color: white;\n}\n\n.imageAvatarLeft .chatDiv {\n  text-align: left;\n  position: relative;\n  background: #20b1d4;\n  border-radius: 6px;\n  border-bottom-right-radius: 0;\n  max-width: 72vw;\n  box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);\n}\n\n.imageAvatarLeft ion-img {\n  width: 35px;\n  height: 35px;\n}\n\n.imageAvatarLeft .sharper.chatDiv {\n  border-radius: 0;\n}\n\n.imageAvatarLeft .sharper-bottom.chatDiv {\n  border-bottom-left-radius: 0;\n  border-bottom-right-radius: 0;\n}\n\n.imageAvatarLeft .sharper-top.chatDiv {\n  border-top-left-radius: 0;\n  border-top-right-radius: 0;\n}\n\nion-fab-button {\n  width: 47px !important;\n  height: 47px !important;\n}\n\nion-input {\n  --placeholder-color: gray;\n}\n\n.chatDivLoader {\n  position: relative;\n  background: #9164ac7a;\n  border-radius: 16px;\n  min-width: 80px;\n  max-width: 80px;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: center;\n          align-items: center;\n  color: white;\n  padding: 2px;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.chatDivLoader .corner-parent-right {\n  background: #9164ac7a;\n  height: 10px;\n  width: 10px;\n  position: absolute;\n  left: -4px;\n  bottom: -2px;\n  border-radius: 0;\n  border-radius: 5px;\n}\n\n.chatDivLoader .corner-parent-right .corner-child-right {\n  width: 6px;\n  height: 6px;\n  border-radius: 3px;\n  background: #9164ac7a;\n  position: absolute;\n  top: 7px;\n  left: -7px;\n}\n\nion-text {\n  font-size: 12px;\n  color: #d7d8dac7;\n}\n\nion-col {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n}\n\n.imageAvatarBottomLoader {\n  max-width: 214px;\n  -webkit-box-align: end;\n          align-items: flex-end;\n  display: -webkit-box;\n  display: flex;\n  border-radius: 50%;\n  position: relative;\n  top: 8px;\n  z-index: 11;\n}\n\n.hidden {\n  visibility: hidden;\n}\n\n.loading-col {\n  -webkit-box-pack: start;\n          justify-content: flex-start;\n  padding-left: 20px;\n}\n\n.right {\n  -webkit-box-pack: start;\n          justify-content: flex-start;\n  padding-left: 20px;\n}\n\n.left {\n  -webkit-box-pack: end;\n          justify-content: flex-end;\n  padding-right: 20px;\n}\n\n.clubbed .imageAvatarRight .chatDiv .corner-parent-right {\n  height: 30px;\n  width: 30px;\n  background: transparent;\n  position: absolute;\n  left: -30px;\n  bottom: 0;\n  overflow: hidden;\n  border-radius: 0;\n}\n\n.clubbed .imageAvatarRight .chatDiv .corner-parent-right .corner-child-right {\n  width: 30px;\n  height: 30px;\n  border-radius: 15px;\n  background: transparent;\n  box-shadow: 15px 12px 0 0px #9164ac7a;\n}\n\n.clubbed .imageAvatarLeft .chatDiv .corner-parent-left {\n  height: 30px;\n  width: 30px;\n  background: transparent;\n  position: absolute;\n  right: -30px;\n  bottom: 0;\n  overflow: hidden;\n  border-radius: 0;\n}\n\n.clubbed .imageAvatarLeft .chatDiv .corner-parent-left .corner-child-left {\n  width: 30px;\n  height: 30px;\n  border-radius: 15px;\n  background: transparent;\n  box-shadow: -15px 12px 0 0px #20b1d4;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvdHJhY2tpbmctZ2F1cmRpYW4vc3JjL2FwcC9jaGF0L2NoYXQucGFnZS5zY3NzIiwic3JjL2FwcC9jaGF0L2NoYXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQUE7RUFDQSxvRkFBQTtBQ0NGOztBRGFBO0VBQ0UsdUJBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FDVkY7O0FEWUE7RUFDRSxvQkFBQTtFQUFBLGFBQUE7RUFDQSx3QkFBQTtVQUFBLHVCQUFBO0VBQ0Esc0JBQUE7VUFBQSxxQkFBQTtBQ1RGOztBRFVFO0VBQ0UsZ0JBQUE7RUFDQSxzQkFBQTtVQUFBLHFCQUFBO0VBQ0Esb0JBQUE7RUFBQSxhQUFBO0VBQ0Esa0JBQUE7RUFFQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0FDVEo7O0FEV0U7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG9CQUFBO0VBQUEsYUFBQTtFQUNBLHdCQUFBO1VBQUEsdUJBQUE7RUFDQSx5QkFBQTtVQUFBLG1CQUFBO0FDVEo7O0FEV0U7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsMENBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxvQkFBQTtFQUFBLGFBQUE7RUFDQSx3QkFBQTtVQUFBLHVCQUFBO0VBQ0EseUJBQUE7VUFBQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FDVEo7O0FEV0U7RUFDRSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtFQUNBLDRCQUFBO0VBR0EsZUFBQTtFQUNBLDJDQUFBO0FDWEo7O0FEYUU7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQ1hKOztBRGNJO0VBQ0UsZ0JBQUE7QUNaTjs7QURnQkk7RUFDRSw0QkFBQTtFQUNBLDZCQUFBO0FDZE47O0FEa0JJO0VBQ0UseUJBQUE7RUFDQSwwQkFBQTtBQ2hCTjs7QURxQkE7RUFDRSxvQkFBQTtFQUFBLGFBQUE7RUFDQSxxQkFBQTtVQUFBLHlCQUFBO0VBQ0Esc0JBQUE7VUFBQSxxQkFBQTtBQ2xCRjs7QURtQkU7RUFDRSxnQkFBQTtFQUNBLHNCQUFBO1VBQUEscUJBQUE7RUFDQSxvQkFBQTtFQUFBLGFBQUE7RUFDQSxrQkFBQTtFQUVBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7QUNsQko7O0FEb0JFO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtFQUFBLGFBQUE7RUFDQSx3QkFBQTtVQUFBLHVCQUFBO0VBQ0EseUJBQUE7VUFBQSxtQkFBQTtBQ2xCSjs7QURvQkU7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsMENBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxvQkFBQTtFQUFBLGFBQUE7RUFDQSx3QkFBQTtVQUFBLHVCQUFBO0VBQ0EseUJBQUE7VUFBQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FDbEJKOztBRG9CRTtFQUNFLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFHQSxlQUFBO0VBQ0EsMENBQUE7QUNwQko7O0FEc0JFO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUNwQko7O0FEdUJJO0VBQ0UsZ0JBQUE7QUNyQk47O0FEeUJJO0VBQ0UsNEJBQUE7RUFDQSw2QkFBQTtBQ3ZCTjs7QUQyQkk7RUFDRSx5QkFBQTtFQUNBLDBCQUFBO0FDekJOOztBRDZCQTtFQUNFLHNCQUFBO0VBQ0EsdUJBQUE7QUMxQkY7O0FEa0NBO0VBQ0UseUJBQUE7QUMvQkY7O0FEaUNBO0VBQ0Usa0JBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxvQkFBQTtFQUFBLGFBQUE7RUFDQSx5QkFBQTtVQUFBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtBQzlCRjs7QUQrQkU7RUFDRSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUM3Qko7O0FEOEJJO0VBQ0UsVUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtBQzVCTjs7QURnQ0E7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7QUM3QkY7O0FEK0JBO0VBQ0Usb0JBQUE7RUFBQSxhQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtFQUNBLHlCQUFBO1VBQUEsbUJBQUE7QUM1QkY7O0FEOEJBO0VBQ0UsZ0JBQUE7RUFDQSxzQkFBQTtVQUFBLHFCQUFBO0VBQ0Esb0JBQUE7RUFBQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0FDM0JGOztBRDZCQTtFQUNFLGtCQUFBO0FDMUJGOztBRDRCQTtFQUNFLHVCQUFBO1VBQUEsMkJBQUE7RUFDQSxrQkFBQTtBQ3pCRjs7QUQ0QkE7RUFDRSx1QkFBQTtVQUFBLDJCQUFBO0VBQ0Esa0JBQUE7QUN6QkY7O0FEMkJBO0VBQ0UscUJBQUE7VUFBQSx5QkFBQTtFQUNBLG1CQUFBO0FDeEJGOztBRDhCTTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQzNCUjs7QUQ0QlE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxxQ0FBQTtBQzFCVjs7QURpQ007RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUMvQlI7O0FEZ0NRO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0Esb0NBQUE7QUM5QlYiLCJmaWxlIjoic3JjL2FwcC9jaGF0L2NoYXQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXIge1xuICAtLWNvbG9yOiAjZmZmZmZmO1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzMyOWNkMSwgIzAwOGRkMywgIzAwN2NkMiwgIzFjNmFjZSwgIzQwNTVjNSk7XG59XG5cbi8vIGlvbi1jb250ZW50IHtcbi8vICAgLS1iYWNrZ3JvdW5kOiByYWRpYWwtZ3JhZGllbnQoYXQgMCA1MCUscmdiKDE3MywgNDIsIDE2MiksIzVCMjU3Myk7XG4vLyB9XG4vLyBpb24taGVhZGVye1xuLy8gICAtLWJhY2tncm91bmQ6ICM1QjI1NzNcbi8vIH1cbi8vIGlvbi10b29sYmFyLFxuLy8gaW9uLWl0ZW0ge1xuLy8gICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuLy8gfVxuXG5pb24tbGlzdCB7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBwYWRkaW5nLXRvcDogMjBweDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbi5pbWFnZUF2YXRhclJpZ2h0IHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgLmltYWdlQXZhdGFyQm90dG9tIHtcbiAgICBtYXgtd2lkdGg6IDIxNHB4O1xuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAvLyBiYWNrZ3JvdW5kOiAjNUIyNTczO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0b3A6IDZweDtcbiAgICB6LWluZGV4OiAxMTtcbiAgfVxuICAuYXZhdGFyIHtcbiAgICB3aWR0aDogNTBweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG4gIC5pbWFnZUF2YXRhckJvdHRvbUljb24ge1xuICAgIHdpZHRoOiAxMnB4O1xuICAgIGhlaWdodDogMTJweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci10ZXJ0aWFyeS10aW50KTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAxNyU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHJpZ2h0OiAxMC4xJTtcbiAgICBjb2xvcjogd2hpdGU7XG4gIH1cbiAgLmNoYXREaXYge1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGJhY2tncm91bmQ6ICM5MTY0YWM3YTtcbiAgICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMDtcbiAgICAvLyBtYXJnaW4tYm90dG9tOiAxNHB4O1xuICAgIC8vIG1pbi13aWR0aDogNzJ2dztcbiAgICBtYXgtd2lkdGg6IDcydnc7XG4gICAgYm94LXNoYWRvdzogLTFweCAxcHggNXB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgfVxuICBpb24taW1nIHtcbiAgICB3aWR0aDogMzVweDtcbiAgICBoZWlnaHQ6IDM1cHg7XG4gIH1cbiAgLnNoYXJwZXIge1xuICAgICYuY2hhdERpdiB7XG4gICAgICBib3JkZXItcmFkaXVzOiAwO1xuICAgIH1cbiAgfVxuICAuc2hhcnBlci1ib3R0b20ge1xuICAgICYuY2hhdERpdiB7XG4gICAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAwO1xuICAgICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDA7XG4gICAgfVxuICB9XG4gIC5zaGFycGVyLXRvcCB7XG4gICAgJi5jaGF0RGl2IHtcbiAgICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDA7XG4gICAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMDtcbiAgICB9XG4gIH1cbn1cblxuLmltYWdlQXZhdGFyTGVmdCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgLmltYWdlQXZhdGFyQm90dG9tIHtcbiAgICBtYXgtd2lkdGg6IDIxNHB4O1xuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAvLyBiYWNrZ3JvdW5kOiAjNUIyNTczO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0b3A6IDZweDtcbiAgICB6LWluZGV4OiAxMTtcbiAgfVxuICAuYXZhdGFyIHtcbiAgICB3aWR0aDogNTBweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG4gIC5pbWFnZUF2YXRhckJvdHRvbUljb24ge1xuICAgIHdpZHRoOiAxMnB4O1xuICAgIGhlaWdodDogMTJweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci10ZXJ0aWFyeS10aW50KTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAxNyU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHJpZ2h0OiAxMC4xJTtcbiAgICBjb2xvcjogd2hpdGU7XG4gIH1cbiAgLmNoYXREaXYge1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGJhY2tncm91bmQ6IHJnYigzMiwgMTc3LCAyMTIpO1xuICAgIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMDtcbiAgICAvLyBtYXJnaW4tYm90dG9tOiAxNHB4O1xuICAgIC8vIG1pbi13aWR0aDogNzJ2dztcbiAgICBtYXgtd2lkdGg6IDcydnc7XG4gICAgYm94LXNoYWRvdzogMXB4IDFweCA1cHggcmdiYSgwLCAwLCAwLCAwLjIpO1xuICB9XG4gIGlvbi1pbWcge1xuICAgIHdpZHRoOiAzNXB4O1xuICAgIGhlaWdodDogMzVweDtcbiAgfVxuICAuc2hhcnBlciB7XG4gICAgJi5jaGF0RGl2IHtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gICAgfVxuICB9XG4gIC5zaGFycGVyLWJvdHRvbSB7XG4gICAgJi5jaGF0RGl2IHtcbiAgICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDA7XG4gICAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMDtcbiAgICB9XG4gIH1cbiAgLnNoYXJwZXItdG9wIHtcbiAgICAmLmNoYXREaXYge1xuICAgICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMDtcbiAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAwO1xuICAgIH1cbiAgfVxufVxuaW9uLWZhYi1idXR0b24ge1xuICB3aWR0aDogNDdweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDQ3cHggIWltcG9ydGFudDtcbn1cbi8vIGlvbi1mYWIge1xuLy8gICBib3R0b206IC0yMnB4ICFpbXBvcnRhbnQ7XG4vLyB9XG4vLyAuZm9vdGVySWNvbiB7XG4vLyAgIGNvbG9yOiByZ2JhKDI0NCwgMjQ1LCAyNDgsIDAuNyk7XG4vLyB9XG5pb24taW5wdXQge1xuICAtLXBsYWNlaG9sZGVyLWNvbG9yOiBncmF5O1xufVxuLmNoYXREaXZMb2FkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJhY2tncm91bmQ6ICM5MTY0YWM3YTtcbiAgYm9yZGVyLXJhZGl1czogMTZweDtcbiAgbWluLXdpZHRoOiA4MHB4O1xuICBtYXgtd2lkdGg6IDgwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogMnB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgLmNvcm5lci1wYXJlbnQtcmlnaHQge1xuICAgIGJhY2tncm91bmQ6ICM5MTY0YWM3YTtcbiAgICBoZWlnaHQ6IDEwcHg7XG4gICAgd2lkdGg6IDEwcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IC00cHg7XG4gICAgYm90dG9tOiAtMnB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIC5jb3JuZXItY2hpbGQtcmlnaHQge1xuICAgICAgd2lkdGg6IDZweDtcbiAgICAgIGhlaWdodDogNnB4O1xuICAgICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgYmFja2dyb3VuZDogIzkxNjRhYzdhO1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgdG9wOiA3cHg7XG4gICAgICBsZWZ0OiAtN3B4O1xuICAgIH1cbiAgfVxufVxuaW9uLXRleHQge1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjZDdkOGRhYzc7XG59XG5pb24tY29sIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uaW1hZ2VBdmF0YXJCb3R0b21Mb2FkZXIge1xuICBtYXgtd2lkdGg6IDIxNHB4O1xuICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IDhweDtcbiAgei1pbmRleDogMTE7XG59XG4uaGlkZGVuIHtcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xufVxuLmxvYWRpbmctY29sIHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG59XG5cbi5yaWdodCB7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xufVxuLmxlZnQge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xufVxuXG4uY2x1YmJlZCB7XG4gIC5pbWFnZUF2YXRhclJpZ2h0IHtcbiAgICAuY2hhdERpdiB7XG4gICAgICAuY29ybmVyLXBhcmVudC1yaWdodCB7XG4gICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGxlZnQ6IC0zMHB4O1xuICAgICAgICBib3R0b206IDA7XG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gICAgICAgIC5jb3JuZXItY2hpbGQtcmlnaHQge1xuICAgICAgICAgIHdpZHRoOiAzMHB4O1xuICAgICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgICAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgICAgICAgIGJveC1zaGFkb3c6IDE1cHggMTJweCAwIDBweCAjOTE2NGFjN2E7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgLmltYWdlQXZhdGFyTGVmdCB7XG4gICAgLmNoYXREaXYge1xuICAgICAgLmNvcm5lci1wYXJlbnQtbGVmdCB7XG4gICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHJpZ2h0OiAtMzBweDtcbiAgICAgICAgYm90dG9tOiAwO1xuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xuICAgICAgICAuY29ybmVyLWNoaWxkLWxlZnQge1xuICAgICAgICAgIHdpZHRoOiAzMHB4O1xuICAgICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgICAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgICAgICAgIGJveC1zaGFkb3c6IC0xNXB4IDEycHggMCAwcHggIzIwYjFkNDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG4vLyAubWVzc2FnZXMtY29udGFpbmVyIHtcbi8vICAgbWluLWhlaWdodDogMTAwJTtcbi8vICAgd2lkdGg6IDEwMCU7XG4vLyAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbi8vICAgZGlzcGxheTogZmxleDtcbi8vICAgb3ZlcmZsb3c6IGhpZGRlbjtcbi8vICAgbWFyZ2luOiAwIGF1dG87XG4vLyB9XG4vLyAuaW5wdXQtYm94e1xuLy8gICBjb2xvcjogd2hpdGU7XG4vLyB9XG4iLCJpb24tdG9vbGJhciB7XG4gIC0tY29sb3I6ICNmZmZmZmY7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMzI5Y2QxLCAjMDA4ZGQzLCAjMDA3Y2QyLCAjMWM2YWNlLCAjNDA1NWM1KTtcbn1cblxuaW9uLWxpc3Qge1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgcGFkZGluZy10b3A6IDIwcHg7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG5cbi5pbWFnZUF2YXRhclJpZ2h0IHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbn1cbi5pbWFnZUF2YXRhclJpZ2h0IC5pbWFnZUF2YXRhckJvdHRvbSB7XG4gIG1heC13aWR0aDogMjE0cHg7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgZGlzcGxheTogZmxleDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogNnB4O1xuICB6LWluZGV4OiAxMTtcbn1cbi5pbWFnZUF2YXRhclJpZ2h0IC5hdmF0YXIge1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiA1MHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5pbWFnZUF2YXRhclJpZ2h0IC5pbWFnZUF2YXRhckJvdHRvbUljb24ge1xuICB3aWR0aDogMTJweDtcbiAgaGVpZ2h0OiAxMnB4O1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItdGVydGlhcnktdGludCk7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAxNyU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTAuMSU7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5pbWFnZUF2YXRhclJpZ2h0IC5jaGF0RGl2IHtcbiAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBiYWNrZ3JvdW5kOiAjOTE2NGFjN2E7XG4gIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMDtcbiAgbWF4LXdpZHRoOiA3MnZ3O1xuICBib3gtc2hhZG93OiAtMXB4IDFweCA1cHggcmdiYSgwLCAwLCAwLCAwLjIpO1xufVxuLmltYWdlQXZhdGFyUmlnaHQgaW9uLWltZyB7XG4gIHdpZHRoOiAzNXB4O1xuICBoZWlnaHQ6IDM1cHg7XG59XG4uaW1hZ2VBdmF0YXJSaWdodCAuc2hhcnBlci5jaGF0RGl2IHtcbiAgYm9yZGVyLXJhZGl1czogMDtcbn1cbi5pbWFnZUF2YXRhclJpZ2h0IC5zaGFycGVyLWJvdHRvbS5jaGF0RGl2IHtcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMDtcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDA7XG59XG4uaW1hZ2VBdmF0YXJSaWdodCAuc2hhcnBlci10b3AuY2hhdERpdiB7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDA7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAwO1xufVxuXG4uaW1hZ2VBdmF0YXJMZWZ0IHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xufVxuLmltYWdlQXZhdGFyTGVmdCAuaW1hZ2VBdmF0YXJCb3R0b20ge1xuICBtYXgtd2lkdGg6IDIxNHB4O1xuICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IDZweDtcbiAgei1pbmRleDogMTE7XG59XG4uaW1hZ2VBdmF0YXJMZWZ0IC5hdmF0YXIge1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiA1MHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5pbWFnZUF2YXRhckxlZnQgLmltYWdlQXZhdGFyQm90dG9tSWNvbiB7XG4gIHdpZHRoOiAxMnB4O1xuICBoZWlnaHQ6IDEycHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci10ZXJ0aWFyeS10aW50KTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDE3JTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHJpZ2h0OiAxMC4xJTtcbiAgY29sb3I6IHdoaXRlO1xufVxuLmltYWdlQXZhdGFyTGVmdCAuY2hhdERpdiB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZDogIzIwYjFkNDtcbiAgYm9yZGVyLXJhZGl1czogNnB4O1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMDtcbiAgbWF4LXdpZHRoOiA3MnZ3O1xuICBib3gtc2hhZG93OiAxcHggMXB4IDVweCByZ2JhKDAsIDAsIDAsIDAuMik7XG59XG4uaW1hZ2VBdmF0YXJMZWZ0IGlvbi1pbWcge1xuICB3aWR0aDogMzVweDtcbiAgaGVpZ2h0OiAzNXB4O1xufVxuLmltYWdlQXZhdGFyTGVmdCAuc2hhcnBlci5jaGF0RGl2IHtcbiAgYm9yZGVyLXJhZGl1czogMDtcbn1cbi5pbWFnZUF2YXRhckxlZnQgLnNoYXJwZXItYm90dG9tLmNoYXREaXYge1xuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAwO1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMDtcbn1cbi5pbWFnZUF2YXRhckxlZnQgLnNoYXJwZXItdG9wLmNoYXREaXYge1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAwO1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMDtcbn1cblxuaW9uLWZhYi1idXR0b24ge1xuICB3aWR0aDogNDdweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDQ3cHggIWltcG9ydGFudDtcbn1cblxuaW9uLWlucHV0IHtcbiAgLS1wbGFjZWhvbGRlci1jb2xvcjogZ3JheTtcbn1cblxuLmNoYXREaXZMb2FkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJhY2tncm91bmQ6ICM5MTY0YWM3YTtcbiAgYm9yZGVyLXJhZGl1czogMTZweDtcbiAgbWluLXdpZHRoOiA4MHB4O1xuICBtYXgtd2lkdGg6IDgwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogMnB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5jaGF0RGl2TG9hZGVyIC5jb3JuZXItcGFyZW50LXJpZ2h0IHtcbiAgYmFja2dyb3VuZDogIzkxNjRhYzdhO1xuICBoZWlnaHQ6IDEwcHg7XG4gIHdpZHRoOiAxMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IC00cHg7XG4gIGJvdHRvbTogLTJweDtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuLmNoYXREaXZMb2FkZXIgLmNvcm5lci1wYXJlbnQtcmlnaHQgLmNvcm5lci1jaGlsZC1yaWdodCB7XG4gIHdpZHRoOiA2cHg7XG4gIGhlaWdodDogNnB4O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIGJhY2tncm91bmQ6ICM5MTY0YWM3YTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgbGVmdDogLTdweDtcbn1cblxuaW9uLXRleHQge1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjZDdkOGRhYzc7XG59XG5cbmlvbi1jb2wge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLmltYWdlQXZhdGFyQm90dG9tTG9hZGVyIHtcbiAgbWF4LXdpZHRoOiAyMTRweDtcbiAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICBkaXNwbGF5OiBmbGV4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdG9wOiA4cHg7XG4gIHotaW5kZXg6IDExO1xufVxuXG4uaGlkZGVuIHtcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xufVxuXG4ubG9hZGluZy1jb2wge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gIHBhZGRpbmctbGVmdDogMjBweDtcbn1cblxuLnJpZ2h0IHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG59XG5cbi5sZWZ0IHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmNsdWJiZWQgLmltYWdlQXZhdGFyUmlnaHQgLmNoYXREaXYgLmNvcm5lci1wYXJlbnQtcmlnaHQge1xuICBoZWlnaHQ6IDMwcHg7XG4gIHdpZHRoOiAzMHB4O1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAtMzBweDtcbiAgYm90dG9tOiAwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBib3JkZXItcmFkaXVzOiAwO1xufVxuLmNsdWJiZWQgLmltYWdlQXZhdGFyUmlnaHQgLmNoYXREaXYgLmNvcm5lci1wYXJlbnQtcmlnaHQgLmNvcm5lci1jaGlsZC1yaWdodCB7XG4gIHdpZHRoOiAzMHB4O1xuICBoZWlnaHQ6IDMwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3gtc2hhZG93OiAxNXB4IDEycHggMCAwcHggIzkxNjRhYzdhO1xufVxuLmNsdWJiZWQgLmltYWdlQXZhdGFyTGVmdCAuY2hhdERpdiAuY29ybmVyLXBhcmVudC1sZWZ0IHtcbiAgaGVpZ2h0OiAzMHB4O1xuICB3aWR0aDogMzBweDtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IC0zMHB4O1xuICBib3R0b206IDA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJvcmRlci1yYWRpdXM6IDA7XG59XG4uY2x1YmJlZCAuaW1hZ2VBdmF0YXJMZWZ0IC5jaGF0RGl2IC5jb3JuZXItcGFyZW50LWxlZnQgLmNvcm5lci1jaGlsZC1sZWZ0IHtcbiAgd2lkdGg6IDMwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIGJveC1zaGFkb3c6IC0xNXB4IDEycHggMCAwcHggIzIwYjFkNDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/chat/chat.page.ts":
/*!***********************************!*\
  !*** ./src/app/chat/chat.page.ts ***!
  \***********************************/
/*! exports provided: ChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPage", function() { return ChatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! socket.io-client */ "./node_modules/socket.io-client/lib/index.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_6__);







// import { Keyboard } from '@ionic-native/keyboard/ngx';
// declare var $: any;
var ChatPage = /** @class */ (function () {
    function ChatPage(activRoute, apiCall, loaderController) {
        this.activRoute = activRoute;
        this.apiCall = apiCall;
        this.loaderController = loaderController;
        this.paramData = {};
        this.msgList = [];
        this.user_input = "";
        this.innerWidth = window.innerWidth;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.fromDate = moment__WEBPACK_IMPORTED_MODULE_5__({ hours: 0 }).format();
        this.toDate = moment__WEBPACK_IMPORTED_MODULE_5__().format();
        if (this.activRoute.snapshot.data['special']) {
            this.paramData = this.activRoute.snapshot.data['special'];
            this.userName = this.paramData.fisrt_name;
            console.log("param data: ", this.paramData);
            this.User = this.islogin._id;
            this.toUser = this.paramData._id;
        }
    }
    ChatPage.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.content.scrollToBottom();
        // This element never changes.
        // let ionapp = document.getElementsByTagName("ion-app")[0];
        window.addEventListener('keyboardDidShow', function (event) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var kbHeight;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                kbHeight = event["keyboardHeight"];
                this.kbHeight = kbHeight;
                return [2 /*return*/];
            });
        }); });
        window.addEventListener('keyboardWillHide', function () {
            // this.keyboard.show();
            // Describe your logic which will be run each time when keyboard is about to be closed.
        });
    };
    ChatPage.prototype.ngOnInit = function () {
        this.openChatSocket();
        this.getChatHistory();
        // this.innerWidth = window.innerWidth;
        // console.log("window test: ", this.innerWidth)
    };
    ChatPage.prototype.openChatSocket = function () {
        this._io = socket_io_client__WEBPACK_IMPORTED_MODULE_6__('https://www.oneqlik.in/userChat', {
            transports: ['websocket']
        });
        this._io.on('connect', function (data) {
            console.log("userChat connect data: ", data);
        });
        var that = this;
        that._io.on(that.User + "-" + that.toUser, function (d4) {
            if (d4 != undefined)
                (function (data) {
                    if (data == undefined) {
                        return;
                    }
                    that.loader = true;
                    setTimeout(function () {
                        that.msgList.push({
                            userId: that.User,
                            userName: that.User,
                            // userAvatar: "../../assets/chat/chat5.jpg",
                            time: new Date().toISOString(),
                            message: data
                        });
                        that.loader = false;
                        that.scrollDown();
                    }, 2000);
                    that.scrollDown();
                })(d4);
        });
    };
    ChatPage.prototype.getChatHistory = function () {
        var _this = this;
        var that = this;
        this.loaderController.create({
            message: 'please wait we are fetching history...'
        }).then(function (loadEl) {
            loadEl.present();
            var url = _this.apiCall.mainUrl + "broadcastNotification/getchatmsg?from=" + _this.islogin._id + "&to=" + that.paramData._id;
            _this.apiCall.getdevicesForAllVehiclesApi(url)
                .subscribe(function (respData) {
                loadEl.dismiss();
                if (respData) {
                    var res = JSON.parse(JSON.stringify(respData));
                    for (var i = 0; i < res.length; i++) {
                        if (res[i].sender === _this.toUser) {
                            _this.msgList.push({
                                userId: _this.User,
                                userName: _this.User,
                                time: res[i].timestamp,
                                message: res[i].message,
                                id: _this.msgList.length + 1
                            });
                        }
                        else {
                            if (res[i].sender === _this.User) {
                                _this.msgList.push({
                                    userId: _this.toUser,
                                    userName: _this.toUser,
                                    time: res[i].timestamp,
                                    message: res[i].message,
                                    id: _this.msgList.length + 1
                                });
                            }
                        }
                    }
                    setTimeout(function () {
                        _this.content.scrollToBottom(100);
                    }, 50);
                    // this.scrollDown();
                }
            }, function (err) {
                loadEl.dismiss();
                console.log("chat err: ", err);
            });
        });
    };
    ChatPage.prototype.sendMsg = function () {
        if (this.user_input !== '') {
            this.msgList.push({
                userId: this.toUser,
                userName: this.toUser,
                time: new Date().toISOString(),
                message: this.user_input,
                id: this.msgList.length + 1
            });
            var that = this;
            that._io.emit('send', this.User, this.toUser, this.user_input); // three parameters, from(who is sending msg), to(to whom ur sending msg), msg(message string)
            this.user_input = "";
            // setTimeout(() => {
            //   this.content.scrollToBottom(100);
            // }, 50);
            // this.scrollDown();
            // setTimeout(() => {
            //   this.senderSends()
            // }, 500);
        }
    };
    // senderSends() {
    //   this.loader = true;
    //   setTimeout(() => {
    //     this.msgList.push({
    //       userId: this.User,
    //       userName: this.User,
    //       // userAvatar: "../../assets/chat/chat5.jpg",
    //       time: new Date().toISOString(),
    //       message: "Sorry, didn't get what you said. Can you repeat that please"
    //     });
    //     this.loader = false;
    //     this.scrollDown()
    //   }, 2000)
    //   this.scrollDown()
    // }
    ChatPage.prototype.scrollDown = function () {
        var _this = this;
        setTimeout(function () {
            console.log("window height: ", _this.innerWidth);
            console.log("keyboard height: ", _this.kbHeight);
            // this.content.scrollToBottom(this.innerWidth)
            _this.content.scrollToPoint(1500, 1500, 500).then(function (res) {
                console.log("scrollToPoint: ", res);
            });
        }, 50);
    };
    ChatPage.prototype.userTyping = function (event) {
        debugger;
        console.log(event);
        this.start_typing = event.target.value;
        this.scrollDown();
    };
    ChatPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('IonContent', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"])
    ], ChatPage.prototype, "content", void 0);
    ChatPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-chat',
            template: __webpack_require__(/*! raw-loader!./chat.page.html */ "./node_modules/raw-loader/index.js!./src/app/chat/chat.page.html"),
            styles: [__webpack_require__(/*! ./chat.page.scss */ "./src/app/chat/chat.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
    ], ChatPage);
    return ChatPage;
}());



/***/ })

}]);
//# sourceMappingURL=chat-chat-module-es5.js.map