(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["gpsone-gpsone-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/gpsone/gpsone.page.html":
/*!*******************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/gpsone/gpsone.page.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>gpsone</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n<ion-card style=\"margin: 20px 30px 30px 30px; height: 65%;\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"1\">\n\n       </ion-col> \n       <ion-col size=\"10\" style=\"margin-top: 30px;\">\n          <span style=\"margin: 30px 13px 30px 15px;\">To connect the watch to the app</span>\n        </ion-col>\n        <ion-col size=\"1\">\n\n          </ion-col>\n     </ion-row>\n     \n     <ion-row>\n        <ion-col size=\"\">\n        </ion-col>\n        <ion-col size=\"12\">\n          <p class=\"text\" style=\"font-size: large;\n          color: rgba(66, 172, 208, 1);\n          text-align: center;\n          margin: auto;\">\n            Please enter the ID or IMEI on back of the watch\n    </p>  \n        </ion-col>  \n        <ion-col size=\"0\">\n        </ion-col>\n      </ion-row>\n      <br>\n      <ion-button expand=\"block\" fill=\"clear\" style=\"width: 85%;\n      margin: auto; border: 1px solid;\">ID or IMEI</ion-button>\n\n      <ion-row> \n        <ion-col size=\"12\" style=\"margin-top: 20px; margin-top: 20px;\n        text-align: center;\">\n            <span>\n                <a href=\"https://www.oneqlik.in\" style=\"color: rgba(66, 172, 208, 1);\">Support to find ID or IMEI</a>\n            </span>  \n        </ion-col>\n      </ion-row>  \n  </ion-grid>  \n</ion-card>  \n</ion-content>\n"

/***/ }),

/***/ "./src/app/gpsone/gpsone.module.ts":
/*!*****************************************!*\
  !*** ./src/app/gpsone/gpsone.module.ts ***!
  \*****************************************/
/*! exports provided: GpsonePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GpsonePageModule", function() { return GpsonePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _gpsone_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./gpsone.page */ "./src/app/gpsone/gpsone.page.ts");







var routes = [
    {
        path: '',
        component: _gpsone_page__WEBPACK_IMPORTED_MODULE_6__["GpsonePage"]
    }
];
var GpsonePageModule = /** @class */ (function () {
    function GpsonePageModule() {
    }
    GpsonePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_gpsone_page__WEBPACK_IMPORTED_MODULE_6__["GpsonePage"]]
        })
    ], GpsonePageModule);
    return GpsonePageModule;
}());



/***/ }),

/***/ "./src/app/gpsone/gpsone.page.scss":
/*!*****************************************!*\
  !*** ./src/app/gpsone/gpsone.page.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dwc29uZS9ncHNvbmUucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/gpsone/gpsone.page.ts":
/*!***************************************!*\
  !*** ./src/app/gpsone/gpsone.page.ts ***!
  \***************************************/
/*! exports provided: GpsonePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GpsonePage", function() { return GpsonePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var GpsonePage = /** @class */ (function () {
    function GpsonePage() {
    }
    GpsonePage.prototype.ngOnInit = function () {
    };
    GpsonePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gpsone',
            template: __webpack_require__(/*! raw-loader!./gpsone.page.html */ "./node_modules/raw-loader/index.js!./src/app/gpsone/gpsone.page.html"),
            styles: [__webpack_require__(/*! ./gpsone.page.scss */ "./src/app/gpsone/gpsone.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], GpsonePage);
    return GpsonePage;
}());



/***/ })

}]);
//# sourceMappingURL=gpsone-gpsone-module-es5.js.map