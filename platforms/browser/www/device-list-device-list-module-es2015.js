(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["device-list-device-list-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/device-list/device-list.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/device-list/device-list.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Device List</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-row style=\"background: #e3e5e8;\">\n          <ion-col text-center>\n            <ion-button fill=\"clear\" expand=\"full\" (click)=\"addDevice()\">\n              <ion-icon slot=\"start\" name=\"add\"></ion-icon>\n              Add Device\n            </ion-button>\n          </ion-col>\n        </ion-row>\n        <ion-list *ngIf=\"deviceList.length > 0\">\n          <ion-item *ngFor=\"let item of deviceList\">\n            <ion-avatar slot=\"start\" class=\"ionAvtar\">\n              <img src=\"assets/Images/watch.png\">\n            </ion-avatar>\n            <ion-label>\n              <p style=\"font-size: 1.1em;\">{{item.Device_Name | titlecase}} {{item.last_name | titlecase}}</p>\n              <p>{{item.Device_ID}}</p>\n            </ion-label>\n            <ion-button slot=\"end\" size=\"default\" color=\"secondary\">Switch</ion-button>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/device-list/device-list.module.ts":
/*!***************************************************!*\
  !*** ./src/app/device-list/device-list.module.ts ***!
  \***************************************************/
/*! exports provided: DeviceListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceListPageModule", function() { return DeviceListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _device_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./device-list.page */ "./src/app/device-list/device-list.page.ts");







const routes = [
    {
        path: '',
        component: _device_list_page__WEBPACK_IMPORTED_MODULE_6__["DeviceListPage"]
    }
];
let DeviceListPageModule = class DeviceListPageModule {
};
DeviceListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_device_list_page__WEBPACK_IMPORTED_MODULE_6__["DeviceListPage"]]
    })
], DeviceListPageModule);



/***/ }),

/***/ "./src/app/device-list/device-list.page.scss":
/*!***************************************************!*\
  !*** ./src/app/device-list/device-list.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-toolbar {\n  --color: #ffffff;\n  --background: linear-gradient(to right, #329cd1, #008dd3, #007cd2, #1c6ace, #4055c5);\n}\n\n.ionAvtar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 55px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvdHJhY2tpbmctZ2F1cmRpYW4vc3JjL2FwcC9kZXZpY2UtbGlzdC9kZXZpY2UtbGlzdC5wYWdlLnNjc3MiLCJzcmMvYXBwL2RldmljZS1saXN0L2RldmljZS1saXN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFBO0VBQ0Esb0ZBQUE7QUNDRjs7QURDQTtFQUNFLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSwwQkFBQTtBQ0VGIiwiZmlsZSI6InNyYy9hcHAvZGV2aWNlLWxpc3QvZGV2aWNlLWxpc3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXIge1xuICAtLWNvbG9yOiAjZmZmZmZmO1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzMyOWNkMSwgIzAwOGRkMywgIzAwN2NkMiwgIzFjNmFjZSwgIzQwNTVjNSk7XG59XG4uaW9uQXZ0YXIge1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgbWF4LXdpZHRoOiA1NXB4ICFpbXBvcnRhbnQ7XG59XG4iLCJpb24tdG9vbGJhciB7XG4gIC0tY29sb3I6ICNmZmZmZmY7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMzI5Y2QxLCAjMDA4ZGQzLCAjMDA3Y2QyLCAjMWM2YWNlLCAjNDA1NWM1KTtcbn1cblxuLmlvbkF2dGFyIHtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogNTVweCAhaW1wb3J0YW50O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/device-list/device-list.page.ts":
/*!*************************************************!*\
  !*** ./src/app/device-list/device-list.page.ts ***!
  \*************************************************/
/*! exports provided: DeviceListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceListPage", function() { return DeviceListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let DeviceListPage = class DeviceListPage {
    constructor(apiCall, loadingController, router) {
        this.apiCall = apiCall;
        this.loadingController = loadingController;
        this.router = router;
        this.deviceList = [];
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    }
    ngOnInit() {
    }
    ionViewDidEnter() {
        this.userDevices();
    }
    userDevices() {
        var baseURLp;
        let that = this;
        baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.userdetails._id + '&email=' + this.userdetails.email;
        if (this.userdetails.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.userdetails._id;
            // this.isSuperAdmin = true;
        }
        else {
            if (this.userdetails.isDealer == true) {
                baseURLp += '&dealer=' + this.userdetails._id;
                // this.isDealer = true;
            }
        }
        that.loadingController.create({
            message: 'Please wait...'
        }).then((loadEl => {
            loadEl.present();
            that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
                .subscribe(respData => {
                loadEl.dismiss();
                var resp = JSON.parse(JSON.stringify(respData));
                this.deviceList = resp.devices;
            }, err => {
                loadEl.dismiss();
                // that.apiCall.stopLoading();
                console.log(err);
            });
        }));
    }
    addDevice() {
        // this.router.navigateByUrl('/device-list');
        this.router.navigateByUrl('/add-device');
    }
};
DeviceListPage.ctorParameters = () => [
    { type: _app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
DeviceListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-device-list',
        template: __webpack_require__(/*! raw-loader!./device-list.page.html */ "./node_modules/raw-loader/index.js!./src/app/device-list/device-list.page.html"),
        styles: [__webpack_require__(/*! ./device-list.page.scss */ "./src/app/device-list/device-list.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], DeviceListPage);



/***/ })

}]);
//# sourceMappingURL=device-list-device-list-module-es2015.js.map