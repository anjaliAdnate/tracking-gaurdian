(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-list-user-list-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/user-list/user-list.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/user-list/user-list.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>User List</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-list *ngIf=\"userItems.length > 0\">\n          <ion-item *ngFor=\"let item of userItems\" (click)=\"gotoChatWindow(item)\">\n            <ion-avatar slot=\"start\" class=\"ionAvtar\">\n              <img src=\"assets/svg/avatar.svg\">\n              <!-- <ion-img src=\"assets/Images/dummy-profile-pic.png\"></ion-img> -->\n            </ion-avatar>\n            <ion-label>\n              <p style=\"font-size: 1.1em;\">{{item.first_name | titlecase}} {{item.last_name | titlecase}}</p>\n              <p>{{item.email}}</p>\n              <!-- <p>consectetur adipiscing elit. Duis ut urna neque.</p> -->\n            </ion-label>\n          </ion-item>\n        </ion-list>\n\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n<!-- <ion-footer *ngIf=\"userItems.length === 0\"> -->\n<ion-footer>\n  <ion-list>\n    <ion-item lines=\"none\">\n      <ion-row>\n        <ion-col size=\"3\">\n          <div style=\"margin: auto;\n          height: 55px;\n          width: 55px;\n          border-radius: 50%;\n          border: 1px solid linear-gradient(to bottom, #69d7f4, #62d3f3, #5ccff2, #55caf1, #4fc6f0);\n          text-align: center;\n          background: linear-gradient(to bottom, #69d7f4, #62d3f3, #5ccff2, #55caf1, #4fc6f0);\"></div>\n        </ion-col>\n        <ion-col size=\"9\">\n          <p style=\"margin: 0px; color: #6b7f97; font-size: 0.9em;\">\n            Hey, in order to use this family chat you first need to invite your family members. Invite them\n            right\n            now!\n          </p>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-list>\n  <ion-row style=\"padding: 0px 16px 0px 16px;\">\n    <ion-col style=\"padding: 0px 16px 40px 16px;\">\n      <ion-button expand=\"block\" class=\"custBtn\" (click)=\"inviteMem()\">Invite / Add member</ion-button>\n    </ion-col>\n  </ion-row>\n</ion-footer>"

/***/ }),

/***/ "./src/app/user-list/user-list.module.ts":
/*!***********************************************!*\
  !*** ./src/app/user-list/user-list.module.ts ***!
  \***********************************************/
/*! exports provided: UserListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserListPageModule", function() { return UserListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _user_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-list.page */ "./src/app/user-list/user-list.page.ts");







var routes = [
    {
        path: '',
        component: _user_list_page__WEBPACK_IMPORTED_MODULE_6__["UserListPage"]
    }
];
var UserListPageModule = /** @class */ (function () {
    function UserListPageModule() {
    }
    UserListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_user_list_page__WEBPACK_IMPORTED_MODULE_6__["UserListPage"]]
        })
    ], UserListPageModule);
    return UserListPageModule;
}());



/***/ }),

/***/ "./src/app/user-list/user-list.page.scss":
/*!***********************************************!*\
  !*** ./src/app/user-list/user-list.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-toolbar {\n  --color: #ffffff;\n  --background: linear-gradient(to right, #329cd1, #008dd3, #007cd2, #1c6ace, #4055c5);\n}\n\n.custBtn {\n  --background: #0aa360;\n  --padding-bottom: 25px;\n  --padding-top: 25px;\n}\n\n.ionAvtar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 55px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvdHJhY2tpbmctZ2F1cmRpYW4vc3JjL2FwcC91c2VyLWxpc3QvdXNlci1saXN0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvdXNlci1saXN0L3VzZXItbGlzdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBQTtFQUNBLG9GQUFBO0FDQ0Y7O0FEQ0E7RUFDRSxxQkFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7QUNFRjs7QURBQTtFQUNFLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSwwQkFBQTtBQ0dGIiwiZmlsZSI6InNyYy9hcHAvdXNlci1saXN0L3VzZXItbGlzdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhciB7XG4gIC0tY29sb3I6ICNmZmZmZmY7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMzI5Y2QxLCAjMDA4ZGQzLCAjMDA3Y2QyLCAjMWM2YWNlLCAjNDA1NWM1KTtcbn1cbi5jdXN0QnRuIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMGFhMzYwO1xuICAtLXBhZGRpbmctYm90dG9tOiAyNXB4O1xuICAtLXBhZGRpbmctdG9wOiAyNXB4O1xufVxuLmlvbkF2dGFyIHtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogNTVweCAhaW1wb3J0YW50O1xufVxuIiwiaW9uLXRvb2xiYXIge1xuICAtLWNvbG9yOiAjZmZmZmZmO1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzMyOWNkMSwgIzAwOGRkMywgIzAwN2NkMiwgIzFjNmFjZSwgIzQwNTVjNSk7XG59XG5cbi5jdXN0QnRuIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMGFhMzYwO1xuICAtLXBhZGRpbmctYm90dG9tOiAyNXB4O1xuICAtLXBhZGRpbmctdG9wOiAyNXB4O1xufVxuXG4uaW9uQXZ0YXIge1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgbWF4LXdpZHRoOiA1NXB4ICFpbXBvcnRhbnQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/user-list/user-list.page.ts":
/*!*********************************************!*\
  !*** ./src/app/user-list/user-list.page.ts ***!
  \*********************************************/
/*! exports provided: UserListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserListPage", function() { return UserListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/data.service */ "./src/app/services/data.service.ts");






var UserListPage = /** @class */ (function () {
    // role: string;
    function UserListPage(apiCall, loadinController, toastController, router, dataService) {
        this.apiCall = apiCall;
        this.loadinController = loadinController;
        this.toastController = toastController;
        this.router = router;
        this.dataService = dataService;
        this.userItems = [];
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    }
    UserListPage.prototype.ngOnInit = function () {
        // this.getChatHistory();
    };
    UserListPage.prototype.ionViewDidEnter = function () {
        this.getUserList();
    };
    UserListPage.prototype.gotoChatWindow = function (item) {
        this.dataService.setData(42, item);
        this.router.navigateByUrl('/chat/42');
        // this.router.navigate(['/chat', navigationExtras]);
    };
    UserListPage.prototype.getUserList = function () {
        var _this = this;
        var url;
        if (this.userdetails.isDealer !== false || this.userdetails.isSuperAdmin !== false) {
            // this.role = "member";
            url = this.apiCall.mainUrl + "users/getAllUsers?dealer=" + this.userdetails._id;
        }
        else {
            // if(this.userdetails.isDealer) {
            // this.role = "admin";
            url = this.apiCall.mainUrl + "users/getAllUsers?user=" + this.userdetails._id;
            // }
        }
        this.loadinController.create({
            message: 'loading users..please wait...'
        }).then(function (loadEl) {
            loadEl.present();
            _this.apiCall.getdevicesForAllVehiclesApi(url)
                .subscribe(function (respData) {
                loadEl.dismiss();
                if (respData) {
                    var res = JSON.parse(JSON.stringify(respData));
                    console.log("respData: ", res);
                    _this.userItems = res;
                }
            }, function (err) {
                console.log(err);
                loadEl.dismiss();
                if (err.error.message) {
                    _this.toastController.create({
                        message: err.error.message,
                        duration: 3000,
                        position: 'bottom'
                    }).then(function (toastEl) {
                        toastEl.present();
                    });
                }
            });
        });
    };
    UserListPage.prototype.inviteMem = function () {
        this.router.navigateByUrl('/add-member-filled');
    };
    UserListPage.ctorParameters = function () { return [
        { type: _app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _services_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"] }
    ]; };
    UserListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-list',
            template: __webpack_require__(/*! raw-loader!./user-list.page.html */ "./node_modules/raw-loader/index.js!./src/app/user-list/user-list.page.html"),
            styles: [__webpack_require__(/*! ./user-list.page.scss */ "./src/app/user-list/user-list.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _services_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"]])
    ], UserListPage);
    return UserListPage;
}());



/***/ })

}]);
//# sourceMappingURL=user-list-user-list-module-es5.js.map