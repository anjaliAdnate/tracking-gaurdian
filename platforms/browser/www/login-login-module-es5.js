(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/login/login.page.html":
/*!*****************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login/login.page.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class=\"mainDiv\">\n  <form #f=\"ngForm\" (ngSubmit)=\"onSubmit(f)\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-row style=\"padding-top: 30px;\">\n          <ion-col class=\"ion-text-center\">\n            <ion-img src=\"assets/Images/icon.png\" style=\"width: 150px; margin: auto\">\n            </ion-img>\n          </ion-col>\n        </ion-row>\n        <ion-card\n          style=\"margin: -25px 30px 30px 30px; padding: 16px 16px 50px 16px; background: white; border-radius: 25px;\">\n          <ion-card-title text-center class=\"ion-padding\">Log In</ion-card-title>\n\n          <div style=\"padding-bottom: 20px;\">\n            <ion-row style=\"border-radius: 25px; border: 1px solid #f8f8f8;\">\n              <ion-col size=\"2\" class=\"ion-text-center\" style=\"padding: 14px;\">\n                <ion-icon name=\"person\"></ion-icon>\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-input type=\"text\" ngModel name=\"email\" required #emailCtrl=\"ngModel\" placeholder=\"Mobile Number\">\n                </ion-input>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col></ion-col>\n            </ion-row>\n            <ion-row style=\"border-radius: 25px; border: 1px solid #f8f8f8;\">\n              <ion-col size=\"2\" class=\"ion-text-center\" style=\"padding: 14px;\">\n                <ion-icon name=\"lock\"></ion-icon>\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-input type=\"password\" ngModel name=\"password\" required #emailCtrl=\"ngModel\" placeholder=\"Password\">\n                </ion-input>\n              </ion-col>\n            </ion-row>\n          </div>\n          <ion-button shape=\"round\" expand=\"block\" class=\"btnCustom\" type=\"submit\">Login</ion-button>\n\n          <ion-row style=\"padding-left: 10px; padding-right: 10px; padding-top: 20px;\">\n            <ion-col size=\"5\" style=\" padding-left: 10px; margin-bottom: 13px; border-bottom: 1px solid #f8f8f8;\">\n            </ion-col>\n            <ion-col size=\"2\" style=\"color: gray;\" class=\"ion-text-center\">OR</ion-col>\n            <ion-col size=\"5\" style=\"margin-bottom: 13px; border-bottom: 1px solid #f8f8f8;\"></ion-col>\n          </ion-row>\n          <ion-button shape=\"round\" expand=\"block\" class=\"btnCustom1\">Continue with facebook</ion-button>\n\n        </ion-card>\n        <p style=\"text-align: center; margin-top: 25px; color: white;;\">Forgot Password?</p>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  </form>\n</ion-content>"

/***/ }),

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");







var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/login/login.page.scss":
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: #fff url('Launch Screen.jpg') no-repeat center center / cover;\n}\n\n.btnCustom {\n  --background: linear-gradient(to bottom, #69d7f4, #62d3f3, #5ccff2, #55caf1, #4fc6f0);\n  --padding-bottom: 23px;\n  --padding-top: 23px;\n}\n\n.btnCustom1 {\n  --background: linear-gradient(to right top, #303d9b, #2b4ca4, #295aac, #2b67b3, #3374b9);\n  --padding-bottom: 23px;\n  --padding-top: 23px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvdHJhY2tpbmctZ2F1cmRpYW4vc3JjL2FwcC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUEwQkE7RUFDRSwyRUFBQTtBQ3pCRjs7QUQ0QkE7RUFDRSxxRkFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7QUN6QkY7O0FENEJBO0VBQ0Usd0ZBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0FDekJGIiwiZmlsZSI6InNyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gLnJjb3JuZXJzMiB7XG4vLyAgICAgYm9yZGVyLXJhZGl1czogMzVweDtcbi8vICAgICBib3JkZXI6IDJweCBzb2xpZCAjNjA5O1xuLy8gICAgIHBhZGRpbmc6IDIwcHg7XG4vLyAgICAgaGVpZ2h0OiA1OHB4O1xuLy8gfVxuLy8gLmJ0biB7XG4vLyAgICAgYm9yZGVyLXJhZGl1czogMzVweDtcbi8vICAgICBiYWNrZ3JvdW5kOiAjNWFjZmYwO1xuLy8gICAgIHBhZGRpbmc6IDIwcHg7XG4vLyAgICAgd2lkdGg6IDIwMHB4O1xuLy8gICAgIGhlaWdodDogNTBweDtcbi8vIH1cbi8vIC5idG4xIHtcbi8vICAgICBib3JkZXItcmFkaXVzOiAzNXB4O1xuLy8gICAgIGJhY2tncm91bmQ6ICMzNDU1YTQ7XG4vLyAgICAgcGFkZGluZzogMjBweDtcbi8vICAgICB3aWR0aDogMjAwcHg7XG4vLyAgICAgaGVpZ2h0OiA1MHB4O1xuLy8gfVxuLy8gLmxvZ2luLWJ1dHRvbntcbi8vICAgICB3aWR0aDogMTAwJTtcbi8vICAgICBoZWlnaHQ6IDUwcHg7XG4vLyAgICAgLS1ib3JkZXItcmFkaXVzOjM1cHg7XG4vLyB9XG5cbmlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmIHVybChcIi4uLy4uL2Fzc2V0cy9JbWFnZXMvTGF1bmNoIFNjcmVlbi5qcGdcIikgbm8tcmVwZWF0IGNlbnRlciBjZW50ZXIgLyBjb3Zlcjtcbn1cblxuLmJ0bkN1c3RvbSB7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgIzY5ZDdmNCwgIzYyZDNmMywgIzVjY2ZmMiwgIzU1Y2FmMSwgIzRmYzZmMCk7XG4gIC0tcGFkZGluZy1ib3R0b206IDIzcHg7XG4gIC0tcGFkZGluZy10b3A6IDIzcHg7XG59XG5cbi5idG5DdXN0b20xIHtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQgdG9wLCAjMzAzZDliLCAjMmI0Y2E0LCAjMjk1YWFjLCAjMmI2N2IzLCAjMzM3NGI5KTtcbiAgLS1wYWRkaW5nLWJvdHRvbTogMjNweDtcbiAgLS1wYWRkaW5nLXRvcDogMjNweDtcbn1cbiIsImlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmIHVybChcIi4uLy4uL2Fzc2V0cy9JbWFnZXMvTGF1bmNoIFNjcmVlbi5qcGdcIikgbm8tcmVwZWF0IGNlbnRlciBjZW50ZXIgLyBjb3Zlcjtcbn1cblxuLmJ0bkN1c3RvbSB7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgIzY5ZDdmNCwgIzYyZDNmMywgIzVjY2ZmMiwgIzU1Y2FmMSwgIzRmYzZmMCk7XG4gIC0tcGFkZGluZy1ib3R0b206IDIzcHg7XG4gIC0tcGFkZGluZy10b3A6IDIzcHg7XG59XG5cbi5idG5DdXN0b20xIHtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQgdG9wLCAjMzAzZDliLCAjMmI0Y2E0LCAjMjk1YWFjLCAjMmI2N2IzLCAjMzM3NGI5KTtcbiAgLS1wYWRkaW5nLWJvdHRvbTogMjNweDtcbiAgLS1wYWRkaW5nLXRvcDogMjNweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/login/login.page.ts":
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");


// import { AuthenticationService } from '../auth.service';
// import { Storage } from '@ionic/storage';

// const TOKEN_KEY = 'auth-token';
var LoginPage = /** @class */ (function () {
    function LoginPage(
    // private router: Router,
    // private loadingController: LoadingController,
    // private appService: AppService,
    // private toastController: ToastController,
    // private alertContoller: AlertController,
    // private storage: Storage,
    authService) {
        this.authService = authService;
        this.data = {};
    }
    LoginPage.prototype.ngOnInit = function () {
    };
    LoginPage.prototype.onSubmit = function (form) {
        // debugger
        if (!form.valid) {
            return;
        }
        // userlogin() {
        // this.submitAttempt = true;
        this.email = form.value.email;
        this.password = form.value.password;
        if (this.email == "") {
            /*alert("invalid");*/
            return false;
        }
        else if (this.password == "") {
            /*alert("invalid");*/
            return false;
        }
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
        var isEmail = validateEmail(this.email);
        var isName = isNaN(this.email);
        var strNum;
        if (isName == false) {
            strNum = this.email.trim();
        }
        if (isEmail == false && isName == false && strNum.length == 10) {
            this.data = {
                "psd": this.password,
                "ph_num": this.email
            };
        }
        else if (isEmail) {
            this.data = {
                "psd": this.password,
                "emailid": this.email
            };
        }
        else {
            this.data = {
                "psd": this.password,
                "user_id": this.email
            };
        }
        this.authService.login(this.data);
        // this.loadingController.create({
        //   message: 'Please wait..logging you in...',
        // }).then((loadEl => {
        //   loadEl.present();
        //   this.appService.authLogin(this.data)
        //     .subscribe(response => {
        //       // this.authService.login();
        //       this.storage.set(TOKEN_KEY, 'Bearer 1234567').then(() => {
        //         this.authenticationState.next(true);
        //       });
        //       var logindetails = JSON.parse(JSON.stringify(response));
        //       // this.logindata = JSON.stringify(response);
        //       // var logindetails = JSON.parse(this.logindata);
        //       var userDetails = window.atob(logindetails.token.split('.')[1]);
        //       var details = JSON.parse(userDetails);
        //       console.log(details.email);
        //       localStorage.setItem("loginflag", "loginflag");
        //       localStorage.setItem('details', JSON.stringify(details));
        //       // localStorage.setItem('condition_chk', details.isDealer);
        //       // localStorage.setItem('VeryFirstLoginUser', details._id)
        //       // localStorage.setItem("INTRO", "INTRO")
        //       loadEl.dismiss();
        //       this.toastController.create({
        //         message: "Welcome! You're logged In successfully.",
        //         duration: 3000,
        //         position: 'bottom'
        //       }).then((toastEl => {
        //         toastEl.present()
        //       }));
        //       // this.router.navigateByUrl('/home');
        //       this.router.navigate(['home']);
        //     },
        //       error => {
        //         // console.log("login error => "+error)
        //         var body = error._body;
        //         var msg = JSON.parse(body);
        //         if (msg.message == "Mobile Phone Not Verified") {
        //           this.alertContoller.create({
        //             header: 'Login failed!',
        //             message: msg.message = !msg.message ? '  Do you Want to verify it?' : msg.message + '  Do you Want to verify it?',
        //             buttons: [
        //               { text: 'Cancel' },
        //               {
        //                 text: 'Okay',
        //                 handler: () => {
        //                   // this.navCtrl.push(MobileVerify);
        //                 }
        //               }
        //             ]
        //           }).then((alertEl => {
        //             alertEl.present();
        //           }));
        //         } else {
        //           this.alertContoller.create({
        //             header: 'Login failed!',
        //             message: msg.message,
        //             buttons: ['Okay']
        //           }).then((alertEl => {
        //             alertEl.present();
        //           }))
        //         }
        //         loadEl.dismiss();
        //       });
        // }));
        // }
    };
    LoginPage.ctorParameters = function () { return [
        { type: _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"] }
    ]; };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/index.js!./src/app/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/login/login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ })

}]);
//# sourceMappingURL=login-login-module-es5.js.map