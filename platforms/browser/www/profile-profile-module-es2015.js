(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-profile-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/profile/profile.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/profile/profile.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Profile</ion-title>\n    <ion-buttons slot=\"end\" (click)=\"showNotifs()\">\n      <ion-img src=\"assets/notification/Reminder.png\" style=\"width: 30px;\"></ion-img>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-list>\n          <ion-item>\n            <ion-row>\n              <ion-col size=\"2\">\n                <ion-img src=\"assets/Images/user.png\" style=\"width: 23px;\"></ion-img>\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-row>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <h4 style=\"margin: 0px;\">Account</h4>\n                  </ion-col>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <p style=\"padding: 0px 0px 5px; margin: 0px; color: #aaa8a9;\" *ngIf=\"userdetails._orgName\">\n                      {{userdetails._orgName}}</p>\n                    <p style=\"padding: 0px 0px 5px; margin: 0px; color: #aaa8a9;\">{{userdetails.phn}}</p>\n                    <p style=\"padding: 0px 0px 5px; margin: 0px; color: #aaa8a9;\">{{userdetails.email}}</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n        </ion-list>\n\n        <ion-list>\n          <ion-item (click)=\"addMember()\">\n            <ion-row>\n              <ion-col size=\"2\">\n                <ion-img src=\"assets/Images/add-contact.png\" style=\"width: 23px;\"></ion-img>\n\n                <!-- <img src=\"assets/Images/add-contact.png\" style=\"width: 23px;\" /> -->\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-row>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <h4 style=\"margin: 0px;\">Member setting</h4>\n                  </ion-col>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <p class=\"para\">{{count}} members added | Add a new member</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n        </ion-list>\n        <ion-list>\n          <ion-item>\n            <ion-row>\n              <ion-col size=\"2\">\n                <ion-img src=\"assets/Images/placeholder.png\" style=\"width: 23px;\"></ion-img>\n\n                <!-- <img src=\"assets/Images/placeholder.png\" style=\"width: 23px;\" /> -->\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-row>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <h4 style=\"margin: 0px;\">Electric fence</h4>\n                  </ion-col>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <p class=\"para\">Select your distance range</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n        </ion-list>\n        <ion-list>\n          <ion-item (click)=\"addSOS()\">\n            <ion-row>\n              <ion-col size=\"3\">\n                <ion-img src=\"assets/Images/phone-call.png\" style=\"width: 23px;\"></ion-img>\n              </ion-col>\n              <ion-col size=\"9\" style=\"margin-left: -10px;\">\n                <ion-row>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <h4 style=\"margin: 0px;\">Emergency number</h4>\n                  </ion-col>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <p class=\"para\">24x7 help</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n        </ion-list>\n        <ion-list>\n          <ion-item (click)=\"updateProfile()\">\n            <ion-row>\n              <ion-col size=\"2\">\n                <ion-img src=\"assets/Images/user (1).png\" style=\"width: 23px;\"></ion-img>\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-row>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <h4 style=\"margin: 0px;\">Profile</h4>\n                  </ion-col>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <p class=\"para\">Change profile photo, update personal info</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n        </ion-list>\n        <ion-list>\n          <ion-item (click)=\"onLogout()\">\n            <ion-row>\n              <ion-col size=\"3\">\n                <ion-img src=\"assets/Images/logout.png\" style=\"width: 23px;\"></ion-img>\n              </ion-col>\n              <ion-col size=\"9\">\n                <ion-row>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <h4 style=\"margin: 0px;\">Log out</h4>\n                  </ion-col>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <p class=\"para\">Log out from app</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n<!-- <ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-list>\n          <ion-item detail>\n            <ion-row>\n              <ion-col size=\"2\">\n                <img src=\"assets/Images/user.png\" style=\"width: 18px;\" />\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-row>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <h5 style=\"margin: 0px;\">Account</h5>\n                  </ion-col>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <p class=\"para\">4 member added</p>\n                    <p class=\"para\">1234567890</p>\n                    <p class=\"para\">oneqlik@gmail.com</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n          <ion-item detail>\n            <ion-row>\n              <ion-col size=\"2\">\n                <img src=\"assets/Images/add-contact.png\" style=\"width: 18px;\" />\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-row>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <h5 style=\"margin: 0px;\">Member setting</h5>\n                  </ion-col>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <p class=\"para\">Add a new member</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n          <ion-item detail>\n            <ion-row>\n              <ion-col size=\"2\">\n                <img src=\"assets/Images/placeholder.png\" style=\"width: 18px;\" />\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-row>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <h5 style=\"margin: 0px;\">Electric fence</h5>\n                  </ion-col>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <p class=\"para\">Select your distance range</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n          <ion-item detail>\n            <ion-row>\n              <ion-col size=\"2\">\n                <img src=\"assets/Images/phone-call.png\" style=\"width: 18px;\" />\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-row>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <h5 style=\"margin: 0px;\">Emergency number</h5>\n                  </ion-col>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <p class=\"para\">24x7 help</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n          <ion-item detail (click)=\"updateProfile()\">\n            <ion-row>\n              <ion-col size=\"2\">\n                <img src=\"assets/Images/user (1).png\" style=\"width: 18px;\" />\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-row>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <h5 style=\"margin: 0px;\">Profile</h5>\n                  </ion-col>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <p class=\"para\">Change profile photo, update personal info</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n          <ion-item detail (click)=\"onLogout()\">\n            <ion-row>\n              <ion-col size=\"2\">\n                <img src=\"assets/Images/logout.png\" style=\"width: 18px;\" />\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-row>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <h5 style=\"margin: 0px;\">Log out</h5>\n                  </ion-col>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <p class=\"para\">Log out from app</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n          <ion-item detail>\n            <ion-row>\n              <ion-col size=\"2\">\n                <img src=\"assets/Images/info.png\" style=\"width: 18px;\" />\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-row>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <h5 style=\"margin: 0px;\">Help</h5>\n                  </ion-col>\n                  <ion-col size=\"12\" class=\"ion-no-padding\">\n                    <p class=\"para\">Open queries</p>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content> -->"

/***/ }),

/***/ "./src/app/profile/profile.module.ts":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.module.ts ***!
  \*******************************************/
/*! exports provided: ProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile.page */ "./src/app/profile/profile.page.ts");







const routes = [
    {
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]
    }
];
let ProfilePageModule = class ProfilePageModule {
};
ProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]]
    })
], ProfilePageModule);



/***/ }),

/***/ "./src/app/profile/profile.page.scss":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-toolbar {\n  --color: #ffffff;\n  --background: linear-gradient(to right, #329cd1, #008dd3, #007cd2, #1c6ace, #4055c5);\n}\n\n.para {\n  padding: 0px 0px 11px;\n  margin: 0px;\n  color: #aaa8a9;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvdHJhY2tpbmctZ2F1cmRpYW4vc3JjL2FwcC9wcm9maWxlL3Byb2ZpbGUucGFnZS5zY3NzIiwic3JjL2FwcC9wcm9maWxlL3Byb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQU9BO0VBQ0UsZ0JBQUE7RUFDQSxvRkFBQTtBQ05GOztBRGdCQTtFQUNFLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7QUNiRiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvcHJvZmlsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBpb24tbGlzdCB7XG4vLyAgIHBhZGRpbmctdG9wOiAwcHg7XG4vLyAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG4vLyB9XG4vLyBoNiB7XG4vLyAgIGZvbnQtc2l6ZTogMTJweDtcbi8vIH1cbmlvbi10b29sYmFyIHtcbiAgLS1jb2xvcjogI2ZmZmZmZjtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMzMjljZDEsICMwMDhkZDMsICMwMDdjZDIsICMxYzZhY2UsICM0MDU1YzUpO1xufVxuXG4vLyAucGFyYSB7XG4vLyAgIHBhZGRpbmc6IDBweCAwcHggNXB4O1xuLy8gICBtYXJnaW46IDBweDtcbi8vICAgY29sb3I6ICNhYWE4YTk7XG4vLyAgIGZvbnQtc2l6ZTogMC45ZW07XG4vLyB9XG5cbi5wYXJhIHtcbiAgcGFkZGluZzogMHB4IDBweCAxMXB4O1xuICBtYXJnaW46IDBweDtcbiAgY29sb3I6ICNhYWE4YTk7XG59XG4iLCJpb24tdG9vbGJhciB7XG4gIC0tY29sb3I6ICNmZmZmZmY7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMzI5Y2QxLCAjMDA4ZGQzLCAjMDA3Y2QyLCAjMWM2YWNlLCAjNDA1NWM1KTtcbn1cblxuLnBhcmEge1xuICBwYWRkaW5nOiAwcHggMHB4IDExcHg7XG4gIG1hcmdpbjogMHB4O1xuICBjb2xvcjogI2FhYThhOTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/profile/profile.page.ts":
/*!*****************************************!*\
  !*** ./src/app/profile/profile.page.ts ***!
  \*****************************************/
/*! exports provided: ProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePage", function() { return ProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");






let ProfilePage = class ProfilePage {
    constructor(router, alertController, authenticationService, platform, apiCall, loadingController) {
        this.router = router;
        this.alertController = alertController;
        this.authenticationService = authenticationService;
        this.platform = platform;
        this.apiCall = apiCall;
        this.loadingController = loadingController;
        this.count = 0;
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    }
    ngOnInit() { }
    ionViewDidEnter() {
        this.getUserList();
    }
    addMember() {
        this.router.navigateByUrl('/add-member-filled');
    }
    updateProfile() {
        this.router.navigate(['prof']);
    }
    showNotifs() {
        this.router.navigate(['notif']);
    }
    onLogout() {
        this.alertController.create({
            message: 'Do you want to logout from app?',
            buttons: [
                {
                    text: 'Yes',
                    handler: () => {
                        this.logout();
                    }
                },
                {
                    text: 'No'
                }
            ]
        }).then((alertEl => {
            alertEl.present();
        }));
    }
    logout() {
        var token = localStorage.getItem("DEVICE_TOKEN");
        var pushdata = {};
        if (token !== null) {
            if (this.platform.is('android')) {
                pushdata = {
                    "uid": this.userdetails._id,
                    "token": token,
                    "os": "android"
                };
            }
            else {
                pushdata = {
                    "uid": this.userdetails._id,
                    "token": token,
                    "os": "ios"
                };
            }
            this.removePushToken(pushdata);
        }
        localStorage.clear();
        this.authenticationService.logout();
        this.authenticationService.authenticationState.subscribe(state => {
            if (state) {
                this.router.navigate(['home']);
            }
            else {
                this.router.navigate(['login']);
            }
        });
    }
    removePushToken(pushdata) {
        var url = this.apiCall.mainUrl + "/users/PullNotification";
        this.loadingController.create({
            message: 'Logging out...'
        }).then((loadEl) => {
            loadEl.present();
            this.apiCall.urlWithdata(url, pushdata)
                .subscribe(respData => {
                loadEl.dismiss();
                if (respData) {
                    var data = JSON.parse(JSON.stringify(respData));
                    console.log("push notifications updated " + data.message);
                    localStorage.clear();
                }
            }, err => {
                loadEl.dismiss();
                console.log(err);
            });
        });
    }
    addSOS() {
        this.router.navigateByUrl('/add-sos');
    }
    getUserList() {
        this.count = 0;
        var url;
        if (this.userdetails.isDealer !== false || this.userdetails.isSuperAdmin !== false) {
            url = this.apiCall.mainUrl + "users/getAllUsers?dealer=" + this.userdetails._id;
        }
        else {
            url = this.apiCall.mainUrl + "users/getAllUsers?user=" + this.userdetails._id;
            // }
        }
        // this.loadingController.create({
        //   message: 'loading users..please wait...'
        // }).then((loadEl) => {
        //   loadEl.present();
        this.apiCall.getdevicesForAllVehiclesApi(url)
            .subscribe(respData => {
            // loadEl.dismiss();
            if (respData) {
                var res = JSON.parse(JSON.stringify(respData));
                // console.log("respData: ", res);
                // this.userItems = res;
                for (var r = 0; r < res.length; r++) {
                    this.count += 1;
                }
            }
        }, err => {
            console.log(err);
            // loadEl.dismiss();
            if (err.error.message) {
                console.log(err.error.message);
            }
        });
        // })
    }
};
ProfilePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
    { type: _app_service__WEBPACK_IMPORTED_MODULE_5__["AppService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] }
];
ProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile',
        template: __webpack_require__(/*! raw-loader!./profile.page.html */ "./node_modules/raw-loader/index.js!./src/app/profile/profile.page.html"),
        styles: [__webpack_require__(/*! ./profile.page.scss */ "./src/app/profile/profile.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
        _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
        _app_service__WEBPACK_IMPORTED_MODULE_5__["AppService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]])
], ProfilePage);



/***/ })

}]);
//# sourceMappingURL=profile-profile-module-es2015.js.map