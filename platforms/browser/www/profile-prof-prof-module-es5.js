(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-prof-prof-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/profile/prof/prof.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/profile/prof/prof.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar no-header>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/profile\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Edit Profile</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <div style=\"text-align: center;\">\n          <ion-img *ngIf=\"!fileUrl\" class=\"circular--square\" src=\"assets/Images/dummy-profile-pic.png\"></ion-img>\n          <ion-img *ngIf=\"fileUrl\" class=\"circular--square\" src=\"{{fileUrl}}\"></ion-img>\n          <ion-fab vertical=\"bottom\" slot=\"fixed\">\n            <ion-fab-button style=\"margin-left: -55px;\" color=\"medium\" (click)=\"selectImage()\">\n              <ion-icon name=\"camera\"></ion-icon>\n            </ion-fab-button>\n          </ion-fab>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-list>\n          <ion-item>\n            <ion-input style=\"font-size: 1.3em; font-weight: 400;\" type=\"text\" placeholder=\"First Name\"\n              [(ngModel)]=\"userdetails.fn\"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-input style=\"font-size: 1.3em; font-weight: 400;\" type=\"text\" placeholder=\"Last Name\"\n              [(ngModel)]=\"userdetails.ln\"></ion-input>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/profile/prof/prof.module.ts":
/*!*********************************************!*\
  !*** ./src/app/profile/prof/prof.module.ts ***!
  \*********************************************/
/*! exports provided: ProfPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfPageModule", function() { return ProfPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _prof_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./prof.page */ "./src/app/profile/prof/prof.page.ts");
/* harmony import */ var _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/crop/ngx */ "./node_modules/@ionic-native/crop/ngx/index.js");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "./node_modules/@ionic-native/file-transfer/ngx/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/Camera/ngx */ "./node_modules/@ionic-native/Camera/ngx/index.js");








// import { ImagePicker } from '@ionic-native/image-picker/ngx';



// import { File } from '@ionic-native/file/ngx';
var routes = [
    {
        path: '',
        component: _prof_page__WEBPACK_IMPORTED_MODULE_6__["ProfPage"]
    }
];
var ProfPageModule = /** @class */ (function () {
    function ProfPageModule() {
    }
    ProfPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_prof_page__WEBPACK_IMPORTED_MODULE_6__["ProfPage"]],
            providers: [
                _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_8__["FileTransfer"],
                _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_7__["Crop"],
                // ImagePicker,
                _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_9__["File"],
                _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_10__["Camera"]
            ]
        })
    ], ProfPageModule);
    return ProfPageModule;
}());



/***/ }),

/***/ "./src/app/profile/prof/prof.page.scss":
/*!*********************************************!*\
  !*** ./src/app/profile/prof/prof.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".circular--square {\n  display: inline-block;\n  position: relative;\n  width: 150px;\n  height: 150px;\n  overflow: hidden;\n  border-radius: 50%;\n  margin: auto;\n}\n\nion-toolbar {\n  --color: #ffffff;\n  --background: linear-gradient(to right, #329cd1, #008dd3, #007cd2, #1c6ace, #4055c5);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvdHJhY2tpbmctZ2F1cmRpYW4vc3JjL2FwcC9wcm9maWxlL3Byb2YvcHJvZi5wYWdlLnNjc3MiLCJzcmMvYXBwL3Byb2ZpbGUvcHJvZi9wcm9mLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDQ0o7O0FEQ0E7RUFDSSxnQkFBQTtFQUNBLG9GQUFBO0FDRUoiLCJmaWxlIjoic3JjL2FwcC9wcm9maWxlL3Byb2YvcHJvZi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2lyY3VsYXItLXNxdWFyZSB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aWR0aDogMTUwcHg7XG4gICAgaGVpZ2h0OiAxNTBweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBtYXJnaW46IGF1dG87XG59XG5pb24tdG9vbGJhciB7XG4gICAgLS1jb2xvcjogI2ZmZmZmZjtcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzMyOWNkMSwgIzAwOGRkMywgIzAwN2NkMiwgIzFjNmFjZSwgIzQwNTVjNSk7XG4gIH0iLCIuY2lyY3VsYXItLXNxdWFyZSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB3aWR0aDogMTUwcHg7XG4gIGhlaWdodDogMTUwcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG5pb24tdG9vbGJhciB7XG4gIC0tY29sb3I6ICNmZmZmZmY7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMzI5Y2QxLCAjMDA4ZGQzLCAjMDA3Y2QyLCAjMWM2YWNlLCAjNDA1NWM1KTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/profile/prof/prof.page.ts":
/*!*******************************************!*\
  !*** ./src/app/profile/prof/prof.page.ts ***!
  \*******************************************/
/*! exports provided: ProfPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfPage", function() { return ProfPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/crop/ngx */ "./node_modules/@ionic-native/crop/ngx/index.js");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "./node_modules/@ionic-native/file-transfer/ngx/index.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/Camera/ngx */ "./node_modules/@ionic-native/Camera/ngx/index.js");



// import { ImagePicker } from '@ionic-native/image-picker/ngx';




var ProfPage = /** @class */ (function () {
    function ProfPage(
    // private imagePicker: ImagePicker,
    crop, transfer, apiCall, loadingController, camera, actionSheetController, 
    // private file: File,
    taostCtrl) {
        this.crop = crop;
        this.transfer = transfer;
        this.apiCall = apiCall;
        this.loadingController = loadingController;
        this.camera = camera;
        this.actionSheetController = actionSheetController;
        this.taostCtrl = taostCtrl;
        this.fileUrl = null;
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        // console.log("user details=> " + JSON.stringify(this.userdetails));
        this.getImgUrl();
    }
    ProfPage.prototype.ngOnInit = function () {
    };
    ProfPage.prototype.pickImage = function (sourceType) {
        var _this = this;
        var url = this.apiCall.mainUrl + "users/uploadProfilePicture";
        var options = {
            quality: 100,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            // let base64Image = 'data:image/jpeg;base64,' + imageData;
            console.log("imageData: ", imageData);
            // debugger
            _this.crop.crop(imageData, { quality: 100 })
                .then(function (newImage) {
                var dlink123 = newImage.split('?');
                var wear = dlink123[0];
                var fileTransfer = _this.transfer.create();
                var uploadOpts = {
                    fileKey: 'photo',
                    // fileName: imageData.substr(imageData.lastIndexOf('/') + 1)
                    fileName: wear.substr(wear.lastIndexOf('/') + 1)
                };
                _this.loadingController.create({
                    message: 'Please wait....',
                }).then(function (loadEl) {
                    loadEl.present();
                    fileTransfer.upload(wear, url, uploadOpts)
                        .then(function (data) {
                        loadEl.dismiss();
                        console.log(data);
                        // this.selectedFile = <File>event.target.files[0];
                        _this.respData = JSON.parse(JSON.stringify(data)).response;
                        console.log("image data response: ", _this.respData);
                        _this.dlUpdate(_this.respData);
                        // this.fileUrl = this.respData.fileUrl;
                        // this.fileUrl = this.respData;
                    }, function (err) {
                        loadEl.dismiss();
                        console.log(err);
                        _this.taostCtrl.create({
                            message: 'Something went wrong while uploading file... Please try after some time..',
                            duration: 2000,
                            position: 'bottom'
                        }).then(function (toastEl) {
                            toastEl.present();
                        });
                    });
                });
            });
        }, function (err) {
            console.log("imageData err: ", err);
            // Handle error
        });
    };
    ProfPage.prototype.selectImage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: "Select Image source",
                            buttons: [{
                                    text: 'Load from Library',
                                    handler: function () {
                                        // this.openGallery();
                                        _this.pickImage(_this.camera.PictureSourceType.SAVEDPHOTOALBUM);
                                    }
                                },
                                {
                                    text: 'Use Camera',
                                    handler: function () {
                                        _this.pickImage(_this.camera.PictureSourceType.CAMERA);
                                    }
                                },
                                {
                                    text: 'Cancel',
                                    role: 'cancel'
                                }
                            ]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfPage.prototype.dlUpdate = function (dllink) {
        var _this = this;
        // debugger
        // let that = this;
        var dlink123 = dllink.split('?');
        var wear = dlink123[0];
        console.log("new download link: ", wear);
        var _burl = this.apiCall.mainUrl + "users/updateImagePath";
        var payload = {
            imageDoc: [dllink],
            _id: this.userdetails._id
        };
        this.loadingController.create({
            message: 'Uploading image...'
        }).then(function (loadEl) {
            loadEl.present();
            _this.apiCall.urlWithdata(_burl, payload)
                .subscribe(function (respData) {
                loadEl.dismiss();
                console.log("check profile upload: ", respData);
                _this.getImgUrl();
            }, function (err) {
                loadEl.dismiss();
            });
        });
    };
    ProfPage.prototype.getImgUrl = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + "users/shareProfileImage?uid=" + this.userdetails._id;
        this.loadingController.create({
            message: 'Loading image...'
        }).then((function (loadEl) {
            loadEl.present();
            _this.apiCall.getdevicesForAllVehiclesApi(url)
                .subscribe(function (resp) {
                loadEl.dismiss();
                console.log("server image url=> ", resp);
                if (JSON.parse(JSON.stringify(resp)).imageDoc.length > 0) {
                    var imgUrl = JSON.parse(JSON.stringify(resp)).imageDoc[0];
                    var str1 = imgUrl.split('public/');
                    _this.fileUrl = "http://13.126.36.205/" + str1[1];
                }
            }, function (err) {
                loadEl.dismiss();
            });
        }));
    };
    ProfPage.ctorParameters = function () { return [
        { type: _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_2__["Crop"] },
        { type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_3__["FileTransfer"] },
        { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
        { type: _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_6__["Camera"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ActionSheetController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] }
    ]; };
    ProfPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-prof',
            template: __webpack_require__(/*! raw-loader!./prof.page.html */ "./node_modules/raw-loader/index.js!./src/app/profile/prof/prof.page.html"),
            styles: [__webpack_require__(/*! ./prof.page.scss */ "./src/app/profile/prof/prof.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_2__["Crop"],
            _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_3__["FileTransfer"],
            src_app_app_service__WEBPACK_IMPORTED_MODULE_4__["AppService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_6__["Camera"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ActionSheetController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]])
    ], ProfPage);
    return ProfPage;
}());



/***/ })

}]);
//# sourceMappingURL=profile-prof-prof-module-es5.js.map