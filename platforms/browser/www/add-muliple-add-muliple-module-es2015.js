(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-muliple-add-muliple-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/add-muliple/add-muliple.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/add-muliple/add-muliple.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/location-setup\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Add {{key | titlecase}}</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"saveGeofence()\" fill=\"clear\" size=\"large\">SAVE</ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid style=\"padding: 0px;\">\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\" style=\"padding: 0px;\">\n        <div style=\"padding: 16px 16px 50px 16px\">\n          <ion-row style=\"border-bottom: 1.5px solid #90b3c3;\">\n            <ion-col size=\"2\" class=\"ion-text-center\" style=\"padding: 14px; font-size: 1.5em;\">\n              <ion-icon name=\"bookmark\"></ion-icon>\n            </ion-col>\n            <ion-col size=\"10\">\n              <ion-input type=\"text\" [(ngModel)]=\"ppname\" required placeholder=\"{{key | titlecase}}\">\n              </ion-input>\n            </ion-col>\n          </ion-row>\n          <ion-row style=\"border-bottom: 1.5px solid #90b3c3;\">\n            <ion-col size=\"2\" class=\"ion-text-center\" style=\"padding: 14px; font-size: 1.5em;\">\n              <ion-icon name=\"pin\"></ion-icon>\n            </ion-col>\n            <ion-col size=\"10\">\n              <ion-input type=\"text\" name=\"address\" required placeholder=\"Addressor location\"\n                [(ngModel)]=\"autocomplete.query\" (ionInput)=\"updateSearch()\">\n              </ion-input>\n            </ion-col>\n          </ion-row>\n\n          <ion-list [ngClass]=\"autocompleteItems.length > 0 ? 'searchclass1' : 'searchclass2'\">\n            <ion-item *ngFor=\"let item of autocompleteItems\" (click)=\"chooseItem(item)\">\n              {{ item.description }}\n            </ion-item>\n          </ion-list>\n\n          <ion-row style=\"border-bottom: 1.5px solid #90b3c3;\">\n            <ion-col size=\"2\" class=\"ion-text-center\" style=\"padding: 14px; font-size: 1.5em;\">\n              <ion-icon style=\"font-size: 17px\" src=\"assets/svg/radius.svg\"></ion-icon>\n            </ion-col>\n            <ion-col size=\"10\">\n              <ion-range style=\"padding: 0px; margin: 0px;\" min=\"200\" max=\"100000\" pin=\"false\" [(ngModel)]=\"fence\"\n                color=\"secondary\" (ionChange)=\"callThis(fence)\">\n              </ion-range>\n            </ion-col>\n          </ion-row>\n\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <!-- <div class=\"map\" #map></div> -->\n  <div #mapGeofence id=\"mapGeofence\"></div>\n</ion-content>"

/***/ }),

/***/ "./src/app/add-muliple/add-muliple.module.ts":
/*!***************************************************!*\
  !*** ./src/app/add-muliple/add-muliple.module.ts ***!
  \***************************************************/
/*! exports provided: AddMuliplePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddMuliplePageModule", function() { return AddMuliplePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_muliple_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-muliple.page */ "./src/app/add-muliple/add-muliple.page.ts");
/* harmony import */ var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/native-geocoder/ngx */ "./node_modules/@ionic-native/native-geocoder/ngx/index.js");








const routes = [
    {
        path: '',
        component: _add_muliple_page__WEBPACK_IMPORTED_MODULE_6__["AddMuliplePage"]
    }
];
let AddMuliplePageModule = class AddMuliplePageModule {
};
AddMuliplePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_add_muliple_page__WEBPACK_IMPORTED_MODULE_6__["AddMuliplePage"]],
        providers: [_ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_7__["NativeGeocoder"]]
    })
], AddMuliplePageModule);



/***/ }),

/***/ "./src/app/add-muliple/add-muliple.page.scss":
/*!***************************************************!*\
  !*** ./src/app/add-muliple/add-muliple.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-toolbar {\n  --color: #ffffff;\n  --background: linear-gradient(to right, #329cd1, #008dd3, #007cd2, #1c6ace, #4055c5);\n}\n\n.map {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  background-color: transparent;\n  opacity: 0;\n  -webkit-transition: opacity 150ms ease-in;\n  transition: opacity 150ms ease-in;\n}\n\n.map.visible {\n  opacity: 1;\n}\n\n.searchclass1 {\n  display: block;\n}\n\n.searchclass2 {\n  display: none;\n}\n\n#mapGeofence {\n  height: 80%;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvdHJhY2tpbmctZ2F1cmRpYW4vc3JjL2FwcC9hZGQtbXVsaXBsZS9hZGQtbXVsaXBsZS5wYWdlLnNjc3MiLCJzcmMvYXBwL2FkZC1tdWxpcGxlL2FkZC1tdWxpcGxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFBO0VBQ0Esb0ZBQUE7QUNDRjs7QURDQTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSw2QkFBQTtFQUNBLFVBQUE7RUFDQSx5Q0FBQTtFQUFBLGlDQUFBO0FDRUY7O0FEQ0E7RUFDRSxVQUFBO0FDRUY7O0FEQUE7RUFDRSxjQUFBO0FDR0Y7O0FEREE7RUFDRSxhQUFBO0FDSUY7O0FEREE7RUFDRSxXQUFBO0VBQ0EsV0FBQTtBQ0lGIiwiZmlsZSI6InNyYy9hcHAvYWRkLW11bGlwbGUvYWRkLW11bGlwbGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXIge1xuICAtLWNvbG9yOiAjZmZmZmZmO1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzMyOWNkMSwgIzAwOGRkMywgIzAwN2NkMiwgIzFjNmFjZSwgIzQwNTVjNSk7XG59XG4ubWFwIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgb3BhY2l0eTogMDtcbiAgdHJhbnNpdGlvbjogb3BhY2l0eSAxNTBtcyBlYXNlLWluO1xufVxuXG4ubWFwLnZpc2libGUge1xuICBvcGFjaXR5OiAxO1xufVxuLnNlYXJjaGNsYXNzMSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuLnNlYXJjaGNsYXNzMntcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuI21hcEdlb2ZlbmNlIHtcbiAgaGVpZ2h0OiA4MCU7XG4gIHdpZHRoOiAxMDAlO1xufSIsImlvbi10b29sYmFyIHtcbiAgLS1jb2xvcjogI2ZmZmZmZjtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMzMjljZDEsICMwMDhkZDMsICMwMDdjZDIsICMxYzZhY2UsICM0MDU1YzUpO1xufVxuXG4ubWFwIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgb3BhY2l0eTogMDtcbiAgdHJhbnNpdGlvbjogb3BhY2l0eSAxNTBtcyBlYXNlLWluO1xufVxuXG4ubWFwLnZpc2libGUge1xuICBvcGFjaXR5OiAxO1xufVxuXG4uc2VhcmNoY2xhc3MxIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5zZWFyY2hjbGFzczIge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4jbWFwR2VvZmVuY2Uge1xuICBoZWlnaHQ6IDgwJTtcbiAgd2lkdGg6IDEwMCU7XG59Il19 */"

/***/ }),

/***/ "./src/app/add-muliple/add-muliple.page.ts":
/*!*************************************************!*\
  !*** ./src/app/add-muliple/add-muliple.page.ts ***!
  \*************************************************/
/*! exports provided: AddMuliplePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddMuliplePage", function() { return AddMuliplePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/native-geocoder/ngx */ "./node_modules/@ionic-native/native-geocoder/ngx/index.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "./node_modules/@ionic-native/android-permissions/ngx/index.js");
/* harmony import */ var _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/location-accuracy/ngx */ "./node_modules/@ionic-native/location-accuracy/ngx/index.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");









let AddMuliplePage = class AddMuliplePage {
    constructor(
    // private renderer: Renderer2,
    loadingController, activatedRoute, navCtrl, nativeGeocoder, androidPermissions, 
    // private geolocation: Geolocation,
    locationAccuracy, apiCall, toastCtrl
    // public geoLocation: Geolocation,
    ) {
        this.loadingController = loadingController;
        this.activatedRoute = activatedRoute;
        this.navCtrl = navCtrl;
        this.nativeGeocoder = nativeGeocoder;
        this.androidPermissions = androidPermissions;
        this.locationAccuracy = locationAccuracy;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.autocompleteItems = [];
        this.autocomplete = {};
        // mapElement: HTMLElement;
        this.fence = 200;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.acService = new google.maps.places.AutocompleteService();
    }
    ngOnInit() {
        this.activatedRoute.paramMap.subscribe((paramMap) => {
            if (!paramMap.has('key')) {
                this.navCtrl.navigateBack('/location-setup');
                return;
            }
            this.key = paramMap.get('key');
            this.ppname = this.key;
            console.log("key: ", paramMap.get('key'));
            this.autocompleteItems = [];
            this.autocomplete = {
                query: '',
                creation_type: 'circular'
            };
        });
    }
    ionViewDidEnter() {
        this.checkGPSPermission();
    }
    //Check if application having GPS access permission  
    checkGPSPermission() {
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(result => {
            if (result.hasPermission) {
                //If having permission show 'Turn On GPS' dialogue
                this.askToTurnOnGPS();
            }
            else {
                //If not having permission ask for permission
                this.requestGPSPermission();
            }
        }, err => {
            console.log(err);
        });
    }
    requestGPSPermission() {
        this.locationAccuracy.canRequest().then((canRequest) => {
            if (canRequest) {
                console.log("4");
            }
            else {
                //Show 'GPS Permission Request' dialogue
                this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
                    .then(() => {
                    // call method to turn on GPS
                    this.askToTurnOnGPS();
                }, error => {
                    //Show alert if user click on 'No Thanks'
                    console.log('requestPermission Error requesting location permissions ' + JSON.stringify(error));
                });
            }
        });
    }
    askToTurnOnGPS() {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
            // When GPS Turned ON call method to get Accurate location coordinates
            // this.getLocationCoordinates()
            this.drawGeofence();
        }, error => console.log('Error requesting location permissions ' + JSON.stringify(error)));
    }
    ngAfterViewInit() {
        this.drawGeofence();
        // this.getGoogleMap().then(googleMaps => {
        //   const mapEl = this.mapElementRef.nativeElement;
        //   const map = new googleMaps.Map(mapEl, {
        //     center: { lat: 18.5204, lng: 73.8567 },
        //     zoom: 10
        //   });
        //   googleMaps.event.addListenerOnce(map, 'idle', () => {
        //     this.renderer.addClass(mapEl, 'visible');
        //   });
        // }).catch(err => {
        //   console.log(err);
        // });
    }
    // private getGoogleMap(): Promise<any> {
    //   const win = window as any;
    //   const googleModule = win.google;
    //   if (googleModule && googleModule.maps) {
    //     return Promise.resolve(googleModule.maps);
    //   }
    //   return new Promise((resolve, reject) => {
    //     const script = document.createElement('script');
    //     script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8';
    //     script.async = true;
    //     script.defer = true;
    //     document.body.appendChild(script);
    //     script.onload = () => {
    //       const loadedGoogleMpdule = win.google;
    //       if (loadedGoogleMpdule && loadedGoogleMpdule.maps) {
    //         resolve(loadedGoogleMpdule.maps);
    //       } else {
    //         reject('Google maps SDK not available.');
    //       }
    //     };
    //   });
    // }
    updateSearch() {
        // debugger
        console.log('modal > updateSearch');
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        let that = this;
        let config = {
            //types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
            input: that.autocomplete.query,
            componentRestrictions: {}
        };
        this.acService.getPlacePredictions(config, function (predictions, status) {
            console.log('modal > getPlacePredictions > status > ', status);
            console.log("lat long not find ", predictions);
            that.autocompleteItems = [];
            predictions.forEach(function (prediction) {
                that.autocompleteItems.push(prediction);
            });
            console.log("autocompleteItems=> " + that.autocompleteItems);
        });
    }
    chooseItem(item) {
        let that = this;
        that.autocomplete.query = item.description;
        // console.log("console items=> " + JSON.stringify(item))
        that.autocompleteItems = [];
        let options = {
            useLocale: true,
            maxResults: 5
        };
        this.nativeGeocoder.forwardGeocode(item.description, options)
            .then((coordinates) => {
            // console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude)
            that.newLat = Number(coordinates[0].latitude);
            that.newLng = Number(coordinates[0].longitude);
            console.log('latitude= ' + that.newLat);
            console.log('longitude= ' + that.newLng);
            debugger;
            if (that.cityCircle) {
                that.cityCircle.remove();
            }
            if (that.markObj) {
                that.markObj.remove();
            }
            let pos = {
                target: new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["LatLng"](that.newLat, that.newLng),
                zoom: 15,
                tilt: 30
            };
            this.map.moveCamera(pos);
            let markTemp = that.map.addMarkerSync({
                title: '',
                position: new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["LatLng"](that.newLat, that.newLng),
                icon: {
                    url: '../assets/Images/321.png',
                    size: {
                        width: 100,
                        height: 100
                    }
                },
            });
            that.markObj = markTemp;
            let ltln = { "lat": that.newLat, "lng": that.newLng };
            let circle = that.map.addCircleSync({
                'center': ltln,
                'radius': that.fence,
                'strokeColor': '#4dc4ec',
                'strokeWidth': 3,
                'fillColor': 'rgb(77, 196, 236, 0.5)'
            });
            that.cityCircle = circle;
            console.log("circle data: => " + that.cityCircle);
        })
            .catch((error) => console.log(error));
    }
    reverseGeocode(lat, lng) {
        let options = {
            useLocale: true,
            maxResults: 5
        };
        this.nativeGeocoder.reverseGeocode(lat, lng, options)
            .then((result) => {
            // debugger
            console.log("reverse geocode result: " + JSON.stringify(result[0]));
            this.convertResult(result[0]);
        })
            .catch((error) => console.log(error));
    }
    convertResult(result) {
        var a = result.thoroughfare ? result.thoroughfare : null;
        var b = result.subThoroughfare ? result.subThoroughfare : null;
        var c = result.subLocality ? result.subLocality : null;
        var d = result.subAdministrativeArea ? result.subAdministrativeArea : null;
        var e = result.postalCode ? result.postalCode : null;
        var f = result.locality ? result.locality : null;
        var g = result.countryName ? result.countryName : null;
        var h = result.administrativeArea ? result.administrativeArea : null;
        let str = '';
        if (a != null && a != 'Unnamed Road')
            str = a + ', ';
        if (b != null && b != 'Unnamed Road')
            str = str + b + ', ';
        if (c != null && c != 'Unnamed Road')
            str = str + c + ', ';
        if (d != null && d != 'Unnamed Road')
            str = str + d + ', ';
        if (e != null && e != 'Unnamed Road')
            str = str + e + ', ';
        if (f != null && f != 'Unnamed Road')
            str = str + f + ', ';
        if (g != null && g != 'Unnamed Road')
            str = str + g + ', ';
        if (h != null && h != 'Unnamed Road')
            str = str + h + ', ';
        console.log("address string: ", str);
        this.autocomplete.query = str;
    }
    drawGeofence() {
        // this.cityCircle = undefined;
        if (this.map != undefined) {
            this.map.remove();
        }
        this.map = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["GoogleMaps"].create('mapGeofence');
        _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["LocationService"].getMyLocation().then((result) => {
            console.log("my location: ", result);
            this.reverseGeocode(Number(result.latLng.lat), Number(result.latLng.lng));
            this.map.animateCamera({
                target: { lat: result.latLng.lat, lng: result.latLng.lng },
                zoom: 15,
                // tilt: 60,
                // bearing: 140,
                duration: 1000,
                padding: 0 // default = 20px
            }).then(() => {
                // alert("Camera target has been changed");
            });
            let markTemp = this.map.addMarkerSync({
                title: ["Current your location:\n",
                    "latitude:" + result.latLng.lat.toFixed(3),
                    "longitude:" + result.latLng.lng.toFixed(3),
                    "speed:" + result.speed,
                    "time:" + result.time].join("\n"),
                position: result.latLng,
                icon: {
                    url: '../assets/Images/321.png',
                    size: {
                        width: 100,
                        height: 100
                    }
                },
            });
            this.markObj = markTemp;
            // let that = this;
            let ltln = { "lat": result.latLng.lat, "lng": result.latLng.lng };
            let circle = this.map.addCircleSync({
                'center': ltln,
                'radius': this.fence,
                'strokeColor': '#4dc4ec',
                'strokeWidth': 2,
                'fillColor': 'rgb(77, 196, 236, 0.5)'
            });
            this.cityCircle = circle;
        });
    }
    callThis(fence) {
        let that = this;
        debugger;
        if (that.cityCircle) {
            that.cityCircle.setRadius(fence);
        }
        // if (that.cityCircle != undefined) {
        //   that.cityCircle.setRadius(fence);
        // }
    }
    saveGeofence() {
        let that = this;
        if (this.ppname && that.cityCircle != undefined) {
            debugger;
            var key;
            if (that.key === 'grocery store') {
                key = "grocery_store";
            }
            var payload = {
                "poi": [
                    {
                        "location": {
                            "type": "Point",
                            "coordinates": [
                                that.cityCircle.getCenter().lng,
                                that.cityCircle.getCenter().lat
                            ]
                        },
                        "poiname": that.ppname,
                        "poi_type": key ? key : that.key,
                        "status": "Active",
                        "user": that.islogin._id,
                        "address": that.autocomplete.query ? that.autocomplete.query : 'N/A',
                        "radius": that.cityCircle.getRadius()
                    }
                ]
            };
            this.loadingController.create({
                message: 'please wait...'
            }).then((loadEl) => {
                loadEl.present();
                var url = this.apiCall.mainUrl + "poi/addpoi";
                this.apiCall.urlWithdata(url, payload)
                    .subscribe(data => {
                    console.log("response adding poi: ", data);
                    loadEl.dismiss();
                    this.navCtrl.pop();
                    this.toastCtrl.create({
                        message: "Geofence added successfully!",
                        duration: 1500,
                        position: 'top'
                    }).then((toastEl) => {
                        toastEl.present();
                    });
                }, err => {
                    loadEl.dismiss();
                    console.log("error adding poi: ", err);
                });
            });
        }
    }
};
AddMuliplePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_4__["NativeGeocoder"] },
    { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_6__["AndroidPermissions"] },
    { type: _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_7__["LocationAccuracy"] },
    { type: _app_service__WEBPACK_IMPORTED_MODULE_8__["AppService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] }
];
AddMuliplePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-muliple',
        template: __webpack_require__(/*! raw-loader!./add-muliple.page.html */ "./node_modules/raw-loader/index.js!./src/app/add-muliple/add-muliple.page.html"),
        styles: [__webpack_require__(/*! ./add-muliple.page.scss */ "./src/app/add-muliple/add-muliple.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_4__["NativeGeocoder"],
        _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_6__["AndroidPermissions"],
        _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_7__["LocationAccuracy"],
        _app_service__WEBPACK_IMPORTED_MODULE_8__["AppService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
        // public geoLocation: Geolocation,
    ])
], AddMuliplePage);



/***/ })

}]);
//# sourceMappingURL=add-muliple-add-muliple-module-es2015.js.map