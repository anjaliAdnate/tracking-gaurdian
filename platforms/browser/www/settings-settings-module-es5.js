(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["settings-settings-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/settings/settings.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/settings/settings.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Setting</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-button expand=\"block\" class=\"btn\">Send Help Alert</ion-button>\n  <ion-card>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-list>\n          <ion-item>\n            <ion-avatar slot=\"start\">\n              <img src=\"assets/Images/location.png\" />\n            </ion-avatar>\n            <ion-label style=\"font-weight: bold;\">Location Update Schedule</ion-label>\n            <ion-icon name=\"arrow-dropright\"></ion-icon>\n          </ion-item>\n        </ion-list>\n\n        <ion-list>\n          <ion-item>\n            <ion-avatar slot=\"start\">\n              <img src=\"assets/Images/location.png\" />\n            </ion-avatar>\n            <ion-label style=\"font-weight: bold;\">Sound Guardian</ion-label>\n            <ion-icon name=\"arrow-dropright\"></ion-icon>\n          </ion-item>\n        </ion-list>\n\n        <ion-list>\n          <ion-item>\n            <ion-avatar slot=\"start\">\n              <img src=\"assets/Images/location.png\" />\n            </ion-avatar>\n            <ion-label style=\"font-weight: bold;\">SMS Alerts</ion-label>\n            <ion-icon name=\"arrow-dropright\"></ion-icon>\n          </ion-item>\n        </ion-list>\n\n        <ion-list>\n          <ion-item>\n            <ion-avatar slot=\"start\">\n              <img src=\"assets/Images/location.png\" />\n            </ion-avatar>\n            <ion-label style=\"font-weight: bold;\">Watch Finder</ion-label>\n            <ion-icon name=\"arrow-dropright\"></ion-icon>\n          </ion-item>\n        </ion-list>\n\n        <ion-list>\n          <ion-item>\n            <ion-avatar slot=\"start\">\n              <img src=\"assets/Images/location.png\" />\n            </ion-avatar>\n            <ion-label style=\"font-weight: bold;\">Timeout</ion-label>\n            <ion-icon name=\"arrow-dropright\"></ion-icon>\n          </ion-item>\n        </ion-list>\n\n        <ion-list>\n          <ion-item>\n            <ion-avatar slot=\"start\">\n              <img src=\"assets/Images/location.png\" />\n            </ion-avatar>\n            <ion-label style=\"font-weight: bold;\">Security</ion-label>\n            <ion-icon name=\"arrow-dropright\"></ion-icon>\n          </ion-item>\n        </ion-list>\n\n        <ion-list>\n          <ion-item>\n            <ion-avatar slot=\"start\">\n              <img src=\"assets/Images/location.png\" />\n            </ion-avatar>\n            <ion-label style=\"font-weight: bold;\">Log Out</ion-label>\n            <ion-icon name=\"arrow-dropright\"></ion-icon>\n          </ion-item>\n        </ion-list>\n\n        <ion-list (click)=\"onTroubleshoot()\">\n          <ion-item>\n            <ion-avatar slot=\"start\">\n              <img src=\"assets/Images/location.png\" />\n            </ion-avatar>\n            <ion-label style=\"font-weight: bold;\">Troubleshooting</ion-label>\n            <ion-icon name=\"arrow-dropright\"></ion-icon>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n  <ion-card>\n    <ion-row>\n      <ion-col>\n        <ion-list>\n          <ion-item>\n            <ion-avatar slot=\"start\">\n\n            </ion-avatar>\n            <ion-label style=\"font-weight: bold;\">Map</ion-label>\n            <p>Normal map</p>\n          </ion-item>\n        </ion-list>\n\n        <ion-list>\n          <ion-item>\n            <ion-avatar slot=\"start\">\n\n            </ion-avatar>\n            <ion-label style=\"font-weight: bold;\">Show places on the map</ion-label>\n            <ion-toggle slot=\"end\" color=\"medium\"></ion-toggle>\n            <!-- <ion-toggle color=\"primary\" slot=\"end\"></ion-toggle> -->\n          </ion-item>\n        </ion-list>\n\n        <ion-list>\n          <ion-item>\n            <ion-avatar slot=\"start\">\n\n            </ion-avatar>\n            <ion-label style=\"font-weight: bold;\">Show my location</ion-label>\n            <ion-toggle slot=\"end\" color=\"medium\" style=\"border: 0px;\"></ion-toggle>\n          </ion-item>\n        </ion-list>\n\n        <ion-list>\n          <ion-item>\n            <ion-avatar slot=\"start\">\n\n            </ion-avatar>\n            <ion-label style=\"font-weight: bold;\">Time</ion-label>\n            <ion-toggle slot=\"end\" color=\"success\" checked></ion-toggle>\n          </ion-item>\n        </ion-list>\n\n        <ion-list>\n          <ion-item>\n            <ion-avatar slot=\"start\">\n\n            </ion-avatar>\n            <ion-label style=\"font-weight: bold;\">Distance</ion-label>\n            <ion-toggle slot=\"end\" color=\"success\" checked></ion-toggle>\n          </ion-item>\n        </ion-list>\n\n\n      </ion-col>\n    </ion-row>\n  </ion-card>\n</ion-content>"

/***/ }),

/***/ "./src/app/settings/settings.module.ts":
/*!*********************************************!*\
  !*** ./src/app/settings/settings.module.ts ***!
  \*********************************************/
/*! exports provided: SettingsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPageModule", function() { return SettingsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _settings_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./settings.page */ "./src/app/settings/settings.page.ts");







var routes = [
    {
        path: '',
        component: _settings_page__WEBPACK_IMPORTED_MODULE_6__["SettingsPage"]
    }
];
var SettingsPageModule = /** @class */ (function () {
    function SettingsPageModule() {
    }
    SettingsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_settings_page__WEBPACK_IMPORTED_MODULE_6__["SettingsPage"]]
        })
    ], SettingsPageModule);
    return SettingsPageModule;
}());



/***/ }),

/***/ "./src/app/settings/settings.page.scss":
/*!*********************************************!*\
  !*** ./src/app/settings/settings.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn {\n  width: 340px;\n  margin: auto;\n  margin-top: 12px;\n  --background: linear-gradient(to left top, #e16156, #e26055, #e26055, #e35f54, #e35e54);\n}\n\nion-list {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\nion-toolbar {\n  --color: #ffffff;\n  --background: linear-gradient(to right, #329cd1, #008dd3, #007cd2, #1c6ace, #4055c5);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvdHJhY2tpbmctZ2F1cmRpYW4vc3JjL2FwcC9zZXR0aW5ncy9zZXR0aW5ncy5wYWdlLnNjc3MiLCJzcmMvYXBwL3NldHRpbmdzL3NldHRpbmdzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1RkFBQTtBQ0NGOztBRENBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBQ0VGOztBREFBO0VBQ0UsZ0JBQUE7RUFDQSxvRkFBQTtBQ0dGIiwiZmlsZSI6InNyYy9hcHAvc2V0dGluZ3Mvc2V0dGluZ3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ0biB7XG4gIHdpZHRoOiAzNDBweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXJnaW4tdG9wOiAxMnB4O1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byBsZWZ0IHRvcCwgI2UxNjE1NiwgI2UyNjA1NSwgI2UyNjA1NSwgI2UzNWY1NCwgI2UzNWU1NCk7XG59XG5pb24tbGlzdCB7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG5pb24tdG9vbGJhciB7XG4gIC0tY29sb3I6ICNmZmZmZmY7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMzI5Y2QxLCAjMDA4ZGQzLCAjMDA3Y2QyLCAjMWM2YWNlLCAjNDA1NWM1KTtcbn1cbiIsIi5idG4ge1xuICB3aWR0aDogMzQwcHg7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLXRvcDogMTJweDtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gbGVmdCB0b3AsICNlMTYxNTYsICNlMjYwNTUsICNlMjYwNTUsICNlMzVmNTQsICNlMzVlNTQpO1xufVxuXG5pb24tbGlzdCB7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgLS1jb2xvcjogI2ZmZmZmZjtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMzMjljZDEsICMwMDhkZDMsICMwMDdjZDIsICMxYzZhY2UsICM0MDU1YzUpO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/settings/settings.page.ts":
/*!*******************************************!*\
  !*** ./src/app/settings/settings.page.ts ***!
  \*******************************************/
/*! exports provided: SettingsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPage", function() { return SettingsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var SettingsPage = /** @class */ (function () {
    function SettingsPage(router) {
        this.router = router;
    }
    SettingsPage.prototype.ngOnInit = function () {
    };
    SettingsPage.prototype.onTroubleshoot = function () {
        this.router.navigateByUrl('/troubleshooting');
    };
    SettingsPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    SettingsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-settings',
            template: __webpack_require__(/*! raw-loader!./settings.page.html */ "./node_modules/raw-loader/index.js!./src/app/settings/settings.page.html"),
            styles: [__webpack_require__(/*! ./settings.page.scss */ "./src/app/settings/settings.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], SettingsPage);
    return SettingsPage;
}());



/***/ })

}]);
//# sourceMappingURL=settings-settings-module-es5.js.map